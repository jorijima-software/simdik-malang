<section class="content-header">
    <h1>Import Data</h1>
</section>
<section class="content">
    <div class="box box-primary">
        <div class="box-header with-border">
            <div class="box-title">Verifikasi Data</div>
        </div>
        <div class="box-body">
            <div class="alert alert-info">
                Pastikan Data dibawah ini sudah benar
            </div>
            <!-- <div class="pull-right"> -->
                <button class="btn btn-success btn-sm" style="margin-bottom: 10px" ng-click="onBtnConfirmClicked()" ng-disabled="disabled">
                    <i class="fa fa-check"></i>&nbsp;Konfirmasi
                </button>
            <!-- </div> -->
            <table class="table table-condensed table-hover table-striped" ng-table="importTable">
                <thead>
                <tr>
                    <th rowspan="2">NUPTK</th>
                    <th rowspan="2">Nama</th>
                    <th rowspan="2">NIK</th>
                    <th colspan="2" class="text-center">Gelar</th>
                    <th rowspan="2">Jenis</th>
                    <th rowspan="2"></th>
                </tr>
                <tr>
                    <th class="text-center">Depan</th>
                    <th class="text-center">Belakang</th>
                </tr>
                </thead>
                <tbody>
                    <tr ng-repeat="row in $data">
                        <td>{{row.nuptk}}</td>
                        <td><a ng-click="onBtnDetailClicked(row)">{{row.nama_lengkap}}</a></td>
                        <td>{{row.nik}}</td>
                        <td>{{row.gelar_depan}}</td>
                        <td>{{row.gelar_belakang}}</td>
                        <td>{{row.jenis_pegawai.nama}}</td>
                        <td>
                            <div class="pull-right">
                                <button class="btn btn-default btn-xs" ng-click="onBtnDetailClicked(row)">Detail</button>
                                <button class="btn btn-primary btn-xs" ng-click="onBtnEditClicked(row)">
                                    Edit
                                </button>
                                <button class="btn btn-danger btn-xs" ng-click="onBtnDeleteClicked(row.id)">
                                    Delete
                                </button>
                            </div>
                        </td>
                    </tr>
                </tbody>

            </table>
        </div>
    </div>
</section>
