<?php
/**
 * Created by PhpStorm.
 * User: ragasubekti
 * Date: 23/10/17
 * Time: 18.32
 */
?>
<?php
/**
 * Created by PhpStorm.
 * User: ragasubekti
 * Date: 23/10/17
 * Time: 16.34
 */
?>

<div class="modal-header">
    <div class="modal-title">Tambah Data Gaji</div>
</div>
<div class="modal-body">
    <form action="">
        <div class="form-group">
            <label for="">No. SK</label>
            <input type="text" class="form-control" ng-model="edit.sk">
        </div>
        <div class="form-group">
            <label for="">TMT (Tanggal Memulai Tugas)</label>
            <p class="input-group">
                <input type="text" class="form-control" uib-datepicker-popup="dd-MM-yyyy" ng-model="edit.tmt"
                       datepicker-options="dateOptions"
                       close-text="Close" is-open="datepicker" placeholder="dd-MM-yyyy" readonly
                       ng-click="datepicker = !datepicker"/>
                <span class="input-group-btn">
            <button type="button" class="btn btn-default" ng-click="datepicker = !datepicker"><i
                        class="glyphicon glyphicon-calendar"></i></button>
          </span>
            </p>
        </div>
        <div class="form-group">
            <label for="">Pangkat Golongan</label>
            <ui-select ng-model="edit.pangkat" theme="bootstrap">
                <ui-select-match placeholder="Pilih golongan"><span ng-if="$select.selected.gol">{{$select.selected.gol}}/{{$select.selected.ruang}}</span>
                </ui-select-match>
                <ui-select-choices repeat="item in rankList track by $index">
                    {{item.gol}}/{{item.ruang}}
                </ui-select-choices>
            </ui-select>
        </div>
        <div class="form-group">
            <label class="control-label">Jumlah Gaji</label>
            <input type="text" class="form-control" ng-model="edit.gaji" ng-currency currency-symbol="Rp " fraction="0">
        </div>
    </form>
</div>
<div class="modal-footer">
    <button class="btn btn-primary btn-sm" ng-click="onBtnSaveClicked()" ng-disabled="isSubmit">
        <i class="fa fa-check" ng-if="!isSubmit"></i><i class="fa fa-circle-o-notch fa-spin" ng-if="isSubmit"></i> <span>Simpan</span>
    </button>
    <button class="btn btn-default btn-sm" ng-click="dismiss()">
        <i class="fa fa-times"></i> <span>Batal</span>
    </button>
</div>
