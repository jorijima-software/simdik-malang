<?php

class RiwayatKerjaPtk extends \Illuminate\Database\Eloquent\Model
{
    protected $table = 'ptk_riwayat_kerja';
    protected $guarded = [];
    public $timestamps = false;

    public function unitKerja() {
        $CI =& get_instance();
        $CI->load->model('Sekolah');

        return $this->hasOne('Sekolah', 'id', 'id_sekolah');
    }

    public function kota() {
        $CI =& get_instance();
        $CI->load->model('Kota');

        return $this->hasOne('Kota', 'id', 'id_kota');
    }

    public function ptk() {
        $CI =& get_instance();
        $CI->load->model('Ptk');
        return $this->hasOne('Ptk', 'id', 'id_data_guru');
    }

}