<div class="modal-header">
    <div class="modal-title">
        <strong>Informasi</strong>
    </div>
</div>
<div class="modal-body" ng-bind-html="message">
</div>
<div class="modal-footer">
    <button class="btn  btn-default btn-sm" ng-click="dismiss()">
        Tutup
    </button>
</div>