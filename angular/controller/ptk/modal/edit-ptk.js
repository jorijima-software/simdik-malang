// Created by Raga Subekti at 07/10/17
angular.module('diknasmlg').controller('editPtk', function (data, $scope, $uibModalInstance, $EmployeeFactory, $SchoolFactory, $TeacherFactory, Notify, $ReligionFactory, $EducationFactory, $DistrictFactory, $PtkFactory) {

    $scope.isSubmit = false;


    $scope.edit = {
        id: data.id,
        nuptk: data.nuptk,
        nama_lengkap: data.nama,
        gelar_depan: '',
        gelar_belakang: '',
        nip_lama: '',
        nip_baru: '',
        pegawai: {
            id: data.id_jenis_pegawai,
            nama: data.jenis_pegawai
        },
        sekolah: {
            id: data.id_sekolah,
            nama: data.nama_sekolah
        },
        rumah: {},
        profil: {},
        sert: {}
    };

    $scope.schoolList = [];
    $scope.employeeTypeList = [];
    $scope.bidangStudiList = [];

    $scope.onRumahRegencyChange = function (query) {
        $DistrictFactory.regency(query, function (reply) {
            $scope.rumahRegencyList = reply;
        })
    };

    $scope.onRumahRegencySelect = function () {
        $DistrictFactory.show($scope.edit.rumah.kota.id, '', function (reply) {
            $scope.rumahKecamatanList = reply;
        })
    };

    $scope.onSekolahRegencySelect = function () {
        $DistrictFactory.show($scope.edit.sekolah_guru.kota.id, '', function (reply) {
            $scope.rumahKecamatanList = reply;
        })
    };

    $scope.onSekolahKecamatanChange = function (query) {
        $DistrictFactory.show($scope.edit.sekolah_guru.kota.id, query, function (reply) {
            $scope.rumahKecamatanList = reply;
        })
    };

    $scope.onRumahKecamatanChange = function (query) {
        if ($scope.edit.rumah && $scope.edit.rumah.kota && $scope.edit.rumah.kota.id) {
            $DistrictFactory.show($scope.edit.rumah.kota.id, query, function (reply) {
                $scope.rumahKecamatanList = reply;
            })
        }
    };

    $scope.dateOptions = {
        formatYear: 'yy',
        maxDate: new Date(),
        startingDay: 1
    };


    $scope.onInputSearchChange = function (query) {
        return $SchoolFactory.get(query, function (reply) {
            $scope.schoolList = reply;
        });
    };

    $scope.onHomeDistrictChange = function () {
        $scope.edit.rumah.kecamatan = $scope.edit.rumah.kecamatan.toUpperCase();
    };

    $scope.onHomeCityChange = function () {
        $scope.edit.rumah.kota = $scope.edit.rumah.kota.toUpperCase();
    };

    $scope.onSchoolNameChange = function () {
        $scope.edit.sekolah_guru.nama = $scope.edit.sekolah_guru.nama.toUpperCase();
    };

    $scope.onSchoolDistrictChange = function () {
        $scope.edit.sekolah_guru.kecamatan = $scope.edit.sekolah_guru.kecamatan.toUpperCase();
    };

    $scope.onSchoolCityChange = function () {
        $scope.edit.sekolah_guru.kota = $scope.edit.sekolah_guru.kota.toUpperCase();
    };

    $scope.onBtnSaveClicked = function () {
        $scope.isSubmit = true;


        $PtkFactory.update($scope.edit).then(function (reply) {
            $scope.isSubmit = false;

            if (reply.error) {
                Notify.error('Kesalahan', reply.error);
            } else if (reply.success) {
                Notify.success('Sukses', 'Berhasil mengubah data PTK');
                $uibModalInstance.close();
            }
        });
    };

    $scope.onBidangStudiChange = function (q) {
        $TeacherFactory.bidangStudi(q, function (reply) {
            $scope.bidangStudiList = reply;
        });
    };

    $scope.onBirthPlaceChange = function () {
        $scope.edit.profil.tempat_lahir = $scope.edit.profil.tempat_lahir.toUpperCase();
    };

    $scope.dismiss = function () {
        $uibModalInstance.dismiss();
    };

    function init() {
        $EmployeeFactory.get(function (reply) {
            $scope.employeeTypeList = reply;
        });

        $SchoolFactory.get('', function (reply) {
            $scope.schoolList = reply;
        });

        $ReligionFactory.show(function (reply) {
            $scope.religionList = reply;
        });

        $EducationFactory.get(function (reply) {
            $scope.educationList = reply;
        });

        $DistrictFactory.regency('', function (reply) {
            $scope.rumahRegencyList = reply;
        });

        $TeacherFactory.bidangStudi('', function (reply) {
            $scope.bidangStudiList = reply;
        });

        $TeacherFactory.get(data.id, function (reply) {

            var birtdate = null;

            if (reply.profil && reply.profil.tgl_lahir) {
                var dates = reply.profil.tgl_lahir.split('-');
                birthdate = new Date(dates[0], dates[1] - 1, dates[2]);
            } else {
                birthdate = new Date();
            }

            $scope.edit.gelar_depan = reply.gelar_depan
            $scope.edit.gelar_belakang = reply.gelar_belakang

            $scope.edit.nip_lama = reply.nip_lama
            $scope.edit.nip_baru = reply.nip_baru

            $scope.edit.pegawai = {
                id: reply.id_jenis_pegawai,
                nama: reply.jenis_pegawai
            }

            $scope.edit.profil = {
                tempat_lahir: (reply.profil && reply.profil.tempat_lahir) ? reply.profil.tempat_lahir : '',
                tanggal_lahir: birthdate,
                agama: {
                    id: (reply.profil && reply.profil.id_jenis_agama) ? reply.profil.id_jenis_agama : null,
                    nama: (reply.profil && reply.profil.nama_agama) ? reply.profil.nama_agama : ''
                },
                nik: reply.nik,
                nama_ibu: reply.nama_ibu
            }

            $scope.edit.sekolah = {
                id: reply.id_sekolah,
                nama: reply.nama_sekolah
            }
            $scope.edit.rumah = {
                kota: {
                    name: reply.rumah.kota
                },
                kecamatan: {
                    name: reply.rumah.kec
                },
                no_telp: reply.rumah.nohp,
                alamat: reply.rumah.alamat_rumah
            };

            $scope.edit.sert = {
                pola: (reply.sertifikasi && reply.sertifikasi.pola_sertifikasi) ? reply.sertifikasi.pola_sertifikasi : null,
                no: (reply.sertifikasi && reply.sertifikasi.no_peserta) ? reply.sertifikasi.no_peserta : '',
                tahun: (reply.sertifikasi && reply.sertifikasi.tahun_sertifikasi) ? reply.sertifikasi.tahun_sertifikasi : '',
                bidang: {
                    id: (reply.sertifikasi && reply.sertifikasi.id_bidang_studi) ? reply.sertifikasi.id_bidang_studi : null,
                    nama: (reply.sertifikasi && reply.sertifikasi.bidang && reply.sertifikasi.bidang.nama) ? reply.sertifikasi.bidang.nama : ''
                }
            };

        });
    }

    init();
});