<?php

class Ptkbaru extends CI_Controller
{

    public function stats()
    {
        $this->load->model('Ptk');

        $dup = Ptk::select('nuptk')->groupBy('nuptk')->whereRaw('LENGTH(nuptk) > 1')->havingRaw('COUNT(*) > 1')->count();
        $all = Ptk::count() - $dup;
        $nuptk = Ptk::whereRaw('LENGTH(nuptk) > 1')->count() - $dup;

        echo json_encode(['all' => $all, 'having' => $nuptk, 'notHaving' => $all - $nuptk]);
    }

    public function findDup()
    {
        $this->load->model('Ptk');

        $ptk = Ptk::select('nuptk')->groupBy('nuptk')->whereRaw('LENGTH(nuptk) > 1')->havingRaw('COUNT(*) > 1');

        echo json_encode(['data' => $ptk->get(), 'count' => count($ptk->get())]);
    }

    public function detailPtk($id)
    {
        header('Content-Type: application/json');
        $this->load->model('Ptk');
        $this->load->model('SertifikasiPtk');

        $ptk = new Ptk;
        $instance = Ptk::find($id);

        if (!$instance) {
            echo json_encode(['error' => 'Data Not Found']);
        } else {
            $pola = ['Portofolio', 'PLPG', 'PPG'];
            $sertifikasi = new SertifikasiPtk;

            $lahir_pns = substr($instance->nip_baru, 0, 8);

            echo json_encode([
                'id' => $instance->id,
                'kepegawaian' => [
                    'nuptk' => $instance->nuptk,
                    'nama' => $instance->nama_lengkap,
                    'jenisPegawai' => ($instance->jenisPegawai) ? $instance->jenisPegawai->nama : '',
                    'unitKerja' => ($instance->unitKerja) ? $instance->unitKerja->nama : '',
                    'masaKerja' => $ptk->masaKerja($instance->id),
                    'nip' => [
                        'lama' => $instance->nip_lama,
                        'baru' => $instance->nip_baru,
                    ],
                    'gelar' => [
                        'depan' => $instance->gelar_depan,
                        'belakang' => $instance->gelar_belakang,
                    ],
                    'pns' => $instance->status_pns,
                ],
                'profil' => [
                    'nik' => $instance->nik,
                    'lahir' => [
                        'tempat' => ($instance->profil) ? $instance->profil->tempat_lahir : '',
                        'tanggal' => ($instance->status_pns == 1 && strlen($instance->nip_baru) > 8)
                            ? substr($lahir_pns, 6, 2) . ' - ' . substr($lahir_pns, 4, 2) . ' - ' . substr($lahir_pns, 0, 4)
                            : (($instance->profil) ? Carbon\Carbon::parse($instance->profil->tgl_lahir)->format('d - m - Y') : ''),
                        'umur' => ($instance->profil)
                        ? (($instance->status_pns == 1 && strlen($instance->nip_baru) > 8)
                            ? Carbon\Carbon::parse(substr($lahir_pns, 0, 4) . '-' . substr($lahir_pns, 4, 2) . '-' . substr($lahir_pns, 6, 2))->diff(Carbon\Carbon::now())->format('%y')
                            : Carbon\Carbon::parse($instance->profil->tgl_lahir)->diff(Carbon\Carbon::now())->format('%y'))
                        : (($instance->status_pns == 1 && strlen($instance->nip_baru)) ? Carbon\Carbon::parse(substr($lahir_pns, 0, 4) . '-' . substr($lahir_pns, 4, 2) . '-' . substr($lahir_pns, 6, 2))->diff(Carbon\Carbon::now())->format('%y') : '0'),
                    ],
                    'agama' => $instance->profil && $instance->profil->agama ? $instance->profil->agama->nama : '',
                    'namaIbu' => $instance->nama_ibu,
                ],
                'rumah' => [
                    'kota' => ($instance->rumah) ? $instance->rumah->kota : '',
                    'kecamatan' => ($instance->rumah) ? $instance->rumah->kec : '',
                    'noTelp' => ($instance->rumah) ? $instance->rumah->nohp : '',
                    'alamat' => ($instance->rumah) ? $instance->rumah->alamat_rumah : '',
                ],
                'sertifikasi' => [
                    'pola' => ($instance->sertifikasi) ? $sertifikasi->namaPola($instance->sertifikasi->pola_sertifikasi) : '',
                    'noPeserta' => ($instance->sertifikasi) ? $instance->sertifikasi->no_peserta : '',
                    'tahun' => ($instance->sertifikasi) ? $instance->sertifikasi->tahun_sertifikasi : '',
                    'bidangStudi' => ($instance->sertifikasi && $instance->sertifikasi->bidangStudi) ? $instance->sertifikasi->bidangStudi->nama : '',
                ],
            ]);
        }
    }

    public function statsPtk($id)
    {
        $this->load->model('Sekolah');

        $sekolah = new Sekolah();

        echo json_encode([
            'ptk' => $sekolah->persenPtk($id)['persen'],
            'pns' => $sekolah->persenPns($id)['persen'],
            'npns' => $sekolah->persenNonPns($id)['persen'],
        ]);
    }

    public function modalKekurangan()
    {
        $this->load->view('ptk/modal/kekurangan');
    }

    public function api_ptk_delete()
    {
        $this->load->model('Ptk');
        $this->load->model('User');

        $_post = json_decode(trim(file_get_contents('php://input')));

        if (!isset($_SESSION['as'])) {
            echo json_encode(['error' => 'Insufficent Priviledge']);
        } else {
            $user = User::find($this->session->uid);
            $ptk = Ptk::find($_post->id);

            if ($this->session->as > 0 && $user->sekolah->id != $ptk->id_sekolah) {
                echo json_encode(['error' => 'Insufficent Priviledge']);
                die();
            }

            $this->load->model('DeletePtk');

            $ptk->deleted = 1;
            $ptk->save();

            if ($dptk = DeletePtk::where('id_data_guru', $_post->id)->first()) {
                $dptk->alasan = $_post->alasan;
                $dptk->save();
            } else {
                DeletePtk::create([
                    'id_data_guru' => $_post->id,
                    'alasan' => $_post->alasan,
                ]);
            }

            echo json_encode([
                'success' => true,
            ]);

        }
    }

    public function modal_hapus()
    {
        $this->load->view('ptk/modal/hapus-ptk');
    }

    public function api_informasi_ptk($id)
    {
        if (!isset($_SESSION['as'])) {
            echo json_encode(['error' => 'Not Authorized']);
            die();
        }

        $this->load->model('Ptk');
        $ptk = Ptk::find($id);
        if (!$ptk) {
            echo json_encode(['error' => 'Data Tidak Ditemukan']);
            die();
        }

        echo json_encode([
            'nuptk' => $ptk->nuptk,
            'nama' => $ptk->nama_lengkap,
            'nip' => $ptk->nip_baru,
            'jenisPegawai' => ($ptk->jenisPegawai) ? $ptk->jenisPegawai->nama : null,
            'unitKerja' => ($ptk->unitKerja) ? $ptk->unitKerja->nama : null,
        ]);
    }

    public function api_insert_kerja($id)
    {
        if (!isset($_SESSION['as'])) {
            echo json_encode(['error' => 'Not Authorized']);
            die();
        }

        $_post = json_decode(trim(file_get_contents('php://input')));

        $this->load->model('RiwayatKerjaPtk');

        if ($_post->lokasi->id == '0') {
            RiwayatKerjaPtk::create([
                'id_data_guru' => $id,
                'tmt' => \Carbon\Carbon::parse($_post->tmt, 'Asia/Jakarta')->addDays(1)->format('Y-m-d'),
                'no_sk' => $_post->sk,
                'id_sekolah' => $_post->unitKerja->id,
                'gaji' => $_post->gaji,
                'lokasi' => $_post->lokasi->id,
            ]);
        } else if ($_post->lokasi->id == '1') {
            RiwayatKerjaPtk::create([
                'id_data_guru' => $id,
                'no_sk' => $_post->sk,
                'tmt' => \Carbon\Carbon::parse($_post->tmt, 'Asia/Jakarta')->addDays(1)->format('Y-m-d'),
                'gaji' => $_post->gaji,
                'lokasi' => $_post->lokasi->id,
                'id_kota' => $_post->kotaUnitKerja->id,
                'nama_unit_kerja' => $_post->namaUnitKerja,
            ]);
        }

        echo json_encode(['success' => true]);
    }

    public function api_show_npns($start = 0, $count = 10)
    {
        if (!isset($_SESSION['as'])) {
            echo json_encode(['error' => 'Not Authorized']);
            die();
        }

        $search = $this->input->get('search');
        $sekolah = $this->input->get('sekolah');

        $this->load->model('Ptk');

        $ptk = Ptk::take($count)
            ->when(strlen($search) > 0, function ($query) use ($search) {
                $query->where(function ($q) use ($search) {
                    $q->where('nuptk', 'like', "%{$search}%")
                        ->orWhere('nama_lengkap', 'like', "%{$search}%")
                        ->orWhere('nip_baru', 'like', "%{$search}%");
                });
            })
            ->when(strlen($sekolah) > 0, function ($query) use ($sekolah) {
                $query->where('id_sekolah', $sekolah);
            })
            ->where('status_pns', 0)->where('deleted', 0);

        if ($this->session->as == '1') {
            $this->load->model('User');
            $user = User::find($this->session->uid);
            $ptk->where('id_sekolah', $user->sekolah->id);
        }

        $parsed = [];

        $total = $ptk->count();

        $ptk->skip($start);

        foreach ($ptk->get() as $key => $row) {
            $lengkap = false;

            $lahir_pns = substr($row->nip_baru, 0, 8);
            
            $umur = ($row->profil)
            ? (($row->status_pns == 1 && strlen($row->nip_baru) > 8)
                ? Carbon\Carbon::parse(substr($lahir_pns, 0, 4) . '-' . substr($lahir_pns, 4, 2) . '-' . substr($lahir_pns, 6, 2))->diff(Carbon\Carbon::now())->format('%y')
                : Carbon\Carbon::parse($row->profil->tgl_lahir)->diff(Carbon\Carbon::now())->format('%y'))
            : (($row->status_pns == 1 && strlen($row->nip_baru)) ? Carbon\Carbon::parse(substr($lahir_pns, 0, 4) . '-' . substr($lahir_pns, 4, 2) . '-' . substr($lahir_pns, 6, 2))->diff(Carbon\Carbon::now())->format('%y') : '0');

            if (
                strlen($row->nik) > 10 && $row->rumah &&
                strlen($row->rumah->alamat_rumah) > 10 &&
                strlen($row->rumah->nohp) >= 10 &&
                $umur >= 20
            ) {
                $lengkap = true;
            }

            array_push($parsed, [
                'id' => $row->id,
                'nama' => $row->nama_lengkap,
                'nuptk' => $row->nuptk,
                'jenisPegawai' => ($row->jenisPegawai) ? $row->jenisPegawai->nama : null,
                'unitKerja' => ($row->unitKerja) ? $row->unitKerja->nama : null,
                'riwayat' => [
                    'sekolah' => $row->riwayatSekolah->count(),
                    'kerja' => $row->riwayatKerja->count(),
                ],
                'status' => [
                    'guru' => !!$row->guru,
                    // 'sertifikasi' =>
                ],

                'kelengkapan' => [
                    'nik' => !!(strlen($row->nik) > 10),
                    'alamat' => !!$row->rumah && strlen($row->rumah->alamat_rumah) > 10,
                    'nohp' => !!$row->rumah && strlen($row->rumah->nohp) >= 10,
                    'umur' => !($umur < 20)
                ],
                'rekening' => !!($row->rekening && strlen($row->rekening->no_rekening) >= 8),
                'lengkap' => $lengkap,
            ]);
        }

        if ($ptk) {
            header('Content-Type: application/json');
            echo json_encode(['data' => $parsed, 'total' => $total]);
        }
    }

    public function api_show_pns($start = 0, $count = 10, $filter = [])
    {
        if (!isset($_SESSION['as'])) {
            echo json_encode(['error' => 'Not Authorized']);
            die();
        }

        $this->load->model('Ptk');

        $search = $this->input->get('search');
        $sekolah = $this->input->get('sekolah');

        $ptk = Ptk::take($count)
            ->where('status_pns', 1)
            ->when(strlen($search) > 0, function ($query) use ($search) {
                $query->where(function ($q) use ($search) {
                    $q
                        ->where('nuptk', 'like', "%{$search}%")
                        ->orWhere('nama_lengkap', 'like', "%{$search}%")
                        ->orWhere('nip_baru', 'like', "%{$search}%");
                });
            })
            ->when(strlen($sekolah) > 0, function ($query) use ($sekolah) {
                $query->where('id_sekolah', $sekolah);
            })
            ->where('deleted', 0);

        if ($this->session->as == '1') {
            $this->load->model('User');
            $user = User::find($this->session->uid);
            $ptk->where('id_sekolah', $user->sekolah->id);
        }

        $parsed = [];

        $total = $ptk->count();

        $ptk->skip($start);

//        var_dump($ptk->toSql());
        //        die();

        foreach ($ptk->get() as $key => $row) {
            $lengkap = false;

            $lahir_pns = substr($row->nip_baru, 0, 8);
            
            $umur = ($row->profil)
            ? (($row->status_pns == 1 && strlen($row->nip_baru) > 8)
                ? Carbon\Carbon::parse(substr($lahir_pns, 0, 4) . '-' . substr($lahir_pns, 4, 2) . '-' . substr($lahir_pns, 6, 2))->diff(Carbon\Carbon::now())->format('%y')
                : Carbon\Carbon::parse($row->profil->tgl_lahir)->diff(Carbon\Carbon::now())->format('%y'))
            : (($row->status_pns == 1 && strlen($row->nip_baru)) ? Carbon\Carbon::parse(substr($lahir_pns, 0, 4) . '-' . substr($lahir_pns, 4, 2) . '-' . substr($lahir_pns, 6, 2))->diff(Carbon\Carbon::now())->format('%y') : '0');

            if (
                strlen($row->nik) > 10 && $row->rumah &&
                strlen($row->rumah->alamat_rumah) > 10 &&
                strlen($row->rumah->nohp) >= 10 &&
                $umur >= 20
            ) {
                $lengkap = true;
            }

            array_push($parsed, [
                'id' => $row->id,
                'nama' => $row->nama_lengkap,
                'nuptk' => $row->nuptk,
                'jenisPegawai' => ($row->jenisPegawai) ? $row->jenisPegawai->nama : null,
                'unitKerja' => ($row->unitKerja) ? $row->unitKerja->nama : null,
                'riwayat' => [
                    'sekolah' => $row->riwayatSekolah->count(),
                    'pangkat' => $row->riwayatPangkat->count(),
                    'gaji' => $row->riwayatGaji->count(),
                ],
                'kelengkapan' => [
                    'nik' => !!(strlen($row->nik) > 10),
                    'alamat' => !!$row->rumah && strlen($row->rumah->alamat_rumah) > 10,
                    'nohp' => !!$row->rumah && strlen($row->rumah->nohp) >= 10,
                    'umur' => !($umur < 20)
                ],
                'lengkap' => $lengkap,
            ]);
        }

        if ($ptk) {
            header('Content-Type: application/json');
            echo json_encode(['data' => $parsed, 'total' => $total]);
        }
    }

}
