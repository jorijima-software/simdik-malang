<?php
/**
 * Created by PhpStorm.
 * User: ragasubekti
 * Date: 21/10/17
 * Time: 13.25
 */
?>

<div class="modal-body">
    Hapus data pangkat guru berikut?
    <table class="table table-striped">
        <tr>
            <th>NIP</th>
            <td>{{detail.nip}}</td>
        </tr>
        <tr>
            <th>Golongan/Ruang</th>
            <td>{{detail.gol}}/{{detail.ruang}}</td>
        </tr>
    </table>
</div>
<div class="modal-footer">
    <button class="btn btn-danger btn-sm" ng-click="onBtnConfirmClicked()">
        <i class="fa fa-check"></i> <span>Konfirmasi</span>
    </button>
    <button class="btn btn-default btn-sm" ng-click="dismiss()">
        <i class="fa fa-times"></i> <span>Batal</span>
    </button>
</div>
