<?php
/**
 * Created by PhpStorm.
 * User: ragasub
 * Date: 12/22/17
 * Time: 7:06 PM
 */
?>

<section class="content-header">
    <h1>Aktifitas Operator</h1>
</section>

<section class="content">
    <div class="box box-info">
        <div class="box-header">
            <div class="box-title">Informasi Operator</div>
        </div>
        <div class="box-body">
            <table class="table table-striped">
                <tr>
                    <th width="25%">NPSN</th>
                    <td>{{detail.npsn}}</td>
                </tr>
                <tr>
                    <th>Sekolah</th>
                    <td>{{detail.nama}}</td>
                </tr>
                <tr>
                    <th>Nama Operator</th>
                    <td>{{detail.nama_operator}}</td>
                </tr>
                <tr>
                    <th>No. HP</th>
                    <td>{{detail.no_hp}}</td>
                </tr>
                <tr>
                    <th>No. Whatsapp</th>
                    <td>{{detail.no_wa}}</td>
                </tr>
                <tr>
                    <th>Last Login</th>
                    <td>{{detail.last_login}}</td>
                </tr>
            </table>
        </div>
    </div>
    <div class="box box-success">
        <div class="box-header">
            <div class="box-title">Aktifitas Operator</div>
        </div>
        <div class="box-body">
            <table class="table table-striped table-condensed" ng-table="activityTable">
                <thead>
                    <th>No</th>
                    <th>Log</th>
                    <th>Waktu</th>
                    <th>Asal</th>
                    <th>Tipe</th>
                </thead>
                <tbody>
                <tr ng-repeat="row in $data">
                    <td>
                    {{($index + 1) + (activityTable._params.count * (activityTable._params.page -1))}}
                    </td>
                    <td>{{row.date}}</td>
                    <td>{{row.log}}</td>
                    <td class="text-uppercase">{{row.source}}</td>
                    <td>
<!--                        <span ng-if="row.source == 'guru'">-->
                            <span ng-if="row.type == 0" class="label label-danger">DELETE</span>
                            <span ng-if="row.type == 1" class="label label-success">INSERT</span>
                            <span ng-if="row.type == 2" class="label label-warning">UPDATE</span>
<!--                        </span>-->
                    </td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>
</section>
