<?php
/**
 * Created by PhpStorm.
 * User: ragasubekti
 * Date: 06/10/17
 * Time: 15.41
 */

class Teacher extends CI_Controller
{


    public function getSchool($id)
    {
        $this->load->model('PtkSekolah');
        header('Content-Type: application/json');
        $ptkSekolah = new PtkSekolah;
        $sekolah = $ptkSekolah->where('id_data_guru', $id)->get();

        $parsed = [];
        foreach($sekolah as $key => $row) {
            $kota   = $ptkSekolah->idKota($row->kota_sekolah);
            $kec    = $ptkSekolah->idKecamatan($row->kec_sekolah);
            array_push($parsed, [
                'id' => $row->id,
                'idPtk' => $row->id_data_guru,
                'kota' => [
                    'id' => $kota ? $kota->id : null,
                    'nama' => $kota ? $kota->name : null
                ],
                'tingkat' => [
                    'id' => $row->tingkatPendidikan ? $row->tingkatPendidikan->id : null,
                    'nama' => $row->tingkatPendidikan ? $row->tingkatPendidikan->nama : null
                ],
                'nama' => $row->nama_sekolah,
                'kecamatan' => [
                    'nama' => $row->kec_sekolah
                ],
                'jurusan' => $row->jurusan,
                'tahun' => [
                    'lulus' => $row->thn_lulus,
                    'masuk' => $row->thn_masuk
                ],
                'status' => $row->status
            ]);
        }

        echo json_encode($parsed);
        // echo json_encode($this->Teacher_model->school($id));
    }

    public function spa_ptk_index()
    {
        if ($this->session->as == 0) {
            $this->load->view('ptk/index');
        } else {
            $this->load->view('ptk/operator/index');
        }
    }

    public function spa_ptk_riwayat_sekolah()
    {
        $this->load->view('ptk/sekolah');
    }

    public function spa_ptk_riwayat_pangkat()
    {
        $this->load->view('ptk/pangkat');
    }

    public function spa_ptk_riwayat_gaji()
    {
        $this->load->view('ptk/gaji');
    }

    public function page_data_guru()
    {
        $content = 'guru/index';

        $js = [
            'angular/factory/teacher.js',
            'angular/factory/guru.js',
            'angular/controller/guru/guru-controller.js',
            'angular/factory/ptk.js',
            'angular/controller/guru/modal/tambah-guru.js'
        ];
        $this->load->view('layout', array('content' => $content, 'js' => $js));
    }

    public function page_tugas_tambahan()
    {
        $content = 'tugas-tambahan/index';
        $js = [
            'angular/factory/tugas.js',
            'angular/factory/teacher.js',
            'angular/controller/tugas-tambahan/tugas-tambahan-controller.js',
            'angular/controller/tugas-tambahan/modal/tambah-tugas-tambahan.js',
            'angular/controller/tugas-tambahan/modal/edit-tugas-tambahan.js'
        ];
        $this->load->view('layout', array('content' => $content, 'js' => $js));
    }

    public function page_dguru_detail($id)
    {

        $this->load->model('Teacher_model');

        $content = 'guru/tugas-mengajar';

        $detail = $this->Teacher_model->get($id);

        $js = [
            'angular/factory/teacher.js',
            'angular/factory/tugas.js',
            'angular/factory/school.js',
            'angular/controller/guru/tugas-mengajar-controller.js',
            'angular/controller/guru/modal/tambah-tugas-mengajar.js'
        ];

        $this->load->view('layout', ['content' => $content, 'detail' => $detail, 'js' => $js]);
    }

    public function page_pangkat($id)
    {
        $this->load->model('Teacher_model');

        $content = 'ptk/pangkat';
        $detail = $this->Teacher_model->get($id);

        $js = [
            'angular/controller/ptk/ptk-pangkat-controller.js',
            'angular/factory/teacher.js',
            'angular/factory/school.js',
            'angular/factory/district.js',
            'angular/factory/rank.js',
            'angular/controller/ptk/modal/edit-pangkat-ptk.js',
            'angular/controller/ptk/modal/tambah-pangkat-ptk.js'
        ];

        $this->load->view('layout', array('content' => $content, 'detail' => $detail, 'js' => $js));

    }

    public function page_gaji($id)
    {
        $this->load->model('Teacher_model');

        $content = 'ptk/gaji';
        $detail = $this->Teacher_model->get($id);

        $js = [

            'angular/factory/teacher.js',
            'angular/factory/rank.js'
        ];

        $this->load->view('layout', array('content' => $content, 'detail' => $detail, 'js' => $js));
    }

    // modal

    public function modal_ptk_detail()
    {
        $this->load->view('ptk/modal/detail');
    }

    public function modal_ptk_tambah()
    {
        $this->load->view('ptk/modal/tambah');
    }

    public function modal_ptk_edit()
    {
        $this->load->view('ptk/modal/edit');
    }

    // api

    public function get($id)
    {
        $this->load->model('Teacher_model');

        header('Content-Type: application/json');
        echo json_encode($this->Teacher_model->get($id));
    }

    public function showOperator($start, $count, $query = null)
    {
        $this->load->model('Teacher_model');
        header('Content-Type: application/json');
        echo json_encode($this->Teacher_model->showOperator($start, $count, $query, null));
    }

    public function show($start, $count, $query = null)
    {
        $this->load->model('Teacher_model');

        header('Content-Type: application/json');
        echo json_encode($this->Teacher_model->show($start, $count, $query, $this->input->get()));
    }

    public function getSchoolTeacher($id, $start, $count, $query = null)
    {
        $this->load->model('Teacher_model');

        header('Content-Type: application/json');
        echo json_encode($this->Teacher_model->getSchoolTeacher($id, $start, $count, $query, $this->input->get()));
    }


    public function edit()
    {
        if ($this->input->method() == 'get') {
            $this->load->view('modal/teacher/edit');
        } else if ($this->input->method() == 'post') {

        }
    }


    public function delete($id = null)
    {
        $this->load->model('Teacher_model');

        if ($this->input->method() == 'get') {
            $this->load->view('modal/teacher/delete');
        } else if ($this->input->method() == 'post') {
            header('Content-Type: application/json');
            echo json_encode($this->Teacher_model->delete($id));
        }
    }


    public function getrank($nip)
    {
        $this->load->model('Teacher_model');
        if ($this->input->method() == 'get') {
            header('Content-Type: application/json');
            echo json_encode($this->Teacher_model->getrank($nip));
        }
    }

    public function statsbox()
    {
        header('Content-Type: application/json');
        $this->load->model('Teacher_model');
        echo json_encode($this->Teacher_model->statsbox());
    }

    public function operatorStatsbox()
    {
        header('Content-Type: application/json');
        $this->load->model('Teacher_model');
        echo json_encode($this->Teacher_model->operatorStatsbox());
    }


    public function page_sekolah($id)
    {
        $this->load->model('Teacher_model');

        $detail = $this->Teacher_model->get($id);

        $js = [
            'angular/controller/ptk/ptk-sekolah-controller.js',
            'angular/factory/teacher.js',
            'angular/controller/ptk/modal/tambah-sekolah-ptk.js',
            'angular/factory/district.js',
            'angular/factory/education.js',
            'angular/controller/ptk/modal/edit-sekolah-ptk.js',
            'angular/controller/ptk/modal/aktivasi-sekolah-ptk.js',
            'angular/factory/school.js'
        ];

        $content = 'ptk/sekolah';
        $this->load->view('layout', array('content' => $content, 'detail' => $detail, 'js' => $js));
    }

    public function modal_sekolah_tambah()
    {
        $this->load->view('ptk/modal/tambah-sekolah');
    }

    public function api_sekolah_tambah($id)
    {

        header('Content-Type: application/json');

        $this->load->model('Teacher_model');
        $data = json_decode(trim(file_get_contents('php://input')));

        $tk = $data->tk;
        $nama = $data->nama;
        $kota = $data->kota;
        $kecamatan = $data->kecamatan;
        $masuk = $data->masuk;
        $lulus = $data->lulus;
        $jurusan = $data->jurusan;

        echo json_encode($this->Teacher_model->sekolah_tambah($id, $tk, $nama, $kota, $kecamatan, $masuk, $lulus, $jurusan));

    }

    public function deleteSchoolModal()
    {
        $this->load->view('modal/school/delete-teacher');
    }

    public function deleteSchool($id)
    {
        $this->load->model('Teacher_model');
        header('Content-Type: application/json');
        echo json_encode($this->Teacher_model->sekolah_delete($id));
    }

    public function modal_sekolah_edit()
    {
        $this->load->view('ptk/modal/edit-sekolah');
    }


    public function editSchool($id)
    {
        $this->load->model('Teacher_model');
        header('Content-Type: application/json');

        $data = json_decode(trim(file_get_contents('php://input')));

        $tk = $data->tk;
        $nama = $data->nama;
        $kota = $data->kota;
        $kecamatan = $data->kecamatan;
        $masuk = $data->masuk;
        $lulus = $data->lulus;
        $jurusan = $data->jurusan;

        echo json_encode($this->Teacher_model->editSchool($id, $tk, $nama, $kota, $kecamatan, $masuk, $lulus, $jurusan));
    }

    public function activateSchoolModal()
    {
        $this->load->view('modal/school/activate-teacher');

    }

    public function activateSchool($id_guru, $id_sekolah)
    {
        $this->load->model('Teacher_model');
        header('Content-Type: application/json');
        echo json_encode($this->Teacher_model->activateSchool($id_guru, $id_sekolah));
    }

    public function addRankModal()
    {
        $this->load->view('modal/rank/add-teacher');
    }

    public function editRankModal()
    {
        $this->load->view('modal/rank/edit-teacher');
    }


    public function deleteRankModal()
    {
        $this->load->view('modal/rank/delete-teacher');
    }

    public function getSalary($id)
    {
        header('Content-Type: application/json');
        $this->load->model('Teacher_model');
        echo json_encode($this->Teacher_model->getSalary($id));
    }

    public function addSalaryModal()
    {
        $this->load->view('modal/salary/add-teacher');
    }

    public function editSalaryModal()
    {
        $this->load->view('modal/salary/edit-teacher');
    }

    public function deleteSalaryModal()
    {
        $this->load->view('modal/salary/delete-teacher');
    }

    public function addSalary($id)
    {
        header('Content-Type: application/json');
        $data = json_decode(trim(file_get_contents('php://input')));

        $sk = $data->sk;
        $pangkat = $data->pangkat;
        $tmt = $data->tmt;
        $gaji = $data->gaji;

        $this->load->model('Teacher_model');

        echo json_encode($this->Teacher_model->addSalary($id, $sk, $tmt, $pangkat, $gaji));
    }

    public function migrate()
    {
        $this->load->model('Teacher_model');
        $this->Teacher_model->migrate_jurusan();
    }

    public function deleteSalary($id)
    {
        header('Content-Type: application/json');

        $this->load->model('Teacher_model');

        echo json_encode($this->Teacher_model->deleteSalary($id));
    }

    public function addRank($id)
    {
        header('Content-Type: application/json');
        $data = json_decode(trim(file_get_contents('php://input')));

        $sk = $data->sk;
        $tmt = $data->tmt;
        $sekolah = $data->sekolah;
        $pangkat = $data->pangkat;
        $area = $data->area;
        $uk = $data->unit_kerja;
        $kota = $data->kota;
        $type = $data->type;
        $this->load->model('Teacher_model');

        echo json_encode($this->Teacher_model->addRank($id, $sk, $tmt, $sekolah, $pangkat, $area, $uk, $kota, $type));
    }

    public function deleteRank($id)
    {
        header('Content-Type: application/json');
        $this->load->model('Teacher_model');
        echo json_encode($this->Teacher_model->deleteRank($id));
    }

    public function manage()
    {
        $content = 'teacher/schoolManage';
        $this->load->view('layout', array('content' => $content));

//        $this->load->view('teacher/schoolManage');
    }

    public function updateRank()
    {
        header('Content-Type: application/json');
        $this->load->model('Teacher_model');

        $data = json_decode(trim(file_get_contents('php://input')));

        $this->Teacher_model->updateRank($data);
    }

    public function updateSalary()
    {
        header('Content-Type: application/json');
        $this->load->model('Teacher_model');

        $data = json_decode(trim(file_get_contents('php://input')));

        $this->Teacher_model->updateSalary($data);
    }

    public function api_tugastb_show($start, $count)
    {
        $this->load->model('Tugastb_model');

        echo json_encode($this->Tugastb_model->show($start, $count));
    }

    public function api_tgtb_op_show($start, $count)
    {

        $this->load->model('Tugastb_model');

        echo json_encode($this->Tugastb_model->get_tgtb_op($start, $count));
    }

    public function modal_tgtb_detail()
    {
        $this->load->view('modal/teacher/tugas/tambah');
    }

    public function api_tgtb_op_detail($id, $start, $count)
    {
        $this->load->model('Tugastb_model');

        echo json_encode($this->Tugastb_model->get_tgtb_detail_op($id, $start, $count));
    }

    public function modal_tgtb_edit()
    {
        $this->load->view('modal/teacher/tugas/edit');
    }

    public function api_select_teacher($query = null)
    {
        // $this->load->model('Teacher_model');

        if(!isset($_SESSION['as'])) {
            echo json_encode(['error' => 'Insufficent Priviledge']);
            die();
        }

        $this->load->model('Ptk');
        $this->load->model('User');

        $guru = Ptk::take(10)->when((strlen($query) > 0), function($qry) use($query) {
            $qry->where(function($q) use ($query) {
                $q
                    ->where('nama_lengkap', 'like', '%'.$query.'%')
                    ->orWhere('nuptk', 'like', '%'.$query.'%')
                    ->orWhere('nip_baru', 'like', '%'.$query.'%');
            });
        })->when(($this->session->as > 0), function($qry) {
            $user = User::find($this->session->uid);
            $qry->where('id_sekolah', $user->sekolah->id);
        })->where('deleted', '0');


        echo json_encode(['data' => $guru->get()]);
        // echo json_encode($this->Teacher_model->selectTeacher($query));
    }

    public function api_insert_tgtb()
    {
        $data = json_decode(trim(file_get_contents('php://input')));

        $this->load->model('Tugastb_model');
        echo json_encode($this->Tugastb_model->insert_tgtb($data));
    }

    public function api_update_tgtb()
    {

        $data = json_decode(trim(file_get_contents('php://input')));

        $this->load->model('Tugastb_model');
        $this->Tugastb_model->update_tgtb($data);
    }

    public function api_delete_tgtb($id)
    {
        $this->load->model('Tugastb_model');
        echo json_encode($this->Tugastb_model->delete_tgtb($id));
    }

    public function api_headmaster_tgtb()
    {
        $this->load->model('Tugastb_model');
        echo json_encode($this->Tugastb_model->headmaster());
    }

    public function modal_mengajar_tambah()
    {
        $this->load->view('modal/teacher/mengajar/tambah.php');
    }

    public function api_mapel_get($query = null)
    {
        $this->load->model('Mengajar_model');

        echo json_encode($this->Mengajar_model->get_mapel($query));
    }

    public function api_dguru_insert()
    {
        $this->load->model('Teacher_model');
        $data = json_decode(trim(file_get_contents('php://input')));

        if (!isset($data->id_guru)) {
            echo json_encode(['error' => 'Mohon pilih dari data ptk!']);
            die();
        }

        $id_guru = $data->id_guru;

        echo json_encode($this->Teacher_model->dguru_insert($id_guru));
    }

    public function api_dguru_show($start = 0, $count = 10, $filter = [])
    {
        $this->load->model('Teacher_model');

        echo json_encode($this->Teacher_model->dguru_show($start, $count, $filter));
    }

    public function modal_dmengajar_tambah()
    {
        $this->load->view('modal/teacher/tugas-mengajar/tambah.php');
    }

    public function api_mapel_select($query = null)
    {
        $this->load->model('Tugas_model');
        echo json_encode($this->Tugas_model->mapelSelect($query));
    }

    public function api_tapel_select($query = null)
    {
        $this->load->model('Tugas_model');
        echo json_encode($this->Tugas_model->getTapel($query));
    }

    public function api_tmengajar_tambah()
    {
        $this->load->model('Tugas_model');

        $data = json_decode(trim(file_get_contents('php://input')));

        echo json_encode($this->Tugas_model->tambah_tmengajar($data));
    }

    public function api_tmengajar_show($id, $start, $count)
    {
        $this->load->model('Tugas_model');

        echo json_encode($this->Tugas_model->show_tmengajar($id, $start, $count));
    }

    public function api_tmengajar_delete($id)
    {
        $this->load->model('Tugas_model');

        echo json_encode($this->Tugas_model->delete_tmengajar($id));
    }

    public function api_tmengajar_update()
    {
        $this->load->model('Tugas_model');
        $data = json_decode(trim(file_get_contents('php://input')));
        $this->Tugas_model->update_tmengajar($data);
    }

    public function api_dguru_update()
    {
        $this->load->model('Teacher_model');

    }

    public function api_dguru_delete($id)
    {
        $this->load->model('Teacher_model');
        echo json_encode($this->Teacher_model->delete_dguru($id));
    }

    public function api_dguru_statsbox()
    {
        $this->load->model('Teacher_model');
        echo json_encode($this->Teacher_model->dguru_statsbox());
    }

    public function api_ptk_get($npsn)
    {
        $this->load->model('Teacher_model');
        echo json_encode($this->Teacher_model->api_ptk($npsn));
    }

    public function api_bstudi_get($query = null)
    {
        $this->load->model('Teacher_model');
        echo json_encode($this->Teacher_model->bstudy_get($query));
    }

}