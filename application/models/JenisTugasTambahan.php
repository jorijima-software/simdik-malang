<?php
/**
 * Created by PhpStorm.
 * User: ragasubekti
 * Date: 2/5/18
 * Time: 2:55 PM
 */

class JenisTugasTambahan extends \Illuminate\Database\Eloquent\Model {
    protected $table = 'jenis_jabatan';
    protected $guarded = [];
    public $timestamps = false;
}