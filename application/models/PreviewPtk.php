<?php
/**
 * Created by PhpStorm.
 * User: ragasubekti
 * Date: 1/12/18
 * Time: 9:11 PM
 */
class PreviewPtk extends \Illuminate\Database\Eloquent\Model {
    protected $table = 'preview_ptk';
    protected $guarded = [];
    public $timestamps = false;

    public function jenisPegawai() {
        $CI =& get_instance();
        $CI->load->model('JenisPegawai');

        return $this->hasOne('JenisPegawai', 'id', 'jenis_pegawai');
    }

    public function namaAgama() {
        $CI =& get_instance();
        $CI->load->model('Agama');

        return $this->hasOne('Agama', 'id', 'agama');
    }
}