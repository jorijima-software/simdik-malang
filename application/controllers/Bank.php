<?php

class Bank extends CI_Controller {
    public function showBank() {
        $this->load->model('JenisBank');
        header('Content-Type: application/json');

        echo json_encode(JenisBank::all());
    }
}