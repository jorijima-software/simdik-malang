<div class="modal-header">
    <strong>Detail Rekening</strong>
</div>
<div class="modal-body">
    <div class="form-group">
        <div class="row">
            <div class="col-md-3">
                <label for="bank">Bank</label>
                <ui-select ng-model="rekening.jenisBank" theme="bootstrap" id="bank" search-enable="false">
                    <ui-select-match placeholder="Pilih Bank">{{$select.selected.nama}}
                    </ui-select-match>
                    <ui-select-choices repeat="item in bankList track by $index">
                        {{item.nama}}
                    </ui-select-choices>
                </ui-select>
            </div>
            <div class="col-md-9">
                <label for="no-rekening">No. Rekening</label>
                <input type="text" class="form-control" ng-model="rekening.noRekening" id="no-rekening" maxlength="10">
            </div>
        </div>
    </div>
    <div class="form-group">
        <label for="nama">Atas Nama</label>
        <input type="text" class="form-control" ng-model="rekening.nama" id="nama" ng-change="rekening.nama = rekening.nama.toUpperCase()">
    </div>
    <div class="form-group">
        <label for="cabang">Cabang</label>
        <input type="text" class="form-control" ng-model="rekening.cabang" id="cabang" ng-change="rekening.cabang = rekening.cabang.toUpperCase()">
    </div>
    <div class="form-group">
        <div class="help-block">
            <span class="label label-warning">PENTING!</span>&nbsp;&nbsp;&nbsp; Pastikan
            <strong>Data Rekening</strong> sesuai dengan Buku Tabungan.
        </div>
    </div>
</div>
<div class="modal-footer">
    <button class="btn btn-primary btn-sm" ng-click="onBtnSaveClicked()">
        <i class="fa fa-check"></i>
        <span>Simpan</span>
    </button>
    <button class="btn btn-default btn-sm" ng-click="dismiss()">
        <i class="fa fa-times"></i>
        <span>Tutup</span>
    </button>
</div>