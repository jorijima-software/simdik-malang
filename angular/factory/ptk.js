angular.module('diknasmlg').factory('$PtkFactory', function ($http, $q) {

    function API(path) {
        return localStorage.getItem('PATHLOC') + path
    }

    function API2(path) {
        return localStorage.getItem('LARAPATH') + path
    }

    var factory = {}

    factory.get = function (id) {
        return $q(function (resolve, reject) {
            $http.get(API('api/2/ptk/' + id)).then(function (response) {
                if (response.status === 200) {
                    return resolve(response.data)
                } else {
                    return reject(response)
                }
            })
        })
    }

    factory.jenisPegawaiSetting = function (start, count) {
        return $q(function (resolve, reject) {
            $http.get(API('ptk/settingJenisPegawai/' + start + '/' + count)).then(function (response) {
                if (response.status == 200) {
                    return resolve(response.data)
                } else {
                    return reject(response)
                }
            })
        })
    }

    factory.bidangStudiSetting = function (start, count) {
        return $q(function (resolve, reject) {
            $http.get(API('ptk/settingBidangStudi/' + start + '/' + count)).then(function (response) {
                if (response.status == 200) {
                    return resolve(response.data)
                } else {
                    return reject(response)
                }
            })
        })
    }

    factory.select = function (name) {
        return $q(function (resolve, reject) {
            $http.get(API('api/teacher/select/' + name)).then(function (reply) {
                if (reply.status == 200) {
                    return resolve(reply.data);
                } else {
                    return reject(reply);
                }
            });
        });
    };

    factory.show = function (start, count, search, filter) {
        return $q(function (resolve, reject) {
            $http.get(API('api/ptk/show/' + start + '/' + count + '/' + search + '?' + jQuery.param(filter))).then(
                function (success) {
                    if (success.status == 200) {
                        return resolve(success.data);
                    } else {
                        return reject(success);
                    }
                }
            );
        });
    };

    factory.stats = function () {
        return $q(function (resolve, reject) {
            $http.get(API('api/ptk/stats')).then(function (r) {
                if (r.status == 200) {
                    return resolve(r.data);
                } else {
                    return reject(r);
                }
            });
        });
    };

    factory.stats2 = function () {
        return $q(function (resolve, reject) {
            $http.get(API('ptkbaru/stats')).then(function (r) {
                if (r.status == 200) {
                    return resolve(r.data)
                } else {
                    return reject(r)
                }
            })
        })
    }

    factory.delete = function (id, alasan) {
        return $q(function (resolve, reject) {
            $http.post(API('api/ptk/delete'), {
                id: id,
                alasan: alasan
            }).then(function (r) {
                if (r.status == 200) {
                    return resolve(r.data);
                } else {
                    return reject(r);
                }
            });
        });
    };

    factory.insert = function (data) {
        return $q(function (resolve, reject) {
            if (data['nama_lengkap'].length <= 0) {
                return resolve({
                    error: 'Nama tidak boleh kosong'
                });
            }

            $http.post(API('api/ptk/insert'), {
                data: {
                    'nuptk': data.nuptk,
                    'nama': data.nama_lengkap,
                    'gelar': {
                        'depan': data.gelar_depan,
                        'belakang': data.gelar_belakang
                    },
                    'nip': {
                        'lama': data.nip_lama,
                        'baru': data.nip_baru
                    },
                    'jenis_pegawai': data.pegawai.id,
                    'sekolah': (data.sekolah && data.sekolah.id) ? data.sekolah.id : null,
                    'nik': data.nik,
                    'nama_ibu': data.ibu
                },
                profil: {
                    'tempat_lahir': data.profil.tempat_lahir,
                    'tanggal_lahir': data.profil.tanggal_lahir,
                    'agama': data.profil.agama
                },
                rumah: {
                    'kota': data.rumah.kota,
                    'kecamatan': data.rumah.kecamatan,
                    'no_telp': data.rumah.no_telp,
                    'alamat': data.rumah.alamat
                }, sertifikasi: {
                    pola: data.sert.pola,
                    no_sert: data.sert.no,
                    tahun: data.sert.tahun,
                    bidang: (data.sert.bidang && data.sert.bidang.id) ? data.sert.bidang.id : null
                }
            }).then(function (reply) {
                if (reply.status == 200) {
                    return resolve(reply.data);
                } else {
                    return reject(reply);
                }
            }, function (err) {
                return reject(err);
            });
        });
    };

    factory.update = function (data) {
        return $q(function (resolve, reject) {
            $http.post(API('api/ptk/update'), {
                data: {
                    'id': data.id,
                    'nuptk': data.nuptk,
                    'nama': data.nama_lengkap,
                    'gelar': {
                        'depan': data.gelar_depan,
                        'belakang': data.gelar_belakang
                    },
                    'nip': {
                        'lama': data.nip_lama,
                        'baru': data.nip_baru
                    },
                    'jenis_pegawai': data.pegawai.id,
                    'sekolah': (data.sekolah && data.sekolah.id) ? data.sekolah.id : null,
                    'nik': data.profil.nik,
                    'nama_ibu': data.profil.nama_ibu
                },
                profil: {
                    'tempat_lahir': data.profil.tempat_lahir,
                    'tanggal_lahir': data.profil.tanggal_lahir,
                    'agama': data.profil.agama,
                },
                rumah: {
                    'kota': data.rumah.kota,
                    'kecamatan': data.rumah.kecamatan,
                    'no_telp': data.rumah.no_telp,
                    'alamat': data.rumah.alamat
                }, sertifikasi: {
                    pola: data.sert.pola,
                    no_sert: data.sert.no,
                    tahun: data.sert.tahun,
                    bidang: (data.sert.bidang && data.sert.bidang.id) ? data.sert.bidang.id : null
                }
            }).then(function (reply) {
                if (reply.status == 200) {
                    return resolve(reply.data)
                } else {
                    return reject(reply);
                }
            });
        });
    };


    factory.detail = function (id) {
        return $q(function (resolve, reject) {
            $http.get(apiPath + 'api/ptk/detail/' + id).then(function (r) {
                if (r.status === 200) {
                    return resolve(r.data);
                } else {
                    return reject(r);
                }
            })
        })
    };

    factory.npns = function (start, count, filter) {
        return $q(function (resolve, reject) {
            $http.get(apiPath + 'api/ptk/npns/' + start + '/' + count + '?' + jQuery.param(filter)).then(function (r) {
                if (r.status === 200) {
                    return resolve(r.data);
                } else {
                    return reject(r);
                }
            })
        })
    }

    factory.pns = function (start, count, filter) {
        return $q(function (resolve, reject) {
            $http.get(apiPath + 'api/ptk/pns/' + start + '/' + count + '?' + jQuery.param(filter)).then(function (r) {
                if (r.status === 200) {
                    return resolve(r.data);
                } else {
                    return reject(r);
                }
            })
        })
    }

    factory.informasi = function (id) {
        return $q(function (resolve, reject) {
            $http.get(apiPath + 'api/ptk/informasi/' + id).then(function (r) {
                if (r.status === 200) {
                    return resolve(r.data);
                } else {
                    return reject(r);
                }
            })
        })
    }

    return factory;
});
