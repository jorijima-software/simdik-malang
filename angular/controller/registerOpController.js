angular.module('diknasmlg').controller('registerOpController', function($scope, $uibModal, NgTableParams, $q, $OperatorFactory, $SchoolFactory) {

    var apiPath = localStorage.getItem('PATHLOC');

    $scope.search = '';

    $scope.educationList = [];
    $scope.filter = {
        tingkat: {}
    };

    $scope.onInputSearchChange = function () {
        $scope.operatorTable.reload();
    };

    $scope.operatorTable = new NgTableParams({}, {
        getData: function (params) {
            var count = params.count();
            var start = count * (params.page() - 1);
            var search = $scope.search;
            var filter = $scope.filter;

            return $q(function (resolve, reject) {
                $OperatorFactory.show(start, count, search, filter, function (reply) {
                    if (reply.error) return reject(reply.error);
                    params.total(reply.total);
                    return resolve(reply.data);
                })
            })
        }
    });

    $scope.onBtnAddOperatorClicked = function () {
        $uibModal.open({
            templateUrl: apiPath + 'registrationop/add',
            size: 'md',
            controller: 'addOperatorModal'
        }).result.then(refreshTable, refreshTable)
    };

    $scope.onFilterTingkatChange = function() {
        refreshTable();
    };

    function refreshTable() {
        $scope.operatorTable.reload();
    }

    function init() {
        $SchoolFactory.type(function(reply) {
            $scope.educationList = reply;
            $scope.educationList.unshift({id: 0, nama: 'Semua'});
        });
    }

    init();
});