/*
    Created by Raga Subekti
    at  03/12/17 11.44 by PhpStorm
*/

var apiPath = localStorage.getItem('PATHLOC');


var routes = [
    {url: '/berita', path: 'spa/berita/index', controller: 'beritaController'},
    {url: '/berita/baru', path: 'spa/berita/baru', controller: 'buatBeritaController'},
    {url: '/berita/edit/:id', path: 'spa/berita/edit', controller: 'editBeritaController'},
    {url: '/berita/detail/:id', path: 'spa/berita/detail', controller: 'detailBeritaController'},
    {url: '/ptk', path: 'spa/teacher/ptk_index', controller: 'ptkController'},
    {url: '/ptk/riwayat/sekolah/:id', path: 'spa/teacher/ptk_riwayat_sekolah', controller: 'ptkController'},
    {url: '/ptk/riwayat/gaji/:id', path: 'spa/teacher/ptk_riwayat_gaji', controller: 'ptkController'},
    {url: '/ptk/riwayat/pangkat/:id', path: 'spa/teacher/ptk_riwayat_pangkat', controller: 'ptkController'},
    {url: '/tiket', path: 'spa/tiket/tiket_index', controller: 'tiketController'},
    {url: '/tiket/tambah', path: 'spa/tiket/tiket_tambah', controller: 'tiketTambahController'},
    {url: '/rombel', path: 'spa/rombel/rombel_index', controller: 'rombelController'},
    {url: '/rombel/detail/:id', path: 'spa/rombel/rombel_detail', controller: 'rombelDetailController'},
    {url: '/operator', path: 'spa/operator/index', controller: 'operatorController'},
    {url: '/operator/activity/:id', path: 'spa/operator/activity', controller: 'operatorActivity'},
    {url: '/guru', path: 'spa/guru/index', controller: ''},
    {url: '/guru/tugas-mengajar/:id', path: 'spa/guru/tugas_mengajar'},
    {url: '/sekolah', path: 'spa/school/index'},
    {url: '/sekolah/detail/:id', path: 'spa/school/detail'},
    {url: '/sekolah/profil', path: 'spa/school/profile'},
    {url: '/log/sms', path: 'spa/log/sms'},
    {url: '/log/ptk', path: 'spa/log/ptk'},
    {url: '/log/login', path: 'spa/log/login'},
    {url: '/log/rombel', path: 'spa/log/rombel'},
    {url: '/log/guru', path: 'spa/log/guru'},
    {url: '/tugas-tambahan', path:'spa/school/tugas_tambahan'},
    {url: '/dashboard', path:'spa/dashboard/index'},
    {url: '/not-found', path:'spa/dashboard/not_found'},
    {url: '/ptk/import/verify-data', path: 'spa/ptk/verify_data', controller: 'PtkVerifikasiImport'},
    {url: '/ptk/import/verify-data/:id', path: 'spa/ptk/verify_data', controller: 'PtkVerifikasiImport'},
    {url: '/ptk/riwayat/kerja/:id', path: 'spa/riwayat/kerja', controller: 'PtkRiwayatKerja'}, 
    {url: '/ptk/riwayat/sekolah/:id/import', path: 'spa/riwayat/sekolah_import', controller: 'PtkRiwayatSekolahImport'},
    {url: '/setting/timer', path: 'spa/timer/setting', controller: 'SettingTimer'},
    {url: '/setting/bidang-studi', path: 'spa/ptk/setting_bidang_studi', controller: 'SettingBidangStudi'},
    {url: '/setting/jenis-pegawai', path: 'spa/ptk/setting_jenis_pegawai', controller: 'SettingJenisPegawai'},
    {url: '/statistik/guru', path: 'statistik/pageGuru', controller: 'StatistikGuru'},
    {url: '/statistik/data-masuk', path: 'statistik/pageDataMasuk', controller: 'StatistikDataMasuk'},
    {url: '/statistik/pensiun', path: 'statistik/pagePensiun', controller: 'StatistikPensiun'},
    {url: '/statistik/insentif', path: 'statistik/pageInsentif', controller: 'StatistikInsentif'}
];

angular.module('diknasmlg').config(function ($routeProvider) {
    routes.forEach(function (route) {
        $routeProvider.when(route.url, {
            templateUrl: apiPath + route.path,
            controller: route.controller
        })
    })

    $routeProvider.otherwise({redirectTo: '/not-found'})
});
