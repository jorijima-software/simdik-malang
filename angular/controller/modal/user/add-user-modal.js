// Created by Raga Subekti at 06/11/17
angular.module('diknasmlg').controller('addUserModal', function($scope, $uibModalInstance, $SchoolFactory, $UserFactory, Notify) {

    $scope.schoolList = [];

    $scope.dismiss = function () {
        $uibModalInstance.dismiss();
    }

    $scope.new = {
        username: '',
        password: '',
        level: 0,
        sekolah: {}
    }

    $scope.passwordType = 'password';

    $scope.onPasswordType = function() {
        if($scope.passwordType == 'password') {
            $scope.passwordType = 'text';
        } else {
            $scope.passwordType = 'password';
        }
    }

    $scope.onInputSearchChange = function (query) {
        return $SchoolFactory.get(query, function (reply) {
            $scope.schoolList = reply;
        })
    };

    $scope.onBtnSaveClicked = function() {
        $UserFactory.create($scope.new, function(reply) {
            if(reply.error) {
                Notify.error('Kesalahan', reply.error);
            } else {
                Notify.success('Sukses', 'Berhasil menambahkan user');
                $uibModalInstance.close();
            }
        })
    }

    function init() {
        $SchoolFactory.get('', function (reply) {
            $scope.schoolList = reply;
        });
    }

    init();

});