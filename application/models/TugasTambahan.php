<?php
/**
 * Created by PhpStorm.
 * User: ragasubekti
 * Date: 2/5/18
 * Time: 3:03 PM
 */

class TugasTambahan extends \Illuminate\Database\Eloquent\Model {
    protected $table = 'data_tugas_tambahan';
    protected $guarded = [];
    public $timestamps = false;

    public function tugas() {
        $CI =& get_instance();
        $CI->load->model('JenisTugasTambahan');
        return $this->hasOne('JenisTugasTambahan', 'id', 'id_jenis_jabatan');
    }

    public function ptk() {
        $CI =& get_instance();
        $CI->load->model('Ptk');
        return $this->hasOne('Ptk', 'id', 'id_data_guru');
    }

    public function sekolah() {
        $CI =& get_instance();
        $CI->load->model('Sekolah');
        return $this->hasOne('Sekolah', 'id', 'id_data_sekolah');
    }
}