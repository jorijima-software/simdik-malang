// Created by Raga Subekti at 07/10/17
angular.module('diknasmlg').factory('$SchoolFactory', function ($http, $q) {
    var factory = {};
    var apiPath = localStorage.getItem('PATHLOC');

    function API2(path) {
        return localStorage.getItem('LARAPATH') + path;
    }

    factory.tableStats = function (start, count, filter) {
        return $q(function(resolve, reject) {
            var params = filter
                params['start'] = start
                params['count'] = count
            $http.get(apiPath + 'api/sekolah/tstats?' + jQuery.param(params)).then(function(response) {
                if(response.status === 200) {
                    return resolve(response.data)
                } else {
                    return reject(response)
                }
            })
        })
    }

    factory.showImport = function(){
        var id = '';
        if(arguments.length > 2) {
            id = arguments[2];
        }

        var start = (arguments[0]) ? arguments[0] : 0;
        var count = (arguments[1]) ? arguments[1] : 10;

        return new Promise(function(resolve, reject) {
            $http.get(apiPath + 'api/ptk/import/show?' + jQuery.param({
                start: start,
                count: count,
                id: id
            })).then(function(result) {
                if(result.status == 200) {
                    return resolve(result.data);
                } else {
                    return reject(result)
                }
            })
        })
    }

    function getCookie(name) {
        var value = "; " + document.cookie;
        var parts = value.split("; " + name + "=");
        if (parts.length == 2) return parts.pop().split(";").shift();
    }

    factory.dashboardPercentage = function () {
        return $q(function (resolve, reject) {
            $http.get(apiPath + 'school/apipersentasesekolah').then(function (reply) {
                return resolve(reply.data);
            })
        })
    }

    factory.percentage = function () {
        return $q(function (resolve, reject) {
            var id = getCookie('_user_id');
            $http.get(apiPath + 'ptkbaru/statsPtk/' + id).then(function (reply) {
                return resolve(reply.data);
            })
        })
    };

    factory.get = function (query, callback) {
        $http.get(apiPath + 'school/get/' + query).then(
            function (reply) {
                if (reply.status == 200) {
                    return callback(reply.data);
                } else {
                    console.log(reply)
                }
            }
        )
    };

    factory.show = function (start, count, query, filter, callback) {
        $http.get(apiPath + 'api/sekolah/' + start + '/' + count + '/' + '?' + jQuery.param(filter)).then(
            function (reply) {
                if (reply.status == 200) {
                    return callback(reply.data);
                } else {
                    console.log(reply)
                }
            }
        )
    };

    factory.insert = function (data, callback) {
        $http.post(apiPath + 'api/school/insert', {
            'nisn': data.nisn,
            'nama': data.nama,
            'tingkat': data.tingkat,
            'alamat': data.alamat,
            'status': data.status,
            'kecamatan': data.kecamatan
        }).then(
            function (reply) {
                if (reply.status == 200) {
                    return callback(reply.data);
                } else {
                    console.log(reply)
                }
            }
        )
    };

    factory.update = function (data, callback) {
        $http.post(apiPath + 'api/school/update', {
            'id': data.id,
            'nama': data.nama,
            'nisn': data.nisn,
            'tingkat': data.tingkat,
            'alamat': data.alamat,
            'status': data.status,
            'kecamatan': data.kecamatan
        }).then(
            function (reply) {
                if (reply.status == 200) {
                    return callback(reply.data);
                } else {
                    console.log(reply)
                }
            }
        )
    };

    factory.delete = function (id, callback) {
        $http.post(apiPath + 'api/school/delete/' + id, {}).then(
            function (reply) {
                if (reply.status == 200) {
                    return callback(reply.data);
                } else {
                    console.log(reply);
                }
            }, function (err) {
                console.log(err);
            }
        )
    };

    factory.operatorStatbox = function (callback) {
        $http.get(apiPath + 'api/operator/school/statsbox').then(
            function (reply) {
                if (reply.status == 200) {
                    return callback(reply.data);
                } else {
                    console.log(reply);
                }
            }, function (err) {
                console.log(err);
            }
        )
    };

    factory.statsbox = function (callback) {
        $http.get(apiPath + 'api/school/statsbox').then(
            function (reply) {
                if (reply.status == 200) {
                    return callback(reply.data);
                } else {
                    console.log(reply);
                }
            }, function (err) {
                console.log(err);
            }
        )
    };

    factory.spread = function (callback) {
        $http.get(apiPath + 'api/school/spread').then(
            function (reply) {
                if (reply.status == 200) {
                    return callback(reply.data);
                } else {
                    console.log(reply);
                }
            }, function (err) {
                console.log(err);
            }
        )
    };

    factory.type = function (callback) {
        $http.get(apiPath + 'api/school/type').then(
            function (reply) {
                if (reply.status == 200) {
                    return callback(reply.data);
                } else {
                    console.log(reply);
                }
            }, function (err) {
                console.log(err);
            }
        )
    };

    factory.showteacherschool = function (start, count, query, callback) {
        $http.get(apiPath + 'api/school/show/teacherschool/' + start + '/' + count + '/' + query).then(
            function (success) {
                if (success.status == 200) {
                    return callback(success.data);
                }
            },
            function (err) {

            }
        );
    };

    factory.showSchoolTeacher = function (id, start, count, query, filter, callback) {
        $http.get(apiPath + 'api/school/' + id + '/teacher/' + start + '/' + count + '/' + query + '?' + jQuery.param(filter)).then(
            function (success) {
                if (success.status == 200) {
                    return callback(success.data);
                }
            },
            function (err) {

            }
        );
    };

    factory.tambahRombel = function (data, callback) {
        $http.post(apiPath + 'api/rombel/tambah', {
            tingkat: data.tingkat,
            rombel: data.rombel,
            tapel: data.tapel
        }).then(
            function (success) {
                if (success.status == 200) {
                    return callback(success.data);
                }
            },
            function (err) {

            }
        );
    };

    factory.updateRombel = function (data, callback) {
        $http.post(apiPath + 'api/rombel/update', {
            id: data.id,
            tingkat: data.tingkat,
            rombel: data.rombel,
            tapel: data.tapel,
            old: data.old
        }).then(
            function (success) {
                if (success.status == 200) {
                    return callback(success.data);
                }
            },
            function (err) {

            }
        );
    };

    factory.showRombel = function (start, count, filter, callback) {

    };

    factory.deleteRombel = function (id, callback) {
        $http.get(apiPath + 'api/rombel/delete/' + id).then(
            function (success) {
                if (success.status == 200) {
                    return callback(success.data);
                }
            },
            function (err) {

            }
        );
    };

    factory.tingkatKelas = function (callback) {
        $http.get(apiPath + 'api/tingkat-kelas').then(
            function (success) {
                if (success.status == 200) {
                    return callback(success.data);
                }
            },
            function (err) {

            }
        );
    };

    factory.rombelStatsbox = function (callback) {
        $http.get(apiPath + 'api/rombel/statsbox').then(
            function (success) {
                if (success.status == 200) {
                    return callback(success.data);
                }
            },
            function (err) {

            }
        );
    };

    factory.updateOperator = function (data) {
        return $q(function (resolve, reject) {
            $http.post(apiPath + 'api/operator/update', data).then(
                function (success) {
                    if (success.status == 200) {
                        return resolve(success.data);
                    } else {
                        return reject(success);
                    }
                },
                function (err) {
                    return reject(err);
                }
            );
        });
    };

    factory.getOperator = function () {
        return $q(function (resolve, reject) {
            $http.get(apiPath + 'api/operator/get').then(
                function (success) {
                    if (success.status == 200) {
                        return resolve(success.data);
                    } else {
                        return reject(success);
                    }
                },
                function (err) {
                    return reject(err);
                }
            );
        })
    };

    factory.resetPassword = function (npsn) {
        return $q(function (s, r) {
            $http.get(apiPath + 'api/sekolah/reset/' + npsn).then(
                function (success) {
                    if (success.status == 200) {
                        return s(success.data);
                    } else {
                        return r(success);
                    }
                },
                function (err) {
                    return r(err);
                }
            );
        })
    };

    factory.detail = function (id) {
        return $q(function (resolve, reject) {
            $http.get(apiPath + 'api/sekolah/detail/' + id).then(function (r) {
                if (r.status === 200) {
                    return resolve(r.data);
                } else {
                    return reject(r);
                }
            })
        })
    };

    factory.mini = (search) => {
        return $q((resolve, reject) => {
            $http.get(apiPath + 'api/sekolah/mini-list?search=' + search).then(response => {
                if(response.status === 200)
                    return resolve(response.data)
                return reject(response)
            })
        })
    }

    return factory;
});