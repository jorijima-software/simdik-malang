<?php
/**
 * Created by PhpStorm.
 * User: ragasubekti
 * Date: 08/10/17
 * Time: 11.26
 */
?>
<div class="modal-header">
    <div class="modal-title">
        <i class="fa fa-eye"></i> &nbsp; Detail Sekolah
    </div>
</div>
<div class="modal-body">
    <uib-tabset active="active">
        <uib-tab heading="Data Sekolah">
            <table class="table table-striped table-condensed">
                <tr>
                    <th>NPSN</th>
                    <td><code>{{detail.nisn}}</code></td>
                </tr>
                <tr>
                    <th>Tingkat</th>
                    <td>{{detail.tingkat}}</td>
                </tr>
                <tr>
                    <th>Nama</th>
                    <td>{{detail.nama}}</td>
                </tr>
                <tr>
                    <th>Kecamatan</th>
                    <td>{{detail.nama_kecamatan}}</td>
                </tr>
                <tr>
                    <th>Alamat</th>
                    <td>{{detail.alamat}}</td>
                </tr>
                <tr>
                    <th>Status</th>
                    <td>{{status.detail.status}}</td>
                </tr>
            </table>
        </uib-tab>
        <uib-tab heading="Data Pegawai">
            <div class="row">
                <table class="table table-striped table-condensed table-bordered"
                       ng-table="teacherTable">
                    <thead style="background:rgba(12, 153, 0, 0.8);  color: white;">
                    <tr>
                        <th rowspan="2" style="vertical-align: middle;text-align: center">No</th>
                        <th rowspan="2" style="vertical-align: middle;text-align: center">NUPTK</th>
                        <th rowspan="2" style="vertical-align: middle;text-align: center">Nama Lengkap</th>
                        <th rowspan="2" style="vertical-align: middle;text-align: center">Jenis Pegawai</th>
                        <th colspan="3" style="vertical-align: middle;text-align: center">Riwayat</th>
                        <th rowspan="2"></th>
                    </tr>
                    <tr>
                        <th style="vertical-align: middle;text-align: center">Sekolah</th>
                        <th style="vertical-align: middle;text-align: center">Pangkat</th>
                        <th style="vertical-align: middle;text-align: center">Gaji Berkala</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr ng-repeat="row in $data">
                        <td>{{($index + 1) + (teacherTable._params.count * (teacherTable._params.page -1))}}</td>
                        <td>{{row.nuptk}}</td>
                        <td>{{row.nama_lengkap}}</td>
                        <td>{{row.jenis_pegawai}}</td>
                        <td>
                            {{row.data_sekolah}}
                        </td>
                        <td>
                            <span ng-if="row.status_pns == 1"> {{row.pangkat_guru}}  </span>
                            <span ng-if="row.status_pns == 0">
                                            Khusus PNS
                            </span>
                        </td>
                        <td>
                            <span ng-if="row.status_pns == 1">{{row.gaji_berkala}} </span>
                            <span ng-if="row.status_pns == 0">
                                Khusus PNS
                            </span>
                        </td>
                        <td>
                            <div class="pull-right">
                                <button class="btn btn-success btn-xs" ng-click="onBtnDetailEmployeeClicked(row)">
                                    <i class="fa fa-eye"></i> Detail
                                </button>
                            </div>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </uib-tab>
    </uib-tabset>
</div>
<div class="modal-footer">
    <button class="btn btn-default btn-sm" ng-click="dismiss()">
        <i class="fa fa-times"></i> Tutup
    </button>
</div>
