/*
    Created by Raga Subekti
    at  17/12/17 21.44 by PhpStorm
*/
angular.module('diknasmlg').controller('rombelController', function ($scope, $RombelFactory, $q, NgTableParams) {

    $scope.statsbox = [];

    $scope.search = ''

    $scope.onSearchChange = function () {
        $scope.schoolTable.reload()
    }

    $scope.schoolTable = new NgTableParams({}, {
        getData: function(params) {
            var count = params.count();
            var start = count * (params.page() - 1);
            var filter = {}
                filter.search = $scope.search
            return $q(function(resolve, reject) {
                $RombelFactory.school(start, count, filter).then(function(r) {
                    if(r || !r.error) {
                        params.total(r.total);
                        return resolve(r.data);
                    } else {
                        return reject(r);
                    }
                })
            })
        }
    });


    function init() {
        $RombelFactory.stats().then(function(reply) {
            $scope.statsbox = reply;
        })
    }

    init();

});