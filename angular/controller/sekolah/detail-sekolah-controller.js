// Created by Raga Subekti at 03/11/17
angular.module('diknasmlg').controller('detailSekolahController', function ($scope, $uibModal, $q, NgTableParams, $SchoolFactory, Notify, $TeacherFactory, $PtkFactory, $routeParams) {

    $scope.routes = $routeParams
    $scope.detail = {}
    $scope.search = ''

    var apiPath = localStorage.getItem('PATHLOC');

    $scope.onBtnImportClicked = function() {
        $uibModal.open({
            templateUrl: apiPath + 'ptk/modal_import_xls',
            controller: 'sekolah.ptk.import'
        })
    }

    $scope.onBtnAddClicked = function () {
        $uibModal.open({
            templateUrl: apiPath + 'modal/ptk/tambah',
            size: 'lg',
            backdrop: 'static',
            controller: 'tambahPtk'
        }).result.then(function () {
            $scope.teacherTable.reload();
        }, function () {
            $scope.teacherTable.reload();
        });
    };

    $scope.onBtnDetailClicked = function (row) {
        $uibModal.open({
            templateUrl: apiPath + 'modal/ptk/detail',
            controller: 'detailPtk',
            size: 'lg',
            resolve: {
                data: function () {
                    return row;
                }
            }
        })
    };

    $scope.onBtnEditClicked = function (row) {
        $uibModal.open({
            templateUrl: apiPath + 'modal/ptk/edit',
            controller: 'editPtk',
            size: 'lg',
            backdrop: 'static',
            resolve: {
                data: function () {
                    return row;
                }
            }
        }).result.then(function () {
            $scope.teacherTable.reload();
        }, function () {
            $scope.teacherTable.reload();
        })
    };


    $scope.onBtnDeleteClicked = function (row) {
        Notify.confirm('Apakah anda yakin ingin menghapus PTK <code>'+ row.nama_lengkap +'</code> ?', function() {
            $PtkFactory.delete(row.id).then(function (reply) {
                if(reply.error) {
                    Notify.error('Kesalahan', reply.error);
                } else {
                    Notify.success('Sukses', 'Data PTK berhasil dihapus');
                }

                $scope.teacherTable.reload();
            });
        }, function() {});
    };

    $scope.search = '';
    $scope.filter = {};

    $scope.teacherTable = new NgTableParams({}, {
        getData: function (params) {
            var count = params.count();
            var start = count * (params.page() - 1);
            // var search = $scope.search;
            var filter = $scope.filter
                filter.search = $scope.search
                filter.sekolah = $scope.routes.id

            return $q(function (resolve, reject) {
                $PtkFactory.pns(start, count, filter).then(function (reply) {
                    if (reply.error) return reject(reply.error);
                    params.total(reply.total);
                    return resolve(reply.data);
                });
            });
        }
    })

    $scope.onSearchKeydown = function($event) {
        if($event.keyCode == 13) {
            $scope.doSearch()
        }
    }

    $scope.doSearch = function() {
        $scope.nonPnsTable.reload()
        $scope.teacherTable.reload()
    }

    $scope.onBtnRekeningDetail = function(row) {
        $uibModal.open({
            templateUrl: apiPath + 'ptkrekcontroller/modaldetail',
            controller: 'ModalPtkRekeningDetail',
            resolve: {
                ptkData: function() {
                    return row
                }
            }
        })
    }

    $scope.onKekuranganClicked = function (row) {
        $uibModal.open({
            templateUrl: apiPath + 'ptkbaru/modalkekurangan',
            controller: function ($scope, $uibModalInstance) {
                $scope.detail = row
                $scope.dismiss = function () {
                    $uibModalInstance.dismiss()
                }
            }
        })
    }

    $scope.nonPnsTable = new NgTableParams({}, {
        getData: function (params) {
            var count = params.count();
            var start = count * (params.page() - 1);
            // var search = $scope.search;
            var filter = $scope.filter
                filter.search = $scope.search
                filter.sekolah = $scope.routes.id

            return $q(function (resolve, reject) {
                $PtkFactory.npns(start, count, filter).then(function (reply) {
                    if (reply.error) return reject(reply.error);
                    params.total(reply.total);
                    return resolve(reply.data);
                });
            });
        }
    })

    function init() {
        $SchoolFactory.detail($scope.routes.id).then(function(r) {
            $scope.detail = r;
        });
    }

    init();
});