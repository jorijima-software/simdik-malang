function init() {
    if (!localStorage.getItem('PATHLOC')) {
        var fullPath = '/';
        localStorage.setItem('PATHLOC', fullPath)
    }
}

init();
var API = localStorage.getItem('PATHLOC');
var app = angular.module('logindiknas', []);
app.controller('phoneVerification', function ($scope, $UserFactory) {
    $scope.timer = 300;

    $scope.verify_code ='';

    $scope.error = null;

    $scope.t = {
        m: 0,
        s: 0
    };

    $scope.onBtnSubmitClicked = function (code) {
        $scope.error = null;
        if(!(code.length > 0)) return $scope.error = 'Masukkan kode verifikasi!';
        $UserFactory.verify(code, function (r) {
            if (r.error) {
                $scope.error = r.error;
            } else {
                window.location = '/dashboard';
            }
        })
    };

    $scope.onBtnResendClicked = function () {
        $scope.error = null;
        $UserFactory.resend(function (r) {
            if (r.error) {
                $scope.error = r.error;
            }
        });

        $UserFactory.timer(function (r) {
            $scope.timer = r;
        });
    };

    function init() {

        $UserFactory.timer(function (r) {
            $scope.timer = r;
        });
        setInterval(function () {
            $UserFactory.timer(function (r) {
                $scope.timer = r;
            })
        }, 10000);

        setInterval(function () {
            if ($scope.timer > 0) {
                $scope.timer--;
            }
            $scope.t.m = (Math.floor($scope.timer / 60) < 10) ? '0' + Math.floor($scope.timer / 60) : Math.floor($scope.timer / 60);
            $scope.t.s = (($scope.timer - ($scope.t.m * 60)) < 10) ? '0' + ($scope.timer - ($scope.t.m * 60)) : $scope.timer - ($scope.t.m * 60);

            jQuery('#timer-countdown').text($scope.t.m + ':' + $scope.t.s);
        }, 1000);
    }

    init();
});

app.factory('$UserFactory', function ($http) {
    return {
        timer: function (callback) {
            $http.get(API + 'api/user/verification/timer').then(
                function (reply) {
                    if (reply.status == 200) {
                        return callback(reply.data);
                    } else {
                        console.error(reply)
                    }
                }
            )
        },
        resend: function (callback) {
            $http.get(API + 'api/user/verification/resend').then(
                function (reply) {
                    if (reply.status == 200) {
                        return callback(reply.data);
                    } else {
                        console.error(reply)
                    }
                }
            )
        },
        verify: function (code, callback) {
            $http.post(API + 'api/user/verification/confirm', {
                code: code
            }).then(
                function (reply) {
                    if (reply.status == 200) {
                        return callback(reply.data);
                    } else {
                        console.error(reply)
                    }
                }
            )
        }
    }
});