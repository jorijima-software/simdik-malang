angular.module('diknasmlg').controller('deleteUserModal', function($scope, $uibModalInstance, data, $UserFactory, Notify) {

    $scope.detail = data;

    $scope.levels = {
        0: 'Admin',
        1: 'Operator'
    }

    $scope.dismiss = function() {
        $uibModalInstance.dismiss();
    }

    $scope.onBtnConfirmClicked = function() {
        $UserFactory.delete(data.id, function(reply) {
            if(reply.error) {
                Notify.error('Kesalahan', reply.error);
            } else {
                Notify.success('Sukses', 'Berhasil menghapus data pengguna');
                $uibModalInstance.close();
            }
        })
    }

})