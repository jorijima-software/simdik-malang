angular.module('diknasmlg').controller('SettingBidangStudi', function(
    $scope,
    $q,
    $uibModal,
    $PtkFactory,
    Notify,
    NgTableParams
) {
    $scope.bidangStudiTable = new NgTableParams({}, {
        getData: function (params) {
            var count = params.count();
            var start = count * (params.page() - 1);
            return $q(function (resolve, reject) {
                $PtkFactory.bidangStudiSetting(start, count).then(function (reply) {
                    if (reply.error) return reject(reply.error)
                    params.total(reply.total)
                    return resolve(reply.data)
                })
            })
        }
    })
})