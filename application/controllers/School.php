<?php
/**
 * Created by PhpStorm.
 * User: ragasubekti
 * Date: 07/10/17
 * Time: 14.00
 */

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

class School extends CI_Controller
{
    public function miniList() {
        header('Content-Type: application/json');
        $find = $this->input->get('search');

        $this->load->model('Sekolah');
        $sekolah = Sekolah::selectRaw('id, nama')->when(strlen($find) > 0, function($q) use ($find) {
            $q->where('nama', 'like', '%'.$find.'%');
        })->take(10)->orderBy('nama', 'ASC')->get();

        echo json_encode($sekolah);
    }

    public function modalPersentase() {
        $this->load->view('sekolah/modal/persentase');
    }

    public function modal_export() {
        $this->load->view('school/export');
    }

    public function apiPersentaseSekolah() {
        $this->load->model('Sekolah');
        $sekolah = new Sekolah();
        echo json_encode($sekolah->allPersentase());
    }

    public function api_export_ptk($id = null) {
        if(!isset($_SESSION['as']) || $this->session->as > 0) {
            echo json_encode(['error' => 'Insufficent Priviledge']);
            die();
        }

        $this->load->model('Sekolah');
        $this->load->model('Ptk');
        $spreadsheet = new Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();

        $sheet
            ->setCellValue('A1', 'No')
            ->setCellValue('B1', 'NUPTK')
            ->setCellValue('C1', 'Nama')
            ->setCellValue('D1', 'Jenis Pegawai')
//            ->setCellValue('D1', 'Unit Kerja')
            ->setCellValue('E1', 'NIP Lama')
            ->setCellValue('F1', 'NIP Baru')
            ->setCellValue('G1', 'Gelar Depan')
            ->setCellValue('H1', 'Gelar Belakang')
            ->setCellValue('I1', 'NIK')
            ->setCellValue('J1', 'Tempat Lahir')
            ->setCellValue('K1', 'Tanggal Lahir')
            ->setCellValue('L1', 'Agama')
            ->setCellValue('M1', 'Nama Ibu')
            ->setCellValue('N1', 'Alamat Rumah')
            ->setCellValue('O1', 'Kota')
            ->setCellValue('P1', 'Kecamatan')
            ->setCellValue('Q1', 'No. Telp')
            ->setCellValue('R1', 'Pola Sertifikasi')
            ->setCellValue('S1', 'No. Peserta')
            ->setCellValue('T1', 'Tahun Sertifikasi')
            ->setCellValue('U1', 'Bidang Studi')
        ;


        foreach (Ptk::where('id_sekolah', $id)->where('deleted', '0')->get() as $key => $row) {
            $no = $key + 1;
            $ix = $key + 2;

            $sheet
                ->setCellValue('A'.$ix, $no)
                ->setCellValue('B'.$ix, $row->nuptk)
                ->setCellValue('C'.$ix, $row->nama_lengkap)
                ->setCellValue('D'.$ix, $row->jenisPegawai->nama)
                ->setCellValue('E'.$ix, $row->nip_lama)
                ->setCellValue('F'.$ix, $row->nip_baru)
                ->setCellValue('G'.$ix, $row->gelar_depan)
                ->setCellValue('H'.$ix, $row->gelar_belakang)
                ->setCellValue('I'.$ix, $row->nik)
                ->setCellValue('J'.$ix, ($row->profil) ? $row->profil->tempat_lahir : '')
                ->setCellValue('K'.$ix, ($row->profil) ? $row->profil->tgl_lahir : '')
                ->setCellValue('L'.$ix, ($row->profil && $row->profil->agama) ? $row->profil->agama->nama : '')
                ->setCellValue('M'.$ix, $row->nama_ibu)
                ->setCellValue('N'.$ix, ($row->rumah) ? $row->rumah->alamat_rumah : '')
                ->setCellValue('O'.$ix, ($row->rumah) ? $row->rumah->kota : '')
                ->setCellValue('P'.$ix, ($row->rumah) ? $row->rumah->kecamatan : '')
                ->setCellValue('Q'.$ix, ($row->rumah) ? $row->rumah->nohp : '')
                ->setCellValue('R'.$ix, ($row->sertifikasi) ? $row->sertifikasi->namaPola($row->sertifikasi->pola_sertifikasi) : '')
                ->setCellValue('S'.$ix, ($row->sertifikasi) ? $row->sertifikasi->no_peserta : '')
                ->setCellValue('T'.$ix, ($row->sertifikasi) ? $row->sertifikasi->tahun_peserta : '')
                ->setCellValue('U'.$ix, ($row->sertifikasi && $row->sertifikasi->bidangStudi) ? $row->sertifikasi->bidangStudi->nama : '')
                ;
        }


        $writer = new Xlsx($spreadsheet);

        $sekolah = Sekolah::find($id);
        $sekolah = $sekolah ? $sekolah->nama : 'NN';

        $name = FCPATH.'uploads/SIMDIK_EXPORT_PTK_'.$sekolah.'_'.uniqid().'.xls';
        $writer->save($name);
        $this->load->helper('download');
        force_download($name, null);
    }

    public function api_export() {
        $this->load->helper('download');

        $this->load->model('Sekolah');

        $spreadsheet = new Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();

        $sheet
            ->setCellValue('A1', 'No')
            ->setCellValue('B1', 'NPSN')
            ->setCellValue('C1', 'Nama Sekolah')
            ->setCellValue('D1', 'Alamat')
            ->setCellValue('E1', 'Kecamatan')
            ->setCellValue('F1', 'No Telp')
            ->setCellValue('G1', 'Operator')
            ->setCellValue('H1', 'Nomor HP')
            ->setCellValue('I1', 'Data Masuk');


//        $sekolah = Sekolah::;
        foreach (Sekolah::orderBy('id_kecamatan', 'ASC')->orderBy('id_jenis_tingkat_sekolah', 'ASC')->get() as $key => $row) {
            $no = $key + 1;
            $ix = $key + 2;
            $persen = $row->ptk->count() > 0 ? round($row->persentase->count() / $row->ptk->count() * 100, 2) : 0;
            $sheet
                ->setCellValue('A'.$ix, $no)
                ->setCellValue('B'.$ix, $row->npsn)
                ->setCellValue('C'.$ix, $row->nama)
                ->setCellValue('D'.$ix, $row->alamat)
                ->setCellValue('E'.$ix, ($row->kecamatan) ? $row->kecamatan->nama : '') 
                ->setCellValue('F'.$ix, $row->notelp)
                ->setCellValue('G'.$ix, ($row->operator) ? $row->operator->nama_operator : '')
                ->setCellValue('H'.$ix, ($row->operator) ? $row->operator->no_hp : '')
                ->setCellValue('I'.$ix, $persen.'%');
        }


        $writer = new Xlsx($spreadsheet);

        $name = FCPATH.'uploads/SIMDIK_EXPORT_SEKOLAH_'.uniqid().'.xls';
        $writer->save($name);
        force_download($name, null);

    }

    public function api_tstats() {
        header('Content-Type: application/json');
        $this->load->model('Sekolah');
        $sekolah = Sekolah::take($count)
            ->when($tingkat = $this->input->get('tingkat'), function ($query) use ($tingkat) {
                if($tingkat['id'] > 0) $query->where('id_jenis_tingkat_sekolah', $tingkat['id']);
            })
            ->when($search = $this->input->get('search'), function ($query) use ($search) {
                $query
                    ->where('nama', 'like', "%{$search}%")
                    ->orWhere('npsn', 'like', "%{$search}%");
            })
            ->when($kecamatan = $this->input->get('kecamatan'), function ($query) use ($kecamatan) {
                if($kecamatan['id'] > 0) $query->where('id_kecamatan', $kecamatan['id']);
            })
        ;
        $total = $sekolah->count();
        $new_sekolah = [];
        $new_sekolah['data'] = [];
        foreach ($sekolah->skip($start)->get() as $key => $row) {
            $persen = $row->ptk->count() > 0 ? round($row->persentase->count() / $row->ptk->count() * 100, 2) : 0;
            array_push($new_sekolah['data'], [
                'id' => $row->id,
                'npsn' => $row->npsn,
                'namaSekolah' => $row->nama,
                'persentase' => $persen,
                'tingkatSekolah' => $row->tingkatSekolah->nama,
                'kecamatan' => $row->kecamatan->nama,
                'alamat' => $row->alamat,
                'idKecamatan' => $row->kecamatan->id,
                'idTingkatSekolah' => $row->tingkatSekolah->id,
                'status' => $row->status,
                'totalPegawai' => $row->ptk->count()
            ]);
        }
        $new_sekolah['total'] = $total;
        echo json_encode($new_sekolah);
    }

    public function api_sekolah_show($start = 0, $count = 10)
    {
        header('Content-Type: application/json');
        $this->load->model('Sekolah');
        $sk = new Sekolah();
        $sekolah = Sekolah::take($count)
            ->when($tingkat = $this->input->get('tingkat'), function ($query) use ($tingkat) {
                if($tingkat['id'] > 0) $query->where('id_jenis_tingkat_sekolah', $tingkat['id']);
            })
            ->when($search = $this->input->get('search'), function ($query) use ($search) {
                $query
                    ->where('nama', 'like', "%{$search}%")
                    ->orWhere('npsn', 'like', "%{$search}%");
            })
            ->when($kecamatan = $this->input->get('kecamatan'), function ($query) use ($kecamatan) {
                if($kecamatan['id'] > 0) $query->where('id_kecamatan', $kecamatan['id']);
            })
            ->when($status = $this->input->get('status'), function($query) use ($status) {
                $query->where('status', $status['id']);
            })
        ;
        $total = $sekolah->count();
        $new_sekolah = [];
        $new_sekolah['data'] = [];
        foreach ($sekolah->skip($start)->get() as $key => $row) {
            $persen = $row->ptk->count() > 0 ? round($row->persentase->count() / $row->ptk->count() * 100, 2) : 0;
            array_push($new_sekolah['data'], [
                'id' => $row->id,
                'npsn' => $row->npsn,
                'namaSekolah' => $row->nama,
                'persentase' => $persen,
                'tingkatSekolah' => $row->tingkatSekolah->nama,
                'kecamatan' => $row->kecamatan->nama,
                'alamat' => $row->alamat,
                'idKecamatan' => $row->kecamatan->id,
                'idTingkatSekolah' => $row->tingkatSekolah->id,
                'status' => $row->status,
                'persentasePns' => $sk->persenPns($row->id)['persen'],
                'persentaseNonPns' => $sk->persenNonPns($row->id)['persen'],
                'totalPegawai' => $row->ptk->count(),
                'operator' => ($row->operator) ? $row->operator->nama_operator : ''
            ]);
        }
        $new_sekolah['total'] = $total;
        echo json_encode($new_sekolah);
    }

    public function spa_index()
    {
        $this->load->view('sekolah/index');
    }

    public function spa_detail()
    {
        $this->load->view('sekolah/detail');
    }

    public function get($query = null)
    {
        $this->load->model('School_model');
        header('Content-Type: application/json');
        echo json_encode($this->School_model->get($query));
    }

    public function show($start, $count, $query = null)
    {
        $this->load->model('School_model');

        header('Content-Type: application/json');

        $filter = $this->input->get();

        echo json_encode($this->School_model->show($start, $count, $query, $filter));
    }

    public function edit()
    {
        $this->getpriv();
        $this->load->view('modal/school/edit');
    }

    public function add()
    {
        $this->getpriv();
        $this->load->view('modal/school/add');
    }


    public function insert()
    {
        $this->getpriv();

        if ($this->input->method() == 'post') {

            header('Content-Type: application/json');
            $data = json_decode(trim(file_get_contents('php://input')));

            $nisn = $data->nisn;
            $tingkat = $data->tingkat;
            $nama = $data->nama;
            $alamat = $data->alamat;
            $kecamatan = $data->kecamatan;
            $status = $data->status;

            $this->load->model('School_model');
            echo json_encode($this->School_model->insert($nisn, $tingkat, $nama, $kecamatan, $alamat, $status));
        } else {
            redirect('/');
        }
    }

    public function update()
    {
        if ($this->input->method() == 'post') {
            header('Content-Type: application/json');
            $data = json_decode(trim(file_get_contents('php://input')));

            $id = $data->id;
            $nisn = $data->nisn;
            $tingkat = $data->tingkat;
            $nama = $data->nama;
            $alamat = $data->alamat;
            $kecamatan = $data->kecamatan;
            $status = $data->status;

            $this->load->model('School_model');
            echo json_encode($this->School_model->update($id, $nisn, $tingkat, $nama, $kecamatan, $alamat, $status));
        } else {
            redirect('/');
        }
    }

    public function delete($id = null)
    {
        $this->getpriv();

        if ($this->input->method() == 'post') {
            $this->load->model('School_model');

            header('Content-Type: application/json');
            echo json_encode($this->School_model->delete($id));
        } else {
            $this->load->view('modal/school/delete');
        }
    }

    public function spread()
    {
        $this->getpriv();

        $this->load->model("School_model");
        header('Content-Type: application/json');
        echo json_encode($this->School_model->graph());
    }

    public function statsbox()
    {
        $this->load->model('School_model');

        header('Content-Type: application/json');
        echo json_encode($this->School_model->statsbox());
    }

    public function type()
    {
        $this->load->model('School_model');
        header('Content-Type: application/json');
        echo json_encode($this->School_model->type());
    }

    private function getpriv()
    {
        if (isset($_SESSION['as']) && isset($_SESSION['uid']) && $_SESSION['as'] > 0) {
            redirect('dashboard');
            die();
        }
    }

    public function showteacherschool($start, $count, $query = null)
    {
        $this->load->model('School_model');

        header('Content-Type: application/json');
        echo json_encode($this->School_model->showteacherschool($start, $count, $query));
    }

    public function spa_profile()
    {
        $this->load->view('school/profile');
//        $this->load->model('School_model');
//        $content = ;
//
//        $js = [
//            'angular/factory/school.js',
//        ];
//
//        $this->load->view('layout', array('content' => $content, 'js' => $js));
    }

    public function manage()
    {

        $result = [];
        $js = [
        ];

        if ($this->input->method() == 'post') {
            $result = $this->School_model->updateProfile($this->input->post());
        }

        $content = 'school/manage';
        $this->load->view('layout', array('content' => $content, 'result' => $result, 'js' => $js));

    }

    public function spa_tugas_tambahan()
    {
        $content = 'tugas-tambahan/index';
        $this->load->view($content);
    }

    public function page_rombel()
    {
        if ($this->session->as > 0) {
            $content = 'rombel/index';

            $js = [
                'angular/controller/rombel/rombel-controller.js',
                'angular/factory/rombel.js',
                'angular/factory/school.js',
                'angular/factory/tugas.js',
                'angular/controller/rombel/modal/tambah-rombel.js',
                'angular/controller/rombel/modal/edit-row-rombel.js'
            ];
        } else {
            $content = null;
            $js = [
                'angular/controller/rombel/rombel-admin.js',
                'angular/factory/rombel.js',
                'angular/factory/school.js',
                'angular/factory/tugas.js',
                'angular/controller/rombel/rombel-detail.js',
            ];
        }

        $this->load->view('layout', array('content' => $content, 'js' => $js));
    }


    public function modal_rombel_tambah()
    {
        $this->load->view('rombel/modal/tambah');
    }

    public function api_tkelas_show()
    {
        $this->load->model('School_model');
        echo json_encode($this->School_model->tk_kelas());
    }

    public function api_rombel_get($npsn)
    {
        $this->load->model('School_model');
        echo json_encode($this->School_model->rombel_total($npsn));
    }

    public function api_profile_get($npsn)
    {
        $this->load->model('School_model');
        echo json_encode($this->School_model->api_profile($npsn));
    }

    public function api_rombel_statsbox()
    {
        $this->load->model('School_model');
        echo json_encode($this->School_model->rombel_statsbox());
    }

    public function modal_operator_ubah()
    {
        $this->load->view('sekolah/operator/ubah');
    }

    public function api_operator_get()
    {
        $this->load->model('School_model');
        header('Content-Type: application/json');
        echo json_encode($this->School_model->get_operator());
    }

    public function api_operator_update()
    {
        if ($this->input->method() == 'post') {
            if ($this->session->as > 0) {
                $this->load->model('School_model');
                header('Content-Type: application/json');
                $data = json_decode(trim(file_get_contents('php://input')));
                echo json_encode($this->School_model->update_operator($data));
            } else {
                $this->load->model('Operator_model');
                header('Content-Type: application/json');

                $data = json_decode(trim(file_get_contents('php://input')));
                echo json_encode($this->Operator_model->operator_update($data));
            }
        }
    }

    public function api_sekolah_reset($npsn)
    {
        if ($this->session->as > 0) {
            return ['error' => 'Insufficient Privilege'];
        } else {
            $this->load->model("School_model");
            echo json_encode($this->School_model->reset_password($npsn));
        }
    }

    public function api_detail($id)
    {
        if ($this->input->method() == 'get') {
            $this->load->model('School_model');
            echo json_encode($this->School_model->getDetail($id));
        }
    }

}