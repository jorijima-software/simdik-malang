// Created by Raga Subekti at 23/10/17
angular.module('diknasmlg').controller('tambahGajiPtk', function ($uibModalInstance, $scope, $RankFactory, $TeacherFactory, Notify, id) {

    $scope.isSubmit = false;

    $scope.rankList = [];

    $scope.new = {
        sk: '',
        tmt: '',
        pangkat: {},
        gaji: ''
    };

    $scope.dismiss = function () {
        $uibModalInstance.dismiss();
    };

    $scope.onBtnSaveClicked = function () {
        $scope.isSubmit = true;

        $TeacherFactory.addSalary($scope.new, id, function (reply) {
            $scope.isSubmit = false;

            if (reply.error) return Notify.error('Kesalahan', reply.error);
            Notify.success('Sukses', 'Sukses menambahkan data gaji');
            $uibModalInstance.close();
        })
    };

    function init() {
        $RankFactory.get(function (reply) {
            $scope.rankList = reply;
        })
    }

    init();

});