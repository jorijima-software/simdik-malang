// Created by Raga Subekti at 18/10/17
angular.module('diknasmlg').controller('tambahPangkatPtk', function ($scope, $uibModalInstance, Notify, $SchoolFactory, $RankFactory, $TeacherFactory, $DistrictFactory, id) {

    $scope.isSubmit = false;

    $scope.datepicker = false;

    $scope.rankList = [];
    $scope.schoolList = [];

    $scope.regencyList = [];

    $scope.new = {
        sk: '',
        tmt: '',
        type: 1,
        area: {},
        sekolah: {},
        pangkat: {},
        unit_kerja: '',
        kota: {}
    };

    $scope.areaList = [
        {id: 0, nama: 'Dalam Malang'},
        {id: 1, nama: 'Luar Malang'}
    ];

    $scope.onSelectInputChange = function () {

    };

    $scope.onRegencyChange = function (query) {
        $DistrictFactory.regency(query, function (reply) {
            $scope.regencyList = reply;
        });
    };

    $scope.onSchoolChange = function (query) {
        $SchoolFactory.get(query, function (reply) {
            $scope.schoolList = reply;
        })
    };


    $scope.dismiss = function () {
        $uibModalInstance.dismiss();
    };

    $scope.onBtnSaveClicked = function () {
        $scope.isSubmit = true;

        $TeacherFactory.addRank($scope.new, id, function (reply) {
            $scope.isSubmit = false;

            if (reply.error) return Notify.error('Kesalahan', reply.error);
            Notify.success('Sukses', 'Berhasil menambahkan data pangkat');
            $uibModalInstance.close();
        })
    };

    function init() {
        $SchoolFactory.get('', function (reply) {
            $scope.schoolList = reply;
        });

        $RankFactory.get(function (reply) {
            $scope.rankList = reply;
        });

        $DistrictFactory.regency('', function (reply) {
            $scope.regencyList = reply;
        });
    }

    init();

});