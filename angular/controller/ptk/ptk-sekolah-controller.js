// Created by Raga Subekti at 16/10/17
angular.module('diknasmlg').controller('ptkSekolahController', function (
    $scope,
    NgTableParams,
    $q,
    $TeacherFactory,
    $uibModal,
    Notify,
    $routeParams,
    $TimerFactory
) {

    $scope.timerEnd
    $scope.router = $routeParams;
    $scope.detail = {};

    var apiPath = localStorage.getItem('PATHLOC');

    $scope.schoolTable = new NgTableParams({}, {
        getData: function (params) {
            return $q(function (resolve, reject) {
                $TeacherFactory.school($scope.router.id, function (reply) {
                    if (reply.error) return reject(reply.error);
                    return resolve(reply);
                })
            })
        }
    });

    $scope.onBtnAddClicked = function () {
        $uibModal.open({
            templateUrl: apiPath + 'modal/ptk/sekolah/tambah',
            controller: 'tambahSekolahPtk',
            resolve: {
                id: function () {
                    return $scope.router.id
                }
            }
        }).result.then(reloadTable, reloadTable);
    };

    $scope.onBtnDeleteClicked = function (row) {
        Notify.confirm('Apakah anda yakin ingin menghapus data riwayat sekolah <code>' + row.nama_sekolah + '</code>', function () {
            $TeacherFactory.deleteSchool(row.id, function (reply) {
                if (reply.error) {
                    Notify.error('Kesalahan', reply.error);
                } else {
                    Notify.success('Sukses', 'Data sekolah berhasil dihapus');
                }

                $scope.schoolTable.reload();
            })
        }, function () {
        });
    };

    $scope.onBtnEditClicked = function (row) {
        $uibModal.open({
            templateUrl: apiPath + 'modal/ptk/sekolah/edit',
            controller: 'editSekolahPtk',
            resolve: {
                data: function () {
                    return row
                }
            }
        }).result.then(reloadTable, reloadTable);
    };

    $scope.onBtnActivateClicked = function (row) {
        $uibModal.open({
            templateUrl: apiPath + 'teacher/activateschoolmodal',
            controller: 'aktivasiSekolahPtk',
            resolve: {
                data: function () {
                    return row
                }
            }
        }).result.then(reloadTable, reloadTable);
    };

    var reloadTable = function () {
        $scope.schoolTable.reload();
    }


    function init() {
        $TeacherFactory.get($scope.router.id, function (r) {
            $scope.detail = r;
            console.log(r);
        })

        $TimerFactory.diff().then(result => {
            $scope.timerEnd = (!result.admin && result.disabled)
            console.dir(result)
        })
    }
    init();


});