// Created by arsyadsr09 at 12/10/2017
angular.module('diknasmlg').controller('articleDistrictController', function ($scope, NgTableParams, $q, $uibModal, $DistrictFactory) {

    var apiPath = localStorage.getItem('PATHLOC');

    $scope.statsbox = {};

    $scope.onBtnDetailClicked = function(row) {
        $uibModal.open({
            templateUrl: apiPath + 'district/detail',
            controller: 'detailDistrictModal',
            size: 'lg',
            resolve: {
                data: function() {
                    return row;
                }
            }
        })
    };

    $scope.kecamatanTable = new NgTableParams({}, {
        getData: function(params) {
            return $q(function (resolve, reject) {
                $DistrictFactory.schoolondistrict(function (reply) {
                    if (reply.error) return reject(reply.error);
                    return resolve(reply);
                })
            })
        }
    });

    function init() {

    }

    init();

});