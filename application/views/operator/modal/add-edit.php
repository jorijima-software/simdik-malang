<?php
/**
 * Created by PhpStorm.
 * User: ragasubekti
 * Date: 06/12/17
 * Time: 20.18
 */
?>

<div class="modal-header">
    {{type}} Informasi Operator
</div>
<div class="modal-body">
    <form>
        <div class="form-group">
            <label for="">Nama Operator</label>
            <input type="text" class="form-control" ng-model="operator.nama">
        </div>
        <div class="form-group">
            <label for="">No. Handphone</label>
            <div class="input-group">
                <div class="input-group-addon">
                    <i class="fa fa-phone"></i>
                </div>
                <input type="text" class="form-control" ng-model="operator.no_hp">
            </div>
        </div>
        <div class="form-group">
            <label for="">No. Whatsapp</label>
            <div class="input-group">
                <div class="input-group-addon">
                    <i class="fa fa-whatsapp"></i>
                </div>
                <input type="text" class="form-control" ng-model="operator.no_wa">
            </div>
        </div>
        <div class="form-group">
            <label for="">Unit Kerja</label>
            <ui-select ng-model="operator.unit_kerja" theme="bootstrap">
                <ui-select-match placeholder="Pilih asal operator">{{$select.selected.nama}}
                </ui-select-match>
                <ui-select-choices repeat="item in schoolList track by $index"
                                   refresh="onSchoolChange($select.search)" refresh-delay="0">
                    {{item.nama}}
                </ui-select-choices>
            </ui-select>
        </div>
    </form>
</div>
<div class="modal-footer">
    <button class="btn btn-sm btn-primary" ng-click="onBtnSaveClicked()">
        <i class="fa fa-check"></i> <span>Simpan</span>
    </button>
    <button class="btn btn-sm btn-default" ng-click="dismiss()">
        <i class="fa fa-times"></i> <span>Batal</span>
    </button>
</div>

