// Created by Raga Subekti at 08/10/17
angular.module('diknasmlg').controller('detailSekolah', function (data, $scope,$uibModal, $uibModalInstance, NgTableParams,$EmployeeFactory, $SchoolFactory, $q) {

    var apiPath = localStorage.getItem('PATHLOC');
    $scope.detail = data;

    $scope.status = {
        0: 'Swasta',
        1: 'Negeri'
    };

    $scope.teacherTable = new NgTableParams({}, {
        getData: function (params) {
            var count = params.count();
            var start = count * (params.page() - 1);
            var query = $scope.detail.id;
            return $q(function (resolve, reject) {
                $SchoolFactory.showteacherschool(start, count, query, function (reply) {
                    if (reply.error) return reject(reply.error);
                    params.total(reply.total);
                    return resolve(reply.data);
                })
            })
        }
    });

    $scope.onBtnDetailEmployeeClicked = function(row) {
        $uibModal.open({
            templateUrl: apiPath + 'Teacher/detail',
            controller: 'detailTeacherModal',
            size: 'lg',
            resolve: {
                data: function() {
                    return row;
                }
            }
        })
    };
    $scope.dismiss = function () {
        $uibModalInstance.dismiss();
    };
    function init() {
    }

    init();

});