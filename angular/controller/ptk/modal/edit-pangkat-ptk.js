// Created by Raga Subekti at 21/10/17
angular.module('diknasmlg').controller('editPangkatPtk', function($scope, $uibModalInstance, $RankFactory, data, $SchoolFactory, $TeacherFactory, Notify, $DistrictFactory) {

    $scope.isSubmit = false;

    // console.log(data)

    $scope.rankList = [];
    $scope.schoolList = [];

    $scope.regencyList = [];

    var tmtdate = data.tmt.split('-');
    tmtdate = new Date(tmtdate[0], tmtdate[1]-1, tmtdate[2]);


    $scope.areaList = [
        {id: 0, nama: 'Dalam Malang'},
        {id: 1, nama: 'Luar Malang'}
    ];

    $scope.edit = {
        sk: data.no_sk,
        golongan: {
            id: data.id_jenis_golongan,
            gol: data.gol,
            ruang: data.ruang
        },
        tmt: tmtdate,
        unit_kerja: data.unit_kerja,
        type: data.type,
        sekolah: {
            id: data.id_sekolah,
            nama: data.nama_sekolah
        },
        id: data.id,
        area: {
            id: data.area,
            nama: (data.area == 1) ? 'Luar Malang' : 'Dalam Malang'
        },
        kota: {
            id: data.id_kota,
            name: data.nama_kota
        }
    };

    $scope.dismiss = function() {
        $uibModalInstance.dismiss();
    };



    $scope.onBtnSaveClicked = function() {
        $scope.isSubmit = true;

        $TeacherFactory.updateRank($scope.edit, function(reply) {
            $scope.isSubmit = false;

            if(reply.error) {
                Notify.error('Kesalahan', reply.error);
            } else {
                Notify.success('Sukses', 'Berhasil mengubah data pangkat guru');
                $uibModalInstance.close();
            }
        })
    };



    $scope.onRegencyChange = function(query) {
        $DistrictFactory.regency(query, function (reply) {
            $scope.regencyList = reply;
        });
    };

    $scope.onSchoolChange = function(query) {
        $SchoolFactory.get(query, function(reply) {
            $scope.schoolList = reply;
        })
    };

    function init() {
        $SchoolFactory.get('', function(reply) {
            $scope.schoolList = reply;
        });

        $RankFactory.get(function(reply) {
            $scope.rankList = reply;
        });


        $DistrictFactory.regency('', function (reply) {
            $scope.regencyList = reply;
        });
    }

    init();


});