angular.module('diknasmlg').controller('articleEmployeeController', function ($scope, $uibModal, NgTableParams, $q, $PtkFactory, $TeacherFactory, $EmployeeFactory) {

    var apiPath = localStorage.getItem('PATHLOC');

    $scope.search = '';
    $scope.edit = {};

    $scope.bgIndex = [
        'bg-aqua',
        'bg-yellow',
        'bg-green',
        'bg-orange',
        'bg-red'
    ];

    $scope.statsbox = {};

    $scope.pegawaiList = [];
    $scope.employeeList = [];

    $scope.filter = {
        nuptk: 0,
        pns: 0,
        pegawai: {}
    };

    $scope.sertifikasi = {
        1:'Lulus Sertifikasi',
        2:'Belum Sertifikasi'
    };

    $scope.onBtnFilterNuptk = function (val) {
        $scope.filter.nuptk = val;
        $scope.teacherTable.reload();
    };

    $scope.onBtnFilterPns = function (val) {
        $scope.filter.pns = val;
        $scope.teacherTable.reload();
    };

    $scope.onFilterPegawaiChange = function() {
        $scope.teacherTable.reload();
    };

    $scope.onBtnDetailClicked = function (row) {
        $uibModal.open({
            templateUrl: apiPath + 'modal/ptk/detail',
            controller: 'detailPtk',
            size: 'lg',
            resolve: {
                data: function () {
                    return row;
                }
            }
        });
    };

    $scope.onInputSearchChange = function () {
        $scope.teacherTable.reload();
    };

    $scope.teacherTable = new NgTableParams({}, {
        getData: function (params) {
            var count = params.count();
            var start = count * (params.page() - 1);
            var search = $scope.search;
            var filter = $scope.filter;
            return $q(function (resolve, reject) {
                $PtkFactory.show(start, count, search, filter).then(function (reply) {
                    if (reply.error) return reject(reply.error);
                    params.total(reply.total);
                    return resolve(reply.data);
                });
            });
        }
    });

    function reloadStats() {
        $PtkFactory.stats().then(function (r) {
            $scope.statsbox = r;
        });
    }

    function init() {
        $PtkFactory.stats().then(function (r) {
            $scope.statsbox = r;
        });
        $EmployeeFactory.get(function (reply) {
            $scope.employeeList = reply;
            $scope.employeeList.unshift({id: '0', nama: 'Semua'});
        });
    }

    init();

});