angular.module('diknasmlg').controller('ModalPtkRekeningDetail', function(
    $scope,
    $uibModalInstance,
    $q,
    $BankFactory,
    $PtkRekeningFactory,
    Notify,
    ptkData
) {

    $scope.error = false

    $scope.bankList = []
    $scope.rekening =  {}

    /**
     * Dismiss the current modal
     */
    $scope.dismiss = function() {
        $uibModalInstance.dismiss()
    }

    /**
     * Store the current data to the database
     */
    $scope.onBtnSaveClicked = function() {
        var data = $scope.rekening
        $PtkRekeningFactory.store(data).then(function(result) {
            if(result && !result.error) {
                Notify.success('Sukses', 'Berhasil mengubah informasi rekening PTK')
                $uibModalInstance.close()
            } else {
                Notify.error('Kesalahan', result.error)
            }
        })
    }

    function init() {
        $scope.rekening.dataPtk = ptkData

        $PtkRekeningFactory.get(ptkData.id).then(function(result) {
            if(result && result.success && !result.error) {
                $scope.rekening.nama = result.data.nama
                $scope.rekening.jenisBank = result.data.jenisBank
                $scope.rekening.cabang = result.data.cabang
                $scope.rekening.noRekening = result.data.noRekening
            } else {
                $scope.rekening.nama = ptkData.nama.toUpperCase()
            }
        })

        $BankFactory.get().then(function(result) {
            if(result && !result.error) {
                $scope.bankList = result
            } else {
                $scope.error = result.error
            }
        })
    }

    init()

})