// Created by Raga Subekti at 08/10/17
angular.module('diknasmlg').controller('detailTeacherModal', function(data, $scope, $uibModalInstance, $TeacherFactory) {

    $scope.detail = {};

    $scope.pola = {
        0: 'Portofolio',
        1: 'PLPG',
        2: 'PPG'
    };

    $scope.dismiss = function() {
        $uibModalInstance.dismiss();
    };

    function init() {
        $TeacherFactory.get(data.id, function (reply) {
            $scope.detail = reply;
        });
    }

    init()

});