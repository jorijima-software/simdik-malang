<?php

/**
 * Created by PhpStorm.
 * User: ragasubekti
 * Date: 1/9/18
 * Time: 5:22 PM
 */
class DataGuru extends \Illuminate\Database\Eloquent\Model
{
    protected $table = 'tugas_guru';

    public function jamMengajar()
    {
        $CI =& get_instance();
        $CI->load->model('TugasMapel');
        $CI->load->model('Semester');
//        var_dump()
        $db = $this
            ->hasMany('TugasMapel', 'id_tugas_guru', 'id_data_guru')
            ->selectRaw('SUM(jumlah_jam) jumlahJam')
            ->where('semester', Semester::where('active', 1)->first()->id)
            ;
//        var_dump($db->toSql());
        return $db;
    }

    public function tugasMengajar()
    {
        $CI =& get_instance();
        $CI->load->model('TugasMapel');
        return $this->hasOne('TugasMapel', 'id_tugas_guru', 'id_data_guru');
    }

    public function ptk()
    {
        $CI =& get_instance();
        $CI->load->model('Ptk');
        return $this->hasOne('Ptk', 'id', 'id_data_guru');
    }
}