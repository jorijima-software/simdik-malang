var app = angular.module('diknasmlg', [
    'ngTable',
    'ui.bootstrap',
    'ui.select',
    'ngSanitize',
    'ng-currency',
    'summernote',
    'ngRoute',
    'ngAnimate',
    'angular-loading-bar',
    'chart.js',
    'angularFileUpload'
]);


function init() {
    if (!localStorage.getItem('PATHLOC')) {
        var fullPath = '/';
        localStorage.setItem('PATHLOC', fullPath);
    }

    if (!localStorage.getItem('LARAPATH')) {
        localStorage.setItem('LARAPATH', '//simdik.diknas.malangkota.go.id:3000/')
    }
}

init();

var apiPath = localStorage.getItem('PATHLOC');

app.run(function ($rootScope, $location) {

    $rootScope.$on('$routeChangeSuccess', function () {
        console.log($location.path());

        // Safely instantiate dataLayer
        var dataLayer = window.dataLayer = window.dataLayer || [];
        dataLayer.push({
            event: 'ngRouteChange',
            attributes: {
                route: $location.path()
            }
        });
    });
});

app.filter('fromNow', function () {
    return function (date) {
        return moment(date).fromNow();
    }
});

app.directive("ngRandomClass", function () {
    return {
        restrict: 'EA',
        replace: false,
        scope: {
            ngClasses: "="
        },
        link: function (scope, elem, attr) {
            elem.addClass(scope.ngClasses[Math.floor(Math.random() * (scope.ngClasses.length))]);
        }
    }
});

app.directive('countdown', [
    'Util', '$interval', '$uibModal', '$TimerFactory', '$q', function (Util, $interval, $uibModal, $TimerFactory, $q) {
        return {
            restrict: 'A',
            scope: {
                date: '@'
            },
            link: function (scope, element) {
                // $interval(function () {
                $TimerFactory.countdown().then(function (result) {
                    if (result.time) {
                        var future
                        future = new Date(result.time)
                        var thediff = Math.floor((future.getTime() - new Date().getTime()) / 1000)

                        if (thediff <= 0 && !result.admin) {
                            $uibModal.open({
                                templateUrl: apiPath + 'timer/timerendmodal',
                                controller: function ($scope, $uibModalInstance) {
                                    $scope.message = result.message
                                    $scope.dismiss = function () {
                                        $uibModalInstance.dismiss()
                                    }
                                }
                            })
                        }

                        $interval(function () {
                            var diff;
                            diff = Math.floor((future.getTime() - new Date().getTime()) / 1000);
                            if (diff > 0) {
                                return element.text(Util.dhms(diff));
                            } else {
                                return "00 hari 00:00:00"
                            }
                        }, 1000)
                    } else {
                        return "00 hari 00:00:00"
                    }
                })
                // }, 10000)
            }
        };
    }
]).factory('Util', [
    function () {
        return {
            dhms: function (t) {
                var days, hours, minutes, seconds;
                days = Math.floor(t / 86400);
                days = days < 10 ? ('0') + days : days;
                t -= days * 86400;
                hours = Math.floor(t / 3600) % 24;
                hours = hours < 10 ? ('0') + hours : hours;
                t -= hours * 3600;
                minutes = Math.floor(t / 60) % 60;
                minutes = minutes < 10 ? ('0') + minutes : minutes;
                t -= minutes * 60;
                seconds = t % 60;
                seconds = seconds < 10 ? ('0') + seconds : seconds;
                return [days + ' hari ', hours + ':', minutes + ':', seconds].join('');
            }
        };
    }
]);


app.filter('breakTrue', function () {
    return function (input) {
        var result = []
        for (var i = 0; i < input.length; i++) {
            if (input[i] == true) {
                result = [true]
            }
        }
        return result;
    };
});