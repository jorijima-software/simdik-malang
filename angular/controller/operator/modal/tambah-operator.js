/*
    Created by Raga Subekti
    at  06/12/17 20.33 by PhpStorm
*/
angular.module('diknasmlg').controller('tambahOperator', function ($scope, $uibModalInstance, $OperatorFactory, $SchoolFactory, Notify, edit) {

    $scope.schoolList = [];

    if (!edit) {
        $scope.type = 'Tambah';
        $scope.operator = {
            nama: '',
            no_hp: '',
            no_wa: '',
            unit_kerja: {}
        }
    } else {
        $scope.type = 'Edit';

        $scope.operator = {
            id: edit.id,
            nama: edit.nama_operator,
            no_hp: edit.no_hp,
            no_wa: edit.no_wa,
            unit_kerja: {
                id: edit.asal_sekolah,
                nama: edit.nama_sekolah
            }
        }
    }

    $scope.onBtnSaveClicked = function () {
        if (!edit) {
            $OperatorFactory.inserts($scope.operator).then(function (r) {
                if (r.error) {
                    Notify.error('Kesalahan', r.error);
                } else {
                    Notify.success('Sukses', 'Sukses menambahkan data operator');
                    $uibModalInstance.close();
                }
            })
        } else {
            $OperatorFactory.updates($scope.operator).then(function (r) {
                if (r.error) {
                    Notify.error('Kesalahan', r.error);
                } else {
                    Notify.success('Sukses', 'Sukses mengubah data operator');
                    $uibModalInstance.close();
                }
            })
        }
    };

    $scope.onSchoolChange = function (search) {
        $SchoolFactory.get(search, function (r) {
            $scope.schoolList = r;
        })
    };

    $scope.dismiss = function () {
        $uibModalInstance.dismiss();
    };


    function init() {
        $SchoolFactory.get('', function (reply) {
            $scope.schoolList = reply;
        });
    }

    init();

});