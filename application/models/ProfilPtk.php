<?php
class ProfilPtk extends \Illuminate\Database\Eloquent\Model
{
    protected $table = 'profil_guru';
    protected $guarded = [];
    public $timestamps = false;

    public function agama() {
        $CI =& get_instance();
        $CI->load->model('Agama');

        return $this->hasOne('Agama', 'id', 'id_jenis_agama');
    }

}