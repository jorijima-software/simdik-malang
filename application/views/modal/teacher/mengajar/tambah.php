<?php
/**
 * Created by PhpStorm.
 * User: ragasubekti
 * Date: 23/11/17
 * Time: 13.28
 */

?>

<div class="modal-header">
    <div class="modal-title">Tambah Data Guru</div>
</div>

<div class="modal-body">
    <form action="">
        <div class="form-group">
            <label for="">Nama</label>
            <ui-select ng-model="new.guru" theme="bootstrap">
                <ui-select-match placeholder="Pilih salah satu PTK">{{$select.selected.nama_lengkap}}
                </ui-select-match>
                <ui-select-choices repeat="item in teacherList" refresh="onInputSearchChange($select.search)"
                                   refresh-delay="0">
                    {{item.nama_lengkap}}
                </ui-select-choices>
            </ui-select>
        </div>
        <div class="form-group">
            <table class="table table-striped">
                <thead>
                <tr>
                    <th colspan="2">Detail PTK</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <th>NUPTK</th>
                    <td>{{new.guru.nuptk}}</td>
                </tr>
                <tr>
                    <th>Nama</th>
                    <td>{{new.guru.nama_lengkap}}</td>
                </tr>
                <tr>
                    <th>Jenis Pegawai</th>
                    <td>{{new.guru.jenis_pegawai}}</td>
                </tr>
                </tbody>
            </table>
        </div>
    </form>
</div>

<div class="modal-footer">
    <button class="btn btn-primary btn-sm" ng-click="onBtnSaveClicked()">
        <i class="fa fa-check"></i> <span>Simpan</span>
    </button>
    <button class="btn btn-default btn-sm" ng-click="dismiss()">
        <i class="fa fa-times"></i> <span>Batal</span>
    </button>
</div>