/**
 * Copyright 2018, Raga Subekti (ragasubekti@outlook.com)
 * All Code written below is not for reuse or copied
 * All rights go to Raga Subekti
 */

'use strict';

angular.module('diknasmlg').controller('StatistikDataMasuk', function(
    $scope,
    $StatistikFactory,
    $DistrictFactory,
    $SchoolFactory,
    NgTableParams,
    $q
)
{
    $scope.statusSekolah = [
        {id: 0, nama: 'Swasta'},
        {id: 1, nama: 'Negeri'}
    ]
    $scope.kecamatanList = []
    $scope.educationList = []

    $scope.filter = {}

    $scope.DataMasukTable = new NgTableParams({}, {
        getData: function(params) {
            let count = params.count()
            let start = count * (params.page() - 1)

            let filter = $scope.filter
                filter.start = start
                filter.count = count

            return $q(function(resolve, reject) {
                $StatistikFactory.dataMasuk(filter).then(function(result) {
                    if(result && !result.error) {
                        let data = result.data
                        params.total(result.total)
                        data = _.orderBy(data,['persentase', 'asc'])
                        return resolve(data)
                    } else {
                        return reject(result.error)
                    }
                })
            })
        }
    })

    $scope.onFilter = () => {
        $scope.DataMasukTable.reload()
    }

    function init() {
        $DistrictFactory.get(function (reply) {
            $scope.kecamatanList = reply
            $scope.kecamatanList.unshift({id: 0, nama: 'Semua'})
        })

        $SchoolFactory.type(function (reply) {
            $scope.educationList = reply
            $scope.educationList.unshift({id: 0, nama: 'Semua'})
        })

    }

    init();
})