angular.module('diknasmlg').controller('editRowRombel', function($scope, $uibModalInstance, $RombelFactory, data, Notify) {

    $scope.rombel = data;
    $scope.error = null;

    $scope.dismiss = function() {
        $uibModalInstance.dismiss();
    };

    $scope.onBtnSaveClicked = function() {
        $scope.error = null;

        if(!$scope.rombel.nama.length > 0) {
            $scope.error = 'Nama Rombel wajib diisi!';
        } else if(isNaN($scope.rombel.laki) || isNaN($scope.rombel.perempuan)) {
            $scope.error = 'Mohon masukkan angka pada jumlah siswa (laki/perempuan)'
        } else {
            var rombel = $scope.rombel;
            $RombelFactory.updateRow(rombel).then(function(r) {
               if(!r.error) {
                   Notify.success('Sukses', 'Berhasil mengubah data rombel!');
                   $uibModalInstance.close();
               }  else {
                   $scope.error = r.error;
               }
            });
        }
    };

});