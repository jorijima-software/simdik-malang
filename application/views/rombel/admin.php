<?php
/**
 * Created by PhpStorm.
 * User: ragasubekti
 * Date: 15/12/17
 * Time: 22.55
 */
$tapel_aktif = $this->User_model->current_tapel();

?>

<section class="content-header" style="margin-bottom: 60px">
    <div class="pull-right" style="">
        <div class="small-box bg-blue" style="padding: 5px">
            <div class="text-center">Tahun Pelajaran Aktif</div>
            <h3 style="margin: 0; padding: 0"><?= $tapel_aktif ?></h3>
        </div>
    </div>
    <h1 style="vertical-align: middle">Data Rombel</h1>
</section>
<section class="content">
    <div class="row hidden-sm hidden-xs">
        <div class="col-lg-3 col-xs-6">
            <div class="small-box bg-yellow">
                <div class="inner">
                    <h3>{{statsbox.total_siswa}}</h3>

                    <p>Jumlah Siswa</p>
                </div>
                <div class="icon">
                    <i class="fa fa-users"></i>
                </div>
            </div>
        </div>
        <div class="col-lg-3 col-xs-6">
            <div class="small-box bg-aqua">
                <div class="inner">
                    <h3>{{statsbox.laki}}</h3>

                    <p>Jumlah Laki-Laki</p>
                </div>
                <div class="icon">
                    <i class="fa fa-male"></i>
                </div>
            </div>
        </div>
        <div class="col-lg-3 col-xs-6">
            <div class="small-box bg-green">
                <div class="inner">
                    <h3>{{statsbox.perempuan}}</h3>
                    <p>Jumlah Perempuan</p>
                </div>

                <div class="icon">
                    <i class="fa fa-female"></i>
                </div>
            </div>
        </div>
        <div class="col-lg-3 col-xs-6">
            <div class="small-box bg-red">
                <div class="inner">
                    <h3>{{statsbox.rombel}}</h3>
                    <p>Jumlah Rombel</p>
                </div>

                <div class="icon">
                    <i class="fa fa-university"></i>
                </div>
            </div>
        </div>
    </div>
    <div class="box box-primary">
        <div class="box-header">
            <div class="box-title">Daftar Rombel Sekolah</div>
        </div>
        <div class="box-body">
            <div class="row">
                <div class="col-md-9"></div>
                <div class="col-md-3">
                    <div class="input-group">
                        <input type="text" class="form-control" placeholder="Pencarian" ng-model="search" ng-keyup="onSearchChange()">
                        <div class="input-group-addon">
                            <i class="fa fa-search"></i>
                        </div>
                    </div>
                </div>
            </div>
            <table class="table table-condensed table-striped" ng-table="schoolTable">
                <thead>
                <tr>
                    <th>No</th>
                    <th>Nama Sekolah</th>
                    <th>Kecamatan</th>
                    <th>Jumlah Rombel</th>
                    <th>Laki</th>
                    <th>Perempuan</th>
                    <th>Total</th>
                    <th></th>
                </tr>
                </thead>
                <tbody>
                <tr ng-repeat="row in $data">
                    <td>{{($index + 1) + (schoolTable._params.count * (schoolTable._params.page -1))}}
                    <td>{{row.nama}}</td>
                    <td>{{row.nama_kecamatan}}</td>
                    <td>
                        <a class="btn btn-xs btn-primary" href="<?= site_url('') ?>#!/rombel/detail/{{row.id}}">
                            <i class="fa fa-eye"></i>
                        </a>
                        {{row.rombel}}
                    </td>
                    <td>{{row.total_laki}}</td>
                    <td>{{row.total_perempuan}}</td>
                    <td>{{row.total}}</td>
                    <td>
                    </td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>
</section>

