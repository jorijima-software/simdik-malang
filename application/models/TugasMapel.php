<?php
/**
 * Created by PhpStorm.
 * User: ragasubekti
 * Date: 1/9/18
 * Time: 8:43 PM
 */
class TugasMapel extends \Illuminate\Database\Eloquent\Model {
    protected $table = 'data_tugas_mapel';

    public function tugasMengajar() {
        $CI =& get_instance();
        $CI->load->model('TugasMengajar');
        return $this->hasOne('TugasMengajar', 'id', 'id_tugas_mengajar');
    }

    public function ptk() {
        $CI =& get_instance();
        $CI->load->model('Ptk');

        return $this->hasOne('Ptk', 'id', 'id_tugas_guru');
    }
}