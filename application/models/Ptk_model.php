<?php

class Ptk_model extends CI_Model
{

    public function detail($id)
    {
        $this->db->select('data_guru.*, jenis_pegawai.nama as jenis_pegawai, data_sekolah.nama as nama_sekolah');
        $this->db->from('data_guru');
        $this->db->join('jenis_pegawai', ' jenis_pegawai.id = data_guru.id_jenis_pegawai', 'left');
        $this->db->join('data_sekolah', 'data_sekolah.id = data_guru.id_sekolah', 'left');

        $this->db->where('data_guru.id', $id);

        $count = clone $this->db;

        if ($count->count_all_results() > 0) {

            $data = $this->db->get()->row();


            $this->db->select('COUNT(*) as data_sekolah');
            $this->db->from('sekolah_guru');
            $this->db->where('id_data_guru', $data->id);

            $data_sekolah = $this->db->get()->row()->data_sekolah;
            $data->data_sekolah = $data_sekolah;

            $this->db->select('COUNT(*) as total_gaji');
            $this->db->from('gaji_berkala_guru');
            $this->db->where('nip', $data->nip_baru);

            $result = $this->db->get()->row();
            $data->gaji_berkala = $result->total_gaji;

            $this->db->select('COUNT(*) as total_jg');
            $this->db->from('pangkat_guru');
            $this->db->where('nip', $data->nip_baru);

            $result = $this->db->get()->row();
            $data->pangkat_guru = $result->total_jg;

            $this->db->select('profil_guru.*, jenis_agama.nama as nama_agama');
            $this->db->from('profil_guru');
            $this->db->join('jenis_agama', 'id_jenis_agama = jenis_agama.id', 'left');

            $this->db->where('id_data_guru', $id);

            $data->profil = $this->db->get()->row();
            $data->rumah = $this->db->get_where('rumah_guru', array('id_data_guru' => $id))->row();
            $data->sertifikasi = $this->db->get_where('sertifikasi_guru', array('id_data_guru' => $id))->row();
            if ($data->sertifikasi) $data->sertifikasi->bidang = $this->db->get_where('jenis_bidang_studi', ['id' => isset($data->sertifikasi->id_bidang_studi) ? $data->sertifikasi->id_bidang_studi : null])->row();
            $status_sertifikasi = $this->db->get_where('sertifikasi_guru', array('id_data_guru' => $id));
            $data->status_sertifikasi = ($status_sertifikasi->num_rows() > 0 && strlen($status_sertifikasi->row()->no_peserta > 0)) ? true : false;

            return $data;

        } else {
            return array();
        }
    }

    public function insert($data, $profil, $rumah, $sert)
    {

        if ($this->session->as > 0) {
            $fq = $this->db->get_where('user', array('id' => $this->session->uid))->row();
            $sq = $this->db->get_where('data_sekolah', array('npsn' => $fq->id_npsn))->row();
        }

        if(strlen($data->nama) < 4) {
            return ['error' => 'Panjang nama minimal 4 karakter!'];
        }

        $this->db->from('data_guru');
        $this->db->where('nuptk', $data->nuptk);
        $this->db->where('deleted', '0');

        if ($this->db->get()->num_rows() > 0 && strlen($data->nuptk) > 2) {
            return array('error' => 'Terdapat data yang sama dalam sistem');
        }

        $this->log('Tambah PTK ' . $data->nama, 1);
        // 3,4,5,6

        $pns_arr = [3, 4, 5, 6];
        $transaction = array();

        $pns = false;

        if (isset($data->jenis_pegawai) && !is_null($data->jenis_pegawai)) {
            foreach ($pns_arr as $key => $row) {
                if ($data->jenis_pegawai == $row) {
                    $pns = true;
                }
            }
        }

        $this->log('Tambah PTK ' . $data->nama, 1);

        $data = array(
            'nuptk' => isset($data->nuptk) ? $data->nuptk : '',
            'nama_lengkap' => $data->nama,
            'gelar_depan' => isset($data->gelar->depan) ? $data->gelar->depan : '',
            'gelar_belakang' => isset($data->gelar->belakang) ? $data->gelar->belakang : '',
            'nip_lama' => isset($data->nip->lama) ? $data->nip->lama : '',
            'nip_baru' => isset($data->nip->baru) ? $data->nip->baru : '',
            'id_jenis_pegawai' => isset($data->jenis_pegawai) ? $data->jenis_pegawai : null,
            'id_sekolah' => ($this->session->as == 0 ? (isset($data->sekolah) ? $data->sekolah : null) : $sq->id),
            'nik' => isset($data->nik) ? $data->nik : '',
            'nama_ibu' => isset($data->nama_ibu) ? $data->nama_ibu : '',
            'status_pns' => $pns
        );


        $this->db->insert('data_guru', $data);
        array_push($transaction, $this->db->insert_id());
        if ($this->db->affected_rows() <= 0) {
            return array('error' => $this->db->error());
        }

        if (isset($profil->tempat_lahir)) {

            $profil = array(
                'tempat_lahir' => $profil->tempat_lahir,
                'tgl_lahir' => Carbon\Carbon::parse($profil->tanggal_lahir, 'Asia/Jakarta')->addDays(1)->format('Y-m-d'),
                'id_jenis_agama' => isset($profil->agama->id) ? $profil->agama->id : null,
                'id_data_guru' => $transaction[0]
            );

            $this->db->insert('profil_guru', $profil);

            array_push($transaction, $this->db->insert_id());
            if ($this->db->affected_rows() <= 0) {
                $this->db->delete('data_guru', array('id' => $transaction[0]));
                return array('error' => $this->db->error());
            }
        }

        if (isset($rumah->kota)) {

            $rumah = array(
                'id_data_guru' => $transaction[0],
                'kota' => isset($rumah->kota->name) ? $rumah->kota->name : '',
                'kec' => isset($rumah->kecamatan->name) ? $rumah->kecamatan->name : '',
                'nohp' => $rumah->no_telp,
                'alamat_rumah' => $rumah->alamat
            );

            $this->db->insert('rumah_guru', $rumah);

            array_push($transaction, $this->db->insert_id());

            if ($this->db->affected_rows() <= 0) {
                $this->db->delete('data_guru', array('id' => $transaction[0]));
                $this->db->delete('profil_guru', array('id' => $transaction[1]));
                return array('error' => $this->db->error());
            }
        }

        $sertifikat = array(
            'id_data_guru' => $transaction[0],
            'pola_sertifikasi' => isset($sert->pola) ? $sert->pola : '',
            'no_peserta' => isset($sert->no_sert) ? $sert->no_sert : '',
            'tahun_sertifikasi' => isset($sert->tahun) ? $sert->tahun : '',
            'id_bidang_studi' => isset($sert->bidang) ? $sert->bidang : null
        );

        $this->db->insert('sertifikasi_guru', $sertifikat);


        return array('success' => true, 'data' => $data);
    }

    public function update($basic, $profile, $rumah, $sert)
    {

        if(strlen($basic->nama) < 4) {
            return ['error' => 'Minimal panjang nama adalah 4 karakter!'];
        }

        $sekolah = $this->db->get_where('data_sekolah', ['npsn' => $this->session->npsn])->row();

        $pns_arr = [3, 4, 5, 6];

        $pns = false;

        if (isset($basic->jenis_pegawai) && !is_null($basic->jenis_pegawai)) {
            foreach ($pns_arr as $key => $row) {
                if ($basic->jenis_pegawai == $row) {
                    $pns = true;
                }
            }
        }

        $data = array(
            'nuptk' => $basic->nuptk,
            'nama_lengkap' => $basic->nama,
            'gelar_depan' => $basic->gelar->depan,
            'gelar_belakang' => $basic->gelar->belakang,
            'nip_lama' => $basic->nip->lama,
            'nip_baru' => $basic->nip->baru,
            'id_jenis_pegawai' => $basic->jenis_pegawai,
            'id_sekolah' => ($this->session->as > 0) ? $sekolah->id : $basic->sekolah,
            'nik' => isset($basic->nik) ? $basic->nik : '',
            'nama_ibu' => isset($basic->nama_ibu) ? $basic->nama_ibu : '',
            'status_pns' => $pns,
            'deleted' => 0
        );

        $this->db->where('id', $basic->id);
        $this->db->update('data_guru', $data);

        $this->log('Update PTK ' . $basic->nama, 2);


        if ($this->db->error()['code'] > 0) {
            return array('error' => $this->db->error()['message']);
        }

        if (isset($profile->tempat_lahir) || isset($profile->tanggal_lahir) || isset($profile->agama->id)) {

            $fq = $this->db->get_where('profil_guru', array('id_data_guru' => $basic->id));

            if ($fq->num_rows() > 0) {

                $dt = $fq->row();

                $this->db->where('id_data_guru', $basic->id);
                $this->db->update('profil_guru', array(
                    'tempat_lahir' => isset($profile->tempat_lahir) ? $profile->tempat_lahir : null,
                    'tgl_lahir' => isset($profile->tanggal_lahir) 
                        ? Carbon\Carbon::parse($profile->tanggal_lahir, 'Asia/Jakarta')->addDays(1)->format('Y-m-d')
                        : ($dt->tgl_lahir ? $dt->tgl_lahir : date_format(date_create(), 'Y-m-d')),
                    'id_jenis_agama' => isset($profile->agama->id) ? $profile->agama->id : null
                ));


                if ($this->db->error()['code'] > 0) {
                    return array('error' => $this->db->error()['message']);
                }

            } else {

                $pd = array(
                    'id_data_guru' => $basic->id,
                    'tempat_lahir' => isset($profile->tempat_lahir) ? $profile->tempat_lahir : null,
                    'tgl_lahir' => isset($profile->tanggal_lahir) ? Carbon\Carbon::parse($profile->tanggal_lahir, 'Asia/Jakarta')->addDays(1)->format('Y-m-d') : Carbon\Carbon::format('Y-m-d'),
                    'id_jenis_agama' => isset($profile->agama->id) ? $profile->agama->id : null
                );

                $this->db->insert('profil_guru', $pd);

                if ($this->db->error()['code'] > 0) {
                    return array('error' => $this->db->error()['message']);
                }

            }
        }

        if (isset($rumah->kota->name) || isset($rumah->kecamatan->name) || isset($rumah->no_telp) || isset($rumah->alamat)) {
            $rt = $this->db->get_where('rumah_guru', array('id_data_guru' => $basic->id));

            if ($rt->num_rows() > 0) {
                $rd = $rt->row();

                $ru = array(
                    'kota' => isset($rumah->kota->name) ? $rumah->kota->name : null,
                    'kec' => isset($rumah->kecamatan->name) ? $rumah->kecamatan->name : null,
                    'nohp' => isset($rumah->no_telp) ? $rumah->no_telp : null,
                    'alamat_rumah' => isset($rumah->alamat) ? $rumah->alamat : null
                );

                $this->db->where('id', $rd->id);
                $this->db->update('rumah_guru', $ru);


                if ($this->db->error()['code'] > 0) {
                    return array('error' => $this->db->error()['message']);
                }

            } else {
                $nd = array(
                    'id_data_guru' => $basic->id,
                    'kota' => isset($rumah->kota->name) ? $rumah->kota->name : null,
                    'kec' => isset($rumah->kecamatan->name) ? $rumah->kecamatan->name : null,
                    'nohp' => isset($rumah->no_telp) ? $rumah->no_telp : null,
                    'alamat_rumah' => isset($rumah->alamat) ? $rumah->alamat : null
                );

                $this->db->insert('rumah_guru', $nd);


                if ($this->db->error()['code'] > 0) {
                    return array('error' => $this->db->error()['message']);
                }

            }
        }

        $sf = $this->db->get_where('sertifikasi_guru', array('id_data_guru' => $basic->id));

        if ($sf->num_rows() > 0) {
            $cr = $sf->row();

            $sertifikat = array(
                'pola_sertifikasi' => isset($sert->pola) ? $sert->pola : '',
                'no_peserta' => isset($sert->no_sert) ? $sert->no_sert : '',
                'tahun_sertifikasi' => isset($sert->tahun) ? $sert->tahun : '',
                'id_bidang_studi' => isset($sert->bidang) ? $sert->bidang : null
            );

            $this->db->where('id', $cr->id);
            $this->db->update('sertifikasi_guru', $sertifikat);


            if ($this->db->affected_rows() > 0 || $this->db->error()['code'] <= 0) {
                return array('success' => true, 'profile' => $profile);
            } else {
                return array('error' => $this->db->error()['message']);
            }

        } else {
            $sertifikat = array(
                'id_data_guru' => $basic->id,
                'pola_sertifikasi' => isset($sert->pola) ? $sert->pola : '',
                'no_peserta' => isset($sert->no_sert) ? $sert->no_sert : '',
                'tahun_sertifikasi' => isset($sert->tahun) ? $sert->tahun : '',
                'id_bidang_studi' => isset($sert->bidang) ? $sert->bidang : null
            );

            $this->db->insert('sertifikasi_guru', $sertifikat);


            if ($this->db->affected_rows() > 0 || $this->db->error()['code'] <= 0) {
                return array('success' => true, 'profile' => $profile);
            } else {
                return array('error' => $this->db->error()['message']);
            }
        }
    }

    public function delete($id)
    {
//        if ($this->session->as > 0) return ['error' => 'Fitur in untuk sementara dimatikan'];

        $q = $this->db->get_where('data_guru', ['id' => $id]);

        if ($q->num_rows() > 0) {
            $this->log('Hapus PTK ' . $q->row()->nama_lengkap, 0);
            $this->db->delete('data_guru', array('id' => $id));
            return ['success' => true];
        } else {
            return ['error' => 'Data tidak ditemukan'];
        }


    }

    public function show($start, $count, $query, $filter)
    {
//        return $this->admin_show($start, $count, $query, $filter);
        if(!isset($_SESSION['as'])) return ['error' => 'You don\'t have access to this page'];
        if ($this->session->as == 0) {
            return $this->admin_show($start, $count, $query, $filter);
        } else if ($this->session->as == 1) {
            return $this->operator_show($start, $count, $query, $filter);
        } else {
            return ['error' => 'Unknown Error'];
        }
    }

    private function operator_show($start, $count, $query, $filter)
    {

        $fq = $this->db->get_where('user', array('id' => $this->session->userdata('uid')))->row();
        $sq = $this->db->get_where('data_sekolah', array('npsn' => $fq->id_npsn))->row();

        $this->db->select('
            data_guru.*, 
            jenis_pegawai.nama as jenis_pegawai, 
            data_sekolah.nama as nama_sekolah,
            (SELECT COUNT(*) FROM sekolah_guru WHERE id_data_guru = data_guru.id) as data_sekolah,
            (SELECT COUNT(*) FROM gaji_berkala_guru WHERE nip = data_guru.nip_baru) as gaji_berkala, 
            (SELECT COUNT(*) FROM pangkat_guru WHERE nip = data_guru.nip_baru) as pangkat_guru,
            (SELECT (CASE WHEN id IS NULL THEN 0 ELSE 1 END) FROM tugas_guru WHERE id_data_guru = data_guru.id) as guru,
            (SELECT (CASE WHEN LENGTH(no_peserta) > 0 THEN 1 ELSE 0 END) FROM sertifikasi_guru WHERE id_data_guru = data_guru.id) as sertifikasi,
        ');

        $this->db->from('data_guru');

        $this->db->where('id_sekolah', $sq->id);


        if (isset($filter['pns'])) {
            switch ($filter['pns']) {
                case  1:
                    $this->db->where('status_pns', 0);
                    break;
                case 2:
                    $this->db->where('status_pns', 1);

            }
        }

        if (isset($filter['nuptk'])) {
            switch ($filter['nuptk']) {
                case 1:
                    $this->db->where('nuptk IS NULL');
                    break;
                case 2:
                    $this->db->where('nuptk IS NOT NULL');
                    break;
            }
        }

        if (isset($filter['pegawai']['id']) && $filter['pegawai']['id'] > 0) {
            $this->db->where('id_jenis_pegawai', $filter['pegawai']['id']);
        }

        $query = urldecode($query);

        if ($query && $query != 'null' && $query !== null) {
            $this->db->where("(nuptk LIKE '%{$query}%' OR nama_lengkap LIKE '%{$query}%' OR nip_lama LIKE '%{$query}%' OR nip_baru LIKE '%{$query}%')");
        };

        $this->db->join('jenis_pegawai', 'jenis_pegawai.id = data_guru.id_jenis_pegawai', 'left');
        $this->db->join('data_sekolah', 'data_sekolah.id = data_guru.id_sekolah', 'left');

        $total = clone $this->db;

        $this->db->limit($count, $start);
        $this->db->order_by('id', 'asc');

        $data = $this->db->get()->result();

        return array('data' => $data, 'total' => $total->count_all_results(), 'params' => $query);
    }

    private function admin_show($start, $count, $query, $filter)
    {
        $this->db->select('
            data_guru.*, 
            jenis_pegawai.nama as jenis_pegawai, 
            data_sekolah.nama as nama_sekolah,
            (SELECT COUNT(*) FROM sekolah_guru WHERE id_data_guru = data_guru.id) as data_sekolah,
            (SELECT COUNT(*) FROM gaji_berkala_guru WHERE nip = data_guru.nip_baru) as gaji_berkala, 
            (SELECT COUNT(*) FROM pangkat_guru WHERE nip = data_guru.nip_baru) as pangkat_guru,
            (SELECT (CASE WHEN id IS NULL THEN 0 ELSE 1 END) FROM tugas_guru WHERE id_data_guru = data_guru.id) as guru,
            (SELECT (CASE WHEN LENGTH(no_peserta) > 0 THEN 1 ELSE 0 END) FROM sertifikasi_guru WHERE id_data_guru = data_guru.id) as sertifikasi,
        ');

        $this->db->from('data_guru');

        if (isset($filter['pns'])) {
            switch ($filter['pns']) {
                case  1:
                    $this->db->where('status_pns', 0);
                    break;
                case 2:
                    $this->db->where('status_pns', 1);
            }
        }

        if (isset($filter['nuptk'])) {
            switch ($filter['nuptk']) {
                case 1:
                    $this->db->where('nuptk IS NULL');
                    break;
                case 2:
                    $this->db->where('nuptk IS NOT NULL');
                    break;
            }
        }

        if (isset($filter['pegawai']['id']) && $filter['pegawai']['id'] > 0) {
            $this->db->where('id_jenis_pegawai', $filter['pegawai']['id']);
        }

        $query = urldecode($query);

        if ($query && $query != 'null' && $query !== null) {
            $this->db->where("(nuptk LIKE '%{$query}%' OR nama_lengkap LIKE '%{$query}%' OR nip_lama LIKE '%{$query}%' OR nip_baru LIKE '%{$query}%')");
        };

        $this->db->join('jenis_pegawai', 'jenis_pegawai.id = data_guru.id_jenis_pegawai', 'left');
        $this->db->join('data_sekolah', 'data_sekolah.id = data_guru.id_sekolah', 'left');

        $total = clone $this->db;

        $this->db->limit($count, $start);
        $this->db->order_by('id', 'asc');

        $data = $this->db->get()->result();

        return array('data' => $data, 'total' => $total->count_all_results(), 'params' => $query);

    }

    public function stats()
    {
        if ($this->session->as == 0) {
            return $this->admin_stats();
        } else {
            return $this->operator_stats();
        }
    }

    private function admin_stats()
    {
        $belum_nuptk = $this->db->query("SELECT COUNT(*) as nuptk FROM data_guru WHERE (nuptk IS NULL OR nuptk = '') AND deleted = '0'")->row()->nuptk;
        $nuptk = $this->db->query("SELECT COUNT(*) as nuptk FROM data_guru WHERE nuptk AND deleted = '0'")->row()->nuptk;
        $total = $this->db->query("SELECT COUNT(*) as nuptk FROM data_guru WHERE deleted = '0'")->row()->nuptk;
        $pegawai = $this->db->query("SELECT COUNT(*) as total, jenis_pegawai.nama as jenis FROM data_guru  INNER JOIN jenis_pegawai ON jenis_pegawai.id = data_guru.id_jenis_pegawai WHERE deleted = '0' GROUP BY jenis_pegawai.nama ")->result();

        $this->db->select("SUM((case when data_sekolah.nama is null then 1 else 0 end)) as total");
        $this->db->from('data_guru');
        $this->db->join('data_sekolah', 'id_sekolah = data_sekolah.id', 'left');
        $this->db->where('deleted', '0');

        $belum = $this->db->get()->row()->total;
        $sudah = $total - $belum;

        $data = array(
            'jenis' => $pegawai,
            'nuptk' => array(
                'belum' => $belum_nuptk,
                'sudah' => $nuptk
            ),
            'uk' => array(
                'belum' => $belum,
                'sudah' => $sudah
            ),
            'total' => $total
        );

        return $data;
    }

    private function operator_stats()
    {
        $fq = $this->db->get_where('user', array('id' => $this->session->userdata('uid')))->row();
        $sq = $this->db->get_where('data_sekolah', array('npsn' => $fq->id_npsn))->row();

        $sch = $sq->id;

        $belum_nuptk = $this->db->query("SELECT COUNT(*) as nuptk FROM data_guru WHERE (nuptk IS NULL OR nuptk = '') AND id_sekolah = '{$sch}' AND deleted = '0' ")->row()->nuptk;
        $nuptk = $this->db->query("SELECT COUNT(*) as nuptk FROM data_guru WHERE nuptk AND id_sekolah = '{$sch}' AND deleted = '0'")->row()->nuptk;
        $total = $this->db->query("SELECT COUNT(*) as nuptk FROM data_guru WHERE id_sekolah = '{$sch}' AND deleted = '0'")->row()->nuptk;
        $pegawai = $this->db->query("SELECT COUNT(*) as total, jenis_pegawai.nama as jenis FROM data_guru INNER JOIN jenis_pegawai ON jenis_pegawai.id = data_guru.id_jenis_pegawai WHERE id_sekolah = '{$sch}' AND deleted = '0' GROUP BY jenis_pegawai.nama ")->result();

        $data = array(
            'jenis' => $pegawai,
            'nuptk' => array(
                'belum' => $belum_nuptk,
                'sudah' => $nuptk
            ),
            'total' => $total
        );

        return $data;
    }

    private function log($log, $type)
    {
        $this->db->insert('log_ptk', [
            'log' => $log,
            'type' => $type,
            'id_user' => $this->session->uid
        ]);
    }

}