<?php
/**
 * Created by PhpStorm.
 * User: ragasubekti
 * Date: 28/11/17
 * Time: 08.36
 */
?>

<div class="modal-header">
    <div class="modal-title">
        Tambah Rombel
    </div>
</div>

<div class="modal-body">
    <form action="">
        <div class="form-group">
            <label for="">Tingkat</label>

        </div><ui-select ng-model="new.tingkat" theme="bootstrap" search-enabled="false">
            <ui-select-match>{{$select.selected.tingkat}}</ui-select-match>
            <ui-select-choices repeat="item in tingkatArray track by $index">
                {{item.tingkat}}
            </ui-select-choices>
        </ui-select>
        <div class="form-group">
            <label for="">Tahun Pelajaran</label>
            <ui-select ng-model="new.tapel" theme="bootstrap" search-enabled="false">
                <ui-select-match>{{$select.selected.tapel}}</ui-select-match>
                <ui-select-choices repeat="item in tapelList track by $index">
                    {{item.tapel}}
                </ui-select-choices>
            </ui-select>
        </div>
        <div class="form-group">
            <label for="">Jumlah Kelas</label>
            <div class="input-group">
                <input type="number" class="form-control" ng-change="onInputChange(much)" ng-model="much">
                <div class="input-group-addon">
                    Kelas
                </div>
            </div>
        </div>
        <div class="form-group">
            <table class="table table-striped">
                <thead>
                <tr>
                    <th width="50px" style="text-align: center">No</th>
                    <th style="text-align: center">Nama Kelas</th>
                    <th width="50px" style="text-align: center">L</th>
                    <th width="50px" style="text-align: center">P</th>
                </tr>
                </thead>
                <tbody>
                <tr ng-repeat="form in arrayKelas">
                    <td style="text-align: center">{{$index+1}}</td>
                    <td style="padding: 0">
                        <input type="text" class="form-control" placeholder="Nama Kelas"
                               ng-model="arrayKelas[$index].nama"
                               ng-change="arrayKelas[$index].nama = arrayKelas[$index].nama.toUpperCase()">
                    </td>
                    <td style="padding: 0">
                        <input type="text" class="form-control" placeholder="Jml Siswa" ng-model="arrayKelas[$index].laki">
                    </td>
                    <td style="padding: 0">
                        <input type="text" class="form-control" placeholder="Jml Siswi" ng-model="arrayKelas[$index].perempuan">
                    </td>
                </tr>
                </tbody>
            </table>
        </div>
    </form>
</div>

<div class="modal-footer">
    <button class="btn btn-primary btn-sm" ng-click="onBtnSaveClicked()" ng-disabled="isSubmit">
        <i class="fa fa-check" ng-if="!isSubmit"></i><i class="fa fa-circle-o-notch fa-spin" ng-if="isSubmit"></i> <span>Simpan</span>
    </button>
    <button class="btn btn-default btn-sm" ng-click="dismiss()">
        <i class="fa fa-times"></i> <span>Batal</span>
    </button>
</div>
