angular.module('diknasmlg').controller('SekolahPersentase', function ($scope, $uibModalInstance, data) {

    $scope.detail = data

    $scope.label = ['Lengkap', 'Tidak']
    $scope.ptk = [data.persentase, (100-data.persentase)]
    $scope.pns = [data.persentasePns, (100-data.persentasePns)]
    $scope.npns = [data.persentaseNonPns, (100-data.persentaseNonPns)]

    $scope.dismiss = function () {
        $uibModalInstance.dismiss()
    }
})