// Created by Raga Subekti at 08/10/17
angular.module('diknasmlg').factory('$DistrictFactory', function ($http) {
    var factory = {};
    var apiPath = localStorage.getItem('PATHLOC');

    factory.get = function (callback) {
        $http.get(apiPath + 'api/district').then(function(reply) {
            if(reply.status == 200) {
                return callback(reply.data);
            } else {
                console.log(reply)
            }
        }, function(err) {
            console.log(err)
        })
    };

    factory.show = function(id, query, callback) {
        // factory.regency = function(query, callback) {
            $http.get(apiPath + 'api/district/show/' + id + '/' + query).then(function(reply) {
                if(reply.status == 200) {
                    return callback(reply.data);
                } else {
                    console.log(reply)
                }
            }, function(err) {
                console.log(err)
            })
        // };
    };

    factory.regency = function(query, callback) {
        $http.get(apiPath + 'api/regency/' + query).then(function(reply) {
            if(reply.status == 200) {
                return callback(reply.data);
            } else {
                console.log(reply)
            }
        }, function(err) {
            console.log(err)
        })
    };

    factory.schoolondistrict = function(callback) {
        $http.get(apiPath + 'api/district/schoolondistrict').then(function(reply) {
            if(reply.status == 200) {
                return callback(reply.data);
            } else {
                console.log(reply)
            }
        }, function(err) {
            console.log(err)
        })
    };
    factory.showschool = function (start, count, query, query2, callback) {
        $http.get(apiPath + 'api/district/showschool/' + start + '/' + count + '/' + query + '/' + query2).then(
            function(reply) {
                if(reply.status == 200) {
                    return callback(reply.data);
                } else {
                    console.log(reply)
                }
            }
        )
    };
    factory.showteacher = function (start, count, query, search, callback) {
        $http.get(apiPath + 'api/district/showteacher/' + start + '/' + count + '/' + query+ '/' + search).then(
            function(reply) {
                if(reply.status == 200) {
                    return callback(reply.data);
                } else {
                    console.log(reply)
                }
            }
        )
    };

    return factory;
});