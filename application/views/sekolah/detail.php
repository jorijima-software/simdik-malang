<?php
/**
 * Created by PhpStorm.
 * User: ragasubekti
 * Date: 01/11/17
 * Time: 17.48
 */


$status = array(
    0 => 'Swasta',
    1 => 'Negeri'
);


$geo = array(
    0 => 'Pedesaan',
    1 => 'Perkotaan',
    2 => 'Daerah Perbatasan'
);

?>


<div ng-controller="detailSekolahController" ng-cloak>

    <section class="content-header">
        <h1>
            Detail Sekolah
        </h1>
    </section>

    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <?php include_once APPPATH . 'views/partials/informasi-sekolah.php'; ?>
            </div>
        </div>
        <?php require_once FCPATH . 'application/views/ptk/table.php'; ?>
        
    </section>

</div>
