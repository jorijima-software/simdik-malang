// Created by Raga Subekti at 17/10/17
angular.module('diknasmlg').controller('tambahSekolahPtk', function (id, Notify, $scope, $uibModalInstance, $DistrictFactory, $EducationFactory, $TeacherFactory) {

    $scope.isSubmit = false;

    $scope.id = id;

    $scope.districtList = [];
    $scope.regencyList = [];

    $scope.educationList = [];

    $scope.new = {
        kota: {},
        kecamatan: {},
        status: {},
        nama: '',
        masuk: '',
        lulus: '',
        jurusan: ''
    };

    $scope.dismiss = function () {
        $uibModalInstance.dismiss();
    };

    $scope.onRegencySelect = function() {
        $DistrictFactory.show($scope.new.kota.id, '', function(reply) {
            $scope.districtList = reply;
        })
    };

    $scope.onInputKotaChange = function(query) {
        $DistrictFactory.regency(query, function(reply) {
            $scope.regencyList = reply;
        });
    };

    $scope.onBtnSaveClicked = function() {
        $scope.isSubmit = true;

        $TeacherFactory.addSchool($scope.id, $scope.new, function(reply) {
            $scope.isSubmit = false;

            if(reply.error) {
                Notify.error('Kesalahan', reply.error)
            } else {
                Notify.success('Sukses', 'Data sekolah guru berhasil ditambahkan');
                $uibModalInstance.close();
            }
        })
    };

    function init() {
        $DistrictFactory.regency('', function (reply) {
            $scope.regencyList = reply;
        });

        $EducationFactory.get(function(reply) {
            $scope.educationList = reply;
        });
    }

    init();


});
