<?php
/**
 * Created by PhpStorm.
 * User: ragasubekti
 * Date: 2/2/18
 * Time: 2:27 AM
 */
?>

<div ng-cloak>
    <section class="content-header">
        <h1>
            Setting Jenis Pegawai
        </h1>
    </section>

    <section class="content">
        <div class="box box-primary">
            <div class="box-body">
                <div style="margin-bottom: 10px">
                    <button class="btn btn-primary btn-sm">
                        Tambah
                    </button>
                </div>
                <div>
                    <table class="table table-striped table-hover" ng-table="jenisPegawaiTable">
                        <thead>
                        <tr>
                            <th>No</th>
                            <th>Nama</th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr ng-repeat="row in $data">
                            <td>{{($index + 1) + (jenisPegawaiTable._params.count * (jenisPegawaiTable._params.page -1))}}</td>
                            <td>{{row.nama}}</td>
                            <td>
                                <div class="pull-right">
                                    <button class="btn btn-xs btn-primary">
                                        <i class="fa fa-pencil"></i> <span>Edit</span>
                                    </button>
                                    <button class="btn btn-xs btn-danger">
                                        <i class="fa fa-trash"></i> <span>Hapus</span>
                                    </button>
                                </div>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </section>
</div>

