// Created by Raga Subekti at 23/10/17
angular.module('diknasmlg').controller('editGajiPtk', function ($uibModalInstance, $scope, $RankFactory, $TeacherFactory, Notify, data) {
    $scope.isSubmit = false;


    $scope.rankList = [];

    var tmtdate = data.tmt.split('-');
    tmtdate = new Date(tmtdate[0], tmtdate[1] - 1, tmtdate[2]);

    $scope.edit = {
        id: data.id,
        sk: data.no_sk,
        tmt: tmtdate,
        pangkat: {
            id: data.id_pangkat_golongan,
            gol: data.gol,
            ruang: data.ruang
        },
        gaji: data.jumlah_gaji
    };

    $scope.dismiss = function () {
        $uibModalInstance.dismiss();
    };

    $scope.onBtnSaveClicked = function () {
        $scope.isSubmit = true;

        $TeacherFactory.updateSalary($scope.edit, function (reply) {
            $scope.isSubmit = false;

            if (reply.error) return Notify.error('Kesalahan', reply.error);
            Notify.success('Sukses', 'Sukses mengubah data gaji');
            $uibModalInstance.close();
        })
    };

    function init() {
        $RankFactory.get(function (reply) {
            $scope.rankList = reply;
        })
    }

    init();

});