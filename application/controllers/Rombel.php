<?php
/**
 * Created by PhpStorm.
 * User: ragas
 * Date: 12/8/2017
 * Time: 1:51 AM
 */

class Rombel extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('Rombel_model', 'rombel');
    }

    public function api_insert()
    {
        if ($this->input->method() == 'post') {
            $data = json_decode(trim(file_get_contents('php://input')));
            echo json_encode($this->rombel->insert($data));
        }
    }

    public function api_show($tapel, $sekolah = null)
    {
        if ($this->input->method() == 'get') {
            echo json_encode($this->rombel->show($tapel, $sekolah));
        }
    }

    public function api_delete()
    {
        if ($this->input->method() == 'post') {
            $data = json_decode(trim(file_get_contents('php://input')));
            echo json_encode($this->rombel->delete($data->id));
        }
    }

    public function api_delete_group()
    {
        if ($this->input->method() == 'post') {
            $data = json_decode(trim(file_get_contents('php://input')));
            echo json_encode($this->rombel->delete_group($data->group));
        }
    }


    public function api_update()
    {
        if ($this->input->method() == 'post') {
            $data = json_decode(trim(file_get_contents('php://input')));
            echo json_encode($this->rombel->update($data));
        }
    }

    public function api_stats()
    {
        echo json_encode($this->rombel->stats());
    }

    public function api_school($start, $count)
    {
        $filter = $this->input->get();
        echo json_encode($this->rombel->school_list($start, $count, $filter));
    }

    public function api_update_row()
    {
        if ($this->input->method() == 'post') {
            $data = json_decode(trim(file_get_contents('php://input')));
            echo json_encode($this->rombel->update_row($data->rombel));
        }
    }

    public function spa_rombel_index()
    {
        if ($this->session->as > 0) {
            $this->load->view('rombel/index');
        } else {
            $this->load->view('rombel/admin');
        }
    }

    public function spa_rombel_detail()
    {
        $this->load->view('rombel/detail');
    }

    public function modal_edit_row()
    {
        $this->load->view('rombel/modal/edit-row');
    }

}
