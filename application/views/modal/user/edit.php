
<div class="modal-header">
    <div class="modal-title">
        <i class="fa fa-pencil"></i> <span>Ubah Pengguna</span>
    </div>
</div>
<div class="modal-body">
    <form>
        <div class="form-group">
            <label>Username</label>
            <input type="text" class="form-control" ng-model="edit.username">
        </div>
        <div class="form-group">
            <label>Password</label>
            <div class="input-group">
                <input type="{{passwordType}}" class="form-control" ng-model="edit.password">
                <div class="input-group-btn">
                    <button class="btn btn-default" ng-click="onPasswordType()">
                        <i class="fa fa-eye"></i>
                    </button>
                </div>

            </div>
            <p class="help-block">Kosongi jika tidak ingin mengubah password</p>
        </div>
        <div class="form-group">
            <label>Level</label>
            <select class="form-control" ng-model="edit.level">
                <option value="0">Admin</option>
                <option value="1">Operator</option>
            </select>
        </div>
        <div class="form-group" ng-if="edit.level == 1">
            <label>Sekolah</label>
            <ui-select ng-model="edit.sekolah" theme="bootstrap">
                <ui-select-match placeholder="Pilih sekolah">
                    {{$select.selected.nama}}
                </ui-select-match>
                <ui-select-choices repeat="item in schoolList track by $index" refresh="onInputSearchChange($select.search)" refresh-delay="0">
                    {{item.nama}}
                </ui-select-choices>
            </ui-select>
        </div>

    </form>
</div>
<div class="modal-footer">
    <button class="btn btn-primary btn-sm" ng-click="onBtnSaveClicked()">
        <i class="fa fa-check"></i> <span>Simpan</span>
    </button>
    <button class="btn btn-default btn-sm" ng-click="dismiss()">
        <i class="fa fa-times"></i> <span>Batal</span>
    </button>
</div>