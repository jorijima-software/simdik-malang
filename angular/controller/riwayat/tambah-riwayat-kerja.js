angular.module('diknasmlg').controller('PtkRiwayatKerjaTambah', function(
    $scope,
    $uibModalInstance,
    $q,
    $SchoolFactory,
    $DistrictFactory,
    Notify,
    $RiwayatFactory,
    $routeParams
) {

    $scope.routes = $routeParams

    $scope.dismiss = function() {
        $uibModalInstance.dismiss();
    }

    $scope.schoolList = []
    $scope.kotaList = []

    $scope.riwayat = {}

    $scope.unitKerjaPlace = [
        {
            id: 0,
            nama: 'Kota Malang'
        },
        {
            id:1 ,
            nama: 'Luar Kota Malang'
        }
    ]

    $scope.onKotaSearch = function (query) {
        $DistrictFactory.regency(query, function (reply) {
            $scope.kotaList = reply;
        });
    };

    $scope.onInputSearchChange = function (query) {
        return $SchoolFactory.get(query, function (reply) {
            $scope.schoolList = reply;
        });
    };

    function init() {
        $SchoolFactory.get('', function (reply) {
            $scope.schoolList = reply
        })

        $DistrictFactory.regency('', function (reply) {
            $scope.kotaList = reply;
        })
    }

    $scope.onBtnSaveClicked = function () {
        var id = $scope.routes && $scope.routes.id ? $scope.routes.id : null
        $RiwayatFactory.tambahKerja(id, $scope.riwayat).then(function (reply) {
            if(reply && !reply.error) {
                Notify.success('Sukses', 'Berhasil menambahkan riwayat kerja')
                $uibModalInstance.close();
            } else {
                Notify.error('Kesalahan', reply.error)
            }
        })
    }

    init()
})