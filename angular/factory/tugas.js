// Created by Raga Subekti at 17/11/17
angular.module('diknasmlg').factory('$TugasFactory', function($http) {

    var apiPath = localStorage.getItem('PATHLOC');

    var factory = {};

    factory.show = function(start, count, callback) {
        $http.get(apiPath + 'api/tugas/' + start + '/' + count).then(
            function(reply) {
                if(reply.status == 200) {
                    return callback(reply.data);
                } else {
                    console.log(reply)
                }
            }
        )
    };

    factory.opShow = function(start, count, callback) {
        $http.get(apiPath + 'api/tugas-tambahan/show/' + start + '/' + count).then(
            function(reply) {
                if(reply.status == 200) {
                    return callback(reply.data);
                } else {
                    console.log(reply)
                }
            }
        )
    };

    factory.detail  = function(id, start, count, callback) {
        $http.get(apiPath + 'api/tugas-tambahan/detail/' + id + '/' + start + '/' + count).then(
            function(reply) {
                if(reply.status == 200) {
                    return callback(reply.data);
                } else {
                    console.log(reply)
                }
            }
        )
    };

    factory.insert = function(data, callback) {
        $http.post(apiPath + 'api/tugas-tambahan/insert', {
            guru: data.guru,
            no_sk: data.no_sk,
            tanggal_sk: data.tanggal_sk,
            tahun_mulai: data.tahun_mulai,
            tahun_selesai: data.tahun_selesai,
            id_jenis_jabatan: data.id_jenis_jabatan
        }).then(
            function(reply) {
                if(reply.status == 200) {
                    return callback(reply.data);
                } else {
                    console.log(reply)
                }
            }
        )
    };

    factory.update = function(data, callback) {
        $http.post(apiPath + 'api/tugas-tambahan/update', {
            id: data.id,
            guru: data.guru,
            no_sk: data.no_sk,
            tanggal_sk: data.tanggal_sk,
            tahun_mulai: data.tahun_mulai,
            tahun_selesai: data.tahun_selesai,
            id_jenis_jabatan: data.id_jenis_jabatan
        }).then(
            function(reply) {
                if(reply.status == 200) {
                    return callback(reply.data);
                } else {
                    console.log(reply)
                }
            }
        )
    };

    factory.getHm = function(callback) {
        $http.get(apiPath + 'api/tugas-tambahan/kepala-sekolah').then(
            function(reply) {
                if(reply.status == 200) {
                    return callback(reply.data);
                } else {
                    console.log(reply)
                }
            }
        )

    };

    factory.delete = function(id, callback) {
        $http.get(apiPath + 'api/tugas-tambahan/delete/' + id).then(
            function(reply) {
                if(reply.status == 200) {
                    return callback(reply.data);
                } else {
                    console.log(reply)
                }
            }
        )
    };

    factory.mapelSelect = function(query, callback) {
        $http.get(apiPath + 'api/mata-pelajaran/' + query).then(
            function(reply) {
                if(reply.status == 200) {
                    return callback(reply.data);
                } else {
                    console.log(reply)
                }
            }
        )
    };

    factory.find = function(query, callback) {

    };

    factory.add = function(nama, callback) {

    };

    factory.getTapel = function(query, callback) {
        $http.get(apiPath + 'api/tahun-pelajaran/' + query).then(
            function(reply) {
                if(reply.status == 200) {
                    return callback(reply.data);
                } else {
                    console.log(reply)
                }
            }
        )
    };

    factory.selectTapel = function(query, callback) {

    };

    return factory;

});