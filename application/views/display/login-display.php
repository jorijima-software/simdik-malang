<?php
/**
 * Created by PhpStorm.
 * User: arsyad09
 * Date: 05/12/17
 * Time: 17:52
 */

die();
if($this->session->is_logged) {
    redirect('/display');
}
?>


<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Dinas Pendidikan Kota Malang | Log in</title>
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <link rel="stylesheet" href="<?=base_url('bower_components/bootstrap/dist/css/bootstrap.min.css')?>">
    <link rel="stylesheet" href="<?=base_url('bower_components/font-awesome/css/font-awesome.min.css')?>">
    <link rel="stylesheet" href="<?=base_url('dist/css/AdminLTE.min.css')?>">
    <link rel="stylesheet" href="<?=base_url('dist/css/index-display.css')?>">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<body class="hold-transition login-page">
<div class="login-box">
    <div class="login-logo">
        <div class="text-center">
            <table>
                <tr>
                    <td rowspan="2">
                        <span class="logo-mini"><img src="<?= base_url('assets/logo.png') ?>" alt="" width="50"></span>
                    </td>
                    <td style="margin: 0; font-size: 18px; text-align: left; padding-left: 10px">Dinas Pendidikan</td>
                </tr>
                <tr>
                    <td style="margin: 0; font-size: 18px; font-weight: 800; text-align: left; padding-left: 10px">Kota Malang</td>
                </tr>
            </table>
        </div>
    </div>
    <!-- /.login-logo -->
    <div class="login-box-body">
        <form action="<?=site_url('Logindisplay')?>" method="post">
            <?php if(isset($error)) { ?>
                <div class="form-group">
                    <div class="alert alert-danger">
                        <i class="fa fa-exclamation-triangle"></i> &nbsp; <?=$error?>
                    </div>
                </div>
            <?php } ?>
            <div class="form-group has-feedback">
                <input type="text" class="form-control" placeholder="Username" name="username">
                <span class="fa fa-user form-control-feedback"></span>
            </div>
            <div class="form-group has-feedback">
                <input type="password" class="form-control" placeholder="Password" name="password">
                <span class="glyphicon glyphicon-lock form-control-feedback"></span>
            </div>
            <div class="form-group">
                <select name="login_as" id="" class="form-control">
                    <option value="0">Admin</option>
                    <option value="1" selected>Operator</option>
                </select>
            </div>
            <div class="row">
                <!-- /.col -->
                <div class="col-xs-4">
                    <button type="submit" class="btn btn-primary btn-block btn-flat">Sign In</button>
                </div>
            </div>
        </form>
    </div>
</div>
<!-- /.login-box-body -->
</div>
<!-- /.login-box -->

<!-- jQuery 3 -->
<script src="<?=base_url('bower_components/jquery/dist/jquery.min.js')?>"></script>
<!-- Bootstrap 3.3.7 -->
<script src="<?=base_url('bower_components/bootstrap/dist/js/bootstrap.min.js')?>"></script>
</body>
</html>
