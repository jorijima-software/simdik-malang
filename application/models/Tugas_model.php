<?php
/**
 * Created by PhpStorm.
 * User: ragasubekti
 * Date: 17/11/17
 * Time: 22.31
 */

class Tugas_model extends CI_Model
{

    public function show($start, $count)
    {
        $this->db->select('*');
        $this->db->from('jenis_jabatan');

        $total = clone $this->db;

        $this->db->limit($count, $start);
        $data = $this->db->get()->result();

        return array('total' => $total->count_all_results(), 'data' => $data);
    }


    public function add($name)
    {
        if (strlen($name) <= 0) {
            return array('error' => 'Nama tidak boleh kosong!');
        } else {
            $fq = $this->db->get_where('jenis_jabatan', array('nama' => $name));

            if ($fq->num_rows() > 0) {
                return array('error' => 'Tugas tambahan tersebut telah terdaftar!');
            } else {
                $insert = array(
                    'nama' => $name
                );

                $this->db->insert('jenis_jabatan', $insert);
            }
        }
    }

    public function delete($id)
    {

        $this->db->where('id', $id);
        $this->db->delete('jenis_jabatan');

        return array('success' => true);
    }

    public function edit($id, $name)
    {
        // check if the thing exist
        if ($this->db->get_where('jenis_jabatan', array('id' => $id))->num_rows() > 0) {
//            $this->d
        } else {
            return array('error' => 'Data tidak ditemukan');
        }
    }

    public function mapelSelect($query)
    {
        $this->db->select('*');
        $this->db->from('tugas_mengajar');
        $this->db->limit(10);
        $this->db->like('nama', $query);

        return $this->db->get()->result();
    }

    public function getTapel($query)
    {
        $this->db->select('*');
        $this->db->order_by('active', 'DESC');
        $this->db->order_by('tapel', 'DESC');
        $this->db->from('tapel');
        $this->db->like('tapel', $query);

        return $this->db->get()->result();
    }

    public function tambah_tmengajar($data)
    {
//        return ['error' => true, $data];
        $insert = array(
            'id_tugas_guru' => $data->id,
            'id_tapel' => $data->tapel,
            'jumlah_jam' => $data->jumlah_jam,
            'id_tugas_mengajar' => $data->mapel, // mata pelajaran XD
            'id_sekolah' => $data->unit_kerja,
            'semester' => $data->semester
        );

        $this->db->insert('data_tugas_mapel', $insert);

        return array('success' => true);
    }

    public function show_tmengajar($id, $start, $count, $filter = [])
    {
        $this->db->select('data_tugas_mapel.*, data_sekolah.nama nama_sekolah, tapel.tapel, tugas_mengajar.nama mata_pelajaran');
        $this->db->from('data_tugas_mapel');
        $this->db->join('tapel', 'id_tapel = tapel.id');
        $this->db->join('tugas_mengajar', 'id_tugas_mengajar = tugas_mengajar.id');
        $this->db->join('data_sekolah', 'id_sekolah = data_sekolah.id');
        $this->db->where('id_tugas_guru', $id);

        $total = clone $this->db;
        $total = $total->count_all_results();

        $this->db->limit($count, $start);

        $data = $this->db->get()->result();

        return array('data' => $data, 'total' => $total);
    }

    public function delete_tmengajar($id)
    {

        if ($this->db->get_where('data_tugas_mapel', array('id' => $id))->num_rows() > 0) {
            $this->db->where('id', $id);
            $this->db->delete('data_tugas_mapel');

            return array('success' => true);
        } else {
            return array('error' => 'Data tidak ditemukan');
        }
    }

    public function update_tmengajar($data) {
        $update = array(
            'id_tapel' => $data->tapel,
            'jumlah_jam' => $data->jumlah_jam,
            'id_tugas_mengajar' => $data->mapel, // mata pelajaran XD
            'id_sekolah' => $data->unit_kerja,
            'semester' => $data->semester
        );

        $this->db->where('id', $data->id);

        $this->db->update('data_tugas_mapel', $update);

        return array('success' => true);
    }


}