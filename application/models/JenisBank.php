<?php

class JenisBank extends \Illuminate\Database\Eloquent\Model {
    protected $table = 'jenis_bank';
    protected $guarded = [];
    public $timestamps = false;

    /**
     * Tambah Jenis Bank
     *
     * @param [string] $nama
     * @return void
     */
    public function tambah($nama) {
        $dup = JenisBank::where('nama', 'like', "%{$nama}%")->count();

        if($dup > 0) {
            return ['error' => 'Nama Bank sudah ada / duplikat'];
        } else {
            JenisBank::create([
                'nama' => strtoupper($nama)
            ]);

            return ['success' => true];
        }
    }
}