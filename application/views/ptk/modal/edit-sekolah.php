<?php
/**
 * Created by PhpStorm.
 * User: ragasubekti
 * Date: 18/10/17
 * Time: 16.46
 */
?>

<div class="modal-header">
    <div class="modal-title">
        Ubah Data Sekolah Guru
    </div>
</div>

<div class="modal-body">
    <form>
        <div class="form-group">
            <label for="">Jenis Tingkat Pendidikan</label>
            <ui-select ng-model="edit.tk" theme="bootstrap" search-enabled="false">
                <ui-select-match>{{$select.selected.nama}}</ui-select-match>
                <ui-select-choices repeat="item in educationList track by $index">
                    {{item.nama}}
                </ui-select-choices>
            </ui-select>
        </div>
        <div class="form-group">
            <label for="">Nama Sekolah</label>
            <input type="text" class="form-control" ng-model="edit.nama">
        </div>
        <div class="form-group">
            <label for="">Jurusan</label>
            <input type="text" class="form-control" placeholder="Nama Jurusan" ng-model="edit.jurusan">
            <p class="help-block">Masukan UMUM jika tidak memiliki jurusan</p>
        </div>
        <div class="form-group">
            <label for="">Kota</label>
            <ui-select ng-model="edit.kota" theme="bootstrap" on-select="onRegencySelect()">
                <ui-select-match>{{$select.selected.name}}</ui-select-match>
                <ui-select-choices repeat="item in regencyList track by $index" refresh="onInputKotaChange($select.search)" refresh-delay="0">
                    {{item.name}}
                </ui-select-choices>
            </ui-select>
        </div>
        <div class="form-group">
            <label for="">Kecamatan</label>
            <ui-select ng-model="edit.kecamatan" theme="bootstrap" ng-disabled="!edit.kota || !edit.kota.name">
                <ui-select-match>{{$select.selected.name}}</ui-select-match>
                <ui-select-choices repeat="item in districtList track by $index" refresh="onInputKecamatanChange($select.search)" refresh-delay="0">
                    {{item.name}}
                </ui-select-choices>
            </ui-select>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label for="">Tahun Masuk</label>
                    <input type="number" class="form-control" ng-model="edit.masuk">
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label for="">Tahun Lulus</label>
                    <input type="number" class="form-control" ng-model="edit.lulus">
                </div>
            </div>
        </div>
    </form>
</div>
<div class="modal-footer">
    <button class="btn btn-primary btn-sm" ng-click="onBtnSaveClicked()" ng-disabled="isSubmit">
        <i class="fa fa-check" ng-if="!isSubmit"></i><i class="fa fa-circle-o-notch fa-spin" ng-if="isSubmit"></i> <span>Simpan</span>
    </button>
    <button class="btn btn-default btn-sm" ng-click="dismiss()">
        <i class="fa fa-times"></i> <span>Batal</span>
    </button>
</div>