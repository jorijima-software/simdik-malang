module.exports = {
    files: [
        "*","*.*","**"
    ],
    server: {
        middleware: [
            require('connect-modrewrite')([
                `^/$ 127.0.0.1 [P]`
            ])
        ]
    }
};