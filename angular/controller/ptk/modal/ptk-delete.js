angular.module('diknasmlg').controller('PtkDeleteModal', function ($scope, $uibModalInstance, data, $PtkFactory, Notify) {

    $scope.detail = data

    $scope.alasan = ''

    $scope.dismiss = function() {
        $uibModalInstance.dismiss()
    }

    $scope.onBtnConfirmClicked = function() {
        $PtkFactory.delete(data.id, $scope.alasan).then(function (result) {
            if(result && !result.error) {
                Notify.success('Sukses', 'Berhasil menghapus data PTK')
                $uibModalInstance.close()
            } else {
                Notify.error('Kesalahan', reply.error)
            }
        })
    }

})