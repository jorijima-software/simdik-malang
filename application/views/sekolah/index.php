<?php
/**
 * Created by PhpStorm.
 * User: ragasubekti
 * Date: 08/10/17
 * Time: 10.34
 */
?>
<div id="wrapper" ng-controller="sekolahController" ng-cloak>

    <section class="content-header">
        <h1>
            Data Sekolah
        </h1>
    </section>

    <section class="content">
        <div class="row hidden-xs hidden-sm">
            <div class="col-lg-3 col-xs-6" ng-repeat="item in statsbox">
                <div class="small-box text-center" ng-random-class ng-classes="bgIndex">
                    <div class="inner">
                        <div class="row">
                            <div class="col-xs-3" style="font-size: smaller">
                                <h4 style="font-weight: 800">{{item.negri}}</h4>
                                <p>Negeri</p>
                            </div>
                            <div class="col-xs-3">
                                <h4 style="font-weight: 800">{{item.swasta}}</h4>
                                <p>Swasta</p>
                            </div>
                            <div class="col-xs-6">
                                <h3>{{item.total}}</h3>
                                <p>Total {{item.tingkat}}</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="box">
            <div class="box-body">
                <div class="row">
                    <div class="col-md-8" style="margin-bottom: 5px">
                        <button class="btn btn-primary btn-sm" ng-click="onBtnAddClicked()">
                            <i class="fa fa-plus"></i> &nbsp; Tambah
                        </button>
                        <a class="btn btn-success btn-sm" href="<?=site_url('sekolah/export')?>" target="_blank">
                            <i class="fa fa-cloud-download"></i>&nbsp;Export (XLS)
                        </a>
                    </div>
                    <div class="col-md-4" style="margin-bottom: 5px">
                        <div class="form-group">
                            <div class="input-group">
                                <div class="input-group-btn">
                                    <button class="btn btn-default" ng-click="isThisHidden = !isThisHidden">
                                        <i class="fa fa-list"></i> <span>Filter</span>
                                    </button>
                                </div>
                                <input type="text" class="form-control" placeholder="Pencarian" ng-model="search"
                                       ng-keydown="onSearchKeydown()">
                                <div class="input-group-btn">
                                    <button class="btn btn-info" ng-click="doSearch()">
                                        <i class="fa fa-search"></i> Cari
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>


                <div ng-hide="!isThisHidden">
                    <div class="well well-sm">
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="">Kecamatan</label>
                                    <ui-select ng-model="filter.kecamatan" theme="bootstrap" search-enabled="false"
                                               on-select="onFilter()">
                                        <ui-select-match placeholder="Pilih kecamatan">
                                            {{$select.selected.nama}}
                                        </ui-select-match>
                                        <ui-select-choices repeat="item in kecamatanList track by $index">
                                            {{item.nama}}
                                        </ui-select-choices>
                                    </ui-select>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="">Tingkat</label> <br>
                                    <ui-select ng-model="filter.tingkat" theme="bootstrap" search-enabled="false"
                                               on-select="onFilter()">
                                        <ui-select-match placeholder="Pilih tingkat">
                                            {{$select.selected.nama}}
                                        </ui-select-match>
                                        <ui-select-choices repeat="item in educationList track by $index">
                                            {{item.nama}}
                                        </ui-select-choices>
                                    </ui-select>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="">Status</label> <br>
                                    <ui-select ng-model="filter.status" theme="bootstrap" search-enabled="false"
                                               on-select="onFilter()">
                                        <ui-select-match placeholder="Pilih status">
                                            {{$select.selected.nama}}
                                        </ui-select-match>
                                        <ui-select-choices repeat="item in statusSekolah track by $index">
                                            {{item.nama}}
                                        </ui-select-choices>
                                    </ui-select>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>


                <div class="row">
                    <div class="col-md-12">
                        <div class="table-responsive">
                            <table class="table table-striped table-condensed" ng-table="schoolTable">
                                <thead>
                                <tr>
                                    <th>No</th>
                                    <th>NPSN</th>
                                    <th>Tingkat</th>
                                    <th>Nama Sekolah</th>
                                    <th>Kecamatan</th>
                                    <th>Total Pegawai</th>
<!--                                    <th>Data Masuk</th>-->
                                    <th></th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr ng-repeat="row in $data">
                                    <td>{{($index + 1) + (schoolTable._params.count * (schoolTable._params.page -1))}}
                                    </td>
                                    <td><a href="<?= site_url() ?>#!/sekolah/detail/{{row.id}}">{{row.npsn}}</a></td>
                                    <td>{{row.tingkatSekolah}}</td>
                                    <td>{{row.namaSekolah}}</td>
                                    <td>{{row.kecamatan}}</td>
                                    <td>{{row.totalPegawai}}</td>
<!--                                    <td ng-click="onPersentaseClicked(row)">-->
<!--                                        <div class="progress-bar" style="width: {{row.persentase}}%"-->
<!--                                             ng-class="{-->
<!--                                                'progress-bar-success': row.persentase >= 80,-->
<!--                                                'progress-bar-warning': row.persentase < 80 && row.persentase >= 50,-->
<!--                                                'progress-bar-danger' : row.persentase < 50,-->
<!--                                                'progress-bar-striped active': row.persentase != 100-->
<!--                                            }"-->
<!--                                        >-->
<!--                                            &nbsp;<span ng-if="row.persentase >= 10"><i class="fa fa-check"-->
<!--                                                                                        ng-if="row.persentase == 100"></i>&nbsp;{{row.persentase}}%</span>-->
<!--                                        </div>-->
<!--                                        <span ng-if="row.persentase < 10">{{row.persentase}}%</span>-->
<!--                                    </td>-->
                                    <td>
                                        <div class="pull-right">
                                            <button ng-click="onPersentaseClicked(row)" uib-tooltip="Persentase Kelengkapan Data" class="btn btn-default btn-xs">
                                                <i class="fa fa-percent"></i>
                                            </button>
                                            <a class="btn btn-success btn-xs"
                                               href="<?= site_url('/sekolah/') ?>{{row.id}}/ptk/export"
                                               uib-tooltip="Export Data PTK"
                                            >
                                                <i class="fa fa-file-excel-o"></i>
                                            </a>
                                            <a class="btn btn-primary btn-xs"
                                               href="<?= site_url('') ?>#!/sekolah/detail/{{row.id}}"
                                               uib-tooltip="Detail Sekolah"
                                            >
                                                <i class="fa fa-eye"></i>
                                            </a>
                                            <button
                                                    class="btn btn-warning btn-xs"
                                                    ng-click="onBtnResetClicked(row)"
                                                    uib-tooltip="Reset Password"
                                            >
                                                <i class="fa fa-undo"></i>
                                            </button>
                                            <button
                                                    class="btn btn-info btn-xs"
                                                    ng-click="onBtnEditClicked(row)"
                                                    uib-tooltip="Edit Sekolah"
                                            >
                                                <i class="fa fa-pencil"></i>
                                            </button>
                                            <button
                                                    class="btn btn-danger btn-xs"
                                                    ng-click="onBtnDeleteClicked(row)"
                                                    uib-tooltip="Hapus Sekolah"
                                            >
                                                <i class="fa fa-trash"></i>
                                            </button>
                                        </div>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

</div>