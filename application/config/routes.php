<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'boilerplate/root';
$route['404_override'] = '';
$route['translate_uri_dashes'] = TRUE;

$route['root'] = 'boilerplate/root';

$route['api/sekolah/mini-list'] = 'school/minilist';

$route['api/2/statistik/insentif/export'] = 'statistik/exportinsentif';
$route['api/2/statistik/insentif'] = 'statistik/apiinsentif';
$route['api/2/statistik/pensiun/all']  = 'statistik/apipensiunstats';
$route['api/2/statistik/pensiun']  = 'statistik/apipensiun';
$route['api/2/statistik/data-masuk']  = 'statistik/apidatamasuk';
$route['api/2/statistik/guru']  = 'statistik/apiguru';
$route['api/2/ptk/(:num)'] = 'ptkbaru/detailptk/$1';
$route['api/bank'] = 'bank/showBank';

$route['api/ptk/rekening/store'] = 'ptkrekcontroller/store'; 
$route['api/ptk/rekening/(:num)'] = 'ptkrekcontroller/get/$1';
// teacher api

$route['modal/ptk/hapus'] = 'ptkbaru/modal_hapus';

$route['api/user/authenticate'] = 'user/api_auth';
$route['api/verify'] = 'user/api_verify';

// rombel
$route['api/rombel/insert'] = 'rombel/api_insert'; // post
$route['api/rombel/update'] = 'rombel/api_update'; // post
$route['api/rombel/update/row'] = 'rombel/api_update_row'; // post
$route['api/rombel/delete'] = 'rombel/api_delete'; // post
$route['api/rombel/delete/group'] = 'rombel/api_delete_group'; // post
$route['api/rombel/show/(:num)'] = 'rombel/api_show/$1'; // get
$route['api/rombel/show/(:num)/(:num)'] = 'rombel/api_show/$1/$2'; // get
$route['api/rombel/stats'] = 'rombel/api_stats'; // get

$route['api/rombel/school/(:num)/(:num)'] =  'rombel/api_school/$1/$2';

/// guru

$route['api/guru'] = 'guru/api_guru_show';
$route['api/guru/(:num)/(:num)'] = 'guru/api_guru_show/$1/$2';
$route['api/guru/stats'] = 'guru/stats';
$route['api/guru/show/(:num)/(:num)'] = 'guru/show/$1/$2';
$route['api/guru/show/(:num)/(:num)/(:any)'] = 'guru/show/$1/$2/$3';
$route['api/guru/insert'] = 'guru/api_insert';
$route['api/guru/update'] = 'guru/api_update';
$route['api/guru/delete/(:num)'] = 'guru/api_delete/$1';


//log

$route['log/rombel'] = 'log/page_rombel';
$route['log/login'] = 'log/page_login';
$route['log/sms'] = 'log/page_sms';
$route['log/guru'] = 'log/page_guru';
$route['log/ptk'] = 'log/page_ptk';

// log api
$route['api/log/rombel/(:num)/(:num)'] = 'log/api_rombel/$1/$2';
$route['api/log/ptk/(:num)/(:num)'] = 'log/api_ptk/$1/$2';
$route['api/log/guru/(:num)/(:num)'] = 'log/api_guru/$1/$2';
$route['api/log/sms/(:num)/(:num)'] = 'log/api_sms/$1/$2';
$route['api/log/login/(:num)/(:num)'] = 'log/api_login/$1/$2';


$route['sekolah/(:num)/ptk/export'] = 'school/api_export_ptk/$1';
$route['sekolah/export'] = 'school/api_export';
$route['api/countdown'] = 'timer/getcountdown';

// ptk
$route['api/ptk/insert'] = 'ptk/api_insert';
$route['api/ptk/update'] = 'ptk/api_update';
$route['api/ptk/delete'] = 'ptkbaru/api_ptk_delete';
//$route['api/ptk/delete/(:num)'] = 'ptk/api_delete/$1';
$route['api/ptk/stats'] = 'ptk/api_stats';

$route['api/ptk/show/(:num)/(:num)'] = 'ptk/api_show/$1/$2';
$route['api/ptk/show/(:num)/(:num)/(:any)'] = 'ptk/api_show/$1/$2/$3';

$route['api/ptk/pns'] = 'ptkbaru/api_show_pns';
$route['api/ptk/pns/(:num)'] = 'ptkbaru/api_show_pns/$1';
$route['api/ptk/pns/(:num)/(:num)'] = 'ptkbaru/api_show_pns/$1/$2';

$route['api/ptk/informasi/(:num)'] = 'ptkbaru/api_informasi_ptk/$1';

$route['api/ptk/npns'] = 'ptkbaru/api_show_npns';
$route['api/ptk/npns/(:num)'] = 'ptkbaru/api_show_npns/$1';
$route['api/ptk/npns/(:num)/(:num)'] = 'ptkbaru/api_show_npns/$1/$2';

$route['api/ptk/detail/(:num)'] = 'ptk/api_detail/$1';

$route['api/ptk/riwayat/kerja/hapus/(:num)'] = 'riwayat/api_kerja_delete/$1';
$route['api/ptk/riwayat/kerja/tambah/(:num)'] = 'ptkbaru/api_insert_kerja/$1';
$route['api/ptk/riwayat/kerja/edit/(:num)'] = 'riwayat/apiKerjaEdit/$1';
$route['api/ptk/riwayat/kerja/(:num)/(:num)/(:num)'] = 'riwayat/api_kerja_show/$1/$2/$3';

// sms verification

$route['api/user/verification/timer'] = 'user/phone_timer';
$route['api/user/verification/resend'] = 'user/resend_sms';
$route['api/user/verification/confirm'] = 'user/verify_phone';

$route['api/sekolah/tstats'] = 'school/api_tstats';
$route['api/sekolah'] = 'school/api_sekolah_show';
$route['api/sekolah/(:num)/(:num)'] = 'school/api_sekolah_show/$1/$2';

// sekolah
$route['api/sekolah/detail/(:num)'] = 'school/api_detail/$1';
$route['api/sekolah/get'] = 'school/get';
$route['api/sekolah/get/(:any)'] = 'school/get/$1';

// kota
$route['api/kota'] = 'district/regency';
$route['api/kota/(:any)'] = 'district/regency/$1';

//kecamatan
$route['api/kecamatan/(:num)'] = 'district/show/$1';
$route['api/kecamatan/(:num)/(:any)'] = 'district/show/$1/$2';


// tugas mengajar api

$route['api/tugas-mengajar/tambah'] = 'teacher/api_tmengajar_tambah';
$route['api/tugas-mengajar/update'] = 'teacher/api_tmengajar_update';
$route['api/tugas-mengajar/delete/(:num)'] = 'teacher/api_tmengajar_delete/$1';
$route['api/tugas-mengajar/show/(:num)/(:num)/(:num)'] = 'teacher/api_tmengajar_show/$1/$2/$3';


//
//$route['api/teacher/insert'] = 'teacher/api_ptk_insert';
//$route['api/teacher/update'] = 'teacher/api_ptk_update';
//$route['api/teacher/statsbox'] = 'teacher/statsbox';
//$route['api/teacher/delete/(:num)'] = 'teacher/delete/$1';
$route['api/teacher/rank/(:num)'] = 'teacher/getrank/$1';
$route['api/teacher/(:num)/(:num)'] = 'teacher/show/$1/$2';
$route['api/teacher/(:num)/(:num)/(:any)'] = 'teacher/show/$1/$2/$3';
$route['api/teacher/(:num)/(:num)/(:any)/(:any)'] = 'teacher/show/$1/$2/$3/$4';
$route['api/teacher/(:num)'] = 'teacher/get/$1';
$route['api/teacher/(:num)/school'] = 'teacher/getschool/$1';

$route['api/operator/teacher/(:num)/(:num)'] = 'teacher/showoperator/$1/$2';
$route['api/operator/teacher/(:num)/(:num)/(:any)'] = 'teacher/showoperator/$1/$2/$3';
$route['api/teacher/(:num)/(:num)/(:any)/(:any)'] = 'teacher/show/$1/$2/$3/$4';

$route['api/teacher/(:num)/add/school'] = 'teacher/api_sekolah_tambah/$1';
$route['api/teacher/(:num)/delete/school'] = 'teacher/deleteschool/$1';
$route['api/teacher/(:num)/edit/school'] = 'teacher/editschool/$1';
$route['api/teacher/(:num)/activate/school/(:num)'] = 'teacher/activateschool/$1/$2';

$route['api/teacher/(:num)/salary'] = 'teacher/getsalary/$1';

$route['api/teacher/(:num)/salary/add'] = 'teacher/addsalary/$1';
$route['api/teacher/(:num)/salary/delete'] = 'teacher/deletesalary/$1';

$route['api/teacher/(:num)/rank/add'] = 'teacher/addrank/$1';
$route['api/teacher/(:num)/rank/delete'] = 'teacher/deleterank/$1';

$route['api/teacher/rank/update'] = 'teacher/updaterank';
$route['api/teacher/salary/update'] = 'teacher/updatesalary';

$route['api/school/insert'] = 'school/insert';
$route['api/school/update'] = 'school/update';
$route['api/school/statsbox'] = 'school/statsbox';
$route['api/school/spread'] = 'school/spread';

$route['api/ptk/import/show'] = 'ptk/api_show_import_ptk';

$route['api/operator/teacher/statsbox'] = 'teacher/operatorStatsbox';

$route['api/school/(:num)/teacher/(:num)/(:num)'] = 'teacher/getSchoolTeacher/$1/$2/$3';
$route['api/school/(:num)/teacher/(:num)/(:num)/(:any)'] = 'teacher/getSchoolTeacher/$1/$2/$3/$4';

$route['api/school/delete/(:num)'] = 'school/delete/$1';
$route['api/school/(:num)/(:num)'] = 'school/show/$1/$2';
$route['api/school/(:num)/(:num)/(:any)'] = 'school/show/$1/$2/$3';
$route['api/school/show/teacherschool/(:num)/(:num)/(:any)'] = 'school/showteacherschool/$1/$2/$3';

$route['api/school/type'] = 'school/type';

$route['api/agama'] = 'religion/show';

$route['api/religion'] = 'religion/show';
$route['api/religion/statsbox'] = 'religion/statsbox';

$route['api/user/show/(:num)/(:num)'] = 'user/show/$1/$2';
$route['api/user/show/(:num)/(:num)/(:any)'] = 'user/show/$1/$2/$3';

$route['api/operator/show/(:num)/(:num)'] = 'registrationop/show/$1/$2';
$route['api/operator/show/(:num)/(:num)/(:any)'] = 'registrationop/show/$1/$2/$3';

$route['api/ranks'] = 'rank/get';

$route['api/pendidikan'] = 'educationlv/get';
$route['api/district'] = 'district/get';
$route['api/regency'] = 'district/regency';
$route['api/regency/(:any)'] = 'district/regency/$1';
$route['api/district/show/(:num)'] = 'district/show/$1';
$route['api/district/show/(:num)/(:any)'] = 'district/show/$1/$2';
$route['api/district/schoolondistrict'] = 'district/schoolondistrict';
$route['api/district/showschool/(:num)/(:num)/(:any)/(:any)'] = 'district/showschool/$1/$2/$3/$4';
$route['api/district/showteacher/(:num)/(:num)/(:any)'] = 'district/showteacher/$1/$2/$3';

$route['logout'] = 'login/logout';
$route['logoutdisplay'] = 'logindisplay/logout';

$route['api/operator/create'] = 'registrationop/create';
//$route['api/operator/delete/(:num)'] = 'registrationop/delete/$1';
//$route['api/operator/update'] = 'registrationop/update';

$route['api/user/create'] = 'user/create';
$route['api/user/delete/(:num)'] = 'user/delete/$1';
$route['api/user/update'] = 'user/update';

$route['api/tugas/(:num)/(:num)'] = 'tugas/show/$1/$2';

$route['api/v2/teacher/show'] = 'teacher/showv2';

$route['api/tugas-tambahan/show/(:num)/(:num)'] = 'teacher/api_tgtb_op_show/$1/$2';
$route['api/tugas-tambahan/detail/(:num)/(:num)/(:num)'] = 'teacher/api_tgtb_op_detail/$1/$2/$3';

$route['api/teacher/select'] = 'teacher/api_select_teacher';
$route['api/teacher/select/(:any)'] = 'teacher/api_select_teacher/$1';

$route['api/tugas-tambahan/insert'] = 'teacher/api_insert_tgtb';
$route['api/tugas-tambahan/update'] = 'teacher/api_update_tgtb';
$route['api/tugas-tambahan/delete/(:num)'] = 'teacher/api_delete_tgtb/$1';
$route['api/tugas-tambahan/kepala-sekolah'] = 'teacher/api_headmaster_tgtb';

$route['daftar-operator'] = 'registrationop/index';

$route['api/lesson/get'] = 'teacher/api_mapel_get';
$route['api/lesson/get/(:any)'] = 'teacher/api_mapel_get/$1';

$route['api/v2/teacher/insert'] = 'teacher/api_dguru_insert';

$route['api/v2/teacher/show/(:num)/(:num)'] = 'teacher/api_dguru_show/$1/$2';
$route['guru/tugas-mengajar/(:num)'] = 'teacher/page_dguru_detail/$1';

// mapel api

$route['api/mata-pelajaran'] = 'teacher/api_mapel_select';
$route['api/mata-pelajaran/(:any)'] = 'teacher/api_mapel_select/$1';

// tahun pelajaran api

$route['api/tahun-pelajaran'] = 'teacher/api_tapel_select';
$route['api/tahun-pelajaran/(:any)'] = 'teacher/api_tapel_select/$1';


$route['api/v2/teacher/update'] = 'teacher/api_dguru_update';

// guru api
$route['guru/tugas-mengajar/(:num)'] = 'teacher/page_dguru_detail/$1';


// login api

// rombel api


// kelas api
$route['api/tingkat-kelas'] = 'school/api_tkelas_show';

// pages

$route['dashboard'] = 'dashboard/page_index';
$route['rombel'] = 'school/page_rombel';

$route['ptk'] = 'teacher/page_ptk';
$route['berita'] = 'berita/page_index';
$route['berita/baru'] = 'berita/page_baru';
$route['guru'] = 'teacher/page_data_guru';
$route['sekolah'] = 'school/page_index';
$route['tugas-tambahan'] = 'teacher/page_tugas_tambahan';
$route['manage-gallery'] = 'gallery/manage';

$route['guru/tugas-mengajar/(:num)'] = 'teacher/page_dguru_detail/$1';
$route['sekolah/detail/(:num)'] = 'school/page_detail/$1';

$route['ptk/riwayat/sekolah/(:num)'] = 'teacher/page_sekolah/$1';
$route['ptk/riwayat/gaji/(:num)'] = 'teacher/page_gaji/$1';
$route['ptk/riwayat/pangkat/(:num)'] = 'teacher/page_pangkat/$1';

$route['phone_verification'] = 'user/page_verify';
$route['change_password'] = 'user/page_urgent';

// modal

$route['modal/ptk/(:any)'] = 'teacher/modal_ptk_$1';
$route['modal/ptk/sekolah/(:any)'] = 'teacher/modal_sekolah_$1';


$route['api/user/login/(:any)/(:any)'] = 'user/api_user_login/$1/$2';

$route['api/sekolah/profil/(:any)'] = 'school/api_profile_get/$1';
$route['api/ptk/get/(:any)'] = 'teacher/api_ptk_get/$1';
$route['api/ptk/detail/(:num)'] = 'teacher/get/$1';

//$route['api/ptk/riwayat/kerja/tambah']


$route['api/bidang-studi'] = 'teacher/api_bstudi_get';
$route['api/bidang-studi/(:any)'] = 'teacher/api_bstudi_get/$1';

// ticket

$route['tiket'] = 'tiket/page_index';
$route['tiket/tambah'] = 'tiket/page_tambah';


$route['api/tiket/show/(:num)/(:num)'] = 'tiket/api_show/$1/$2';
$route['api/tiket/delete/(:num)'] = 'tiket/api_delete/$1';
$route['api/tiket/id'] = 'tiket/api_ticket_id';

$route['spa/(:any)/(:any)'] = '$1/spa_$2';
$route['api/berita/target'] = 'berita/api_target_get';
$route['api/berita/target/(:any)'] = 'berita/api_target_get/$1';

$route['api/sekolah/reset/(:any)'] = 'school/api_sekolah_reset/$1';
$route['operator'] = 'operator/page_index';

//operator

$route['api/operator/get'] = 'school/api_operator_get';
$route['api/operator/update'] = 'school/api_operator_update';
$route['api/operator/insert'] = 'operator/api_operator_insert';
$route['api/operator/delete/(:num)'] = 'operator/api_operator_delete/$1';
$route['api/operator/detail/(:any)'] = 'operator/api_detail/$1';
$route['api/operator/activity/(:any)/(:num)/(:num)'] = 'operator/api_activity/$1/$2/$3';

$route['modal/operator/tambah'] = 'operator/modal_operator_tambah';

$route['api/(:any)/(:any)/(:any)'] = '$1/api_$2_$3';
$route['api/(:any)/(:any)'] = '$1/api_$1_$2';

$route['modal/sekolah/operator/ubah'] = 'school/modal_operator_ubah';

//gallery

$route['api/gallery/delete/(:num)'] = 'gallery/delete/$1';
$route['api/gallery/show/(:num)/(:num)/(:any)'] = 'gallery/show/$1/$2/$3';

//Article
$route['display/district'] = 'article/article_district';
$route['display/employee'] = 'article/article_employee';
$route['display/school'] = 'article/article_school';
$route['display/percentage'] = 'article/article_percentage';
$route['display/statistic'] = 'article/article_statistic';
$route['display/registration'] = 'registrationop/index';
$route['display/gallery'] = 'gallery/index';

$route['modal/(:any)/(:any)'] = '$1/modal_$2';
$route['api/xls/ptk/sekolah/import'] = 'import/upload_parse_ptk_sekolah';
$route['api/xls/ptk/sekolah/import/(:num)'] = 'import/upload_parse_ptk_sekolah/$1';
$route['api/import/ptk/verify-data/confirm'] = 'import/confirm_import';
$route['api/import/ptk/verify-data/confirm/(:num)'] = 'import/confirm_import/$1';
$route['api/import/ptk/edit/(:num)'] = 'ptk/api_update_import_ptk';
$route['api/import/ptk/delete/(:num)'] = 'ptk/api_delete_import_ptk/$1';

$route['api/jenis-pegawai'] = 'employees/get';  