angular.module('diknasmlg').controller('detailDistrictModal', function (data, $scope, $uibModalInstance, NgTableParams,$uibModal, $DistrictFactory, $q) {

    var apiPath = localStorage.getItem('PATHLOC');

    $scope.detail = data;

    $scope.search = '';

    $scope.status = {
        0: 'Swasta',
        1: 'Negeri'
    };


    $scope.dismiss = function () {
        $uibModalInstance.dismiss();
    };

    $scope.onBtnDetailClicked = function(row) {
        $uibModal.open({
            templateUrl: apiPath + 'school/detail',
            controller: 'detailSchoolModal',
            resolve: {
                data: function () {
                    return row;
                }
            }
        })
    };
    $scope.onBtnDetailEmployeeClicked = function(row) {
        $uibModal.open({
            templateUrl: apiPath + 'Teacher/detail',
            controller: 'detailTeacherModal',
            size: 'lg',
            resolve: {
                data: function() {
                    return row;
                }
            }
        })
    };
    $scope.dataTKTable = new NgTableParams({}, {
        getData: function (params) {
            var count = params.count();
            var start = count * (params.page() - 1);
            var kecamatan = $scope.detail.nama;
            var tingkat = 'TK';

            return $q(function (resolve, reject) {
                $DistrictFactory.showschool(start, count, kecamatan, tingkat, function (reply) {
                    if (reply.error) return reject(reply.error);
                    params.total(reply.total);
                    return resolve(reply.data);
                })
            })
        }
    });
    $scope.dataSDTable = new NgTableParams({}, {
        getData: function (params) {
            var count = params.count();
            var start = count * (params.page() - 1);
            var kecamatan = $scope.detail.nama;
            var tingkat = 'SD';

            return $q(function (resolve, reject) {
                $DistrictFactory.showschool(start, count, kecamatan, tingkat, function (reply) {
                    if (reply.error) return reject(reply.error);
                    params.total(reply.total);
                    return resolve(reply.data);
                })
            })
        }
    });
    $scope.dataSMPTable = new NgTableParams({}, {
        getData: function (params) {
            var count = params.count();
            var start = count * (params.page() - 1);
            var kecamatan = $scope.detail.nama;
            var tingkat = 'SMP';

            return $q(function (resolve, reject) {
                $DistrictFactory.showschool(start, count, kecamatan, tingkat, function (reply) {
                    if (reply.error) return reject(reply.error);
                    params.total(reply.total);
                    return resolve(reply.data);
                })
            })
        }
    });
    $scope.teacherTable = new NgTableParams({}, {
        getData: function (params) {
            var count = params.count();
            var start = count * (params.page() - 1);
            var kecamatan = $scope.detail.id;
            var search = $scope.search;
            return $q(function (resolve, reject) {
                $DistrictFactory.showteacher(start, count, kecamatan, search, function (reply) {
                    if (reply.error) return reject(reply.error);
                    params.total(reply.total);
                    console.log(reply.data)
                    return resolve(reply.data);
                })
            })
        }
    });
    $scope.onInputSearchChange = function () {
        $scope.teacherTable.reload();
    };

});