angular.module('diknasmlg').controller('PtkRiwayatKerjaEdit', function(
    $scope,
    $uibModalInstance,
    $q,
    $SchoolFactory,
    $DistrictFactory,
    Notify,
    $RiwayatFactory,
    $routeParams,
    data
) {
    $scope.riwayat = {}

    $scope.dismiss = function() {
        $uibModalInstance.dismiss();
    }

    $scope.schoolList = []
    $scope.kotaList = []

    $scope.riwayat = {}

    $scope.unitKerjaPlace = [
        {
            id: 0,
            nama: 'Kota Malang'
        },
        {
            id:1 ,
            nama: 'Luar Kota Malang'
        }
    ]

    $scope.onKotaSearch = function (query) {
        $DistrictFactory.regency(query, function (reply) {
            $scope.kotaList = reply;
        });
    };

    $scope.onInputSearchChange = function (query) {
        return $SchoolFactory.get(query, function (reply) {
            $scope.schoolList = reply;
        });
    };

    function init() {
        $SchoolFactory.get('', function (reply) {
            $scope.schoolList = reply
        })

        $DistrictFactory.regency('', function (reply) {
            $scope.kotaList = reply;
        })

        $scope.riwayat.sk = data.sk
        $scope.riwayat.gaji = data.gaji
        $scope.riwayat.tmt = moment(data.tmt).toDate()
        $scope.riwayat.lokasi = !data.namaKota ? $scope.unitKerjaPlace[0] : $scope.unitKerjaPlace[1],
        $scope.riwayat.kotaUnitKerja = {name: data.namaKota, id: data.idKota}
        $scope.riwayat.namaUnitKerja = data.unitKerja
        $scope.riwayat.unitKerja = {nama: data.unitKerja, id: data.idUnitKerja}
    }

    $scope.onBtnSaveClicked = function () {
        var id = $scope.routes && $scope.routes.id ? $scope.routes.id : null
        $RiwayatFactory.editKerja(data.id, $scope.riwayat).then(function (reply) {
            if(reply && !reply.error) {
                Notify.success('Sukses', 'Berhasil mengubah riwayat kerja')
                $uibModalInstance.close();
            } else {
                Notify.error('Kesalahan', reply.error)
            }
        })
    }

    init()
    
})