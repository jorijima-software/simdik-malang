<?php
/**
 * Created by PhpStorm.
 * User: arsyad09
 * Date: 10/7/17
 * Time: 5:26 PM
 */
die();
$userinfo = $this->User_model->userinfo();
?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Dinas Pendidikan Kota Malang</title>
    <link rel="stylesheet" href="<?= base_url('bower_components/bootstrap/dist/css/bootstrap.min.css') ?>">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="<?= base_url('bower_components/font-awesome/css/font-awesome.min.css') ?>">
    <link rel="stylesheet" href="<?= base_url('bower_components/angular-bootstrap/ui-bootstrap-csp.css') ?>">
    <link rel="stylesheet" href="<?= base_url('bower_components/angular-ui-select/dist/select.min.css') ?>">
    <link rel="stylesheet" href="<?= base_url('bower_components/noty/lib/noty.css') ?>">
    <link rel="stylesheet" href="<?= base_url('dist/css/sly-horizontal.css') ?>">

    <link rel="stylesheet"
          href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
    <link href="https://fonts.googleapis.com/css?family=Oswald:400,500,600,700" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="<?= base_url('dist/css/margin.css') ?>">
    <link rel="stylesheet" href="<?= base_url('dist/css/index-display.css') ?>">
    <link rel="stylesheet" href="<?= base_url('dist/css/index-display.css') ?>">
    <link rel="stylesheet" media='screen and (max-width: 425px)'
          href="<?= base_url('dist/css/index-display(mobile-potrait).css') ?>">
    <link rel="stylesheet" media='screen and (min-width: 426px) and (max-width: 1024px)'
          href="<?= base_url('dist/css/index-display(mobile-landscape).css') ?>">

</head>
<body>
<?php
if($this->session->is_logged) { ?>
    <div class="nav navbar-nav up-navbar header-navbar" style="padding: 12px; font-weight: 600">
        <div class="container">
            <a class="navbar-brand" href="<?= site_url('display') ?>">
                <img src="<?= base_url('assets/logo-header.png') ?>">
            </a>
            <div class="collapse navbar-collapse" id="app-navbar-collapse">
                <ul class="nav navbar-nav navbar-right">
                    <li>
                        <a style="font-size: 12pt;color: green" href="<?= site_url('/Registrationop') ?>">REGISTER
                            OPERATOR</a>
                    </li>
                    <li class="dropdown user user-menu">
                        <a class="btn btn-user" style="font-size: 12pt;" class="dropdown-toggle" data-toggle="dropdown">
                            <span class="hidden-xs"><?php
                                if (is_null($userinfo->nama)) {
                                    echo $userinfo->username;
                                } else {
                                    echo $userinfo->nama;
                                }
                                ?></span>
                            <i class="fa fa-caret-down"></i>
                        </a>
                        <ul class="dropdown-menu dropdown-list ">
                            <li class="user-footer">
                                <div class="pull-left">
                                    <a href="<?= site_url('/dashboard') ?>" class="btn btn-primary btn-header">Mode Admin</a>
                                </div>
                                <div class="pull-right">
                                    <a href="<?= site_url('/logoutdisplay') ?>" class="btn btn-primary btn-header">Sign out</a>
                                </div>
                            </li>
                        </ul>
                    </li>
                </ul>
            </div>
        </div>
    </div>
<?php } else { ?>
    <div class="nav navbar-nav up-navbar header-navbar" style="padding: 12px; font-weight: 600">
        <div class="container">
            <a class="navbar-brand" href="<?= site_url('display') ?>">
                <img src="<?= base_url('assets/logo-header.png') ?>">
            </a>
            <div class="collapse navbar-collapse" id="app-navbar-collapse">
                <ul class="nav navbar-nav navbar-right">
                    <li>
                        <a style="font-size: 12pt;color: green" href="<?= site_url('/Logindisplay') ?>">LOGIN</a>
                    </li><li>
                        <a style="font-size: 12pt;color: green" href="<?= site_url('/Registrationop') ?>">REGISTER
                            OPERATOR</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
<?php }?>
<div class="body-background" data-parallax="scroll" data-image-src="<?= base_url('assets/background.png') ?>">
    <section class="header-display">
        <div class="force-center">
            <img src="<?= base_url('assets/logo.png') ?>" alt="" class="logo-display align-center">
        </div>
        <div class="row col-12">
            <h2 class="brand-header-display align-center">Sistem Informasi Manajemen<br>Dinas Pendidikan Kota Malang
            </h2>
            <h5 class="detail-header align-center"><p>Database Terlengkap Dinas Pendidikan Kota Malang.</p></h5>
        </div>
        <div class="row">
            <div class="force-center">
                <!--<div class="input-group search-header">
                        <span class="input-group-btn">
                             <button class="btn btn-default search-radius-button" readonly="true" type="button"><i class="fa fa-search"></i></button>
                        </span>
                    <input type="text" class="form-control search-radius-input" ng-keyup="onInputSearchChange()" placeholder="Search for...">
                </div>-->
            </div>
        </div>
        <!--<img src="<?/*= base_url('assets/abah-anton.png') */?>" alt="" class="abah-anton pull-right">-->
    </section>
    <section class="sly-scroller">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="wrap">
                        <div id="centered" class="frame">
                            <ul class="clearfix">
                                <li class="divided">
                                    <div class="divided-image"
                                         style="background-image: url('<?= base_url('assets/card-statistic.png') ?>');"></div>
                                    <div class="divided-info">
                                        <div class="divided-info-detail">
                                            <div class="divided-info-button">
                                                <a href="<?= site_url('display/statistic') ?>"
                                                   class="btn btn-config-card btn-bg-orange">DATA STATISTIK</a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="divided-desc">
                                        <h5 class="align-center"> Data Statistik Tentang Sekolah dan Pegawai Di Kota
                                            Malang</h5>
                                    </div>
                                </li>
                                <li class="divided">
                                    <div class="divided-image"
                                         style="background-image: url('<?= base_url('assets/card-employee.png') ?>');"></div>
                                    <div class="divided-info">
                                        <div class="divided-info-detail">
                                            <div class="divided-info-button">
                                                <a href="<?= site_url('display/employee') ?>"
                                                   class="btn btn-config-card btn-bg-green">DATA PEGAWAI</a>
                                            </div>
                                            <h4 class="title-md align-center"></h4>
                                        </div>
                                    </div>
                                    <div class="divided-desc">
                                        <h5 class="align-center"> Data Pegawai TK,SD,SMP <br>Di Kota Malang</h5>
                                    </div>
                                </li>
                                <li class="divided">
                                    <div class="divided-image"
                                         style="background-image: url('<?= base_url('assets/card-school.png') ?>');"></div>
                                    <div class="divided-info">
                                        <div class="divided-info-detail">
                                            <div class="divided-info-button">
                                                <a href="<?= site_url('display/school') ?>"
                                                   class="btn btn-config-card btn-bg-blue">DATA SEKOLAH</a>
                                            </div>
                                            <h4 class="title-md align-center"></h4>
                                        </div>
                                    </div>
                                    <div class="divided-desc">
                                        <h5 class="align-center"> Data Sekolah TK,SD,SMP <br>Di Kota Malang</h5>
                                    </div>
                                </li>
                                <li class="divided">
                                    <div class="divided-image"
                                         style="background-image: url('<?= base_url('assets/card-report.png') ?>');"></div>
                                    <div class="divided-info">
                                        <div class="divided-info-detail">
                                            <div class="divided-info-button">
                                                <a href="<?= site_url('display/district') ?>"
                                                   class="btn btn-config-card btn-bg-purple">DATA KECAMATAN</a>
                                            </div>
                                            <h4 class="title-md align-center"></h4>
                                        </div>
                                    </div>
                                    <div class="divided-desc">
                                        <h5 class="align-center"> Data Kecamatan Beserta Jumlah Sekolah dan Pegawai</h5>
                                    </div>
                                </li>
                                <li class="divided">
                                    <div class="divided-image"
                                         style="background-image: url('<?= base_url('assets/card-man.png') ?>');"></div>
                                    <div class="divided-info">
                                        <div class="divided-info-detail">
                                            <div class="divided-info-button">
                                                <a href="<?= site_url('display/percentage') ?>"
                                                   class="btn btn-config-card btn-bg-pink"
                                                   style="height:50px; line-height: 20px">PROSENTASE<br>PNS/CPNS</a>
                                            </div>
                                            <h4 class="title-md align-center"></h4>
                                        </div>
                                    </div>
                                    <div class="divided-desc">
                                        <h5 class="align-center"> Data Prosentase PNS/CPNS <br>Di Kota Malang</h5>
                                    </div>
                                </li>
                            </ul>
                        </div>
                        <div class="controls center">
                            <button class="btn btn-prev prev"><i class="fa fa-chevron-left"></i></button>
                            <button class="btn btn-next next"><i class="fa fa-chevron-right"></i></button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
<section class="footer-display">
    <div class="top-footer">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <table class="logo" style="margin-top: 10px;text-align: center">
                        <tr>
                            <td><img src="<?= base_url('assets/kemendikbud.png') ?>" width="70px"
                                                 height="70px" style="margin-right: 10px">&nbsp&nbsp
                            </td>
                            <td class="footer-text" style="width: 210px">KEMENTERIAN PENDIDIKAN<br> DAN KEBUDAYAAN</td>

                            <td><img src="<?= base_url('assets/logo.png') ?>" width="70px" height="70px"
                                                 style="margin-left: 30px;margin-right: 10px;">&nbsp&nbsp
                            </td>
                            <td class="footer-text" style="width: 110px;">PEMERINTAH<br> KOTA MALANG</td>

                            <td><img src="<?= base_url('assets/logo-jatim.png') ?>" width="60px"
                                                 height="70px" style="margin-left: 40px;margin-right: 10px">&nbsp&nbsp
                            </td>
                            <td class="footer-text" style="width: 200px;">PEMERINTAH<br>PROVINSI JAWA
                                TIMUR
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
            <hr style="margin-bottom: 5px">
            <div class="row">
                <div class="col-12">
                    <table style="width: 100%;text-align: center">
                        <tr>
                            <td width="100%">
                                <span class="footer-text">DINAS PENDIDIKAN KOTA MALANG</span>
                            </td>
                        </tr>
                        <tr>
                            <td width="100%">
                                <span class="footer-text" >Jl. Veteran No.19, Ketawanggede, Kec.Lowokwaru, Jawa Timur 65145, Indonesia</span>
                            </td>
                        </tr>
                        <tr>
                            <td width="100%">
                                <span class="footer-text">Telp. (0341) 551333; E-mail: disdik@malangkota.go.id</span>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <div class="bottom-footer"></div>
</section>
<!-- jQuery 3 -->
<script src="<?= base_url('bower_components/jquery/dist/jquery.min.js') ?>"></script>
<!-- Bootstrap 3.3.7 -->
<script src="<?= base_url('bower_components/bootstrap/dist/js/bootstrap.min.js') ?>"></script>
<!-- AdminLTE App -->
<script src="<?= base_url('dist/js/adminlte.min.js') ?>"></script>

<script src="<?= base_url('bower_components/noty/lib/noty.min.js') ?>"></script>

<script src="<?= base_url('bower_components/angular/angular.js') ?>"></script>
<script src="<?= base_url('bower_components/ng-table/bundles/ng-table.min.js') ?>"></script>
<script src="<?= base_url('bower_components/angular-bootstrap/ui-bootstrap-tpls.min.js') ?>"></script>
<script src="<?= base_url('bower_components/angular-sanitize/angular-sanitize.min.js') ?>"></script>

<script src="<?= base_url('bower_components/angular-ui-select/dist/select.min.js') ?>"></script>
<script src="<?= base_url('bower_components/sly/dist/sly.min.js') ?>"></script>
<script src="<?= base_url('bower_components/angular-google-chart/ng-google-chart.min.js') ?>"></script>


<script src="<?= base_url('angular/app.js') ?>"></script>
<script src="<?= base_url('angular/factory/teacher.js') ?>"></script>
<script src="<?= base_url('angular/factory/employee.js') ?>"></script>
<script src="<?= base_url('angular/factory/school.js') ?>"></script>

<script src="<?= base_url('angular/services/notify.js') ?>"></script>
<script src="<?= base_url('dist/js/horizontal.js') ?>"></script>
<script src="<?= base_url('dist/js/parallax.js') ?>"></script>

<script>
    $(window).scroll(function() {
        var header = $('.header-navbar'),
            scroll = $(window).scrollTop();

        if (scroll >= 1){
            header.addClass('fix-navbar').removeClass('up-navbar');
        }
        else {
            header.addClass('up-navbar').removeClass('fix-navbar');
        }

    });
</script>
</body>
</html>