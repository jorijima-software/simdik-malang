<?php
/**
 * Created by PhpStorm.
 * User: ragasubekti
 * Date: 1/18/18
 * Time: 8:43 PM
 */
?>

<div class="modal-header">
    <div class="modal-title">
        Edit Riwayat Kerja
    </div>
</div>

<div class="modal-body">
    <div class="form-group">
        <label>No. SK</label>
        <input type="text" class="form-control" ng-model="riwayat.sk">
    </div>
    <div class="form-group">
        <label>TMT</label>
        <div class="input-group">
            <input type="text" class="form-control" uib-datepicker-popup="dd-MM-yyyy"
                   ng-model="riwayat.tmt"
                   datepicker-options="dateOptions"
                   close-text="Close" is-open="datepicker" placeholder="dd-mm-yyyy"
                   ng-click="datepicker = !datepicker"/>
            <span class="input-group-btn">
                <button type="button" class="btn btn-default" ng-click="datepicker = !datepicker">
                    <i class="glyphicon glyphicon-calendar"></i>
                </button>
            </span>
        </div>
        <p class="help-block">Format: Tanggal-Bulan-Tahun</p>
    </div>
    <div class="form-group">
        <label>Gaji</label>
        <div class="input-group">
            <input type="text" class="form-control" ng-model="riwayat.gaji" ng-currency>
            <div class="input-group-addon">Rupiah</div>
        </div>
    </div>
    <div class="form-group">
        <label>Unit Kerja</label>
        <ui-select ng-model="riwayat.lokasi" theme="bootstrap" search-enabled="false">
            <ui-select-match placeholder="Pilih Lokasi Unit Kerja">{{$select.selected.nama}}
            </ui-select-match>
            <ui-select-choices repeat="item in unitKerjaPlace track by $index"
                               refresh="onInputSearchChange($select.search)" refresh-delay="0">
                {{item.nama}}
            </ui-select-choices>
        </ui-select>
    </div>
    <div class="form-group" ng-if="riwayat.lokasi && riwayat.lokasi.id == 0">
        <label>Nama Unit Kerja</label>
        <ui-select ng-model="riwayat.unitKerja" theme="bootstrap">
            <ui-select-match placeholder="Ketik atau pilih unit kerja">{{$select.selected.nama}}
            </ui-select-match>
            <ui-select-choices repeat="item in schoolList track by $index"
                               refresh="onInputSearchChange($select.search)" refresh-delay="0">
                {{item.nama}}
            </ui-select-choices>
        </ui-select>
    </div>
    <div class="form-group" ng-if="riwayat.lokasi && riwayat.lokasi.id == 1">
        <label>Kota</label>
        <ui-select ng-model="riwayat.kotaUnitKerja" theme="bootstrap">
            <ui-select-match placeholder="Ketik atau pilih kota">{{$select.selected.name}}
            </ui-select-match>
            <ui-select-choices repeat="item in kotaList track by $index"
                               refresh="onKotaSearch($select.search)" refresh-delay="0">
                {{item.name}}
            </ui-select-choices>
        </ui-select>
    </div>
    <div class="form-group" ng-if="riwayat.lokasi && riwayat.lokasi.id == 1">
        <label>Nama Unit Kerja</label>
        <input type="text" class="form-control" ng-change="riwayat.namaUnitKerja = riwayat.namaUnitKerja.toUpperCase()" ng-model="riwayat.namaUnitKerja">
    </div>
</div>
<div class="modal-footer">
    <button class="btn btn-primary" ng-click="onBtnSaveClicked()">Simpan</button>
    <button class="btn btn-default" ng-click="dismiss()">Batal</button>
</div>