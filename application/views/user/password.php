<?php
/**
 * Created by PhpStorm.
 * User: ragasubekti
 * Date: 04/11/17
 * Time: 13.33
 */
?>


<div ng-controller="schoolProfileController">
    <section class="content-header">
        <h1>
            Ubah Password
        </h1>
    </section>

    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="box">
                    <div class="box-body">
                        <?php if(isset($result['error'])) { ?>
                        <div class="alert alert-danger">
                            <i class="fa fa-times"></i> <strong>ERROR!</strong> &nbsp; <?=$result['error']?>
                        </div>
                        <?php } else if (isset($result['success'])) { ?>
                        <div class="alert alert-success">
                            <i class="fa fa-check"></i> <strong>SUCCESS!</strong> &nbsp; Berhasil mengubah kata sandi.
                        </div>
                        <?php } ?>
                        <div class="row">
                            <div class="col-md-2"></div>
                            <div class="col-md-8">
                                <form action="<?= site_url('user/password') ?>" class="form-horizontal" method="post">
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">
                                            Password Lama
                                        </label>
                                        <div class="col-sm-10">
                                            <input type="password" class="form-control" name="current">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">
                                            Password Baru
                                        </label>
                                        <div class="col-sm-10">
                                            <input type="password" class="form-control" name="new">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">
                                            Ulangi Password
                                        </label>
                                        <div class="col-sm-10">
                                            <input type="password" class="form-control" name="verify">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-sm-2"></div>
                                        <div class="col-sm-10">
                                            <button class="btn btn-primary btn-sm">
                                                <i class="fa fa-check"></i> <span>Simpan</span>
                                            </button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <div class="col-md-2"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>

