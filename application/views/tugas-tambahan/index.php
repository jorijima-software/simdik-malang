<?php
/**
 * Created by PhpStorm.
 * User: ragasubekti
 * Date: 29/11/17
 * Time: 06.28
 */
?>


    <div ng-controller="tugasTambahanController">

        <section class="content-header">
            <h1>
                Data Tugas Tambahan
            </h1>
        </section>

        <section class="content">
            <div class="row">
                <div class="col-md-12">
                    <div class="box">
                        <div class="box-body">
                            <table class="table table-striped table-condensed" ng-table="tugasTable">
                                <thead>
                                    <tr>
                                        <th>
                                            No.
                                        </th>
                                        <th>Nama Tugas Tambahan</th>
                                        <th>Jumlah</th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr ng-repeat="row in $data">
                                        <td>{{row.id}}</td>
                                        <td>{{row.nama}}</td>
                                        <td>{{row.total}}</td>
                                        <td>
                                            <div class="pull-right">
                                                <button class="btn btn-xs btn-primary" ng-click="onBtnDetailClicked(row)">
                                                    <i class="fa fa-eye"></i>
                                                    <span>Detail</span>
                                                </button>
                                            </div>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </section>

    </div>