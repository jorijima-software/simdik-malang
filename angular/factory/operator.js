// Created by arsyad09 at 08/10/17
angular.module('diknasmlg').factory('$OperatorFactory', function ($http, $q) {


    var apiPath = localStorage.getItem('PATHLOC');

    var factory = {};

    factory.show = function (start, count, search, filter, callback) {
        $http.get(apiPath + 'api/operator/show/' + start + '/' + count + '/' + search + '?' + jQuery.param(filter)).then(
            function (reply) {
                if (reply.status == 200) {
                    return callback(reply.data);
                } else {
                    console.log(reply)
                }
            }
        )
    };

    factory.shows = function (start, count, search, filter) {
        return $q(function (resolve, reject) {
            $http.get(apiPath + 'api/operator/show/' + start + '/' + count + '/' + search + '?' + jQuery.param(filter)).then(
                function (reply) {
                    if (reply.status == 200) {
                        return resolve(reply.data);
                    } else {
                        return reject(reply);
                    }
                }
            )
        });
    };

    factory.create = function (data, callback) {
        $http.post(apiPath + 'api/operator/create', {
            nama_operator: data.nama_operator,
            asal_sekolah: data.asal_sekolah,
            no_hp: data.no_hp,
            no_wa: data.no_wa
        }).then(
            function (reply) {
                if (reply.status == 200) {
                    return callback(reply.data);
                } else {
                    console.log(reply)
                }
            }
        )
    };

    factory.inserts = function (data) {
        return $q(function (s, r) {
            $http.post(apiPath + 'api/operator/insert', {
                nama_operator: data.nama,
                asal_sekolah: data.unit_kerja,
                no_hp: data.no_hp,
                no_wa: data.no_wa
            }).then(
                function (reply) {
                    if (reply.status == 200) {
                        return s(reply.data);
                    } else {
                        return r(reply);
                    }
                }
            )
        });
    };

    factory.deletes = function (id) {
        return $q(function (r, x) {
            $http.post(apiPath + 'api/operator/delete/' + id, {}).then(
                function (reply) {
                    if (reply.status == 200) {
                        return r(reply.data);
                    } else {
                        return x(reply)
                    }
                }
            )
        })
    };

    factory.update = function (data, callback) {
        $http.post(apiPath + 'api/operator/update', {
            id: data.id,
            nama_operator: data.nama_operator,
            asal_sekolah: data.asal_sekolah,
            no_hp: data.no_hp,
            no_wa: data.no_wa
        }).then(
            function (reply) {
                if (reply.status == 200) {
                    return callback(reply.data);
                } else {
                    console.log(reply)
                }
            }
        )
    };

    factory.updates = function (data) {
        return $q(function (s, r) {
            $http.post(apiPath + 'api/operator/update', {
                id: data.id,
                nama_operator: data.nama,
                asal_sekolah: data.unit_kerja,
                no_hp: data.no_hp,
                no_wa: data.no_wa
            }).then(
                function (reply) {
                    if (reply.status == 200) {
                        return s(reply.data);
                    } else {
                        return r(reply);
                    }
                }
            )
        })
    };

    factory.detail = function (npsn) {
        return $q(function (resolve, reject) {
            $http.get(apiPath + 'api/operator/detail/' + npsn).then(function (r) {
                if (r.status === 200) {
                    return resolve(r.data);
                } else {
                    return reject(r);
                }
            })
        })
    };

    factory.activity = function (npsn, start, count) {
        return $q(function (resolve, reject) {
            $http.get(apiPath + 'api/operator/activity/' + npsn + '/' + start + '/' + count).then(function (r) {
                if (r.status === 200) {
                    return resolve(r.data);
                } else {
                    return reject(r);
                }
            })
        });
    };

    return factory;

});