angular.module('diknasmlg').controller('tambahDataMengajar', function ($scope, $uibModalInstance, data, edit, Notify, $TeacherFactory, $LessonFactory) {

    $scope.mapelList = [];
    $scope.teacherList = [];

    if(!edit) {
        $scope.new = {
            guru: {}
        };
    } else {
        $scope.new = {
            id: data.id_tugas,
            guru: data
        }
    }

    $scope.onInputSearchChange = function (query) {
        $TeacherFactory.selectTeacher(query, function (reply) {
            $scope.teacherList = reply.data;
        })
    };

    $scope.onBtnSaveClicked = function () {
        if (!edit) {
            $TeacherFactory.buatGuru($scope.new, function (reply) {
                if (reply.error) {
                    Notify.error('Kesalahan', reply.error);
                } else {
                    Notify.success('Sukses', 'Data guru berhasil ditambahkan');
                    $uibModalInstance.close();
                }
            })
        } else {
            $TeacherFactory.updateGuru($scope.new, function(reply) {
                if (reply.error) {
                    Notify.error('Kesalahan', reply.error);
                } else {
                    Notify.success('Sukses', 'Data guru berhasil diubah');
                    $uibModalInstance.close();
                }
            })
        }
    };

    $scope.dismiss = function () {
        $uibModalInstance.dismiss();
    };

    function init() {
        $LessonFactory.get('', function (reply) {
            $scope.mapelList = reply;
        });

        $TeacherFactory.selectTeacher('', function (reply) {
            $scope.teacherList = reply.data;
        });
    }

    init();

});