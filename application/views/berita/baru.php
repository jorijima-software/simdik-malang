<?php
/**
 * Created by PhpStorm.
 * User: ragasub
 * Date: 12/1/17
 * Time: 11:08 AM
 */
?>

<div ng-controller="buatBeritaController" ng-cloak>

    <section class="content-header">
        <h1>Buat Berita Baru</h1>
    </section>

    <section class="content">
        <div class="box">
            <div class="box-body">
                <form class="form-horizontal">
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Kepada</label>
                        <div class="col-sm-10">
                        <ui-select ng-model="new.kepada" theme="bootstrap">
                            <ui-select-match placeholder="PILIH atau KETIK target">{{$select.selected.nama}}</ui-select-match>
                            <ui-select-choices repeat="item in targetList track by $index" group-by="'target'" refresh="onTargetChange($select.search)" refresh-delay="0">
                                {{item.nama}}
                            </ui-select-choices>
                        </ui-select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Judul</label>

                        <div class="col-sm-10">
                            <input type="text" class="form-control" placeholder="Judul Berita" ng-model="new.judul">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Isi Berita</label>
                        <div class="col-sm-10">
                            <summernote height="400" ng-model="new.isi"></summernote>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-2"></div>
                        <div class="col-sm-10">
                            <button class="btn btn-primary btn-sm" ng-click="onBtnSaveClicked()">
                                <i class="fa fa-check"></i> <span>Simpan</span>
                            </button>

                            <a class="btn btn-default btn-sm" href="#!/berita">
                                <i class="fa fa-times"></i> <span>Batal</span>
                            </a>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </section>

</div>