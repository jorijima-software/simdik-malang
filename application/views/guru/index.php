<?php
/**
 * Created by PhpStorm.
 * User: ragasubekti
 * Date: 17/11/17
 * Time: 22.04
 */
?>
<div ng-controller="guruController">

    <section class="content-header">
        <h1>Data Guru</h1>
    </section>

    <section class="content">
        <div class="row hidden-sm hidden-xs">
            <div class="col-lg-4 col-xs-6">
                <div class="small-box bg-red">
                    <div class="inner">
                        <h3>{{statsbox.kurang}}</h3>

                        <p>Kurang Dari 24 Jam</p>
                    </div>
                    <div class="icon">
                        <i class="fa fa-times"></i>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-xs-6">
                <div class="small-box bg-green">
                    <div class="inner">
                        <h3>{{statsbox.lebih}}</h3>

                        <p>Lebih Dari 24 Jam</p>
                    </div>
                    <div class="icon">
                        <i class="fa fa-check"></i>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-xs-6">
                <div class="small-box bg-yellow">
                    <div class="inner">
                        <h3>{{statsbox.total}}</h3>

                        <p>Total Guru</p>
                    </div>
                    <div class="icon">
                        <i class="fa fa-list"></i>
                    </div>
                </div>
            </div>
        </div>
        <div class="box">
            <div class="box-body">
                <div class="row">
                    <div class="col-md-9">
                        <button class="btn btn-primary btn-sm" style="margin-bottom: 10px" ng-click="onBtnAddClicked()" ng-disabled="timerEnd">
                            <i class="fa fa-plus"></i> <span>Tambah</span>
                        </button>
                    </div>
                    <div class="col-md-3">
                        <div class="input-group">
                            <input type="text" class="form-control" placeholder="Pencarian" ng-model="search" ng-keyup="onSearchChange()">
                            <div class="input-group-addon">
                                <i class="fa fa-search"></i>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="table-responsive">
                    <table class="table table-striped table-condensed" ng-table="dataGuruTable">
                        <thead>
                        <tr>
                            <th>
                                No
                            </th>
                            <th>
                                NUPTK
                            </th>
                            <th>Nama</th>
                            <th>
                                Jenis
                            </th>
                            <th>Unit Kerja</th>
                            <th>
                                Jumlah Jam
                            </th>
                            <th>
                                Tugas Mengajar
                            </th>
                            <!--                        <th>Sertifikasi</th>-->
                            <th></th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr ng-repeat="row in $data">
                            <td>{{($index + 1) + (dataGuruTable._params.count * (dataGuruTable._params.page -1))}}</td>
                            <td>{{row.nuptk}}</td>
                            <td>{{row.namaPtk}}</td>
                            <td>{{row.jenis}}</td>
                            <td>{{row.unitKerja}}</td>
                            <td>
                                <span class="label label-danger" ng-if="row.jumlahJam < 24">{{row.jumlahJam}} Jam</span>
                                <span class="label label-success"
                                      ng-if="row.jumlahJam >= 24">{{row.jumlahJam}} Jam</span></td>
                            <td>
                                <a class="btn btn-xs btn-primary"
                                   href="<?= site_url('/') ?>#!/guru/tugas-mengajar/{{row.id}}">
                                    <i class="fa fa-eye"></i>
                                </a> &nbsp;
                                {{row.tugasMengajar}}
                            </td>
                            <td>
                                <div class="pull-right">
                                    <button class="btn btn-xs btn-primary" ng-click="onBtnEditClicked(row)" ng-disabled="timerEnd">
                                        <i class="fa fa-pencil"></i> <span>Edit</span>
                                    </button>
                                    <button class="btn btn-danger btn-xs" ng-click="onBtnDeleteClicked(row)" ng-disabled="timerEnd">
                                        <i class="fa fa-times"></i> <span>Hapus</span>
                                    </button>
                                </div>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </section>

</div>