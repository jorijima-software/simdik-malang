<?php

class RiwayatPangkatPtk extends \Illuminate\Database\Eloquent\Model
{
    protected $table = 'pangkat_guru';
    protected $guarded = [];
    public $timestamps = false;

    public function ptk() {
        $CI =& get_instance();
        $CI->load->model('Ptk');

        return $this->belongsTo('Ptk', 'id', 'id_data_guru');
    }

}