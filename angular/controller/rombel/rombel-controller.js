angular.module('diknasmlg').controller('rombelController', function($scope, $uibModal, $q, NgTableParams, $RombelFactory, Notify, $TugasFactory) {

    var apiPath = localStorage.getItem('PATHLOC');

    $scope.tapel = {};

    $scope.tapelList = [];
    $scope.statsbox = [];

    $scope.onBtnAddClicked = function() {
        $uibModal.open({
            templateUrl: apiPath + 'school/modal_rombel_tambah',
            controller: 'tambahRombel',
            resolve: {
                data: function() {
                    return null;
                }
            },
            backdrop: 'static'
        }).result.then(function() {
            $scope.rombelTable.reload();
            reloadStats();
        }, function () {
            $scope.rombeltable.reload();
            reloadstats();
        })
    };

    $scope.onBtnEditClicked = function(row) {
        $uibModal.open({
            templateUrl: apiPath + 'school/modal_rombel_tambah',
            controller: 'tambahRombel',
            resolve: {
                data: function() {
                    return row;
                }
            },
            backdrop: 'static'
        }).result.then(function() {
            $scope.rombelTable.reload();
            reloadStats();
        }, function () {
            $scope.rombelTable.reload();
            reloadStats();
        });
    };

    $scope.onBtnDeleteAllClicked = function(group) {
        var tapel = group[0].tahun_pelajaran;
        var tingkat = group[0].tingkat;
        Notify.confirm('Apaka anda yakin ingin menghapus semua rombel Tahun Pelajaran <strong>' + tapel  + '</strong> Tingkat <strong>'+tingkat+'</strong>', function() {
            $RombelFactory.deleteAll(group).then(function(r) {
                if(!r.error) {
                    Notify.success('Sukses!', 'Rombel berhasil dihapus');
                    $scope.rombelTable.reload();
                } else {
                    Notify.error('Kesalahan', r.error);
                }
            })
        }, function() {

        })
    };

    $scope.onBtnDeleteClicked = function(row) {
        Notify.confirm('Apakah anda yakin ingin menghapus rombel ini?', function () {
            $RombelFactory.delete(row.id).then(function (reply) {
                if(reply.error) {
                    Notify.error('Kesalahan', reply.error);
                } else {
                    Notify.success('Sukses', 'Data rombel berhasil dihapus');
                }
                $scope.rombelTable.reload();
                reloadStats();
            });
        }, function () {
        });
    };

    $scope.rombelTable = new NgTableParams({
        group: 'tingkat',
        sorting: { tingkat: "ASC" }
    }, {
        counts: [],
        getData: function(params) {
            var tapel = ($scope.tapel && $scope.tapel && $scope.tapel.active) ? $scope.tapel.active.id : 0;
            return $q(function(resolve, reject) {
                $RombelFactory.show(tapel).then(function(reply) {
                    if(reply.error) return reject(reply.error);
                    params.total(reply.total);
                    return resolve(reply.data);
                });
            });
        }
    });

    $scope.onBtnEditClicked = function(row) {
        $uibModal.open({
            templateUrl: apiPath + 'modal/rombel/edit_row',
            controller: 'editRowRombel',
            resolve: {
                data: function() {
                    return row;
                }
            }
        }).result.then(function() {
            $scope.rombelTable.reload();
        }, function () {
            $scope.rombelTable.reload();
        })
    };

    $scope.onTapelSelect = function() {
        $scope.rombelTable.reload();
        console.log($scope.tapel)
    };

    function reloadStats() {
        $RombelFactory.stats().then(function (reply) {
            $scope.statsbox = reply;
        })
    }

    function init() {
        $RombelFactory.stats().then(function(reply) {
            $scope.statsbox = reply;
        });

        $TugasFactory.getTapel('', function (reply) {
            $scope.tapelList = reply;

            reply.map(function (data) {
                if(data.active == 1) {
                    $scope.tapel.active = data;
                }
            });

            $scope.rombelTable.reload();
        });
    }

    init();

});