/*
    Created by Raga Subekti
    at  29/11/17 00.35 by PhpStorm
*/

angular.module('diknasmlg').controller('guruController', function (
    $scope, 
    $uibModal, 
    $q, 
    $GuruFactory, 
    NgTableParams, 
    Notify,
    $TimerFactory
) {

    var apiPath = localStorage.getItem('PATHLOC');

    $scope.timerEnd
    $scope.statsbox = {};
    $scope.search = '';

    $scope.onBtnAddClicked = function () {
        $uibModal.open({
            templateUrl: apiPath + 'teacher/modal_mengajar_tambah',
            controller: 'tambahGuru',
            resolve: {
                data: function () {
                    return null;
                },
                edit: function () {
                    return false;
                }
            }
        }).result.then(refreshTable, refreshTable)
    };

    $scope.dataGuruTable = new NgTableParams({}, {
        getData: function (params) {
            var count = params.count();
            var start = count * (params.page() - 1);
            var filter = {};
            filter.search = $scope.search;
            return $q(function (resolve, reject) {
                $GuruFactory.show(start, count, filter).then(function (reply) {
                    if (reply.error) return reject(reply.error);
                    params.total(reply.total);
                    return resolve(reply.data);
                })
            })
        }
    });

    $scope.onBtnEditClicked = function (row) {
        $uibModal.open({
            templateUrl: apiPath + 'teacher/modal_mengajar_tambah',
            controller: 'tambahGuru',
            resolve: {
                data: function () {
                    return row;
                },
                edit: function () {
                    return true;
                }
            }
        }).result.then(refreshTable, refreshTable)
    };

    $scope.onBtnDeleteClicked = function (row) {
        Notify.confirm('Apakah anda yakin ingin menghapus data tersebut?', function () {
            $GuruFactory.delete(row.idTugas).then(function (reply) {
                if (reply.error) {
                    Notify.error('Kesalahan', reply.error);
                } else {
                    Notify.success('Sukses', 'Berhasil menghapus data guru');
                }

                refreshTable();
            })
        }, function () {
        })
    };

    $scope.onSearchChange = function () {
        $scope.dataGuruTable.reload();
    };

    function refreshTable() {
        $scope.dataGuruTable.reload();
    }

    function init() {
        $GuruFactory.stats().then(function (reply) {
            if (reply.error) {

            } else {
                $scope.statsbox = reply;
            }
        })

        $TimerFactory.diff().then(result => {
            $scope.timerEnd = (!result.admin && result.disabled)
        })
    }

    init();

});