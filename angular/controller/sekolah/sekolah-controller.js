// Created by Raga Subekti at 08/10/17
angular.module('diknasmlg').controller('sekolahController', function ($scope, $SchoolFactory, NgTableParams, $uibModal, $q, $DistrictFactory, Notify) {

    var apiPath = localStorage.getItem('PATHLOC');

    $scope.bgIndex = [
        'bg-aqua',
        'bg-yellow',
        'bg-green',
        'bg-orange',
        'bg-red'
    ];

    $scope.statsbox = {};

    $scope.educationList = [];
    $scope.kecamatanList = [];
    $scope.statusSekolah = [
        {id: 0, nama: 'Swasta'},
        {id: 1, nama: 'Negeri'}
    ]

    $scope.search = '';

    $scope.filter = {
        kecamatan: {},
        tingkat: {}
    };

    $scope.onBtnAddClicked = function () {
        $uibModal.open({
            templateUrl: apiPath + 'school/add',
            size: 'lg',
            controller: 'tambahSekolah'
        }).result.then(refreshTable, refreshTable)
    };

    $scope.onPersentaseClicked = function (row) {
        $uibModal.open({
            templateUrl: apiPath + 'school/modalPersentase',
            size: 'md',
            controller: 'SekolahPersentase',
            resolve: {
                data: function () {
                    return row
                }
            }
        })
    }

    $scope.onBtnExportClicked = function() {
        $uibModal.open({
            templateUrl: apiPath + 'modal/school/export',
            controller: 'SekolahExport'
        })
    }


    $scope.onBtnEditClicked = function (row) {
        $uibModal.open({
            templateUrl: apiPath + 'school/edit',
            size: 'lg',
            resolve: {
                data: function () {
                    return row;
                }
            },
            controller: 'editSekolah'
        }).result.then(refreshTable, refreshTable)
    };

    $scope.onBtnResetClicked = function (row) {
        Notify.confirm('Apakah anda yakin ingin <i>mereset</i> password sekolah <code>' + row.namaSekolah + '</code>', function () {
            $SchoolFactory.resetPassword(row.npsn).then(function(r) {
                if(r.error) {
                    Notify.error('Kesalahan', r.error);
                } else {
                    Notify.success('Sukses', 'Berhasil mereset password');
                }
            })
        }, function () {

        })
    };

    $scope.onInputSearchChange = function () {
        $scope.schoolTable.reload();
    };

    $scope.onBtnDeleteClicked = function (row) {
        // console.log(row)
        Notify.confirm('Apakah anda yakin ingin menghapus sekolah <code>' + row.namaSekolah + '</code>', function () {
            $SchoolFactory.delete(row.id, function (reply) {
                $scope.schoolTable.reload();

                if (reply.error) {
                    Notify.error('Kesalahan', reply.error);
                } else {
                    Notify.success('Sukses', 'Data sekolah berhasil dihapus');
                }
            });

        }, function () {
        });


    };

    $scope.onBtnDetailClicked = function (row) {
        $uibModal.open({
            templateUrl: apiPath + 'school/detail',
            controller: 'detailSchoolModal',
            size: 'lg',
            resolve: {
                data: function () {
                    return row;
                }
            }
        })
    };

    $scope.schoolTable = new NgTableParams({}, {
        getData: function (params) {
            var count = params.count();
            var start = count * (params.page() - 1);
            var search = $scope.search;
            var filter = $scope.filter;
            filter.search = search;

            return $q(function (resolve, reject) {
                $SchoolFactory.show(start, count, search, filter, function (reply) {
                    if (reply.error) return reject(reply.error);
                    params.total(reply.total);
                    return resolve(reply.data);
                })
            })
        }
    });

    $scope.onSearchKeydown = function($event) {
        if($event.keyCode == 13) {
            $scope.doSearch()
        }
    }

    $scope.doSearch = function() {
        $scope.schoolTable.reload()
    }

    $scope.onFilter = function () {
        refreshTable()
    }

    function refreshTable() {
        $scope.schoolTable.reload();
    }

    function init() {
        $SchoolFactory.statsbox(function (reply) {
            $scope.statsbox = reply;
        });

        $DistrictFactory.get(function (reply) {
            $scope.kecamatanList = reply;

            $scope.kecamatanList.unshift({id: 0, nama: 'Semua'});
        });

        $SchoolFactory.type(function (reply) {
            $scope.educationList = reply;

            $scope.educationList.unshift({id: 0, nama: 'Semua'});
        });

    }

    init();

});