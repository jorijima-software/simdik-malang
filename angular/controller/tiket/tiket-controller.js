/*
    Created by Raga Subekti
    at  13/12/17 22.31 by PhpStorm
*/
angular.module('diknasmlg').controller('tiketController', function ($scope, $q, $TiketFactory, NgTableParams, $uibModal, Notify) {


    function API(path) {
        return localStorage.getItem('PATHLOC') + path;
    }

    $scope.groupData = {
        0: 'Data Operator',
        1: 'Data Profil Sekolah',
        2: 'Data PTK',
        3: 'Data Guru',
        4: 'Data Rombel'
    };

    $scope.tiketTable = new NgTableParams({}, {
        getData: function (params) {
            var count = params.count();
            var start = count * (params.page() - 1);
            var filter = {};

            // TODO: Add filter

            return $q(function (resolve, reject) {
                $TiketFactory.show(start, count, filter).then(function (r) {
                    if (!r.error) {
                        params.total(r.total);
                        return resolve(r.data);
                    } else {
                        return reject(r);
                    }
                })
            })
        }
    });

    $scope.onBtnDeleteClicked = function(row) {
        Notify.confirm('Apakah anda yakin ingin menghapus tiket tersebut?', function() {
            $TiketFactory.delete(row.id).then(function(r) {
                if(!r.error) {
                    Notify.success('Sukses!', 'Berhasil menghapus tiket!');
                    $scope.tiketTable.reload();
                } else {
                    Notify.error('Kesalahan', r.error);
                }
            })
        }, function() {})
    }

});