<?php
/**
 * Created by PhpStorm.
 * User: ragasub
 * Date: 11/24/17
 * Time: 10:54 AM
 */
?>

<div class="modal-header">
    <div class="modal-title">
        Tambah Data Tugas Mengajar
    </div>
</div>
<div class="modal-body">
    <form action="">
        <div class="form-group">
            <label for="">Tahun Pelajaran</label>
            <ui-select ng-model="new.tapel" theme="bootstrap">
                <ui-select-match placeholder="Pilih tahun pelajaran">{{$select.selected.tapel}}
                </ui-select-match>
                <ui-select-choices repeat="item in tapelList track by $index"
                                   refresh="onTapelChange($select.search)" refresh-delay="0">
                    {{item.tapel}}
                </ui-select-choices>
            </ui-select>
        </div>
        <div class="form-group">
            <label for="">Jumlah Jam</label>
            <div class="input-group">
                <input type="number" class="form-control" ng-model="new.jumlah_jam">
                <div class="input-group-addon">
                    Jam
                </div>
            </div>
        </div>
        <div class="form-group">
            <label for="">Mata Pelajaran</label>
            <ui-select ng-model="new.mapel" theme="bootstrap">
                <ui-select-match placeholder="Pilih mata pelajaran">{{$select.selected.nama}}
                </ui-select-match>
                <ui-select-choices repeat="item in mapelList track by $index"
                                   refresh="onMapelChange($select.search)" refresh-delay="0">
                    {{item.nama}}
                </ui-select-choices>
            </ui-select>
        </div>
        <div class="form-group">
            <label for="">Unit Kerja</label>
            <ui-select ng-model="new.sekolah" theme="bootstrap">
                <ui-select-match placeholder="Pilih unit kerja">{{$select.selected.nama}}
                </ui-select-match>
                <ui-select-choices repeat="item in unitKerjaList track by $index"
                                   refresh="onUkChange($select.search)" refresh-delay="0">
                    {{item.nama}}
                </ui-select-choices>
            </ui-select>
        </div>
        <div class="form-group">
            <label for="">Semester</label>
            <select class="form-control" ng-model="new.semester">
                <option value="1">Ganjil</option>
                <option value="2">Genap</option>
            </select>
        </div>
    </form>
</div>
<div class="modal-footer">
    <button class="btn btn-sm btn-primary" ng-click="onBtnSaveClicked()">
        <i class="fa fa-check"></i> <span>Simpan</span>
    </button>
    <button class="btn btn-sm btn-default" ng-click="dismiss()">
        <i class="fa fa-times"></i> <span>Batal</span>
    </button>
</div>