<?php
/**
 * Created by PhpStorm.
 * User: arsyad09
 * Date: 11/22/17
 * Time: 3:38 AM
 */
?>

<div class="modal-header">
    <div class="modal-title">
        <i class="fa fa-plus"></i> <span>Tambah Operator</span>
    </div>
</div>
<div class="modal-body">
    <div class="container-fluid" style="margin-top: 5px">
        <form>
            <div class="form-group">
                <label>Nama Lengkap</label>
                <input type="text" class="form-control" name="nama_operator" ng-model="new.nama_operator">
            </div>
            <div class="form-group">
                <label>Asal Sekolah</label>
                <ui-select ng-model="new.asal_sekolah" theme="bootstrap">
                    <ui-select-match placeholder="Ketik atau pilih asal sekolah">{{$select.selected.nama}}
                    </ui-select-match>
                    <ui-select-choices repeat="item in schoolList track by $index"
                                       refresh="onSchoolSelectChange($select.search)" refresh-delay="0">
                        {{item.nama}}
                    </ui-select-choices>
                </ui-select>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label>Nomor Handphone</label>
                        <input type="text" class="form-control" name="no_hp" ng-model="new.no_hp">
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label>Nomor WhatsApp</label>
                        <input type="text" class="form-control" name="no_wa" ng-model="new.no_wa">
                    </div>
                </div>
            </div>
        </form>
</div>
<div class="modal-footer">
    <button class="btn btn-primary btn-sm" ng-click="onBtnSaveClicked()" ng-disabled="!new.nama_operator || !new.asal_sekolah || !new.no_hp || !new.no_wa">
        <i class="fa fa-check"></i> <span>Simpan</span>
    </button>
    <button class="btn btn-default btn-sm" ng-click="dismiss()">
        <i class="fa fa-times"></i> <span>Batal</span>
    </button>
</div>
