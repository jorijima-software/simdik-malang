<?php
/**
 * Created by PhpStorm.
 * User: ragas
 * Date: 12/8/2017
 * Time: 3:17 AM
 */


class Guru extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('Guru_model', 'guru');
    }

    public function api_guru_show($start = 0, $count = 10)
    {
        header('Content-Type: application/json');

        if(!isset($_SESSION['as'])) {
            echo json_encode(['error' => 'Insufficent Priviledge!']);
            die();
        }

        $this->load->model('DataGuru');
        $this->load->model('User');
        $guru = DataGuru::take($count)
            ->when($search = $this->input->get('search'), function ($query) use ($search) {
                $query->whereHas('ptk', function ($q) use ($search) {
                    $q->where('nuptk', 'like', "%{$search}%")->orWhere('nama_lengkap', 'like', "%{$search}%");
                });
            })->when(User::find($this->session->uid)->level > 0, function ($query) {
                $query->whereHas('ptk', function ($q) {
                    $user = User::find($this->session->uid)->sekolah;
                   $q->where('id_sekolah', $user->id);
                });
            });
        $total = $guru->count();
        $return = ['data' => [], 'total' => $total];
        foreach ($guru->skip($start)->get() as $key => $row) {
            array_push($return['data'], [
                'id' => !!$row->ptk ? $row->ptk->id : 0,
                'idTugas' => $row->id,
                'nuptk' => !!$row->ptk ? $row->ptk->nuptk : null,
                'namaPtk' => !!$row->ptk ? $row->ptk->nama_lengkap : null,
                'jenis' => !!$row->ptk ? $row->ptk->jenisPegawai->nama : null,
                'jumlahJam' => $row->jamMengajar->sum('jumlahJam'),
                'unitKerja' => !!$row->ptk ? $row->ptk->unitKerja->nama : null,
                'tugasMengajar' => !!$row->tugasMengajar ? $row->tugasMengajar->tugasMengajar->nama : 'Kosong'
            ]);
        }
        echo json_encode($return);
    }

    public function spa_index()
    {
        $this->load->view('guru/index');
    }

    public function spa_tugas_mengajar()
    {
        $this->load->view('guru/tugas-mengajar');
    }

    public function stats()
    {
        echo json_encode($this->guru->stats());
    }

    public function show($start, $count)
    {
        $filter = $this->input->get();
        echo json_encode($this->guru->show($start, $count, $filter));
    }

    public function api_insert()
    {
        if ($this->input->method() == 'post') {
            $data = json_decode(trim(file_get_contents('php://input')));
            echo json_encode($this->guru->insert($data));
        }
    }

    public function api_update()
    {
        if ($this->input->method() == 'post') {
            $data = json_decode(trim(file_get_contents('php://input')));
            echo json_encode($this->guru->update($data));
        }
    }

    public function api_delete($id)
    {
        if ($this->input->method() == 'post') {
            echo json_encode($this->guru->delete($id));
        }
    }


}