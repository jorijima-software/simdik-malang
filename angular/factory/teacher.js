angular.module('diknasmlg').factory('$TeacherFactory', function ($http) {

    var factory = {};
    var apiPath = localStorage.getItem('PATHLOC');

    factory.get = function (id, callback) {
        $http.get(apiPath + 'api/teacher/' + id).then(
            function (success) {
                if (success.status == 200) {
                    return callback(success.data);
                }
            }
        );
    };


    // DEPRECATED USE `$PtkFactory` INSTEAD

    // factory.show = function (start, count, search, filter, callback) {
    // };
    // factory.showOperator = function (start, count, search, filter, callback) {
    // };
    // factory.insert = function (data, callback) {
    // };
    // factory.delete = function (id, callback) {
    // };

    factory.update = function (data, callback) {
        $http.post(apiPath + 'api/teacher/update', {
            data: {
                'id': data.id,
                'nuptk': data.nuptk,
                'nama': data.nama_lengkap,
                'gelar': {
                    'depan': data.gelar_depan,
                    'belakang': data.gelar_belakang
                },
                'nip': {
                    'lama': data.nip_lama,
                    'baru': data.nip_baru
                },
                'jenis_pegawai': data.pegawai.id,
                'sekolah': (data.sekolah && data.sekolah.id) ? data.sekolah.id : null,
                'nik': data.profil.nik,
                'nama_ibu': data.profil.nama_ibu
            },
            profil: {
                'tempat_lahir': data.profil.tempat_lahir,
                'tanggal_lahir': data.profil.tanggal_lahir,
                'agama': data.profil.agama,
            },
            rumah: {
                'kota': data.rumah.kota,
                'kecamatan': data.rumah.kecamatan,
                'no_telp': data.rumah.no_telp,
                'alamat': data.rumah.alamat
            }, sertifikasi: {
                pola: data.sert.pola,
                no_sert: data.sert.no,
                tahun: data.sert.tahun,
                bidang: (data.sert.bidang && data.sert.bidang.id) ? data.sert.bidang.id : null
            }
        }).then(function (reply) {
            if (reply.status == 200) {
                return callback(reply.data)
            } else {
                console.log(reply)
            }
        }, function (err) {
            console.log(err);
        })
    };

    factory.statsbox = function (callback) {
        $http.get(apiPath + 'api/teacher/statsbox').then(function (reply) {
            if (reply.status == 200) {
                return callback(reply.data)
            } else {
                console.log(reply)
            }
        }, function (err) {
            console.log(err);
        })
    };

    factory.operatorStatsbox = function (callback) {
        $http.get(apiPath + 'api/operator/teacher/statsbox').then(function (reply) {
            if (reply.status == 200) {
                return callback(reply.data)
            } else {
                console.log(reply)
            }
        }, function (err) {
            console.log(err);
        })
    };

    factory.rank = function (nip, callback) {
        $http.get(apiPath + 'api/teacher/rank/' + nip).then(function (reply) {
            if (reply.status == 200) {
                return callback(reply.data)
            } else {
                console.log(reply)
            }
        }, function (err) {
            console.log(err);
        })
    };

    factory.school = function (id, callback) {
        $http.get(apiPath + 'api/teacher/' + id + '/school').then(function (reply) {
            if (reply.status == 200) {
                return callback(reply.data)
            } else {
                console.log(reply)
            }
        }, function (err) {
            console.log(err);
        })
    };

    factory.addSchool = function (id, data, callback) {
        $http.post(apiPath + 'api/teacher/' + id + '/add/school', {
            nama: data.nama,
            tk: data.tingkat_pend.id,
            kota: data.kota.name,
            kecamatan: data.kecamatan.name,
            masuk: data.masuk,
            lulus: data.lulus,
            jurusan: (data.jurusan) ? data.jurusan : '',
            status: data.status.id
        }).then(function (reply) {
            if (reply.status == 200) {
                return callback(reply.data)
            } else {
                console.log(reply)
            }
        }, function (err) {
            console.log(err);
        })
    };

    factory.deleteSchool = function (id, callback) {
        $http.post(apiPath + 'api/teacher/' + id + '/delete/school', {}).then(function (reply) {
            if (reply.status == 200) {
                return callback(reply.data)
            } else {
                console.log(reply)
            }
        }, function (err) {
            console.log(err);
        })
    };

    factory.updateSchool = function (data, callback) {
        $http.post(apiPath + 'api/teacher/' + data.id + '/edit/school', {
            nama: data.nama,
            tk: data.tk.id,
            kota: data.kota.name,
            kecamatan: data.kecamatan.name,
            jurusan: (data.jurusan) ? data.jurusan : '',
            masuk: data.masuk,
            lulus: data.lulus
        }).then(function (reply) {
            if (reply.status == 200) {
                return callback(reply.data)
            } else {
                console.log(reply)
            }
        }, function (err) {
            console.log(err);
        })
    };

    factory.activateSchool = function (idTeacher, idSchool, callback) {
        $http.post(apiPath + 'api/teacher/' + idTeacher + '/activate/school/' + idSchool, {}).then(function (reply) {
            if (reply.status == 200) {
                return callback(reply.data)
            } else {
                console.log(reply)
            }
        }, function (err) {
            console.log(err);
        })
    };

    factory.addSalary = function (data, idTeacher, callback) {
        $http.post(apiPath + 'api/teacher/' + idTeacher + '/salary/add', {
            nip: data.nip,
            tmt: data.tmt,
            sk: data.sk,
            gaji: data.gaji,
            pangkat: data.pangkat
        }).then(function (reply) {
            if (reply.status == 200) {
                return callback(reply.data)
            } else {
                console.log(reply)
            }
        }, function (err) {
            console.log(err);
        })
    };

    factory.updateSalary = function (data, callback) {
        $http.post(apiPath + 'api/teacher/salary/update', {
            id: data.id,
            nip: data.nip,
            tmt: data.tmt,
            sk: data.sk,
            gaji: data.gaji,
            pangkat: data.pangkat
        }).then(function (reply) {
            if (reply.status == 200) {
                return callback(reply.data)
            } else {
                console.log(reply)
            }
        }, function (err) {
            console.log(err);
        })
    };

    factory.deleteSalary = function (id, callback) {
        $http.get(apiPath + 'api/teacher/' + id + '/salary/delete').then(function (reply) {
            if (reply.status == 200) {
                return callback(reply.data)
            } else {
                console.log(reply)
            }
        }, function (err) {
            console.log(err);
        })
    };

    factory.addRank = function (data, id, callback) {
        $http.post(apiPath + 'api/teacher/' + id + '/rank/add', {
            area: data.area,
            sk: data.sk,
            tmt: data.tmt,
            sekolah: data.sekolah,
            pangkat: data.pangkat,
            unit_kerja: data.unit_kerja,
            kota: data.kota,
            type: data.type
        }).then(function (reply) {
            if (reply.status == 200) {
                return callback(reply.data)
            } else {
                console.log(reply)
            }
        }, function (err) {
            console.log(err);
        })
    };

    factory.updateRank = function (data, callback) {
        console.log(data);
        // return;
        $http.post(apiPath + 'api/teacher/rank/update/', {
            area: (data.area && data.area.id) ? data.area.id : 0,
            sk: data.sk,
            tmt: data.tmt,
            sekolah: data.sekolah,
            pangkat: data.golongan,
            id: data.id,
            kota: data.kota,
            unit_kerja: data.unit_kerja,
            type: data.type
        }).then(function (reply) {
            if (reply.status == 200) {
                return callback(reply.data)
            } else {
                console.log(reply)
            }
        }, function (err) {
            console.log(err);
        })
    };

    factory.deleteRank = function (id, callback) {
        $http.get(apiPath + 'api/teacher/' + id + '/rank/delete').then(function (reply) {
            if (reply.status == 200) {
                return callback(reply.data)
            } else {
                console.log(reply)
            }
        }, function (err) {
            console.log(err);
        })
    };

    factory.showNew = function (start, count, filter, query, callback) {
        $http.get(apiPath + 'api/v2/teacher/show?' + jQuery.param({
            start: start,
            count: count,
            filter: filter,
            query: query
        }))
    };

    factory.selectTeacher = function (query, callback) {
        $http.get(apiPath + 'api/teacher/select/' + query).then(function (reply) {
            if (reply.status == 200) {
                return callback(reply.data)
            } else {
                console.log(reply)
            }
        }, function (err) {
            console.log(err);
        })
    };



    factory.showGuru = function (start, count, filter, callback) {

    };

    factory.tambahTugasMengajar = function (data, callback) {

        $http.post(apiPath + 'api/tugas-mengajar/tambah', {
            id: data.id,
            tapel: data.tapel.id,
            jumlah_jam: data.jumlah_jam,
            mapel: data.mapel.id,
            unit_kerja: data.sekolah.id,
            semester: data.semester
        }).then(function (reply) {
            if (reply.status == 200) {
                return callback(reply.data)
            } else {
                console.log(reply)
            }
        }, function (err) {
            console.log(err);
        })
    };

    factory.showTugasMengajar = function (id, start, count, filter, callback) {
        $http.get(apiPath + 'api/tugas-mengajar/show/' + id + '/' + start + '/' + count + '?' + jQuery.param(filter))
            .then(function (reply) {
                if (reply.status == 200) {
                    return callback(reply.data)
                } else {
                    console.log(reply)
                }
            }, function (err) {
                console.log(err);
            })
    };

    factory.deleteTugasMengajar = function (id, callback) {
        $http.get(apiPath + 'api/tugas-mengajar/delete/' + id).then(function (reply) {
            if (reply.status == 200) {
                return callback(reply.data)
            } else {
                console.log(reply)
            }
        }, function (err) {
            console.log(err);
        })
    };

    factory.updateTugasMengajar = function (data, callback) {
        $http.post(apiPath + 'api/tugas-mengajar/update', {
            id: data.id,
            tapel: data.tapel.id,
            jumlah_jam: data.jumlah_jam,
            mapel: data.mapel.id,
            unit_kerja: data.sekolah.id,
            semester: data.semester
        }).then(function (reply) {
            if (reply.status == 200) {
                return callback(reply.data)
            } else {
                console.log(reply)
            }
        }, function (err) {
            console.log(err);
        })
    };

    factory.updateGuru = function (data, callback) {
        
    };

    factory.hapusGuru = function (id, callback) {
        $http.get(apiPath + 'api/v2/teacher/delete/' + id).then(function (reply) {
            if (reply.status == 200) {
                return callback(reply.data)
            } else {
                console.log(reply)
            }
        }, function (err) {
            console.log(err);
        })
    };

    factory.dguruStatsbox = function (callback) {

    };

    factory.bidangStudi = function(query, callback) {
        $http.get(apiPath + 'api/bidang-studi/' + query).then(function (reply) {
            if (reply.status == 200) {
                return callback(reply.data)
            } else {
                console.log(reply)
            }
        }, function (err) {
            console.log(err);
        })
    };

    return factory;

});