<?php
/**
 * Created by PhpStorm.
 * User: ragasubekti
 * Date: 2/4/18
 * Time: 4:01 PM
 */
?>

<div class="modal-header">
    Informasi Kekurangan Data <strong class="text-uppercase">{{detail.nama}}</strong>
</div>
<div class="modal-body">
    <div ng-if="!detail.kelengkapan.nik"><b>NIK</b> Belum Terisi/Belum Lengkap</div>
    <div ng-if="!detail.kelengkapan.alamat"><b>Alamat</b> Belum Terisi/Belum Lengkap</div>
    <div ng-if="!detail.kelengkapan.nohp"><b>No. Handphone</b> Belum Terisi/Belum Lengkap</div>
    <div ng-if="!detail.kelengkapan.umur"><b>Tanggal Lahir</b> Salah</div>
</div>
<div class="modal-footer">
    <button class="btn btn-default btn-sm" ng-click="dismiss()">
        <i class="fa fa-times"></i> <span>Tutup</span>
    </button>
</div>
