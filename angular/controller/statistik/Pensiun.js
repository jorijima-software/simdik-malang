angular.module('diknasmlg').controller('StatistikPensiun', function (
    $scope,
    $StatistikFactory,
    $DistrictFactory,
    $LevelFactory,
    $SchoolFactory,
    $q,
    NgTableParams
) {
    $scope.year = []
    $scope.filter = {}
    $scope.all = []
    $scope.tingkatSekolah = []
    $scope.sekolahList = []
    $scope.kecamatanMalang = []

    $scope.PensiunTable = new NgTableParams({}, {
        getData: params => {
            let count = params.count()
            let start = count * (params.page() - 1)
            let filter = $scope.filter
            filter.start = start
            filter.count = count
            return $q((resolve, reject) => {
                $StatistikFactory.pensiun(filter).then(result => {
                    if (result && !result.error) {
                        params.total(result.total)
                        return resolve(result.data)
                    } else {
                        return reject(result.error)
                    }
                })
            })
        }
    })

    $scope.onInputSearchChange = search => {
        $SchoolFactory.mini(search).then(reply => {
            $scope.sekolahList = reply
        })
    }

    $scope.onFilter = () => {
        $scope.PensiunTable.reload()
    }

    function init() {
        $StatistikFactory.pensiunAll().then(result => {
            $scope.all = result.data
        })

        $DistrictFactory.get(reply => {
            $scope.kecamatanMalang = reply
        })

        $LevelFactory.get(reply => {
            $scope.tingkatSekolah = reply
        })

        $SchoolFactory.mini('').then(reply => {
            $scope.sekolahList = reply
        })

        let years = parseInt(moment().format('Y'), 10)
        $scope.year[0] = years + 1
        $scope.year[1] = years + 2
        $scope.year[2] = years + 3
    }

    init()
})