// Created by Raga Subekti at 23/10/17
angular.module('diknasmlg').factory('$SalaryFactory', function ($http) {

    var factory = {};
    var apiPath = localStorage.getItem('PATHLOC');

    factory.get = function (id, callback) {
        $http.get(apiPath + 'api/teacher/' + id + '/salary').then(
            function (reply) {
                if (reply.status == 200) {
                    return callback(reply.data);
                } else {
                    console.log(reply)
                }
            }
        )
    };

    return factory;

});