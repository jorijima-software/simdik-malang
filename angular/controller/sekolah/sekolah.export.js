angular
    .module('diknasmlg')
    .controller('SekolahExport', function(
        $scope,
        $uibModalInstance,
        $q,
        $SchoolFactory,
        $DistrictFactory
    ) {


        $scope.tingkatSekolah = []
        $scope.kecamatan = []
        $scope.kelengkapan = [
            {
                id: 0,
                nama: 'Semua'
            },
            {
                id: 1,
                nama: 'Kurang Dari 50%'
            },
            {
                id: 2,
                nama: 'Lebih Dari 50%'
            },
            {
                id: 3,
                nama: '100%'
            }
        ]

        // general function
        $scope.dismiss = function() {
            $uibModalInstance.dismiss()
        }

        function init() {
            $DistrictFactory.get(function(reply) {
                $scope.kecamatan = reply

                $scope.kecamatan.unshift({
                    id: 0,
                    nama: 'Semua'
                })
            })

            $SchoolFactory.type(function (reply) {
                $scope.tingkatSekolah = reply;

                $scope.tingkatSekolah.unshift({
                    id: 0,
                    nama: 'Semua'
                })
            })
        }

        init()

    })