<?php
/**
 * Created by PhpStorm.
 * User: arsyad09
 * Date: 06/12/17
 * Time: 23:17
 */

$userinfo = $this->User_model->userinfo();
?>

<div class="modal-header">
    <div class="modal-title">
        <i class="fa fa-plus"></i> <span>Upload Photo</span>
    </div>
</div>
<div class="modal-body">
    <div class="container-fluid" style="margin-top: 5px">
        <form action="<?= site_url('gallery/create') ?>" method="post">
            <div class="form-group">
                <label>Judul</label>
                <input type="text" class="form-control" name="tittle" ng-model="new.tittle" placeholder="Judul Foto">
            </div>
            <div class="form-group">
                <label>Deskripsi</label>
                <textarea class="form-control" name="description" ng-model="new.description" placeholder="Deskripsi Foto"></textarea>
            </div>
            <div class="form-group">
                <input type="hidden" class="form-control" name="uploader" ng-model="new.uploader" value="<?php
                if (is_null($userinfo->nama)) {
                    echo $userinfo->username;
                } else {
                    echo $userinfo->nama;
                }
                ?>">
            </div>

            <div class="form-group">
                <label for="" class="control-label col-sm-2">Pilih Gambar</label>
                <div class="col-sm-10">
                    <input type="file" class="form-control" multiple="false" name="photo" ng-model="new.photo" accept="image/*">
                </div>
            </div>
        </form>
    </div>
    <div class="modal-footer">
        <button class="btn btn-primary btn-sm" type="submit" title="Pastikan Anda Sudah Login!" <!--ng-disabled="!new.tittle || !new.uploader"-->>
            <i class="fa fa-check"></i> <span>Simpan</span>
        </button>
        <button class="btn btn-default btn-sm" ng-click="dismiss()">
            <i class="fa fa-times"></i> <span>Batal</span>
        </button>
    </div>
