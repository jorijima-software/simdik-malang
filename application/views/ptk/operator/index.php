<?php
/**
 * Created by PhpStorm.
 * User: ragasubekti
 * Date: 03/11/17
 * Time: 10.19
 */
?>

<div ng-controller="ptkController">

    <section class="content-header">
        <h1>
            Data PTK
        </h1>
    </section>

    <section class="content">
        <div class="row hidden-sm hidden-xs">
            <div class="col-lg-4 col-xs-6">
                <div class="small-box bg-yellow">
                    <div class="inner">
                        <h3>{{statsbox.total}}</h3>

                        <p>Total PTK</p>
                    </div>
                    <div class="icon">
                        <i class="fa fa-graduation-cap"></i>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-xs-6">
                <div class="small-box bg-aqua">
                    <div class="inner">
                        <h3>{{statsbox.nuptk.belum}}</h3>

                        <p>Belum mempunyai NUPTK</p>
                    </div>
                    <div class="icon">
                        <i class="fa fa-times"></i>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-xs-6">
                <div class="small-box bg-green">
                    <div class="inner">
                        <h3>{{statsbox.nuptk.sudah}}</h3>
                        <p>Sudah mempunyai NUPTK</p>
                    </div>

                    <div class="icon">
                        <i class="fa fa-check"></i>
                    </div>
                </div>
            </div>
            <div class="col-lg-2 col-xs-6" ng-repeat="item in statsbox.jenis">
                <div class="small-box" ng-random-class ng-classes="bgIndex">
                    <div class="inner">
                        <h3>{{item.total}}</h3>
                        <p>Pegawai {{item.jenis}}</p>
                    </div>
                </div>
            </div>
        </div>

        <?php require_once FCPATH . 'application/views/ptk/table.php'; ?>
    </section>

</div>