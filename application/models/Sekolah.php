<?php
/**
 * Created by PhpStorm.
 * User: ragasubekti
 * Date: 1/9/18
 * Time: 5:20 PM
 */

use Illuminate\Database\Eloquent\Model as Eloquent;

class Sekolah extends Eloquent
{
    protected $table = 'data_sekolah';
    protected $guarded = [];
    public $timestamps = false;

    public function ptk()
    {
        $CI =& get_instance();
        $CI->load->model('Ptk');
        return $this->hasMany('Ptk', 'id_sekolah', 'id')->where('deleted', '0');
    }

    public function persentase()
    {
        $CI =& get_instance();
        $CI->load->model('Ptk');

        return $this
            ->hasMany('Ptk', 'id_sekolah', 'id')
            ->where('deleted', '0')
            ->whereRaw('LENGTH(nik) > 0')
            ->whereHas('rumah', function ($query) {
                $query
                    ->whereRaw('LENGTH(alamat_rumah) >= 10')
                    ->whereRaw('LENGTH(nohp) > 0');
            });
    }

    public function tingkatSekolah()
    {
        $CI = &get_instance();
        $CI->load->model('TingkatSekolah');
        return $this
            ->hasOne('TingkatSekolah', 'id', 'id_jenis_tingkat_sekolah');
    }

    public function kecamatan()
    {

        $CI = &get_instance();
        $CI->load->model('KecamatanMalang');

        return $this->hasOne('KecamatanMalang', 'id', 'id_kecamatan');
    }

    public function operator()
    {

        $CI = &get_instance();
        $CI->load->model('Operator');

        return $this->hasOne('Operator', 'asal_sekolah', 'id');
    }

    public function percentageStats()
    {
        return $this
            ->with('ptk')
            ->selectRaw('id_jenis_tingkat_sekolah, COUNT(*) as total')
            ->orderBy('id_jenis_tingkat_sekolah', 'ASC')
            ->whereHas('ptk', function ($query) {
                $query
                    ->whereRaw('LENGTH(nik) > 5')
                    ->whereHas('rumah', function ($q) {
                        $q
                            ->whereRaw('LENGTH(alamat_rumah) > 10')
                            ->whereRaw('LENGTH(nohp) >= 10');
                    });
            })
            ->groupBy('id_jenis_tingkat_sekolah')
            ->get();
    }

    public function allPersentase()
    {
        $CI = &get_instance();
        $CI->load->model('TingkatSekolah');

        $stats = $this->percentageStats();
        $real = $this
            ->selectRaw('id_jenis_tingkat_sekolah, COUNT(*) as total')
            ->orderBy('id_jenis_tingkat_sekolah', 'ASC')
            ->groupBy('id_jenis_tingkat_sekolah')
            ->get();

        $return = [];
        foreach ($stats as $key => $row) {
            $return[] = [
                'tingkat' => TingkatSekolah::find($real[$key]['id_jenis_tingkat_sekolah'])->nama,
                'total' => $real[$key]['total'],
                'complete' => $row['total'],
                'percent' => round($row['total'] / $real[$key]['total'] * 100, 2),
            ];
        }
        return $return;
    }

    public function persenNonPns($id = null)
    {
        $CI = &get_instance();
        $CI->load->model('Ptk');

        $total = Ptk::where('status_pns', '0')->where('deleted', '0')->when($id, function ($q) use ($id) {
            $q->where('id_sekolah', $id);
        })->count();
        $ttl = Ptk::where('status_pns', '0')
            ->where('deleted', '0')
            ->when($id, function ($q) use ($id) {
                $q->where('id_sekolah', $id);
            })
            ->with('riwayatKerja')
            ->with('riwayatSekolah')
            ->whereHas('riwayatKerja')
            ->whereHas('riwayatSekolah')
            ->count();

        if($total > 0) {
            $persen = round($ttl / $total * 100, 2);
        } else {
            $persen = 0;
        }
        return ['total' => $total, 'ttl' => $ttl, 'persen' => $persen];
    }

    public function persenPtk($id = null) {
        $CI = &get_instance();
        $CI->load->model('Ptk');

        $total = Ptk::where('deleted', '0')->when($id, function ($q) use ($id) {
            $q->where('id_sekolah', $id);
        })->count();

        $ttl = Ptk::where('deleted', '0')
            ->when($id, function ($q) use ($id) {
                $q->where('id_sekolah', $id);
            })
            ->whereRaw('LENGTH(nik) > 10')
            ->whereHas('rumah', function($q) {
                $q
                    ->whereRaw('LENGTH(alamat_rumah) > 10')
                    ->whereRaw('LENGTH(nohp) >= 10');
            })->count();

        if ($total > 0) {
            $persen = round($ttl / $total * 100, 2);
        }
        return ['total' => $total, 'ttl' => $ttl, 'persen' => ($persen) ? $persen : 0];
    }

    public function persenPns($id = null)
    {
        $CI = &get_instance();
        $CI->load->model('Ptk');

        $total = Ptk::where('status_pns', '1')->where('deleted', '0')->when($id, function ($q) use ($id) {
            $q->where('id_sekolah', $id);
        })->count();
        $ttl = Ptk::where('status_pns', '1')
            ->where('deleted', '0')
            ->when($id, function ($q) use ($id) {
                $q->where('id_sekolah', $id);
            })
            ->with('riwayatPangkat')
            ->with('riwayatGaji')
            ->with('riwayatSekolah')
            ->whereHas('riwayatPangkat')
            ->whereHas('riwayatGaji')
            ->whereHas('riwayatSekolah')
            ->count();

        if ($total > 0) {
            $persen = round($ttl / $total * 100, 2);
        } else {
            $persen = 0;
        }
        return ['total' => $total, 'ttl' => $ttl, 'persen' => $persen];
    }
}