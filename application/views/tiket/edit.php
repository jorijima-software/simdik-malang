<?php
/**
 * Created by PhpStorm.
 * User: ragasubekti
 * Date: 12/12/17
 * Time: 13.18
 */

 // Basically just copy pasta of ze tambah thing
?>

<section class="content-header" style="margin-bottom: 60px">
    <div class="pull-right" style="">
        <div class="small-box bg-orange" style="padding: 5px">
            <div class="text-center">ID Tiket</div>
            <h3 style="margin: 0; padding: 0">#<?= $ticket_id ?></h3>
        </div>
    </div>
    <h1>Buat Tiket Baru</h1>
</section>

<section class="content">
    <div class="box box-warning">
        <div class="box-header">
            <div class="box-title">Buat Tiket Baru</div>
        </div>
        <div class="box-body">
            <?php echo form_open_multipart('tiket/tambah', 'class="form-horizontal"') ?>
            <div class="form-group">
                <div class="col-sm-12">
                    <?php if (isset($result['error'])) { ?>
                        <div class="alert alert-danger">
                            <i class="fa fa-exclamation-triangle"></i> <strong>ERROR!</strong> <br>
                            <p><?= $result['error'] ?></p>
                        </div>
                    <?php } else if (isset($result['success'])) { ?>
                        <div class="alert alert-success">
                            <i class="fa fa-check"></i> <strong>SUCCESS!</strong> <br>
                            <p>Berhasil membuat tiket baru!</p>
                        </div>
                    <?php } ?>
                </div>

            </div>
            <div class="form-group">
                <!--                    <div class="col-sm-2"></div>-->
                <label class="control-label col-sm-2">Nama Permasalahan: </label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" placeholder="NAMA PERMASALAHAN" name="nama"
                           value="<?= $this->input->post('nama') ?>">
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-2">Group Data: </label>
                <div class="col-sm-10">
                    <select name="group_data" class="form-control">
                        <option value="0">Data Operator</option>
                        <option value="1">Data Profile Sekolah</option>
                        <option value="2">Data PTK</option>
                        <option value="3">Data Guru</option>
                        <option value="4">Data Rombel</option>
                    </select>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-2">Keterangan</label>
                <div class="col-sm-10">
                    <textarea name="keterangan" rows="5" class="form-control"
                              placeholder="KETERANGAN"><?= $this->input->post('keterangan') ?></textarea>
                </div>
            </div>
            <!-- <div class="form-group">
                <label class="control-label col-sm-2">Informasi Error</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" name="informasi">
                </div>
            </div> -->
            <div class="form-group">
                <label class="control-label col-sm-2">Screenshot</label>
                <div class="col-sm-10">
                    <input type="file" class="form-control" accept="image/*" multiple="false" name="screenshot">
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-2"></div>
                <div class="col-sm-10">
                    <button class="btn btn-primary btn-sm">
                        <i class="fa fa-check"></i> <span>Kirim</span>
                    </button>
                    <a href="<?= site_url('tiket') ?>" class="btn btn-default btn-sm">
                        <i class="fa fa-caret-left"></i> <span>Kembali</span>
                    </a>
                </div>
            </div>
            </form>
        </div>
    </div>
</section>
