<div class="box">
    <div class="box-body">
        <div class="row">
            <div class="col-md-8" style="margin-bottom: 5px">
                <button class="btn btn-primary btn-sm" ng-click="onBtnAddClicked()" ng-disabled="timerEnd">
                    <i class="fa fa-plus"></i>
                    &nbsp; Tambah
                </button>
            </div>
            <div class="col-md-4" style="margin-bottom: 5px">
                <div class="form-group">
                    <div class="input-group">
                        <div class="input-group-btn">
                            <button class="btn btn-default" ng-click="isThisHidden = !isThisHidden">
                                <i class="fa fa-list"></i>
                                <span>Filter</span>
                            </button>
                        </div>
                        <input type="text" class="form-control" placeholder="Pencarian" ng-model="search" ng-keydown="onSearchKeydown($event)">
                        <div class="input-group-btn">
                            <button class="btn btn-info" ng-click="doSearch()">
                                <i class="fa fa-search"></i> <span>Cari</span>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div ng-hide="!isThisHidden">
            <div class="well well-sm">
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="">Jenis Pegawai</label>
                            <ui-select ng-model="filter.pegawai" theme="bootstrap" search-enabled="false" on-select="onFilterPegawaiChange()">
                                <ui-select-match placeholder="Pilih jenis pegawai">
                                    {{$select.selected.nama}}
                                </ui-select-match>
                                <ui-select-choices repeat="item in employeeList track by $index">
                                    <span>{{item.nama}}</span>
                                </ui-select-choices>
                            </ui-select>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="">NUPTK</label>
                            <br>
                            <div class="btn-group btn-group-sm">
                                <button class="btn btn-default" ng-click="onBtnFilterNuptk(0)" ng-class="{'active': filter.nuptk == 0}">Semua</button>
                                <button class="btn btn-default" ng-click="onBtnFilterNuptk(1)" ng-class="{'active': filter.nuptk == 1}">Belum</button>
                                <button class="btn btn-default" ng-click="onBtnFilterNuptk(2)" ng-class="{'active': filter.nuptk == 2}">Sudah</button>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="">PNS</label>
                            <br>
                            <div class="btn-group btn-group-sm">
                                <button class="btn btn-default active" ng-click="onBtnFilterPns(0)" ng-class="{'active': filter.pns == 0}">Semua</button>
                                <button class="btn btn-default" ng-click="onBtnFilterPns(1)" ng-class="{'active': filter.pns == 1}">Non PNS</button>
                                <button class="btn btn-default" ng-click="onBtnFilterPns(2)" ng-class="{'active': filter.pns == 2}">PNS</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <uib-tabset>
            <uib-tab heading="PNS">
                <div class="table-responsive">
                    <table class="table table-striped table-condensed table-bordered table" style="margin-top: 10px" ng-table="teacherTable">
                        <thead style="background: rgba(60, 141, 188, 0.8); color: white;">
                            <tr>
                                <th rowspan="2" style="vertical-align: middle;text-align: center">No</th>
                                <th rowspan="2" style="vertical-align: middle;text-align: center">NUPTK</th>
                                <th rowspan="2" style="vertical-align: middle;text-align: center">Nama Lengkap</th>
                                <th rowspan="2"></th>
                                <th rowspan="2" style="vertical-align: middle;text-align: center">Jenis Pegawai</th>
                                <th rowspan="2" style="vertical-align: middle;text-align: center">Unit Kerja</th>
                                <th colspan="3" style="vertical-align: middle;text-align: center">Riwayat</th>
                                <th rowspan="2"></th>
                            </tr>
                            <tr>
                                <th style="vertical-align: middle;text-align: center">Sekolah</th>
                                <th style="vertical-align: middle;text-align: center">Pangkat</th>
                                <th style="vertical-align: middle;text-align: center">Gaji Berkala</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr ng-repeat="row in $data">
                                <td>{{($index + 1) + (teacherTable._params.count * (teacherTable._params.page -1))}}</td>
                                <td>{{row.nuptk}}</td>
                                <td>
                                    {{row.nama}}
                                    <span ng-if="row.guru > 0">
                                        <i class="fa fa-graduation-cap"></i>
                                    </span>
                                    <span ng-if="row.sertifikasi > 0">
                                        <i class="fa fa-star-o"></i>
                                    </span>
                                </td>
                                <td>
                                    <span ng-if="!row.lengkap">
                                        <button class="btn btn-xs btn-danger" ng-click="onKekuranganClicked(row)">
                                            <i class="fa fa-exclamation-triangle"></i>
                                        </button>
                                    </span>
                                </td>
                                <td>{{row.jenisPegawai}}</td>
                                <td>{{row.unitKerja}}</td>
                                <td>
                                    <a class="btn btn-primary btn-xs" href="<?=site_url('/')?>#!/ptk/riwayat/sekolah/{{row.id}}">
                                        <i class="fa fa-eye"></i>
                                    </a> {{row.riwayat.sekolah}}
                                </td>
                                <td>
                                    <a class="btn btn-primary btn-xs" href="<?=site_url('/')?>#!/ptk/riwayat/pangkat/{{row.id}}">
                                        <i class="fa fa-eye"></i>
                                    </a> {{row.riwayat.pangkat}}
                                </td>
                                <td>
                                    <a class="btn btn-primary btn-xs" href="<?=site_url('/')?>#!/ptk/riwayat/gaji/{{row.id}}">
                                        <i class="fa fa-eye"></i>
                                    </a> {{row.riwayat.gaji}}
                                </td>
                                <td>
                                    <div class="pull-right">
                                        <button class="btn btn-primary btn-xs" ng-click="onBtnDetailClicked(row)">
                                            <i class="fa fa-eye"></i> Detail
                                        </button>
                                        <button class="btn btn-info btn-xs" ng-click="onBtnEditClicked(row)" ng-disabled="timerEnd">
                                            <i class="fa fa-pencil"></i> Ubah
                                        </button>
                                        <button class="btn btn-danger btn-xs" ng-click="onBtnDeleteClicked(row)" ng-disabled="timerEnd">
                                            <i class="fa fa-trash"></i> Hapus
                                        </button>
                                    </div>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </uib-tab>
            <uib-tab heading="Non-PNS">
                <div class="table-responsive">
                    <table class="table table-striped table-condensed table-bordered table" style="margin-top: 10px" ng-table="nonPnsTable">
                        <thead style="background: rgba(60, 141, 188, 0.8); color: white;">
                            <tr>
                                <th rowspan="2" style="vertical-align: middle;text-align: center">No</th>
                                <th rowspan="2" style="vertical-align: middle;text-align: center">NUPTK</th>
                                <th rowspan="2" style="vertical-align: middle;text-align: center">Nama Lengkap</th>
                                <th rowspan="2"></th>
                                <th rowspan="2" style="vertical-align: middle;text-align: center">Jenis Pegawai</th>
                                <th rowspan="2" style="vertical-align: middle;text-align: center">Unit Kerja</th>
                                <th colspan="2" style="vertical-align: middle;text-align: center">Riwayat</th>
                                <th rowspan="2"></th>
                                <th rowspan="2"></th>
                            </tr>
                            <tr>
                                <th style="vertical-align: middle;text-align: center">Sekolah</th>
                                <th style="vertical-align: middle;text-align: center">Kerja</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr ng-repeat="row in $data">
                                <td>{{($index + 1) + (nonPnsTable._params.count * (nonPnsTable._params.page -1))}}</td>
                                <td>{{row.nuptk}}</td>
                                <td>
                                    {{row.nama}}
                                    <span ng-if="row.guru > 0">
                                        <i class="fa fa-graduation-cap"></i>
                                    </span>
                                    <span ng-if="row.sertifikasi > 0">
                                        <i class="fa fa-star-o"></i>
                                    </span>
                                </td>

                                <td>
                                    <span ng-if="!row.lengkap">
                                        <button class="btn btn-xs btn-danger" ng-click="onKekuranganClicked(row)">
                                            <i class="fa fa-exclamation-triangle"></i>
                                        </button>
                                    </span>
                                </td>
                                <td>{{row.jenisPegawai}}</td>
                                <td>{{row.unitKerja}}</td>
                                <td>
                                    <a class="btn btn-primary btn-xs" href="<?=site_url('/')?>#!/ptk/riwayat/sekolah/{{row.id}}">
                                        <i class="fa fa-eye"></i>
                                    </a>
                                    {{row.riwayat.sekolah}}
                                </td>
                                <td>
                                    <a class="btn btn-primary btn-xs" href="<?=site_url('/')?>#!/ptk/riwayat/kerja/{{row.id}}">
                                        <i class="fa fa-eye"></i>
                                    </a> {{row.riwayat.kerja}}
                                </td>
                                <td>
                                    <button class="btn btn-xs" ng-click="onBtnRekeningDetail(row)" ng-class="{
                                        'btn-danger': !row.rekening,
                                        'btn-success': row.rekening
                                    }">
                                        <i class="fa fa-btc"></i>
                                        <span>Rekening</span>
                                    </button>
                                </td>
                                <td>
                                    <div class="pull-right">
                                        <button class="btn btn-primary btn-xs" ng-click="onBtnDetailClicked(row)">
                                            <i class="fa fa-eye"></i> Detail
                                        </button>
                                        <button class="btn btn-info btn-xs" ng-click="onBtnEditClicked(row)" ng-disabled="timerEnd">
                                            <i class="fa fa-pencil"></i> Ubah
                                        </button>
                                        <button class="btn btn-danger btn-xs" ng-click="onBtnDeleteClicked(row)" ng-disabled="timerEnd">
                                            <i class="fa fa-trash"></i> Hapus
                                        </button>
                                    </div>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </uib-tab>
        </uib-tabset>
    </div>
</div>