<?php

class Log extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('Log_model');
    }

    public function latest() {
        echo json_encode($this->Log_model->latestfive());
    }

    public function spa_sms()
    {
        $this->load->view('log/sms');
    }

    public function spa_login()
    {
        $this->load->view('log/login');
    }

    public function spa_rombel()
    {
        $this->load->view('log/rombel');
    }

    public function spa_guru()
    {
        $this->load->view('log/guru');
    }

    public function spa_ptk()
    {
        $this->load->view('log/ptk');
    }

    public function page_rombel()
    {
        $this->load->view('layout', ['content' => 'log/rombel', 'js' => [
            'angular/factory/log.js',
            'angular/controller/log/log-rombel.js'
        ]]);
    }

    public function page_login()
    {
        $this->load->view('layout', ['content' => 'log/login', 'js' => [
            'angular/factory/log.js',
            'angular/controller/log/log-login.js'
        ]]);
    }

    public function page_sms()
    {
        $this->load->view('layout', ['content' => 'log/sms', 'js' => [
            'angular/factory/log.js',
        ]]);
    }

    public function page_ptk()
    {
        $this->load->view('layout', ['content' => 'log/ptk', 'js' => [
            'angular/factory/log.js',
            'angular/controller/log/log-ptk.js'
        ]]);
    }

    public function page_guru()
    {
        $this->load->view('layout', ['content' => 'log/guru', 'js' => [
            'angular/factory/log.js',
            'angular/controller/log/log-guru.js'
        ]]);
    }

    public function api_rombel($start = 0, $count = 10, $filter = [])
    {
        echo json_encode($this->Log_model->rombel($start, $count, $filter));
    }

    public function api_guru($start = 0, $count = 10, $filter = [])
    {
        echo json_encode($this->Log_model->guru($start, $count, $filter));
    }

    public function api_ptk($start = 0, $count = 10, $filter = [])
    {
        echo json_encode($this->Log_model->ptk($start, $count, $filter));
    }

    public function api_login($start = 0, $count = 10, $filter = [])
    {
        echo json_encode($this->Log_model->login($start, $count, $filter));
    }

    public function api_sms($start = 0, $count = 10, $filter = [])
    {
        echo json_encode($this->Log_model->sms($start, $count));
    }


}