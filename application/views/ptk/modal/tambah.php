<?php
/**
 * Created by PhpStorm.
 * User: ragasubekti
 * Date: 06/10/17
 * Time: 16.55
 */
?>
<div class="modal-header">
    <div class="modal-title">
        <i class="fa fa-plus"></i>&nbsp;Tambah Guru
    </div>
</div>
<div class="modal-body">
    <uib-tabset active="active">
        <uib-tab heading="Data">
            <div class="container-fluid" style="margin-top: 5px">
                <form>
                    <div class="form-group">
                        <label>NUPTK</label>
                        <input type="text" name="nuptk" ng-model="new.nuptk" class="form-control">
                    </div>
                    <div class="form-group">
                        <label>Nama Lengkap</label>
                        <input type="text" class="form-control" name="nama_lengkap" ng-model="new.nama_lengkap">
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Gelar Depan</label>
                                <input type="text" class="form-control" name="gelar_depan" ng-model="new.gelar_depan">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Gelar Belakang</label>
                                <input type="text" class="form-control" name="gelar_belakang"
                                       ng-model="new.gelar_belakang">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>NIP Lama</label>
                                <input type="text" class="form-control" name="nip_lama" ng-model="new.nip_lama">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>NIP Baru</label>
                                <input type="text" class="form-control" name="nip_baru" ng-model="new.nip_baru">
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label>Jenis Pegawai</label>
                        <ui-select ng-model="new.pegawai" theme="bootstrap" search-enabled="false">
                            <ui-select-match placeholder="Pilih jenis pegawai">{{$select.selected.nama}}
                            </ui-select-match>
                            <ui-select-choices repeat="item in employeeTypeList">
                                {{item.nama}}
                            </ui-select-choices>
                        </ui-select>
                    </div>
                    <div class="form-group">
                        <label>Unit Kerja</label>
                        <?php if ($this->session->as == 0) { ?>
                            <ui-select ng-model="new.sekolah" theme="bootstrap">
                                <ui-select-match placeholder="Ketik atau pilih asal sekolah">{{$select.selected.nama}}
                                </ui-select-match>
                                <ui-select-choices repeat="item in schoolList track by $index"
                                                   refresh="onInputSearchChange($select.search)" refresh-delay="0">
                                    {{item.nama}}
                                </ui-select-choices>
                            </ui-select>
                        <?php } else {
                            $userinfo = $this->User_model->userinfo();

                            echo "<br><b style='font-size: larger'>$userinfo->nama</b>";
                        } ?>
                    </div>
                </form>
            </div>
        </uib-tab>
        <uib-tab heading="Profil">
            <div class="container-fluid" style="margin-top: 5px">

                <form action="">
                    <div class="form-group">
                        <label for="">Tempat Lahir</label>
                        <input type="text" class="form-control" name="tempat_lahir" ng-model="new.profil.tempat_lahir"
                               ng-change="onBirthPlaceChange()">
                    </div>
                    <div class="form-group">
                        <label for="">Tanggal Lahir</label>
                        <p class="input-group">
                            <input type="text" class="form-control" uib-datepicker-popup="yyyy-MM-dd"
                                   ng-model="new.profil.tanggal_lahir"
                                   datepicker-options="dateOptions"
                                   close-text="Close" is-open="datepicker" placeholder="yyyy-MM-dd" readonly
                                   ng-click="datepicker = !datepicker"/>
                            <span class="input-group-btn">
            <button type="button" class="btn btn-default" ng-click="datepicker = !datepicker"><i
                        class="glyphicon glyphicon-calendar"></i></button>
          </span>
                        </p>
                    </div>
                    <div class="form-group">
                        <label for="">Agama</label>
                        <ui-select ng-model="new.profil.agama" theme="bootstrap" search-enabled="false">
                            <ui-select-match placeholder="Pilih agama">{{$select.selected.nama}}
                            </ui-select-match>
                            <ui-select-choices repeat="item in religionList track by $index"
                                               refresh="onInputSearchChange($select.search)" refresh-delay="0">
                                {{item.nama}}
                            </ui-select-choices>
                        </ui-select>
                    </div>
                    <div class="form-group">
                        <label for="">NIK</label>
                        <input type="text" class="form-control" ng-model="new.nik">
                    </div>
                    <div class="form-group">
                        <label for="">Nama Ibu</label>
                        <input type="text" class="form-control" ng-model="new.ibu">
                    </div>
                </form>
            </div>
        </uib-tab>
        <uib-tab heading="Rumah">
            <div class="container-fluid" style="margin-top: 5px">
                <!--                <form action="">-->
                <div class="form-group">
                    <label for="">Kota</label>
                    <ui-select ng-model="new.rumah.kota" theme="bootstrap" on-select="onRumahRegencySelect()">
                        <ui-select-match placeholder="PILIH atau KETIK kota/kabupaten">{{$select.selected.name}}
                        </ui-select-match>
                        <ui-select-choices repeat="item in rumahRegencyList track by $index"
                                           refresh="onRumahRegencyChange($select.search)" refresh-delay="0">
                            {{item.name}}
                        </ui-select-choices>
                    </ui-select>
                </div>
                <div class="form-group">
                    <label for="">Kecamatan</label>
                    <ui-select ng-model="new.rumah.kecamatan" theme="bootstrap" ng-disabled="!new.rumah.kota.id">
                        <ui-select-match placeholder="PILIH atau KETIK kecamatan">{{$select.selected.name}}
                        </ui-select-match>
                        <ui-select-choices repeat="item in rumahKecamatanList track by $index"
                                           refresh="onRumahKecamatanChange($select.search)" refresh-delay="0">
                            {{item.name}}
                        </ui-select-choices>
                    </ui-select>
                </div>
                <div class="form-group">
                    <label>No. Telp</label>
                    <input type="text" class="form-control" ng-model="new.rumah.no_telp">
                </div>
                <div class="form-group">
                    <label for="">Alamat</label>
                    <textarea class="form-control" ng-model="new.rumah.alamat"></textarea>
                </div>
            </div>
        </uib-tab>
        <uib-tab heading="Sertifikasi">
            <div class="container-fluid" style="margin-top: 5px">
                <div class="form-group">
                    <label>Pola</label>
                    <select class="form-control" ng-model="new.sert.pola">
                        <option value="0">Portofolio</option>
                        <option value="1">PLPG</option>
                        <option value="2">PPG</option>
                    </select>
                </div>
                <div class="form-group">
                    <label>No. Peserta</label>
                    <input type="text" class="form-control" ng-model="new.sert.no">
                </div>
                <div class="form-group">
                    <label>Tahun</label>
                    <input type="text" class="form-control" ng-model="new.sert.tahun">
                </div>
                <div class="form-group">
                    <label>Bidang Studi</label>
                    <ui-select ng-model="new.sert.bidang" theme="bootstrap">
                        <ui-select-match placeholder="PILIH atau KETIK bidang studi"><b>{{$select.selected.kode}}</b> -
                            {{$select.selected.nama}}
                        </ui-select-match>
                        <ui-select-choices repeat="item in bidangStudiList track by $index"
                                           refresh="onBidangStudiChange($select.search)" refresh-delay="0">
                            <b>{{item.kode}}</b> - {{item.nama}}
                        </ui-select-choices>
                    </ui-select>
                    <!--                    <input type="text" class="form-control" ng-model="new.sert.bidang">-->
                </div>
            </div>
        </uib-tab>
    </uib-tabset>

</div>
<div class="modal-footer">
    <button class="btn btn-primary btn-sm" ng-click="onBtnSaveClicked()" ng-disabled="isSubmit">
        <i class="fa fa-check" ng-if="!isSubmit"></i><i class="fa fa-circle-o-notch fa-spin" ng-if="isSubmit"></i> &nbsp;<span>Simpan</span>
    </button>
    <button class="btn btn-default btn-sm" ng-click="dismiss()"><i class="fa fa-times"></i> &nbsp; Batal</button>
</div>