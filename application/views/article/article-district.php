<?php
/**
 * Created by PhpStorm.
 * User: arsyad09
 * Date: 10/12/17
 * Time: 11:51 AM
 */
?>
<div class="container" style="margin-top: 75px" ng-controller="articleDistrictController">
    <div class="row">
        <div class="col-md-8" style="margin-bottom: 5px">
            <h2 class="header-on-page">DATA KECAMATAN</h2>
        </div>
        <div class="col-md-4" style="margin-bottom: 5px">

        </div>
    </div>
    <div class="table-responsive" style="height: 320px;background: white">
        <table class="table table-striped table-condensed" ng-table="kecamatanTable">
            <thead style="background:rgba(12, 153, 0, 0.8);" >
            <tr>
                <th class="align-center" style="vertical-align: middle" rowspan="2">No</th>
                <th width="700px" style="vertical-align: middle" class="align-center" rowspan="2">Nama</th>
                <th class="align-center" colspan="4">Jumlah</th>
                <th rowspan="2"></th>
            </tr>
            <tr>
                <th class="align-center">TK</th>
                <th class="align-center">SD</th>
                <th class="align-center">SMP</th>
                <th class="align-center">Pegawai</th>
            </tr>
            </thead>
            <tbody>
            <tr ng-repeat="row in $data">
                <td class="align-center">{{$index + 1}}</td>
                <td>{{row.nama}}</td>
                <td class="align-center">{{row.tk}}</td>
                <td class="align-center">{{row.sd}}</td>
                <td class="align-center">{{row.smp}}</td>
                <td class="align-center">{{row.total_pegawai}}</td>
                <td>
                    <div class="pull-right">
                        <button class="btn btn-success btn-xs" ng-click="onBtnDetailClicked(row)">
                            <i class="fa fa-eye"></i> Detail
                        </button>
                    </div>
                </td>
            </tr>
            </tbody>
        </table>
    </div>
</div>
