<?php
/**
 * Created by PhpStorm.
 * User: ragas
 * Date: 12/8/2017
 * Time: 1:39 AM
 */

class Ptk extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('Ptk_model', 'ptk');
        // header('Content-Type: application/json');
    }

    public function settingJenisPegawai($start = 0, $count = 10) {
        $this->load->model('JenisPegawai');
        $jpegawai = JenisPegawai::take($count)->orderBy('id', 'DESC');
        $total = $jpegawai->count();

        echo json_encode([
            'total' => $total,
            'data' => $jpegawai->skip($start)->get()
        ]);
    }

    public function settingBidangStudi($start = 0, $count = 10) {
        $this->load->model('BidangStudi');
        $bstudi = BidangStudi::take($count)->orderBy('id', 'DESC');
        $total = $bstudi->count();

        echo json_encode([
            'total' => $total,
            'data' => $bstudi->skip($start)->get()
        ]);
    }

    public function spa_setting_jenis_pegawai() {
        $this->load->view('setting/jenis-pegawai');
    }

    public function spa_setting_bidang_studi() {
        $this->load->view('setting/bidang-studi');
    }

    public function modal_ptk_edit()
    {
        $this->load->view('ptk/modal/import-edit');
    }

    public function modal_ptk_detail()
    {
        $this->load->view('ptk/modal/import-detail');
    }

    public function api_delete_import_ptk($id)
    {
        if (!isset($_SESSION['as'])) {
            echo json_encode(['error' => 'Insufficent Priviledge']);
        } else {
            $this->load->model('PreviewPtk');
            if ($this->session->as == 0) {
                PreviewPtk::destroy($id);
                echo json_encode(['success' => true]);
            } else {
                $this->load->model('User');
                $sekolah = User::find($this->session->uid);

                if (PreviewPtk::find($id)->id_sekolah == $sekolah->sekolah->id) {
                    PreviewPtk::destroy($id);
                    echo json_encode(['success' => true]);
                } else {
                    echo json_encode(['error' => 'Insufficent Priviledge']);
                }
            }
        }
    }

    public function api_update_import_ptk()
    {
        if (!isset($_SESSION['as'])) {
            echo json_encode(['error' => 'Insufficent Priviledge']);
        } else {
            $this->load->model('PreviewPtk');

            header('Content-Type: application/json');
            $_input = json_decode(trim(file_get_contents('php://input')));

            if ($this->session->as == 0) {
                PreviewPtk::where('id', $_input->id)->update([
                    'nama_lengkap' => $_input->nama_lengkap,
                    'nuptk' => $_input->nuptk,
                    'nik' => $_input->nik,
                    'nip' => $_input->nip,
                    'gelar_depan' => $_input->gelar_depan,
                    'gelar_belakang' => $_input->gelar_belakang,
                    'jenis_pegawai' => $_input->jenis_pegawai->id,
                    'tempat_lahir' => $_input->tempat_lahir,
                    'tanggal_lahir' => date_format(date_create($_input->tanggal_lahir), 'Y-m-d'),
                    'agama' => $_input->nama_agama->id,
                    'nama_ibu' => $_input->nama_ibu,
                    'no_telp' => $_input->no_telp,
                    'alamat' => $_input->alamat,
                ]);

                echo json_encode(['success' => true]);
            } else {
                $this->load->model('User');
                $sekolah = User::find($this->session->uid);

                if (PreviewPtk::find($_input->id)->id_sekolah == $sekolah->sekolah->id) {
                    PreviewPtk::where('id', $_input->id)->update([
                        'nama_lengkap' => $_input->nama_lengkap,
                        'nuptk' => $_input->nuptk,
                        'nik' => $_input->nik,
                        'nip' => $_input->nip,
                        'gelar_depan' => $_input->gelar_depan,
                        'gelar_belakang' => $_input->gelar_belakang,
                        'jenis_pegawai' => $_input->jenis_pegawai->id,
                        'tempat_lahir' => $_input->tempat_lahir,
                        'tanggal_lahir' => date_format(date_create($_input->tanggal_lahir), 'Y-m-d'),
                        'agama' => $_input->nama_agama->id,
                        'nama_ibu' => $_input->nama_ibu,
                        'no_telp' => $_input->no_telp,
                        'alamat' => $_input->alamat,
                    ]);
    
                    echo json_encode(['success' => true]);
                } else {
                    echo json_encode(['error' => 'Insufficent Priviledge']);
                }
            }
        }
    }

    public function api_show_import_ptk()
    {

        $this->load->model('PreviewPtk');

        if (isset($_SESSION['as'])) {

            if ($this->session->as == 0 && !$this->input->get('id')) {
                echo json_encode(['error' => 'You need more arguments']);
                die();
            }

            $list = PreviewPtk::take($this->input->get('count'));

            if ($this->session->as == 0) {
                $filter = $this->input->get('id') ? $list->where('id_sekolah', $this->input->get('id')) : $list;
            } else if ($this->session->as == 1) {
                $this->load->model('User');
                $user = User::find($this->session->uid);
                // var_dump($user);
                // die();
                $filter = $list->where('id_sekolah'
                    , $user->sekolah->id);
            } else {
                echo json_encode(['error' => 'You don\'t have access to this API']);
                die();
            }

            $total = $filter->count();            

            $take = $filter->skip($this->input->get('start'));
 
            $data = [];
            foreach ($take->get() as $key => $row) {
                $item = $row;
                $item['nama_jenis_pegawai'] = $row->jenisPegawai ? $row->jenisPegawai->nama : null;
                $item['nama_agama'] = $row->namaAgama ? $row->namaAgama->nama : null;
                array_push($data, $item);
            }

            echo json_encode(['data' => $data, 'total' => $total]);
        } else {
            echo json_encode(['error' => 'You don\'t have access to this API']);
        }

    }

    public function spa_verify_data()
    {
        $this->load->view('ptk/verifikasi');
    }

    public function modal_import_xls()
    {
        $this->load->view('ptk/modal/sekolah-import');
    }

    public function api_insert()
    {
        if ($this->input->method() == 'post') {
            header('Content-Type: application/json');
            $data = json_decode(trim(file_get_contents('php://input')));

            $basic = $data->data;
            $profil = $data->profil;
            $rumah = $data->rumah;
            $sert = $data->sertifikasi;

            $this->load->model('Teacher_model');

            echo json_encode($this->ptk->insert($basic, $profil, $rumah, $sert));
        }
    }

    public function api_update()
    {
        if ($this->input->method() == 'post') {
            header('Content-Type: application/json');
            $raw = json_decode(trim(file_get_contents('php://input')));

            $data = $raw->data;
            $profile = $raw->profil;
            $rumah = $raw->rumah;
            $sert = $raw->sertifikasi;

            echo json_encode($this->ptk->update($data, $profile, $rumah, $sert));
        }
    }

    public function api_delete($id = null)
    {
        if ($this->input->method() == 'post') {
            echo json_encode($this->ptk->delete($id));
        }
    }

    public function api_show($start, $count, $search = null)
    {
        if ($this->input->method() == 'get') {
            $filter = $this->input->get();
            echo json_encode($this->ptk->show($start, $count, $search, $filter));
        }
    }

    // show only pns
    public function api_show_pns() {
           
        $this->load->model('Ptk', 'PtkModel');

        $ptk = PtkModel::take(10);

        $data = $ptk
                    ->where('status_pns', 1)
                    ->get();

        if($data) {
            header('Content-Type: application/json');
            echo json_encode($data);
        }
    }

    // show only non pns
    public function api_show_npns() {

    }

    public function api_stats()
    {
        if ($this->input->method() == 'get') {
            echo json_encode($this->ptk->stats());
        }
    }

    public function api_detail($id)
    {
        echo json_encode($this->ptk->detail($id));
    }

    // Get a single matching row from Ptk_model
    public function api_get($id)
    {

    }

}
