<?php
/**
 * Created by PhpStorm.
 * User: ragasubekti
 * Date: 08/10/17
 * Time: 11.09
*/
?>
<div class="modal-header">
    <div class="modal-title">
        <i class="fa fa-plus"></i> &nbsp; Tambah Sekolah
    </div>
</div>
<div class="modal-body">
    <form>
        <div class="form-group">
            <label for="">NPSN</label>
            <input type="text" class="form-control" name="nisn" ng-model="new.nisn">
        </div>
        <div class="row">
            <div class="col-md-2 col-xs-4">
                <div class="form-group">
                    <label>Tingkat Sekolah</label>
                    <ui-select ng-model="new.tingkat" theme="bootstrap">
                        <ui-select-match>{{$select.selected.nama}}</ui-select-match>
                        <ui-select-choices repeat="item in levelList track by $index" refresh="onInputSearchChange($select.search)" refresh-delay="0">
                            {{item.nama}}
                        </ui-select-choices>
                    </ui-select>
                </div>
            </div>
            <div class="col-md-10 col-xs-6">
                <div class="form-group">
                    <label>Nama Sekolah</label>
                    <input type="text" class="form-control" name="nama" ng-model="new.nama">
                </div>
            </div>
        </div>
        <div class="form-group">
            <label>Status</label>
            <ui-select ng-model="new.status" theme="bootstrap">
                <ui-select-match>{{$select.selected.nama}}</ui-select-match>
                <ui-select-choices repeat="item in statusList track by $index" refresh="onInputSearchChange($select.search)" refresh-delay="0">
                    {{item.nama}}
                </ui-select-choices>
            </ui-select>
        </div>
        <div class="form-group">
            <label for="">Alamat</label>
            <input type="text" class="form-control" name="alamat" ng-model="new.alamat">
        </div>
        <div class="form-group">
            <label for="">Kecamatan</label>
            <ui-select ng-model="new.kecamatan" theme="bootstrap">
                <ui-select-match>{{$select.selected.nama}}</ui-select-match>
                <ui-select-choices repeat="item in districtList track by $index" refresh="onInputSearchChange($select.search)" refresh-delay="0">
                    {{item.nama}}
                </ui-select-choices>
            </ui-select>
        </div>
    </form>
</div>
<div class="modal-footer">
    <button class="btn btn-primary btn-sm" ng-click="onBtnSaveClicked()" ng-disabled="isSubmit">
        <i class="fa fa-check" ng-if="!isSubmit"></i><i class="fa fa-circle-o-notch fa-spin" ng-if="isSubmit"></i> <span>Simpan</span>
    </button>
    <button class="btn btn-default btn-sm" ng-click="dismiss()"><i class="fa fa-times"></i> &nbsp; Batal</button>
</div>
