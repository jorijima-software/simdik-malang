<?php
require_once FCPATH.'vendor/ifsnop/mysqldump-php/src/Ifsnop/Mysqldump/Mysqldump.php';

/**
 * Created by PhpStorm.
 * User: ragasubekti
 * Date: 02/12/17
 * Time: 14.23
 */
use Ifsnop\Mysqldump as IMysqldump;

class Backup_model extends CI_Model {

    public function all_tables() {

        $username = $this->db->username;
        $password = $this->db->password;
        $hostname = $this->db->hostname;
        $database = $this->db->database;

        $latest = md5(uniqid());


        try {
            $dump = new IMysqldump\Mysqldump('mysql:host='.$hostname.';dbname='.$database, $username, $password);
            $dump->start(APPPATH .'backup/'.$latest.'.sql');
        } catch (\Exception $e) {
            echo 'mysqldump-php error: ' . $e->getMessage();
        }


//        die();


        $config = Array(
            'protocol' => 'smtp',
            'smtp_host' => 'ssl://smtp.googlemail.com',
            'smtp_port' => 465,
            'smtp_user' => 'simdik.malang.db@gmail.com',
            'smtp_pass' => 'iniadalahpassword',
            'mailtype'  => 'html',
            'charset'   => 'iso-8859-1'
        );

        $this->load->library('email', $config);
        $this->email->set_newline("\r\n");
        //
        $this->email->from('No Reply');
        $this->email->to(['rfilmriot@gmail.com', 'idhenz@gmail.com']);


        $this->email->attach(APPPATH . 'backup/'. $latest .'.sql');
        $this->email->subject('Backup Database SIMDIK (' . date('d F Y h:i') . ')');
        $this->email->message("Ini adalah email yang terkirim secara otomatis oleh sistem. Mohon jangan balas email ini.<br> Terimakasih<br><br><br><small color='#cccccc'>Kode Unik: ".md5(uniqid().uniqid()).'</small>');

        $this->email->send();

        return $latest;
    }

}
