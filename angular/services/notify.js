// Created by Raga Subekti at 07/10/17
angular.module('diknasmlg').service('Notify', function() {
    this.success = function(title, message) {
        new Noty({
            text: '<i class="fa fa-check"></i> <strong>' + title + '</strong> <br> ' + message,
            timeout: 5000,
            layout: 'topRight',
            type: 'success',
            theme: 'mint'
        }).show();
    };

    this.error = function(title, message) {
        new Noty({
            text: '<i class="fa fa-times"></i> <strong>' + title + '</strong> <br> ' + message,
            timeout: 5000,
            layout: 'topRight',
            type: 'error',
            theme: 'mint'
        }).show();
    };

    this.confirm = function(message, onConfirm, onDismiss) {
        var n = new Noty({
            text: message,
            layout: 'center',
            modal: true,
            buttons: [
                Noty.button('Konfirmasi', 'btn btn-success', function () {
                    onConfirm();
                    n.close();
                }, {id: 'button1', 'data-status': 'ok'}),

                Noty.button('Batal', 'btn btn-times', function () {
                    onDismiss();
                    n.close();
                })
            ]
        }).show();
    }
});