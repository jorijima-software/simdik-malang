<?php
/**
 * Created by PhpStorm.
 * User: arsyad09
 * Date: 11/21/17
 * Time: 10:47 PM
 */
?>
<div class="container" style="margin-top: 75px" ng-controller="registerOpController">
    <div class="row">
        <div class="col-md-8" style="margin-bottom: 5px">
            <h2 class="header-on-page">Registrasi Operator Sekolah</h2>
        </div>
        <div class="col-md-4" style="margin-bottom: 5px">

        </div>
    </div>
    <div class="row">
        <div class="col-md-8">
            <button class="btn btn-success btn-sm" style="margin: 10px;margin-left: 0" ng-click="onBtnAddOperatorClicked()">
                <i class="fa fa-plus"></i> &nbsp; Daftar Operator
            </button>
        </div>
        <div class="col-md-4" style="margin-bottom: 5px">
            <div class="form-group">
                <div class="input-group">
                    <div class="input-group-btn">
                        <button class="btn btn-default" ng-click="isThisHidden = !isThisHidden">
                            <i class="fa fa-list"></i> <span>Filter</span>
                        </button>
                    </div>
                    <input type="text" class="form-control" placeholder="Pencarian" ng-model="search"
                           ng-keyup="onInputSearchChange()">
                    <div class="input-group-addon">
                        <i class="fa fa-search"></i>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div ng-hide="!isThisHidden">
        <div class="well well-sm">
            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="">Tingkat</label> <br>
                        <ui-select ng-model="filter.tingkat" theme="bootstrap" search-enabled="false" on-select="onFilterTingkatChange()">
                            <ui-select-match placeholder="Pilih tingkat">
                                {{$select.selected.nama}}
                            </ui-select-match>
                            <ui-select-choices repeat="item in educationList track by $index">
                                {{item.nama}}
                            </ui-select-choices>
                        </ui-select>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="table-responsive" style="padding-bottom:20px;background: white; min-height: 320px">
        <table class="table table-striped table-condensed" ng-table="operatorTable">
            <thead style="background:rgba(12, 153, 0, 0.8);" >
            <tr>
                <th class="align-center" style="vertical-align: middle">No</th>
                <th style="vertical-align: middle" class="align-center">Nama Operator</th>
                <th class="align-center">Asal Sekolah</th>
                <th class="align-center">No. Handphone</th>
                <th class="align-center">No. WhatsApp</th>
            </tr>
            </thead>
            <tbody>
            <tr ng-repeat="row in $data">
                <td class="align-center">{{($index + 1) + (operatorTable._params.count * (operatorTable._params.page -1))}}</td>
                <td style="text-transform: uppercase">{{row.nama_operator}}</td>
                <td>{{row.nama_sekolah}}</td>
                <td class="align-center">{{row.no_hp | limitTo:9}}xxx</td>
                <td class="align-center">{{row.no_wa | limitTo:9}}xxx</td>
            </tr>
            </tbody>
        </table>
        <button class="btn btn-warning btn-lg" style="margin-left: 45%" ng-click="onBtnAddOperatorClicked()">
            Daftar Sekarang!
        </button>
    </div>
</div>
