angular.module('diknasmlg').controller('articleStatisticController', function ($scope, $uibModal, NgTableParams, $q, $DistrictFactory, $SchoolFactory) {


    $scope.bgIndex = [
        'bg-aqua',
        'bg-yellow',
        'bg-green',
        'bg-orange',
        'bg-red'
    ];


    $scope.chartConfig = {
        chart: {
            type: 'column',
            margin: 75,
            options3d: {
                enabled: true,
                alpha: 15,
                beta: 15,
                depth: 110
            }
        },
        title: {
            text: 'Ketersebaran Sekolah di Kota Malang'
        },
        plotOptions: {
            column: {
                depth: 40,
                stacking: true,
                grouping: false,
                groupZPadding: 10
            }
        },
        xAxis: {
            categories: ['Blimbing', 'Kedungkandang', 'Klojen', 'Lowokwaru', 'Sukun']
        },
        series: [{
            data: [67, 60, 72, 71, 68],
            stack: 4,
            name: 'TK'
        }, {
            data: [57, 55, 45, 58, 57],
            stack: 3,
            name:'SD'
        }, {
            data: [21, 16, 26, 22, 16],
            stack:2,
            name:'SMP'
        }, {
            data: [0, 0, 0, 0, 0],
            stack:1,
            name:'SMA'
        }, {
            data: [0, 0, 0, 0, 0],
            stack:0,
            name:'SMK'
        }]
    };


    function init() {

    }

    init();
});