angular.module('diknasmlg').controller('CountdownController', function(
    $scope,
    $uibModalInstance,
    $TimerFactory,
    $q,
    $Notify
) {

    $scope.timer = ''
    $scope.error = ''

    function init() {
        $TimerFactory.countdown().then(function(result) {
            if(result && !result.error) {
                $scope.timer = result
            } else {
                $scope.error = result.error
            }
        })
    }

    init()

})