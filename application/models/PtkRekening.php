<?php

class PtkRekening extends \Illuminate\Database\Eloquent\Model
{
    protected $table = 'ptk_rekening';
    protected $guarded = [];

    /**
     * Tambah/ Update Data Rekening PTK
     *
     * @param [object] $data
     * @return void
     */
    public function store($data)
    {
        // $CI =& get_instance();
        // $CI->load->model('PtkRekening');

        if(!isset($data->jenisBank) || ($data->jenisBank && !$data->jenisBank->id)) {
            echo json_encode(['error' => 'Bank wajib dipilih!']);
            die();
        }

        try {
            $cur = PtkRekening::where('data_guru_id', $data->dataPtk->id);
            if ($cur->count() > 0) {
                $cur->first()->update([
                    'nama' => $data->nama, // Atas Nama
                    'jenis_bank_id' => $data->jenisBank->id, // ID Jenis Bank
                    'no_rekening' => $data->noRekening, // No Rekening
                    'cabang' => $data->cabang, // Cabang
                ]);

                return ['success' => true, 'updated' => true];
            } else {
                PtkRekening::create([
                    'nama' => $data->nama, // Atas Nama (default: Nama PTK)
                    'data_guru_id' => $data->dataPtk->id, // ID Data PTK (tabel `data_guru`)
                    'jenis_bank_id' => $data->jenisBank->id, // ID Jenis Bank (tabel `jenis_bank`),
                    'no_rekening' => $data->noRekening, // No Rekening
                    'cabang' => $data->cabang, // Cabang Bank
                ]);

                return ['success' => true];
            }
        } catch (\Illuminate\Database\QueryException $e) {
            return ['error' => $e->getMessage()];
        }
    }

    public function bank()
    {
        $CI = &get_instance();
        $CI->load->model('JenisBank');

        return $this->hasOne('JenisBank', 'id', 'jenis_bank_id');
    }

    public function ptk()
    {
        $CI = &get_instance();
        $CI->load->model('Ptk');

        return $this->hasOne('Ptk', 'id', 'data_guru_id');
    }
}
