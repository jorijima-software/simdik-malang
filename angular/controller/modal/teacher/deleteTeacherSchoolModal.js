// Created by Raga Subekti at 18/10/17
angular.module('diknasmlg').controller('deleteTeacherSchoolModal', function(data, Notify, $uibModalInstance, $scope, $TeacherFactory) {

    $scope.data = data;

    $scope.dismiss = function() {
        $uibModalInstance.dismiss();
    };

    $scope.onBtnConfirmClicked = function() {
        $TeacherFactory.deleteSchool(data.id, function(reply) {
            if(reply.error) {
                Notify.error('Kesalahan', reply.error);
            } else {
                Notify.success('Sukses', 'Data sekolah berhasil dihapus');
                $uibModalInstance.close();
            }
        })
    };

});