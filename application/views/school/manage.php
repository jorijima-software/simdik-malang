<?php
/**
 * Created by PhpStorm.
 * User: Raga
 * Date: 11/8/2017
 * Time: 2:51 AM
 */

//$this->load->model('User_model');
//$this->load->model('School_model');

$schinfo = $this->User_model->profile();
$el = $this->School_model->getEl();
$rl = $this->School_model->getRl();

$status = array(
    0 => 'Swasta',
    1 => 'Negeri'
);

$geo = array(
    0 => 'Pedesaan',
    1 => 'Perkotaan',
    2 => 'Daerah Perbatasan'
);

?>

<div>
    <section class="content-header">
        <h1>
            Ubah Profil Sekolah
        </h1>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <?php echo form_open_multipart('school/manage', 'class="form-horizontal"') ?>

                <div class="box">
                    <div class="box-body">
                        <?php if(isset($result['error'])) { ?>
                        <div class="alert alert-danger">
                            <i class="fa fa-exclamation-triangle"></i> <strong>ERROR!</strong> &nbsp; <?=$result['error']?>
                        </div>
                        <?php } else if(isset($result['success'])) { ?>

                        <div class="alert alert-success">
                            <i class="fa fa-check"></i> <strong>SUCCESS!</strong> &nbsp; Berhasil mengubah informasi profil sekolah
                        </div>
                        <?php } ?>

                        <div class="form-group">
                            <label for="" class="control-label col-sm-2">NPSN</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" name="npsn" value="<?= $schinfo->npsn ?>" disabled>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="" class="control-label col-sm-2">Nama Sekolah</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" name="nama" value="<?= $schinfo->nama ?>" disabled>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="" class="control-label col-sm-2">Tingkat Sekolah</label>
                            <div class="col-sm-10">
                                <select name="tingkat" class="form-control" disabled>
                                    <?php foreach ($el as $key => $row) {
                                        if ($schinfo->id_jenis_tingkat_sekolah == $row->id) {
                                            echo "<option value=\"{$row->id}\" selected>{$row->nama}</option>";
                                        } else {
                                            echo "<option value=\"{$row->id}\">{$row->nama}</option>";
                                        }
                                    } ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="" class="control-label col-sm-2">Kecamatan</label>
                            <div class="col-sm-10">
                                <select name="kecamatan" class="form-control">
                                    <?php foreach ($rl as $key => $row) {
                                        if ($schinfo->id_kecamatan == $row->id) {
                                            echo "<option value=\"{$row->id}\" selected>{$row->nama}</option>";
                                        } else {
                                            echo "<option value=\"{$row->id}\">{$row->nama}</option>";
                                        }
                                    } ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-2">Desa</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" value="<?= $schinfo->desa ?>" name="desa">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-sm-2">Kode Pos</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" value="<?= $schinfo->kode_pos ?>"
                                       name="kodepos">
                            </div>
                        </div>


                        <div class="form-group">
                            <label class="control-label col-sm-2">Alamat</label>
                            <div class="col-sm-10">
                                <textarea name="alamat" class="form-control"><?= $schinfo->alamat ?></textarea>
                            </div>
                        </div>


                        <div class="form-group">
                            <label class="control-label col-sm-2">No Telp.</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" value="<?= $schinfo->notelp ?>" name="notelp">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-sm-2">Faksimile</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" value="<?= $schinfo->fax ?>" name="fax">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-sm-2">Email</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" value="<?= $schinfo->email ?>" name="email">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-sm-2">Website</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" value="<?= $schinfo->website ?>" name="website">
                            </div>
                        </div>


                        <div class="form-group">
                            <label for="" class="control-label col-sm-2">Status</label>
                            <div class="col-sm-10">
                                <select name="status" class="form-control">
                                    <?php foreach ($status as $key => $row) {
                                        if ($schinfo->status == $key) {
                                            echo "<option value=\"{$key}\" selected>{$row}</option>";
                                        } else {
                                            echo "<option value=\"{$key}\">{$row}</option>";
                                        }
                                    } ?>
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="" class="control-label col-sm-2">Geografis</label>
                            <div class="col-sm-10">
                                <select name="geografis" id="" class="form-control">
                                    <?php foreach ($geo as $key => $row) {
                                        if ($schinfo->geografis == $key) {
                                            echo "<option value=\"{$key}\" selected>{$row}</option>";
                                        } else {
                                            echo "<option value=\"{$key}\">{$row}</option>";
                                        }
                                    } ?>
                                </select>
                            </div>
                        </div>


                        <div class="form-group">
                            <label for="" class="control-label col-sm-2">Pilih Gambar</label>
                            <div class="col-sm-10">
                                <input type="file" class="form-control" multiple="false" name="gambar" accept="image/*">
                            </div>
                        </div>
                    </div>
                    <div class="box-footer">
                        <div class="pull-right">
                            <button class="btn btn-primary btn-sm" type="submit">
                                <i class="fa fa-check"></i> <span>Simpan</span>
                            </button>
                        </div>
                    </div>
                    </form>

                </div>
            </div>
        </div>
    </section>
</div>

