angular.module('diknasmlg').controller('dataGuruController', function(
    $scope, 
    $uibModal, 
    $q, 
    $TeacherFactory, 
    NgTableParams, 
    Notify,
    $TimerFactory
) {

    $scope.timerEnd
    var apiPath = localStorage.getItem('PATHLOC');

    $scope.onBtnAddClicked = function() {
        $uibModal.open({
            templateUrl: apiPath + 'teacher/modal_mengajar_tambah',
            controller: 'tambahDataMengajar',
            resolve: {
                data: function() {
                    return null;
                },
                edit: function () {
                    return false;
                }
            }
        }).result.then(refreshTable, refreshTable)
    };

    $scope.dataGuruTable = new NgTableParams({}, {
        getData: function(params) {
            var count = params.count();
            var start = count * (params.page() - 1);
            return $q(function (resolve, reject) {
                $TeacherFactory.showGuru(start, count, {}, function(reply) {
                    if(reply.error) return reject(reply.error);
                    params.total(reply.total);
                    return resolve(reply.data);
                })
            })
        }
    });

    $scope.onBtnEditClicked = function(row) {
        $uibModal.open({
            templateUrl: apiPath + 'teacher/modal_mengajar_tambah',
            controller: 'tambahDataMengajar',
            resolve: {
                data: function() {
                    return row;
                },
                edit: function () {
                    return true;
                }
            }
        }).result.then(refreshTable, refreshTable)
    };

    $scope.onBtnDeleteClicked = function(row) {
        Notify.confirm('Apakah anda yakin ingin menghapus data tersebut?', function() {
            $TeacherFactory.hapusGuru(row.id_tugas, function(reply) {
                if(reply.error) {
                    Notify.error('Kesalahan', reply.error);
                } else {
                    Notify.success('Sukses', 'Berhasil menghapus data guru');
                }

                refreshTable();
            })
        }, function() {})
    };

    function refreshTable() {
       $scope.dataGuruTable.reload();
    }

    function init() {
        $TimerFactory.diff().then(result => {
            $scope.timerEnd = (!result.admin && result.disabled)
        })
    }

    init()

});