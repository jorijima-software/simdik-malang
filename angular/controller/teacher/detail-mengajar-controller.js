angular.module('diknasmlg').controller('detailMengajarController', function($scope, $uibModal, NgTableParams, $q, $TeacherFactory, Notify) {

    var apiPath = localStorage.getItem('PATHLOC');

    var nip     = window.location.pathname.split('/');
    $scope.nip  = nip[nip.length - 1];

    $scope.onBtnAddClicked = function() {
        $uibModal.open({
            templateUrl: apiPath + 'teacher/modal_dmengajar_tambah',
            controller: 'tambahTugasMengajar',
            resolve: {
                data: function() {
                    return null;
                }
            }
        }).result.then(refreshTable, refreshTable)
    };

    $scope.semester = {
        1: 'Ganjil',
        2: 'Genap'
    };

    $scope.tmengajarTable = new NgTableParams({}, {
        getData: function(params) {
            var count = params.count();
            var start = count * (params.page() - 1);
            return $q(function(resolve, reject) {
                $TeacherFactory.showTugasMengajar($scope.nip, start, count, {}, function(reply) {
                    if(reply.error)  return reject(reply.error);
                    params.total(reply.total);
                    return resolve(reply.data);
                })
            })
        }
    });

    $scope.onBtnDeleteClicked = function(row) {
        Notify.confirm('Apakah anda yakin ingin menghapus data tersebut?', function() {
            $TeacherFactory.deleteTugasMengajar(row.id, function(reply) {
                if(reply.error) {
                    Notify.error('Kesalahan', reply.error);
                } else {
                    Notify.success('Sukses', 'Berhasil menghapus data tugas mengajar')
                }

                refreshTable();
            })
        }, function() {})
    };

    $scope.onBtnEditClicked = function(row) {
        $uibModal.open({
            templateUrl: apiPath + 'teacher/modal_dmengajar_tambah',
            controller: 'tambahTugasMengajar',
            resolve: {
                data: function() {
                    return row;
                }
            }
        }).result.then(refreshTable, refreshTable)
    };

    function refreshTable() {
        $scope.tmengajarTable.reload();
    }


});