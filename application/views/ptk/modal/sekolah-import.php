<?php
/**
 * Created by PhpStorm.
 * User: ragasubekti
 * Date: 1/12/18
 * Time: 7:10 PM
 */
?>

<div class="modal-header">
    Import XLS
</div>
<div class="modal-body">
    <a class="btn btn-default btn-sm" href="<?=base_url('simdik_template_ptk.xls')?>">
        <i class="fa fa-cloud-download"></i>&nbsp;Download Contoh XLS
    </a>
    <div class="form-group">
        <label style="border: dotted 3px lightgray; padding: 20px 10px; width: 100%; margin-top: 10px" nv-file-drop=""
               uploader="uploader">
            <input type="file" nv-file-select="" uploader="uploader" ng-hide="true"/>
            Drop atau Pilih XLS disini
        </label>
    </div>
    <div class="form-group" ng-if="uploader.queue.length > 0">
        File yang terpilih:
        <table class="table table-striped table-hover">
            <tr ng-repeat="row in uploader.queue">
                <td>{{row.file.name}}</td>
                <td>{{ row.file.size/1024/1024|number:2 }} MB</td>
            </tr>
        </table>
    </div>
    <button type="button" class="btn btn-success btn-s" ng-click="uploader.uploadAll()">
        <span class="fa fa-file-excel-o">&nbsp;</span> Import
    </button>
</div>
<div class="modal-footer">

</div>