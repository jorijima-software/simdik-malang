<?php
/**
 * Created by PhpStorm.
 * User: arsyad09
 * Date: 06/12/17
 * Time: 20:07
 */

class Gallery_model extends CI_Model {

    public function show($start, $count, $search)
    {
        $this->db->select('*');
        $this->db->from('galeri_foto');
        if($search > 0 && $search != 'null' && !is_null($search)) {
            $this->db->like('uploader', $search);
        }

        $total = clone $this->db;

        $this->db->limit($count, $start);

        return array('data' => $this->db->get()->result(), 'total' => $total->count_all_results());
    }

    public function create($data)
    {
        $config['upload_path'] = FCPATH . '/uploads/';
        $config['allowed_types'] = 'gif|jpg|png';
        $config['encrypt_name'] = true;
        $config['width'] = '1280';

        $this->load->library('upload/Gallery', $config);

        if (!empty($_FILES['photo']['name'])) {
            if (!$this->upload->do_upload('photo')) {
                return array('error' => $this->upload->display_errors());
            } else {
                $pic = array('upload_data' => $this->upload->data());
            }
        }

        $data = array(
            'tittle' => $data['tittle'],
            'description' => isset($data['description']) ? $data['description'] : null,
            'uploader' => $data['uploader'],
            'files_name' => $pic['upload_data']['file_name']
        );

        $this->db->insert('galeri_foto', $data);

        if ($this->db->affected_rows() > 0) {
            return array('success' => true);
        } else {
            if ($this->db->error()['code'] > 0) {
                return array('error' => $this->db->error()['message']);
            } else {
                return array('success' => true, 'anu' => $_FILES['gambar']);
            }
        }
    }

    public function delete($id)
    {
        $this->db->where('id', $id);
        $this->db->delete('galeri_foto');

        return array('success' => true);
    }
}