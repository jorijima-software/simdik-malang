angular.module('diknasmlg').controller('profilOperator', function ($scope, $uibModalInstance, $SchoolFactory, Notify) {
    var API = localStorage.getItem('PATHLOC');

    $scope.operator = {
        nama: '',
        no_hp: '',
        no_wa: ''
    };

    $scope.dismiss = function () {
        $uibModalInstance.dismiss();
    };

    $scope.onBtnSaveClicked = function () {
        $SchoolFactory.updateOperator($scope.operator).then(function(r) {
            if(r.success) {
                Notify.success('Sukses', 'Berhasil memperbarui data operator');
                $uibModalInstance.close();
            } else {
                Notify.error('Kesalahan', r.error);
            }
        })
    };

    function init() {
        $SchoolFactory.getOperator().then(function (r) {
            if (r) {
                $scope.operator = {
                    nama: r.nama_operator,
                    no_hp: r.no_hp,
                    no_wa: r.no_wa
                }
            }
        }).catch(function(err) {
            console.log(err);
        })
    }

    init();

});