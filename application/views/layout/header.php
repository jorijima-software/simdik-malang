<?php
/**
 * Created by PhpStorm.
 * User: ragasubekti
 * Date: 06/10/17
 * Time: 15.51
 */

//$this->load->model('User_model');
$userinfo = $this->User_model->userinfo();
if (!$userinfo) {
    redirect('login');
}
?>

<header class="main-header">

    <!-- Logo -->
    <a href="<?= site_url('/') ?>#!/dashboard" class="logo">
        <div class="logo-mini">
<!--            <img src="--><?//= base_url('assets/logo.png') ?><!--" alt="" width="40">-->
            <div style="font-family: 'Product Sans', sans-serif; line-height: 1">
                <b>SIMDIK</b>
            </div>
        </div>

        <span class="logo-lg">
            <div class="row">
                <div class="col-sm-1 text-right">
                    <img src="<?= base_url('assets/logo.png') ?>" width="40">
                </div>
                <div class="col-sm-10" style="font-family: 'Product Sans', sans-serif; line-height: 1">
                    <span style="font-size: xx-small">Dinas Pendidikan Kota Malang</span>
                    <b>SIMDIK</b>
                </div>
            </div>

            <!--            <span style="display: inline">-->
            <!--            <span style="display: inline; font-size: 12px; margin-top: -10px">Dinas Pendidikan</span> <br> <span-->
            <!--                        style="display: inline; font-size: 12px">Kota Malang</span>-->
            <!--                </span>-->
        </span>
    </a>

    <nav class="navbar navbar-static-top" role="navigation">
        <a href="" class="sidebar-toggle" data-toggle="push-menu" role="button">
            <span class="sr-only">Toggle navigation</span>
        </a>
        <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">

                <li>
                    <a style="padding: 8px 0">
                        <span class="btn btn-danger" style="margin: 0;padding: 4px">
                            Batas Update Data &nbsp;
                        <span style="font-weight: 800;display: inline-block; font-size: 16px; font-family: 'Roboto Mono', monospace;"
                              countdown='' date='January 31, 2018 23:59:59'>00 hari 00:00:00</span>
                        </span>
                    </a>
                </li>
                <li class="dropdown user user-menu">
                    <a class="dropdown-toggle" data-toggle="dropdown">
                        <span class="hidden-xs"><?php
                            if (is_null($userinfo->nama)) {
                                echo $userinfo->username;
                            } else {
                                echo $userinfo->nama;
                            }
                            ?></span>
                    </a>
                    <ul class="dropdown-menu">
                        <li class="user-footer">
                            <div class="pull-left">
                                <a href="<?= site_url('user/profile') ?>" class="btn btn-default btn-flat">Profile</a>
                            </div>
                            <div class="pull-right">
                                <a href="<?= site_url('logout') ?>" class="btn btn-default btn-flat">Sign out</a>
                            </div>
                        </li>
                    </ul>
                </li>

            </ul>
        </div>
    </nav>
</header>
