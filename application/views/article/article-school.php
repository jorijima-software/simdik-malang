<?php
/**
 * Created by PhpStorm.
 * User: arsyad09
 * Date: 10/11/17
 * Time: 4:53 PM
 */
?>
<div class="container" style="margin-top: 75px" ng-controller="articleSchoolController">
    <div class="row hidden-xs hidden-sm">
        <div class="col-lg-3 col-xs-6" ng-repeat="item in statsbox">
            <div class="small-box text-center" ng-random-class ng-classes="bgIndex">
                <div class="inner">
                    <div class="row">
                        <div class="col-xs-3" style="font-size: smaller">
                            <h4 style="font-weight: 800">{{item.negri}}</h4>
                            <p>Negeri</p>
                        </div>
                        <div class="col-xs-3">
                            <h4 style="font-weight: 800">{{item.swasta}}</h4>
                            <p>Swasta</p>
                        </div>
                        <div class="col-xs-6">
                            <h3>{{item.total}}</h3>
                            <p>Total {{item.tingkat}}</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-8" style="margin-bottom: 5px">
            <h2 class="header-on-page">DATA SEKOLAH</h2>
            <!--<button class="btn btn-export"></button>
            <button class="btn btn-cetak"></button>-->
        </div>
        <div class="col-md-4" style="margin-bottom: 5px">
            <div class="form-group">
                <div class="input-group">
                    <div class="input-group-btn">
                        <button class="btn btn-default" ng-click="isThisHidden = !isThisHidden">
                            <i class="fa fa-list"></i> <span>Filter</span>
                        </button>
                    </div>
                    <input type="text" class="form-control" placeholder="Pencarian" ng-model="search"
                           ng-keyup="onInputSearchChange()">
                    <div class="input-group-addon">
                        <i class="fa fa-search"></i>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-12">
            <div ng-hide="!isThisHidden">
                <div class="well well-sm">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="">Kecamatan</label>
                                <ui-select ng-model="filter.kecamatan" theme="bootstrap" search-enabled="false" on-select="onFilterKecamatanChange()">
                                    <ui-select-match placeholder="Pilih kecamatan">
                                        {{$select.selected.nama}}
                                    </ui-select-match>
                                    <ui-select-choices repeat="item in kecamatanList track by $index">
                                        {{item.nama}}
                                    </ui-select-choices>
                                </ui-select>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="">Tingkat</label> <br>
                                <ui-select ng-model="filter.tingkat" theme="bootstrap" search-enabled="false" on-select="onFilterTingkatChange()">
                                    <ui-select-match placeholder="Pilih tingkat">
                                        {{$select.selected.nama}}
                                    </ui-select-match>
                                    <ui-select-choices repeat="item in educationList track by $index">
                                        {{item.nama}}
                                    </ui-select-choices>
                                </ui-select>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="table-responsive" style="background: white">
                <table class="table table-striped table-condensed table-bordered" ng-table="schoolTable">
                    <thead style="background:rgba(12, 153, 0, 0.8); color: white;">
                    <tr>
                        <th>No</th>
                        <th>NPSN</th>
                        <th>Tingkat</th>
                        <th>Nama Sekolah</th>
                        <th>Kecamatan</th>
                        <th width="105px">Total Pegawai</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr ng-repeat="row in $data" >
                        <td>{{($index + 1) + (schoolTable._params.count * (schoolTable._params.page -1))}}
                        </td>
                        <td>{{row.npsn}}</td>
                        <td>{{row.tingkat_sekolah}}</td>
                        <td>{{row.nama}}</td>
                        <td>{{row.nama_kecamatan}}</td>
                        <td style="text-align: center">{{row.total_pegawai}}</td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>