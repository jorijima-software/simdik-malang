/*
    Created by Raga Subekti
    at  13/12/17 22.28 by PhpStorm
*/
angular.module('diknasmlg').factory('$TiketFactory', function($http, $q) {

    function API(path) {
        return localStorage.getItem('PATHLOC') + path;
    }

    var factory = {};

    factory.show = function(start, count, filter) {
        return $q(function(resolve, reject) {
            $http.get(API('api/tiket/show/'+start+'/'+count)).then(function(r) {
                if(r.status === 200) {
                    return resolve(r.data);
                } else {
                    return reject(r);
                }
            })
        })
    };

    factory.delete = function(id) {
        return $q(function(resolve, reject) {
            $http.post(API('api/tiket/delete/' + id), {}).then(function(r) {
                if(r.status === 200) {
                    return resolve(r.data);
                } else {
                    return reject(r);
                }
            })
        })
    };

    factory.insert = function(tiket) {
        return $q(function(resolve, reject) {
            $http.post(API('api/tiket/insert'), tiket).then(function(r) {
                if(r.status === 200) {
                    return resolve(r.data);
                } else {
                    return reject(r);
                }
            })
        })
    };

    factory.getTicketId = function() {
        return $q(function(resolve, reject) {
            $http.get(API('api/tiket/id')).then(function(r) {
                if(r.status === 200) {
                    return resolve(r.data);
                } else {
                    return reject(r);
                }
            })
        })
    };

    return factory;

});