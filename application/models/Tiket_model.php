<?php
/**
 * Created by PhpStorm.
 * User: ragasubekti
 * Date: 12/12/17
 * Time: 19.27
 */
class Tiket_model extends CI_Model {

    public function create() {
        // basically get the post value from here

        $input = $this->input->post();

        $nama = $input['nama'];
        $group_data = $input['group_data'];
        $keterangan = $input['keterangan'];

        if(strlen($nama) < 4) {
            return ['error' => 'Panjang minimal nama permasalahan adalah 4 karakter'];
        } elseif (strlen($group_data) <= 0) {
            return ['error' => 'Mohon pilih group data!'];
        } elseif (strlen($keterangan) < 10) {
            return ['error' => 'Panjang minimal keterangan adalah 10 karakter!'];
        }

        $this->db->insert('data_tiket', [
            'id_tiket' => $this->ticket_id(),
            'nama' => $nama,
            'keterangan' => $input['keterangan'],
            'status' => 1, // DEFAULT Status is Open (1)
            'group_data' => $input['group_data'],
            'id_user' => $this->session->uid
        ]);

        //
        $_POST = []; // reset the arrays
        return ['success' => true];
    }

    public function update() {

    }

    public function reply() {

    }

    public function update_reply() {

    }

    public function delete($id) {
        if($this->db->get_where('data_tiket', ['id' => $id])->num_rows() > 0) {
            $this->db->where('id', $id);
            $this->db->delete('data_tiket');

            return ['success' => true];
        } else {
            return ['error' => 'Tiket tidak ditemukan'];
        }
    }

    /**
     * return a string or a number of the last `tiket id`
     */
    public function ticket_id() {
        // the function is basically to get the last id and add WAN to it :D
        $this->db->order_by('id', 'DESC');
        $q = $this->db->get('data_tiket');
        $last_id = isset($q->row()->id) ? $q->row()->id : 0;

        // TICKET_ID Structure : {DATE}{HOUR}{ID}

        $ticket_id = date('dHi').($last_id+1);

        return ['id' => $ticket_id];
    }

    public function show($start, $count) {
        $this->db->select('data_tiket.*, data_sekolah.nama as nama_sekolah, user.username');
        $this->db->from('data_tiket');
        $this->db->join('user', 'id_user = user.id');
        $this->db->join('data_sekolah', 'user.id_npsn = data_sekolah.npsn', 'left'); // since admin doesn't have id_npsn


        $total = clone $this->db;
        $total = $total->count_all_results();

        $this->db->limit($count, $start);

        $data = $this->db->get()->result();

        return ['data' => $data, 'total' => $total];
    }


}