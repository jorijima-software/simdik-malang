<?php
/**
 * Created by PhpStorm.
 * User: ragasubekti
 * Date: 02/12/17
 * Time: 07.55
 */
?>

<div class="content-header">
    <a class="btn btn-default btn-sm" href="#!/berita">
        <i class="fa fa-caret-left"></i> <span>Kembali</span>
    </a>
</div>
<section class="content">
    <div class="box box-info">
        <div class="box-body">
            <div class="mailbox-read-info">
                <h3>{{berita.judul}}</h3>
                <h5>From: <span ng-if="berita.nama">{{berita.nama}}</span> <span ng-if="!berita.nama">{{berita.username}}</span>
                    <span class="mailbox-read-time pull-right">{{berita.created_at}}</span></h5>
            </div>
            <div class="mailbox-read-message">
            <p ng-bind-html="berita.informasi" id="isi"></p>
            </div>
        </div>
    </div>
</section>

<style type="text/css">
    #isi img {
        max-width: 100%;
    }
</style>