<?php
/**
 * Created by PhpStorm.
 * User: ragasubekti
 * Date: 06/12/17
 * Time: 16.00
 */

class Operator extends CI_Controller
{

    public function page_index()
    {
        $content = null;
        $js = [

        ];

        $this->load->view('layout', ['content' => $content, 'js' => $js]);
    }

    public function spa_index() {
        $this->load->view('operator/index');
    }

    public function spa_activity() {
        $this->load->view('operator/activity');
    }

    public function api_activity($npsn, $start, $count) {
        $this->load->model('Log_model');
        echo json_encode($this->Log_model->operator_activity($npsn, $start, $count));
    }

    public function api_detail($npsn) {
        $this->load->model('Operator_model');
        echo json_encode($this->Operator_model->detail($npsn));
    }

    public function api_operator_delete($id = null)
    {
        if ($this->input->method() == 'post') {
            header('Content-Type: application/json');
            $this->load->model('Operator_model');
            echo json_encode($this->Operator_model->delete($id));
        }
    }

    public function api_operator_insert() {
        if ($this->input->method() == 'post') {
            header('Content-Type: application/json');
            $this->load->model('Operator_model');
            $data = json_decode(trim(file_get_contents('php://input')));
            echo json_encode($this->Operator_model->operator_insert($data));
        }
    }

    public function modal_operator_tambah() {
        $this->load->view('operator/modal/add-edit');
    }

}