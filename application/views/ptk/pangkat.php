<?php
/**
 * Created by PhpStorm.
 * User: ragasubekti
 * Date: 11/10/17
 * Time: 17.13
 */
?>


    <div ng-controller="ptkPangkatController" ng-cloak>

        <section class="content-header">
            <h1>
                Riwayat Pangkat PTK
            </h1>
        </section>

        <section class="content">
            <div class="row">
                <div class="col-md-12">
                    <div class="box">
                        <div class="box-header with-border">
                            <div class="box-title">
                                Informasi Guru
                            </div>
                        </div>
                        <div class="box-body">
                            <table class="table table-consended table-striped">
                                <tr>
                                    <th>Nama</th>
                                    <td>{{detail.nama_lengkap}}</td>
                                </tr>
                                <tr>
                                    <th>NUPTK</th>
                                    <td>{{detail.nuptk}}</td>
                                </tr>
                                <tr>
                                    <th>NIP</th>
                                    <td>{{detail.nip_baru}}</td>
                                </tr>
                                <tr>
                                    <th>Jenis Pegawai</th>
                                    <td>{{detail.jenis_pegawai}}</td>
                                </tr>
                                <tr>
                                    <th>Unit Kerja</th>
                                    <td>{{detail.nama_sekolah}}</td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="box">
                        <div class="box-header with-border">
                            <div class="box-title">
                                Informasi Pangkat
                            </div>
                        </div>
                        <div class="box-body">
                            <div class="row">
                                <div class="col-md-9">
                                    <button class="btn btn-primary btn-sm" ng-click="onBtnAddClicked()" ng-disabled="timerEnd">
                                        <i class="fa fa-plus"></i> Tambah
                                    </button>
                                </div>
                                <div class="col-md-3">
                                    <div class="pull-right">
                                        <div class="pull-right">
                                            <span class="btn btn-info" uib-tooltip="Masa Kerja dihitung mulai dari TMT paling awal/lama">
                                                Masa Kerja
                                                <strong>{{ptk.kepegawaian.masaKerja}}</strong>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <table class="table table-striped" ng-table="rankTable">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>No. SK</th>
                                        <th>TMT Golongan</th>
                                        <th>Kota</th>
                                        <th>Golongan/Ruang</th>
                                        <th>Jenis</th>
                                        <th>Unit Kerja</th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tr ng-repeat="row in $data">
                                    <td>{{($index + 1) + (rankTable._params.count * (rankTable._params.page -1))}}</td>
                                    <td>{{row.no_sk}}</td>
                                    <td>{{row.tmt | date:"dd-MM-yyyy"}}</td>
                                    <td>
                                        <span ng-if="row.area == 1">{{row.nama_kota}}</span>
                                        <span ng-if="row.area != 1">KOTA MALANG</span>
                                    </td>
                                    <td>{{row.gol}}/{{row.ruang}}</td>
                                    <td>
                                        <span ng-if="row.type == 0">CPNS</span>
                                        <span ng-if="row.type == 1">PNS</span>
                                    </td>
                                    <td>
                                        <span ng-if="row.area != 1">{{row.nama_sekolah}}</span>
                                        <span ng-if="row.area == 1">{{row.unit_kerja}}</span>
                                    </td>
                                    <td>
                                        <div class="pull-right">
                                            <button class="btn btn-primary btn-info btn-xs" ng-click="onBtnEditClicked(row)" ng-disabled="timerEnd">
                                                <i class="fa fa-pencil"></i> Edit
                                            </button>
                                            <button class="btn btn-danger btn-xs" ng-click="onBtnDeleteClicked(row)" ng-disabled="timerEnd">
                                                <i class="fa fa-trash"></i> Hapus
                                            </button>
                                        </div>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>