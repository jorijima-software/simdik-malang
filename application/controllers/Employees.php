<?php
/**
 * Created by PhpStorm.
 * User: ragasubekti
 * Date: 07/10/17
 * Time: 12.20
 */
class Employees extends CI_Controller {

    public function get() {
        $this->load->model('Employee_model');
        header('Content-Type: application/json');
        echo json_encode($this->Employee_model->get());
    }

}