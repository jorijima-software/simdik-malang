<?php
/**
 * Created by PhpStorm.
 * User: ragasubekti
 * Date: 1/9/18
 * Time: 5:22 PM
 */

class Ptk extends \Illuminate\Database\Eloquent\Model
{
    protected $table = 'data_guru';
    protected $guarded = [];
    public $timestamps = false;

    public function confirmImport($id)
    {
        $CI = &get_instance();

        $CI->load->model('Sekolah');
        $CI->load->model('PreviewPtk');
        $CI->load->model('RumahPtk');
        $CI->load->model('ProfilPtk');
        $CI->load->model('LogPtk');

        $sekolah = Sekolah::where('id', $id)->first();
        $ptk = PreviewPtk::where('id_sekolah', $id)->get();

        if ($sekolah && $ptk) {
            foreach ($ptk as $key => $row) {
                $current = Ptk::create([
                    'id_sekolah' => $row->id_sekolah,
                    'nuptk' => $row->nuptk,
                    'nama_lengkap' => $row->nama_lengkap,
                    'nip_baru' => $row->nip,
                    'nik' => $row->nik,
                    'nama_ibu' => $row->nama_ibu,
                    'gelar_depan' => $row->gelar_depan,
                    'gelar_belakang' => $row->gelar_belakang,
                    'id_jenis_pegawai' => $row->jenis_pegawai,
                    'status_pns' => in_array($row->jenis_pegawai, [3, 4, 5, 6]) ? '1' : '0',
                ]);

                RumahPtk::create([
                    'id_data_guru' => $current->id,
                    'alamat_rumah' => $row->alamat,
                    'nohp' => $row->no_telp,
                ]);

                ProfilPtk::create([
                    'id_data_guru' => $current->id,
                    'id_jenis_agama' => $row->agama,
                    'tempat_lahir' => $row->tempat_lahir,
                    'tgl_lahir' => date_create($row->tanggal_lahir) ? date_format(date_create($row->tanggal_lahir), 'Y-m-d') : date('Y-m-d  '),
                ]);

                LogPtk::create([
                    'id_user' => $_SESSION['uid'],
                    'date' => date("Y-m-d H:i:s"),
                    'type' => 1,
                    'log' => 'Import PTK (' . $row->nama_lengkap . ')',
                ]);

                PreviewPtk::destroy($row->id);

            }

            return ['success' => true];
        } else {
            return ['error' => 'Data Sekolah tidak ditemukan, mohon hubungi Admin'];
        }
    }

    public function import_xls($data)
    {
        $CI = &get_instance();
        $CI->load->model('PreviewPtk');
        $CI->load->model('JenisPegawai');
        $CI->load->model('Agama');
        foreach ($data as $key => $row) {

            $jenis = JenisPegawai::where('nama', 'like', "%{$row['jenis_pegawai']}%")->first();
            $agama = Agama::where('nama', 'like', "%{$row['agama']}%")->first();
            $ptk = PreviewPtk::create([
                'nuptk' => $row['nuptk'],
                'nama_lengkap' => $row['nama_lengkap'],
                'nik' => $row['nik'],
                'nip' => $row['nip'],
                'gelar_depan' => $row['gelar_depan'],
                'gelar_belakang' => $row['gelar_belakang'],
                'jenis_pegawai' => $jenis ? $jenis->id : null,
                'tempat_lahir' => $row['tempat_lahir'],
                'tanggal_lahir' => $row['tanggal_lahir'],
                'agama' => $agama ? $agama->id : null,
                'nama_ibu' => $row['nama_ibu'],
                'no_telp' => $row['no_hp'],
                'alamat' => $row['alamat'],
                'id_sekolah' => $row['id_sekolah'],
            ]);

            if (!$ptk) {
                return ['error' => 'Kesalahan saat membaca file'];
            }
        }
    }

    public function sertifikasi()
    {
        $CI = &get_instance();
        $CI->load->model('SertifikasiPtk');
        return $this->hasOne('SertifikasiPtk', 'id_data_guru', 'id');
    }

    public function profil()
    {
        $CI = &get_instance();
        $CI->load->model('ProfilPtk');
        return $this->hasOne('ProfilPtk', 'id_data_guru', 'id');
    }

    public function rumah()
    {
        $CI = &get_instance();
        $CI->load->model('RumahPtk');
        return $this->hasOne('RumahPtk', 'id_data_guru', 'id');
    }

    public function jenisPegawai()
    {
        $CI = &get_instance();
        $CI->load->model('JenisPegawai');
        return $this->hasOne('JenisPegawai', 'id', 'id_jenis_pegawai');
    }

    public function unitKerja()
    {
        $CI = &get_instance();
        $CI->load->model('Sekolah');
        return $this->hasOne('Sekolah', 'id', 'id_sekolah');
    }

    public function riwayatSekolah()
    {
        $CI = &get_instance();
        $CI->load->model('RiwayatSekolahPtk');

        return $this->hasMany('RiwayatSekolahPtk', 'id_data_guru', 'id');
    }

    public function riwayatPangkat()
    {
        $CI = &get_instance();
        $CI->load->model('RiwayatPangkatPtk');

        return $this->hasMany('RiwayatPangkatPtk', 'nip', 'nip_baru');
    }

    public function riwayatGaji()
    {
        $CI = &get_instance();
        $CI->load->model('RiwayatGajiPtk');

        return $this->hasMany('RiwayatGajiPtk', 'nip', 'nip_baru');
    }

    public function riwayatKerja()
    {
        $CI = &get_instance();
        $CI->load->model('RiwayatKerjaPtk');

        return $this->hasMany('RiwayatKerjaPtk', 'id_data_guru', 'id');
    }

    public function guru()
    {
        $CI = &get_instance();
        $CI->load->model('DataGuru');

        return $this->hasOne('DataGuru', 'id_data_guru', 'id');
    }

    public function rekening()
    {
        $CI = &get_instance();
        $CI->load->model('PtkRekening');

        return $this->hasOne('PtkRekening', 'data_guru_id', 'id');
    }

    public function umur($id) {

    }

    public function masaKerja($id)
    {
        $CI = &get_instance();
        $CI->load->model('RiwayatKerjaPtk');
        $CI->load->model('RiwayatPangkatPtk');

        $instance = Ptk::find($id);
        if ($instance->status_pns > 0) {
            $oldestPangkat = RiwayatPangkatPtk::where('nip', $instance->nip_baru)->orderBy('tmt', 'ASC')->first();
            return ($oldestPangkat)
                ? Carbon\Carbon::parse($oldestPangkat->tmt)->diff(Carbon\Carbon::now())->format('%y Tahun %m Bulan')
                : 0 . ' Tahun';
        } else {
            $oldestKerja = RiwayatKerjaPtk::where('id_data_guru', $id)->orderBy('tmt', 'ASC')->first();
            return ($oldestKerja)
                ? Carbon\Carbon::parse($oldestKerja->tmt)->diff(Carbon\Carbon::now())->format('%y Tahun %m Bulan')
                : 0 . ' Tahun';
        }

    }

}
