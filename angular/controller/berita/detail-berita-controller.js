angular.module('diknasmlg').controller('detailBeritaController', function($scope, $routeParams, $BeritaFactory) {
    $scope.routes = $routeParams;

    $scope.berita = {};

    function init() {
        $BeritaFactory.detail($scope.routes.id, function(reply) {
            $scope.berita = reply;
        })
    }

    init();

});