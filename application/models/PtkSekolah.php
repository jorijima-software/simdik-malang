<?php

class PtkSekolah extends \Illuminate\Database\Eloquent\Model
{
    protected $table = 'sekolah_guru';
    protected $guarded = [];
    public $timestamps = false;

    /**
     * Return object of Jenis Tingkat pendidikan of current Ptk
     *
     * @return void
     */
    public function tingkatPendidikan()
    {
        $CI = &get_instance();
        $CI->load->model('JenisTingkatPendidikan');

        return $this->hasOne('JenisTingkatPendidikan', 'id', 'id_jenis_tingkat_pend');
    }

    public function ptk()
    {
        $CI = &get_instance();
        $CI->load->model('Ptk');

        return $this->hasOne('Ptk', 'id', 'id_data_guru');
    }

    public function idKecamatan($kecamatan)
    {
        $CI = &get_instance();
        $CI->load->model('Kecamatan');

        return Kecamatan::where('name', $kecamatan)->first();
    }

    public function idKota($kota)
    {
        $CI = &get_instance();
        $CI->load->model('Kota');

        return Kota::where('name', $kota)->first();
    }
}
