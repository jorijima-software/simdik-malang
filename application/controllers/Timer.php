<?php

use Carbon\Carbon;

class Timer extends CI_Controller {

    public function deleteCountdown($id) {
        if(!isset($_SESSION['as']) || $this->session->as > 0) {
            echo json_encode([
                'error' => 'Insufficent Priviledge'
            ]);
            die();
        }

        $this->load->model('TimerModel');
        $timer = TimerModel::find($id);
        if($timer->active == 1) {
            echo json_encode(['error' => 'Tidak dapat menghapus timer karena sedang aktif']);
            die();
        }

        TimerModel::destroy($id);

        echo json_encode(['success' => true]);
    }

    public function editCountdown($id) {
        if(!isset($_SESSION['as']) || $this->session->as > 0) {
            echo json_encode([
                'error' => 'Insufficent Priviledge'
            ]);
            die();
        }

        $_post = json_decode(trim(file_get_contents('php://input')));

        $this->load->model('TimerModel');

        TimerModel::find($id)->update([
            'message' => $_post->message,
            'endtime' => Carbon::parse($_post->endtime)->format('Y-m-d 23:59:59')
        ]);

        echo json_encode(['success' => true]);
    }

    public function activateCountdown($id) {
        if(!isset($_SESSION['as']) || $this->session->as > 0) {
            echo json_encode([
                'error' => 'Insufficent Priviledge'
            ]);
            die();
        }

        $this->load->model('TimerModel');

        TimerModel::query()->update(['active' => 0]);

        TimerModel::find($id)->update(['active' => 1]);

        echo json_encode(['success' => true]);
    }

    public function addCountdown() {
        if(!isset($_SESSION['as']) || $this->session->as > 0) {
            echo json_encode([
                'error' => 'Insufficent Priviledge'
            ]);
            die();
        }

        $_post = json_decode(trim(file_get_contents('php://input')));

        $this->load->model('TimerModel');

        $timer = new TimerModel();
        $timer->endtime = Carbon::parse($_post->endtime)->format('Y-m-d 23:59:59');
        $timer->message = $_post->message;
        $timer->active = '0';
        $timer->save();

        echo json_encode(['success' => true]);
    }

    public function apiCountdown($start = 0, $count = 10) {
        $this->load->model('TimerModel');

        $timer = TimerModel::take(10)->orderBy('id', 'DESC');
        $total = $timer->count();

        echo json_encode([
            'data' => $timer->skip($start)->get(),
            'total' => $total
        ]);
    }

    public function getCountdown()
    {
        $this->load->model('TimerModel');

        $timer = TimerModel::where('active', 1)->first();
        echo json_encode([
            'time' => ($timer) ? Carbon::parse($timer->endtime)->format('F d, Y H:i:s') : null,
            'message' => $timer->message,
            'admin' => !!($this->session->as == '0')
        ]);
    }

    public function settingUpdateModal() {
        $this->load->view('setting/modal/timer/edit');
    }

    public function settingTambahModal() {
        $this->load->view('setting/modal/timer/tambah');
    }

    public function timerEndModal() {
        $this->load->view('timer-end');
    }

    public function spa_setting() {
        $this->load->view('setting/timer');
    }

}