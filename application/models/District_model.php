<?php

/**
 * Created by PhpStorm.
 * User: ragasubekti
 * Date: 08/10/17
 * Time: 11.05
 */
class District_model extends CI_Model
{
    public function get()
    {
        return $this->db->get('kecamatan')->result();
    }

    public function regency($query)
    {
        $this->db->select('*');
        $this->db->from('regencies');
        if(strlen($query) > 0 && $query != 'null' && !is_null($query)) {
            $this->db->like('name', $query);
        }

        $this->db->limit(10);

        return $this->db->get()->result();
    }

    public function district($id, $query) {
        $this->db->select('*');
        $this->db->from('districts');

        if(strlen($query) > 0 && $query != 'null' && !is_null($query)) {
            $this->db->like('name', $query);
        }

        $this->db->where('regency_id', $id);

        return $this->db->get()->result();

    }
    public function schoolondistrict()
    {
        $school = $this->db->query("
            SELECT
                COUNT(*) as total,
                SUM(CASE WHEN id_jenis_tingkat_sekolah = 1 then 1 ELSE 0 END) as tk,
                SUM(CASE WHEN id_jenis_tingkat_sekolah = 2 then 1 ELSE 0 END) as sd,
                SUM(CASE WHEN id_jenis_tingkat_sekolah = 3 then 1 ELSE 0 END) as smp,
                SUM(CASE WHEN id_jenis_tingkat_sekolah = 4 then 1 ELSE 0 END) as sma,
                SUM(CASE WHEN id_jenis_tingkat_sekolah = 5 then 1 ELSE 0 END) as smk,
                kecamatan.nama as nama,kecamatan.id
            FROM
                data_sekolah
                JOIN jenis_tingkat_sekolah ON jenis_tingkat_sekolah.id = data_sekolah.id_jenis_tingkat_sekolah
                JOIN kecamatan ON kecamatan.id = data_sekolah.id_kecamatan
            GROUP BY
                id_kecamatan
        ")->result();
        $tpegawai = $this->db->query("
            SELECT
                COUNT(*) as total,
                id_kecamatan as id
            FROM
                data_guru
                JOIN data_sekolah ON data_sekolah.id = data_guru.id_sekolah
            GROUP BY
                id_kecamatan
        ")->result();
        foreach ($school as $key=>$row){
            foreach ($tpegawai as $data=>$row2 ){
                if($row->id == $row2->id) {
                    $school[$key]->total_pegawai = $row2->total;
                }
            }
        }

        return $school;

        /*return $this->db->schoolondistrict()->result();*/
    }
    public function showschool($start, $count, $query, $query2)
    {
        $this->db->select('data_sekolah.*, kecamatan.nama as nama_kecamatan, jenis_tingkat_sekolah.nama as tingkat_sekolah');
        $this->db->from('data_sekolah');

        if ($query && $query != 'null' && $query !== null) {
            $this->db->or_like('kecamatan.nama', $query);
        }
        if ($query2 && $query2 != 'null' && $query2 !== null) {
            $this->db->like('jenis_tingkat_sekolah.nama', $query2);
        }

        $this->db->join('kecamatan', 'kecamatan.id = data_sekolah.id_kecamatan');
        $this->db->join('jenis_tingkat_sekolah', 'jenis_tingkat_sekolah.id = data_sekolah.id_jenis_tingkat_sekolah');

        $total = clone $this->db;
        $this->db->limit($count, $start);
        $this->db->order_by('id', 'asc');

        return array('data' => $this->db->get()->result(), 'total' => $total->count_all_results());
    }

    public function showteacher($start, $count, $query, $search = null )
    {
        $this->db->select('data_guru.*, jenis_pegawai.nama as jenis_pegawai, data_sekolah.nama as nama_sekolah');
        $this->db->from('data_guru');

        if ($query && $query != 'null' && $query !== null) {
            $this->db->like('id_kecamatan', $query);
        };
        if ($search && $search != 'null' && $search !== null) {
            $this->db->like('nuptk', $search);
            $this->db->or_like('nama_lengkap', $search);
            $this->db->or_like('nip_lama', $search);
        };

        $this->db->join('jenis_pegawai', 'jenis_pegawai.id = data_guru.id_jenis_pegawai');
        $this->db->join('data_sekolah', 'data_sekolah.id = data_guru.id_sekolah', 'left');

        $total = clone $this->db;

        $this->db->limit($count, $start);
        $this->db->order_by('id', 'asc');

        $data = $this->db->get()->result();
        foreach ($data as $key => $row) {

            $this->db->select('COUNT(*) as data_sekolah');
            $this->db->from('sekolah_guru');
            $this->db->where('id_data_guru', $row->id);

            $data_sekolah = $this->db->get()->row()->data_sekolah;
            $data[$key]->data_sekolah = $data_sekolah;

            $this->db->select('COUNT(*) as total_gaji');
            $this->db->from('gaji_berkala_guru');
            $this->db->where('nip', $row->nip_baru);

            $data[$key]->gaji_berkala = $this->db->get()->row()->total_gaji;


            $this->db->select('COUNT(*) as total_jg');
            $this->db->from('pangkat_guru');
            $this->db->where('nip', $row->nip_baru);

            $result = $this->db->get()->row();
            $data[$key]->pangkat_guru = $result->total_jg;


        }

        return array('data' => $data, 'total' => $total->count_all_results());
    }
}