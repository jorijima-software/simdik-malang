<?php

/**
 * Created by PhpStorm.
 * User: ragasubekti
 * Date: 1/18/18
 * Time: 7:16 AM
 */
class Riwayat extends CI_Controller
{

    public function apiKerjaEdit($id)
    {
        $this->load->model('User');
        $this->load->model('RiwayatKerjaPtk');

        if (!isset($_SESSION['as'])) {
            echo json_encode(['error' => 'Insufficent Priviledge']);
        }

        $instance = RiwayatKerjaPtk::find($id);
        
        if ($instance) {
            $_post = json_decode(trim(file_get_contents('php://input')));
            if ($_post->lokasi->id == '0') {
                $instance->update([
                    'tmt' => \Carbon\Carbon::parse($_post->tmt)->addDays(1)->format('Y-m-d'),
                    'no_sk' => $_post->sk,
                    'id_sekolah' => $_post->unitKerja->id,
                    'gaji' => $_post->gaji,
                    'lokasi' => $_post->lokasi->id,
                ]);
                
            } else if ($_post->lokasi->id == '1') {
                $instance->update([
                    'no_sk' => $_post->sk,
                    'tmt' => \Carbon\Carbon::parse($_post->tmt)->addDays(1)->format('Y-m-d'),
                    'gaji' => $_post->gaji,
                    'lokasi' => $_post->lokasi->id,
                    'id_kota' => $_post->kotaUnitKerja->id,
                    'nama_unit_kerja' => $_post->namaUnitKerja
                ]);
            }

            echo json_encode(['success' => true]);
        } else {
            echo json_encode(['error' => 'Data Not Found']);
        }

    }

    public function api_kerja_delete($id)
    {
        $this->load->model('User');
        $this->load->model('RiwayatKerjaPtk');

        if (!isset($_SESSION['as'])) {
            echo json_encode(['error' => 'Insufficent Priviledge']);
        } else if ($this->session->as > 0) {
            $instance = RiwayatKerjaPtk::find($id);
            $user = User::find($this->session->uid);

//            if($instance->ptk->sekolah->id != $user->sekolah->id) {
            //                echo json_encode(['error' => 'Insufficent Priviledge']);
            //            } else {
            RiwayatKerjaPtk::destroy($id);
            echo json_encode(['success' => true]);
//            }
        } else {
            RiwayatKerjaPtk::destroy($id);
            echo json_encode(['success' => true]);
        }
    }

    public function api_kerja_show($id, $start = 0, $count = 10)
    {
        $this->load->model('RiwayatKerjaPtk');

        $riwayat = RiwayatKerjaPtk::take($count);

        $riwayat->where('id_data_guru', $id);

        $total = clone $riwayat;
        $total = $total->count();

        $data = [];
        foreach ($riwayat->skip($start)->get() as $key => $row) {
            array_push($data, [
                'id' => $row->id,
                'unitKerja' => ($row->lokasi == '0') ?
                (($row->unitKerja) ? $row->unitKerja->nama : null) :
                $row->nama_unit_kerja,
                'idUnitKerja' => ($row->lokasi == '0') ?
                (($row->unitKerja) ? $row->unitKerja->id : null) : null,
                'namaKota' => $row->kota ? $row->kota->name : null,
                'idKota' => $row->kota ? $row->kota->id : null,
                'gaji' => $row->gaji,
                'tmt' => $row->tmt,
                'sk' => $row->no_sk,
            ]);
        }

        echo json_encode(['total' => $total, 'data' => $data]);
    }

    public function modalKerjaEdit()
    {
        $this->load->view('ptk/modal/riwayat/edit');
    }

    public function modal_kerja_tambah()
    {
        $this->load->view('ptk/modal/tambah-riwayat-kerja');
    }

    public function spa_kerja()
    {
        $this->load->view('ptk/riwayat-kerja');
    }

}
