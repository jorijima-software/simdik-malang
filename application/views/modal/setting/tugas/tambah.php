<?php
/**
 * Created by PhpStorm.
 * User: ragasubekti
 * Date: 17/11/17
 * Time: 22.58
 */
?>

<form ng-submit="onBtnSubmit()">


<div class="modal-header">
    Tambah Tugas Tambahan
</div>
<div class="modal-body">
        <div class="form-group">
            <label>Nama</label>
            <input type="text" class="form-control" ng-model="new.nama">
        </div>
</div>

<div class="modal-footer">
    <button class="btn btn-primary btn-sm" ng-click="onBtnSaveClicked()">
        <i class="fa fa-check"></i> <span>Simpan</span>
    </button>
    <button class="btn btn-default btn-sm" ng-click="dismiss()">
        <i class="fa fa-times"></i> <span>Batal</span>
    </button>
</div>

</form>
