angular.module('diknasmlg').controller('ptkController', function (
    $scope, 
    $uibModal, 
    NgTableParams, 
    $q, 
    $TeacherFactory, 
    $EmployeeFactory, 
    Notify, 
    $routeParams, 
    $PtkFactory, 
    $LogFactory, 
    $TimerFactory
) {
    $scope.timerEnd
    $scope.stats2

    $scope.router = $routeParams;

    $scope.latestLog = [];
    $scope.labels = [];
    $scope.data = [];

    var apiPath = localStorage.getItem('PATHLOC');

    $scope.search = '';
    $scope.edit = {};


    $scope.bgIndex = [
        'bg-aqua',
        'bg-yellow',
        'bg-green',
        'bg-orange',
        'bg-red'
    ];

    $scope.statsbox = {};

    $scope.pegawaiList = [];
    $scope.employeeList = [];

    $scope.filter = {
        nuptk: 0,
        pns: 0,
        pegawai: {}
    };

    $scope.onSearchKeydown = function($event) {
        if($event.keyCode == 13) {
            $scope.doSearch()
        }
    }

    $scope.doSearch = function() {
        $scope.nonPnsTable.reload()
        $scope.teacherTable.reload()
    }

    $scope.onBtnRekeningDetail = function(row) {
        $uibModal.open({
            templateUrl: apiPath + 'ptkrekcontroller/modaldetail',
            controller: 'ModalPtkRekeningDetail',
            resolve: {
                ptkData: function() {
                    return row
                }
            }
        })
    }

    $scope.onKekuranganClicked = function (row) {
        $uibModal.open({
            templateUrl: apiPath + 'ptkbaru/modalkekurangan',
            controller: function ($scope, $uibModalInstance) {
                $scope.detail = row
                $scope.dismiss = function () {
                    $uibModalInstance.dismiss()
                }
            }
        })
    }

    $scope.onBtnImportClicked = function () {
        $uibModal.open({
            templateUrl: apiPath + 'ptk/modal_import_xls',
            controller: 'sekolah.ptk.import'
        })
    }

    $scope.onBtnFilterNuptk = function (val) {
        $scope.filter.nuptk = val;
        $scope.teacherTable.reload();
        $scope.nonPnsTable.reload();
    };

    $scope.onBtnFilterPns = function (val) {
        $scope.filter.pns = val;
        $scope.teacherTable.reload();
        $scope.nonPnsTable.reload();
    };

    $scope.onFilterPegawaiChange = function () {
        $scope.teacherTable.reload();
        $scope.nonPnsTable.reload();
    };

    $scope.onBtnAddClicked = function () {
        $uibModal.open({
            templateUrl: apiPath + 'modal/ptk/tambah',
            size: 'lg',
            backdrop: 'static',
            controller: 'tambahPtk'
        }).result.then(function () {
            $scope.teacherTable.reload();
            $scope.nonPnsTable.reload();
        }, function () {
            $scope.teacherTable.reload();
            $scope.nonPnsTable.reload();
        });
    };

    $scope.onBtnDetailClicked = function (row) {
        $uibModal.open({
            templateUrl: apiPath + 'modal/ptk/detail',
            controller: 'detailPtk',
            size: 'lg',
            resolve: {
                data: function () {
                    return row;
                }
            }
        });
    };

    $scope.onBtnDeleteClicked = function (row) {
        $uibModal.open({
            templateUrl: apiPath + 'modal/ptk/hapus',
            controller: 'PtkDeleteModal',
            resolve: {
                data: function() {
                    return row;
                }
            }
        }).result.then(function() {
            $scope.nonPnsTable.reload()
            $scope.teacherTable.reload()
        }, function() {
            $scope.nonPnsTable.reload()
            $scope.teacherTable.reload()
        })
    };

    $scope.onBtnEditClicked = function (row) {
        $uibModal.open({
            templateUrl: apiPath + 'modal/ptk/edit',
            controller: 'editPtk',
            size: 'lg',
            backdrop: 'static',
            resolve: {
                data: function () {
                    return row;
                }
            }
        }).result.then(function () {
            $scope.teacherTable.reload();
            $scope.nonPnsTable.reload();
        }, function () {
            $scope.teacherTable.reload();
            $scope.nonPnsTable.reload();
        });
    };

    $scope.onInputSearchChange = function () {
        $scope.teacherTable.reload();
        $scope.nonPnsTable.reload();
    };

    $scope.teacherTable = new NgTableParams({}, {
        getData: function (params) {
            var count = params.count();
            var start = count * (params.page() - 1);
            // var search = $scope.search;
            var filter = $scope.filter;
                filter.search = $scope.search;
            return $q(function (resolve, reject) {
                $PtkFactory.pns(start, count, filter).then(function (reply) {
                    if (reply.error) return reject(reply.error);
                    params.total(reply.total);
                    return resolve(reply.data);
                })
            })
        }
    })

    $scope.nonPnsTable = new NgTableParams({}, {
        getData: function (params) {
            var count = params.count();
            var start = count * (params.page() - 1);
            // var search = $scope.search;
            var filter = $scope.filter;
            filter.search = $scope.search;
            return $q(function (resolve, reject) {
                $PtkFactory.npns(start, count, filter).then(function (reply) {
                    if (reply.error) return reject(reply.error);
                    params.total(reply.total);
                    return resolve(reply.data);
                });
            });
        }
    })

    function reloadStats() {
        $PtkFactory.stats().then(function (r) {
            $scope.statsbox = r;
        });
    }

    function init() {

        $PtkFactory.stats().then(function (r) {
            $scope.statsbox = r;
            var data = [];

            r.jenis.forEach(function(d) {
                $scope.data.push(d.total)
                $scope.labels.push(d.jenis)
                // data.push({
                //     name: d.jenis,
                //     y: d.total
                // })
            });

        });

        $PtkFactory.stats2().then(function(result) {
            $scope.stats2 = result
        })

        $EmployeeFactory.get(function (reply) {
            $scope.employeeList = reply;
            $scope.employeeList.unshift({id: '0', nama: 'Semua'});
        });

        $LogFactory.latest().then(function (r) {
            r.forEach(function (d) {
                $scope.latestLog.push({
                    log: d.log,
                    type: d.type,
                    date: moment(d.date).format('DD/MM/YYYY HH:mm:ss'),
                    namaSekolah: d.namaSekolah,
                    username: d.username
                })
            })
        })
        $TimerFactory.diff().then(result => {
            $scope.timerEnd = (!result.admin && result.disabled)
            console.dir(result)
        })
    }

    init();
});