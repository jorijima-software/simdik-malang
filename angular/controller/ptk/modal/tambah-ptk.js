// Created by Raga Subekti at 07/10/17
angular.module('diknasmlg').controller('tambahPtk', function ($scope, $EmployeeFactory, $SchoolFactory, $q, $uibModalInstance, $TeacherFactory, Notify, $ReligionFactory, $EducationFactory, $DistrictFactory, $PtkFactory) {

    $scope.datepicker = false;

    $scope.isSubmit = false;

    $scope.schoolList = [];
    $scope.employeeTypeList = [];
    $scope.religionList = [];
    $scope.educationList = [];
    $scope.sekolahStatus = [
        {id: 0, nama: 'Tidak Aktif'},
        {id: 1, nama: 'Aktif'}
    ];

    $scope.rumahRegencyList = [];
    $scope.rumahKecamatanList = [];
    $scope.bidangStudiList = [];

    $scope.onRumahRegencyChange = function (query) {
        $DistrictFactory.regency(query, function (reply) {
            $scope.rumahRegencyList = reply;
        });
    };

    $scope.onRumahRegencySelect = function() {
        $DistrictFactory.show($scope.new.rumah.kota.id, '', function(reply) {
            $scope.rumahKecamatanList = reply;
        });
    };

    $scope.onSekolahRegencySelect = function() {
        $DistrictFactory.show($scope.new.sekolah_guru.kota.id, '', function(reply) {
            $scope.rumahKecamatanList = reply;
        });
    };


    $scope.onBidangStudiChange = function(q) {
        $TeacherFactory.bidangStudi(q, function (reply) {
            $scope.bidangStudiList = reply;
        });
    };

    $scope.onSekolahKecamatanChange = function(query) {
        $DistrictFactory.show($scope.new.sekolah_guru.kota.id, query, function(reply) {
            $scope.rumahKecamatanList = reply;
        });
    };

    $scope.onRumahKecamatanChange = function(query) {
        $DistrictFactory.show($scope.new.rumah.kota.id, query, function(reply) {
            $scope.rumahKecamatanList = reply;
        });
    };

    $scope.new = {
        'nuptk': '',
        'nama_lengkap': '',
        'gelar_depan': '',
        'gelar_belakang': '',
        'nip_lama': '',
        'nip_baru': '',
        'pegawai': {},
        'sekolah': {},
        'profil': {
            tempat_lahir: '',
            tanggal_lahir: new Date(),
            agama: {}
        },
        'rumah': {
            kota: '',
            kecamatan: '',
            no_telp: '',
            alamat: ''
        },
        'sekolah_guru': {},
        'sert': {
        }
    };

    $scope.dateOptions = {
        formatYear: 'yy',
        maxDate: new Date(),
        startingDay: 1
    };


    $scope.onInputSearchChange = function (query) {
        return $SchoolFactory.get(query, function (reply) {
            $scope.schoolList = reply;
        });
    };

    $scope.onHomeDistrictChange = function () {
        $scope.new.rumah.kecamatan = $scope.new.rumah.kecamatan.toUpperCase();
    };

    $scope.onHomeCityChange = function () {
        $scope.new.rumah.kota = $scope.new.rumah.kota.toUpperCase();
    };

    $scope.onSchoolNameChange = function () {
        $scope.new.sekolah_guru.nama = $scope.new.sekolah_guru.nama.toUpperCase();
    };

    $scope.onSchoolDistrictChange = function () {
        $scope.new.sekolah_guru.kecamatan = $scope.new.sekolah_guru.kecamatan.toUpperCase();
    };

    $scope.onSchoolCityChange = function () {
        $scope.new.sekolah_guru.kota = $scope.new.sekolah_guru.kota.toUpperCase();
    };

    $scope.onBtnSaveClicked = function () {
        $scope.isSubmit = true;
        $PtkFactory.insert($scope.new).then(function (reply) {
            $scope.isSubmit = false;
            if (reply.error) {
                Notify.error('Kesalahan', reply.error);
            } else if (reply.success) {
                Notify.success('Sukses', 'Berhasil menambahkan data PTK');
                $uibModalInstance.close();
            }
        });
    };

    $scope.onBirthPlaceChange = function () {
        $scope.new.profil.tempat_lahir = $scope.new.profil.tempat_lahir.toUpperCase();
    };

    $scope.dismiss = function () {
        $uibModalInstance.dismiss();
    };

    function init() {

        $EmployeeFactory.get(function (reply) {
            $scope.employeeTypeList = reply;
        });

        $SchoolFactory.get('', function (reply) {
            $scope.schoolList = reply;
        });

        $ReligionFactory.show(function (reply) {
            $scope.religionList = reply;
        });

        $EducationFactory.get(function (reply) {
            $scope.educationList = reply;
        });

        $DistrictFactory.regency('', function (reply) {
            $scope.rumahRegencyList = reply;
        });

        $TeacherFactory.bidangStudi('', function (reply) {
            $scope.bidangStudiList = reply;
        });

    }

    init();

});