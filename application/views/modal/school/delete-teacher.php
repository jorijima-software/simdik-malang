<?php
/**
 * Created by PhpStorm.
 * User: ragasubekti
 * Date: 18/10/17
 * Time: 16.05
 */
?>

<div class="modal-body">
Apakah anda yakin ingin menghapus data sekolah berikut:
    <table class="table table-striped table-condensed table-bordered">
        <tr>
            <th>Nama Sekolah</th>
            <td>{{data.nama_sekolah}}</td>
        </tr>
        <tr>
            <th>Tingkat</th>
            <td>{{data.tingkat_pend}}</td>
        </tr>
    </table>
</div>
<div class="modal-footer">
    <button class="btn btn-danger btn-sm" ng-click="onBtnConfirmClicked()">
        <i class="fa fa-check"></i> <span>Konfirmasi</span>
    </button>
    <button class="btn btn-default btn-sm" ng-click="dismiss()">
        <i class="fa fa-times"></i> <span>Batal</span>
    </button>
</div>