<?php
/**
 * Created by PhpStorm.
 * User: ragasubekti
 * Date: 06/12/17
 * Time: 16.02
 */
?>

<section class="content-header">
    <h1>Data Operator</h1>
</section>

<section class="content" ng-controller="operatorController">
    <div class="box">
        <div class="box-body">

            <div class="row">
            <div class="col-sm-8">
                <button class="btn btn-primary btn-sm" style="margin-bottom: 5px" ng-click="onBtnAddClicked()">
                    <i class="fa fa-plus"></i> <span>Tambah</span>
                </button>
            </div>
            <div class="col-sm-4">
                <div class="pull-right">
                    <div class="input-group">
                        <input type="text" class="form-control" ng-model="search" ng-change="onSearchChange()"
                               placeholder="Pencarian">
                        <div class="input-group-addon">
                            <i class="fa fa-search"></i>
                        </div>
                    </div>
                </div>
            </div>
            </div>

<!--            <div class="table-responsive">-->

                <table class="table table-condensed table-striped" ng-table="operatorTable">
                    <thead>
                    <tr>
                        <th>No.</th>
                        <th>NPSN</th>
                        <th>Sekolah</th>
                        <th>Nama</th>
                        <th>No. HP</th>
                        <th>No. Whatsapp</th>
                        <th>Kode Verifikasi</th>
                        <th>Last Login</th>
                        <th></th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr ng-repeat="row in $data">
                        <td>
                            {{($index + 1) + (operatorTable._params.count * (operatorTable._params.page -1))}}
                        </td>
                        <td>{{row.npsn}}</td>
                        <td>{{row.nama_sekolah.substring(0, 18)}}<span ng-if="row.nama_sekolah.length > 18">...</span></td>
                        <td>{{row.nama_operator.substring(0, 18)}}<span ng-if="row.nama_operator.length > 18">...</span></td>
                        <td>{{row.no_hp}}</td>
                        <td>{{row.no_wa}}</td>
                        <td><code ng-if="row.verify.length > 0" class="text-uppercase">{{row.verify}}</code></td>
                        <td>{{row.last_login}}</td>

                        <td>
                            <div class="pull-right">
                                <a href="<?=site_url('/')?>#!/operator/activity/{{row.npsn}}" class="btn btn-info btn-xs">
                                    <i class="fa fa-search"></i> <span>Aktifitas</span>
                                </a>
                                <button class="btn btn-primary btn-xs" ng-click="onBtnEditClicked(row)">
                                    <i class="fa fa-pencil"></i> <span>Edit</span>
                                </button>
                                <button class="btn btn-danger btn-xs" ng-click="onBtnDeleteClicked(row)">
                                    <i class="fa fa-times"></i> <span>Hapus</span>
                                </button>
                            </div>
                        </td>
                    </tr>
                    </tbody>
                </table>

<!--            </div>-->
        </div>
    </div>
</section>