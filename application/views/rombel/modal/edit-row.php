<?php
/**
 * Created by PhpStorm.
 * User: ragasub
 * Date: 12/22/17
 * Time: 3:47 PM
 */
?>

<div class="modal-header">
    <div class="modal-title">Edit Rombel</div>
</div>
<div class="modal-body">
    <div class="alert alert-danger" ng-if="error">
        <i class="fa fa-exclamation-triangle"></i> {{error}}
    </div>
    <div class="form-group">
        <label>Nama Rombel</label>
        <input type="text" class="form-control" ng-model="rombel.nama">
    </div>
    <div class="row">
        <div class="col-sm-6">
            <div class="form-group">
                <label>Laki-Laki</label>
                <input type="text" class="form-control" ng-model="rombel.laki">
            </div>
        </div>
        <div class="col-sm-6">
            <div class="form-group">
                <label>Perempuan</label>
                <input type="text" class="form-control" ng-model="rombel.perempuan">
            </div>
        </div>
    </div>
</div>

<div class="modal-footer">
    <button class="btn btn-primary btn-sm" ng-click="onBtnSaveClicked()" ng-disabled="isSubmit">
        <i class="fa fa-check" ng-if="!isSubmit"></i><i class="fa fa-circle-o-notch fa-spin" ng-if="isSubmit"></i> <span>Simpan</span>
    </button>
    <button class="btn btn-default btn-sm" ng-click="dismiss()">
        <i class="fa fa-times"></i> <span>Batal</span>
    </button>
</div>