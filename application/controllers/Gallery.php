<?php
/**
 * Created by PhpStorm.
 * User: arsyad09
 * Date: 01/12/17
 * Time: 19:23
 */

class Gallery extends CI_Controller
{
    public function index()
    {
        $content = 'article/article-gallery';
        $this->load->view('article', array('content' => $content));
    }

    public function upload() {
        $this->load->view('modal/gallery/upload-photo');
    }
    public function delete_modal() {
        $this->load->view('modal/gallery/delete');
    }

    public function show($start, $count, $search) {
        $this->load->model('Gallery_model');

        header('Content-Type: application/json');
        echo json_encode($this->Gallery_model->show($start, $count, $search));
    }

    public function delete($id)
    {
        header('Content-Type: application/json');

        $this->load->model('Gallery_model');

        echo json_encode($this->Gallery_model->delete($id));
    }

    public function create()
    {
        if ($this->input->method() == 'post') {

            header('Content-Type: application/json');
            $data = json_decode(trim(file_get_contents('php://input')));

            $tittle = $data->tittle;
            $description = $data->description;
            $uploader = $data->uploader;

            $this->load->model('Gallery_model');
            echo json_encode($this->Gallery_model->create($tittle, $description, $uploader));
        }
    }
    public function manage(){
        $content = 'article/article-manage-gallery';
        $this->load->view('article', array('content' => $content));
    }
}