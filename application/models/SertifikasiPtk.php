<?php

class SertifikasiPtk extends \Illuminate\Database\Eloquent\Model
{
    protected $table = "sertifikasi_guru";
    protected $guarded = [];

    public function bidangStudi() {
        $CI =& get_instance();
        $CI->load->model('BidangStudi');
        return $this->hasOne('BidangStudi', 'id','id_bidang_studi');
    }

    public function namaPola($int) {
        if($int === '0') {
            return 'Portofolio';
        } else if($int === '1') {
            return 'PLPG';
        } else if($int === '2') {
            return 'PPG';
        } else {
            return '';
        }
    }
}