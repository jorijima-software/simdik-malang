<?php
/**
 * Created by PhpStorm.
 * User: ragasubekti
 * Date: 2/2/18
 * Time: 6:37 PM
 */
?>
<div class="modal-header">
    <strong>Update Timer</strong>
</div>
<div class="modal-body">
    <div class="form-group">
        <label>Waktu Habis</label>
        <div class="input-group">
            <input type="text" class="form-control" uib-datepicker-popup="dd-MM-yyyy"
                   ng-model="timer.endtime"
                   close-text="Close" is-open="datepicker" placeholder="dd-mm-yyyy"
                   ng-click="datepicker = !datepicker"/>
            <div class="input-group-addon">
                <i class="fa fa-calendar"></i>
            </div>
        </div>
        <p class="help-block"><strong>Format: </strong> Tanggal-Bulan-Tahun</p>
    </div>
    <div class="form-group">
        <label>Pesan</label>
        <summernote height="200" ng-model="timer.message"></summernote>
    </div>
</div>
<div class="modal-footer">
    <button class="btn btn-btn-sm btn-primary" ng-click="onBtnSaveClicked()">
        <i class="fa fa-check"></i> <span>Simpan</span>
    </button>
    <button class="btn btn-default" ng-click="dismiss()">
        <i class="fa fa-times"></i> <span>Batal</span>
    </button>
</div>

