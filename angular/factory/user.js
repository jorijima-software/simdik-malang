// Created by Raga Subekti at 26/10/17

angular.module('diknasmlg').factory('$UserFactory', function($http) {

    var factory = {};
    var apiPath = localStorage.getItem('PATHLOC');

    factory.show = function(start, count, search, callback) {
        $http.get(apiPath + 'api/user/show/' + start + '/' + count + '/' + search).then(
            function(reply) {
                if(reply.status == 200) {
                    return callback(reply.data);
                } else {
                    console.log(reply)
                }
            }
        )
    };

    factory.create = function(data, callback) {
        $http.post(apiPath + 'api/user/create', {
            username: data.username,
            password: data.password,
            level: data.level,
            sekolah: data.sekolah
        }).then(
            function(reply) {
                if(reply.status == 200) {
                    return callback(reply.data);
                } else {
                    console.log(reply)
                }
            }
        )
    };

    factory.delete = function(id, callback) {
        $http.post(apiPath + 'api/user/delete/' + 1, {}).then(
            function(reply) {
                if(reply.status == 200) {
                    return callback(reply.data);
                } else {
                    console.log(reply)
                }
            }
        )
    };

    factory.update = function(data, callback) {
        $http.post(apiPath + 'api/user/update', {
            id: data.id,
            username: data.username,
            password: data.password,
            sekolah: data.sekolah,
            level: data.level
        }).then(
            function(reply) {
                if(reply.status == 200) {
                    return callback(reply.data);
                } else {
                    console.log(reply)
                }
            }
        )
    };

    factory.resendVerification = function() {

    };

    return factory;
});
