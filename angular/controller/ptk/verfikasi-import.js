angular.module('diknasmlg').controller('PtkVerifikasiImport', function(
    $scope, 
    $uibModal, 
    $SchoolFactory,
    $ImportFactory,
    NgTableParams, 
    $routeParams, 
    $q,
    Notify,
    $location
) {
    
    $scope.routes = $routeParams;
    $scope.disabled = false;

    var PATH = localStorage.getItem('PATHLOC');

    $scope.importTable =  new NgTableParams({}, {
        counts: [],
        getData: function(params) {
            return $q(function(resolve, reject) {
                var count = params.count();
                var start = count * (params.page() - 1);
                var id = $scope.routes && $scope.routes.id ? $scope.routes.id : null;
                $SchoolFactory.showImport(start, count, id).then(function(result) {
                    if(result && !result.error) {
                        if(result.total <= 0)  {
                            $scope.disabled = true
                        } else {
                            $scope.disabled = false
                        }
                        params.total(result.total)
                        return resolve(result.data);
                    } else {
                        return reject(result);
                    }
                })
            })
        }
    })

    $scope.onBtnDetailClicked = function(row) {
        //modal_ptk_detail
        $uibModal.open({
            templateUrl: PATH + 'ptk/modal_ptk_detail',
            controller: 'PtkImportDetail',
            resolve: {
                data: function() {
                    return row;
                }
            }
        })
    }

    $scope.onBtnEditClicked = function(row) {
        $uibModal.open({
            templateUrl: PATH + 'ptk/modal_ptk_edit',
            controller: 'PtkImportEdit',
            resolve: {
                data: function() {
                    return row;
                }
            }
        })
    }

    $scope.onBtnDeleteClicked = function(id) {
        Notify.confirm('Apakah anda yakin ingin menghapus data ini?', function() {
            $ImportFactory.delete(id).then(function(result) {
                if(result && !result.error) {
                    Notify.success('Sukses', 'Berhasil menghapus data')
                    $scope.importTable.reload();
                } else {
                    Notify.error('Kesalahan', result.error)
                }
            })
        }, function() {})
    }

    $scope.onBtnConfirmClicked = function() {
        Notify.confirm('Apakah anda yakin ingin memasukkan data tersebut?', function() {
            var id = $scope.routes.id ? $scope.routes.id : '';
            $ImportFactory.confirm(id).then(function(reply) {
                if(reply && !reply.error) {
                    Notify.success('Sukses', 'Verifikasi Data berhasil')
                    if(id) {
                        $location.path('/sekolah/detail/' + id)
                    } else {
                        $location.path('/ptk')
                    }
                } else {
                    Notify.error('Kesalahan', reply.error)
                }
            })
        }, function() {})
    }

})