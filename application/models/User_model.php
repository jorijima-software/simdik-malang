<?php
/**
 * Created by PhpStorm.
 * User: ragasubekti
 * Date: 12/10/17
 * Time: 16.29
 */

use Firebase\JWT\JWT;

class User_model extends CI_Model
{

    public function __construct()
    {
        parent::__construct();
        $this->load->helper('cookie');
    }

    public function get()
    {
        $this->db->get_where('username');
    }

    public function userinfo()
    {
        $this->db->select('user.*, data_sekolah.nama, data_operator.*');
        $this->db->from('user');
        $this->db->join('data_sekolah', 'id_npsn = data_sekolah.npsn', 'left');
        $this->db->join('data_operator', 'data_sekolah.id = data_operator.asal_sekolah', 'left');
        $this->db->where('user.id', $this->session->userdata('uid'));
        return $this->db->get()->row();
    }

    public function show($start, $count, $search)
    {
        $this->db->select('user.*, data_sekolah.id id_sekolah, data_sekolah.nama nama_sekolah');
        $this->db->from('user');

        if ($search && strlen($search) > 0 && $search != NULL && !is_null($search)) {
            $this->db->like('username', $search);
        }

        $this->db->join('data_sekolah', 'id_npsn = data_sekolah.npsn', 'left');

        $total = clone $this->db;

        $this->db->limit($count, $start);

        return array('data' => $this->db->get()->result(), 'total' => $total->count_all_results());
    }

    public function api_verify_jwt($jwt)
    {
        $key = 'ragasubekti.jwt.diknasmlg';
        $decoded = JWT::decode($jwt, $key, array('HS256'));
        if ($decoded) {
            return ['valid' => true];
        } else {
            return ['valid' => false];
        }
    }

    // same as login but return a valid json response with token instead
    public function api_login_post()
    {
        $data = json_decode(trim(file_get_contents('php://input')));

        if ($data->username != 'ragasub') {
            return ['error' => 'Username tidak ditemukan di database'];
        }

        $expireOn = date_add(date_create(), date_interval_create_from_date_string('7 days'));

        $key = "ragasubekti.jwt.diknasmlg";
        $token = array(
            'userInfo' => [
                'username' => 'ragasub',
                'name' => 'Raga Subekti',
                'phoneNumber' => '082234347364'
            ],
            'loggedAs' => 0,
            'isLogged' => true,
            'expireOn' => date_format($expireOn, 'D M d Y H:i:s O'),
            'needPhoneVerification' => false,
            'userId' => 69
        );

        $jwt = JWT::encode($token, $key);
//        $decoded = JWT::decode($jwt, $key, array('HS256'));
        return ['success' => true, 'needPhoneVerification' => false, 'token' => $jwt];
    }

    public function login($username, $password, $as)
    {
        $q = $this->db->get_where('user', array('username' => $username));

        if ($q && $q->row()) {

            $result = $q->row();
            if (password_verify($password, $result->password)) {
                if ($username == $password) {
                    $this->session->set_userdata('need_change_password', true);
                }

                if ($as == $result->level) {
                    $is_sms = isset($_ENV['SMS_ENABLED']) ? $_ENV['SMS_ENABLED'] : false;
                    if ($is_sms && ($as > 0) && (!$q->row()->verify_uuid || !$this->input->cookie('USER_UUID') || ($q->row()->verify_uuid != $this->input->cookie('USER_UUID')))) {

                        $ds = $this->db->get_where('data_sekolah', ['npsn' => $username])->row();
                        $go = $this->db->get_where('data_operator', ['asal_sekolah' => $ds->id])->row();
                        $this->input->set_cookie('_user_id', $ds->id, (86400 * 30));

                        if ($go) {
                            $this->db->where('username', $username);
                            if ($q->row()->last_sent == date('Y-m-d')) {
                                if ($q->row()->sent > 0) {
                                    $this->db->update('user', ['sent' => $q->row()->sent + 1]);
                                } else {
                                    $this->db->update('user', ['sent' => 1]);
                                }
                            } else {
                                $this->db->update('user', ['sent' => 1, 'last_sent' => date('Y-m-d')]);
                            }


                            $sent = $this->db->get_where('user', ['username' => $username])->row()->sent;

                            $session = [
                                'is_logged' => true,
                                'uid' => $result->id,
                                'as' => $as,
                                'npsn' => ($as > 0) ? $username : null,
                                'phone_timer' => ($sent <= 3) ? time() + 300 : 0,
                                'phone_verification' => true
                            ];

                            $this->set_session($session);

                            if ($sent <= 3) {
                                $verify = substr(md5(uniqid()), 0, 5);

                                $target = $go->no_hp;
                                $message = 'Kode Verifikasi aplikasi SIMDIK adalah: ' . $verify;
                                $result = $this->send_sms($target, $message, ['username' => $username]);

                                $this->db->where('username', $username);
                                $this->db->update('user', ['verify' => $verify]);

                                $this->sms_log('Sukses Mengirim', 0, $result['credit']);
                            } else {
                                $this->db->order_by('id', 'desc');
                                $cr = $this->db->get_where('log_sms')->row()->last_credit;

                                $this->sms_log('Batas Maksimal Pengiriman SMS Tercapai', 2, $cr);
                                $this->session->set_userdata('sms_max', true);
                            }

                            $this->login_log($username, NULL, 'Login Sukses', 1);

                            redirect('/phone_verification');
                        } else {

                            $this->login_log($username, NULL, 'Login Sukses', 1);

                            $session = [
                                'is_logged' => true,
                                'uid' => $result->id,
                                'as' => $as,
                                'npsn' => ($as > 0) ? $username : null,
                                'phone_timer' => 0,
                                'phone_verification' => false
                            ];

                            $this->set_session($session);

                            if($as > 0) {
                                redirect('/#!/sekolah/profil');
                            } else {
                                redirect('/#!/dashboard');
                            }
                        }
                    } else {
                        $this->login_log($username, NULL, 'Login Sukses', 1);
                        if($result->level > 0) $this->input->set_cookie('_user_id', $this->db->get_where('data_sekolah', ['npsn' => $username])->row()->id, (86400 * 30));
                        $session = [
                            'is_logged' => true,
                            'uid' => $result->id,
                            'as' => $as,
                            'npsn' => ($as > 0) ? $username : null,
                            'phone_timer' => 0,
                            'phone_verification' => false
                        ];

                        $this->set_session($session);

                        if($as > 0) {
                            redirect('/#!/sekolah/profil');
                        } else {
                            redirect('/#!/dashboard');
                        }
                    }

                } else {
                    $this->login_log($username, null, 'Login Gagal, Salah Memilih Hak Akses', 3);

                    return array('error' => 'Anda tidak dapat mempunyai akses ke level tersebut.');
                }
            } else {
                $this->login_log($username, $password, 'Login Gagal, Password Salah', 0);
                return array('error' => 'Password salah');
            }
        } else {
            $this->login_log($username, $password, 'Login Gagal, Tidak Ditemukan di Database', 2);
            return array('error' => 'Username tidak ditemukan');
        }
    }

    public function profile()
    {
        $ud = $this->db->get_where('user', array('id' => $this->session->userdata('uid')))->row();


        if ($ud->id_npsn) {

            $this->db->select('data_sekolah.*, kecamatan.nama nama_kecamatan, jenis_tingkat_sekolah.nama nama_tingkat');
            $this->db->from('data_sekolah');
            $this->db->join('kecamatan', 'id_kecamatan = kecamatan.id');
            $this->db->join('jenis_tingkat_sekolah', 'id_jenis_tingkat_sekolah = jenis_tingkat_sekolah.id');
            $this->db->where('npsn', $ud->id_npsn);

            return $this->db->get()->row();

        } else {
            return null;
        }
    }

    public function create($data)
    {
        $fu = $this->db->get_where('user', array('username' => $data->username));
        if ($fu->num_rows() > 0) {
            return array('error' => 'Username telah terpakai');
        } else {
            $nu = array(
                'username' => $data->username,
                'password' => password_hash($data->password, PASSWORD_DEFAULT),
                'level' => $data->level,
                'id_npsn' => isset($data->sekolah->npsn) ? $data->sekolah->npsn : null
            );

            $this->db->insert('user', $nu);

            return array('success' => true);
        }
    }

    public function delete($id)
    {
        $this->db->where('id', $id);

        $this->db->delete('user');

        return array('success' => true);
    }

    public function changePassword($data)
    {
        $user = $this->db->get_where('user', array('id' => $this->session->uid))->row();

        if (password_verify($data['current'], $user->password)) {
            $this->session->set_userdata('need_change_password', false);
            if ($data['new'] != $data['verify']) {
                return array('error' => 'Verifikasi kata sandi tidak cocok');
            } else {
                $ud = array(
                    'password' => password_hash($data['new'], PASSWORD_DEFAULT)
                );

                $this->db->where('id', $this->session->uid);
                $this->db->update('user', $ud);

                return array('success' => true);
            }
        } else {
            return array('error' => 'Kata sandi saat ini salah');
        }
    }

    public function update($data)
    {
        $ud = array(
            'username' => $data->username,
            'level' => $data->level,
            'id_npsn' => (isset($data->sekolah->npsn)) ? $data->sekolah->npsn : null,
        );

        if (strlen($data->password) > 0) {
            $ud['password'] = password_hash($data->password, PASSWORD_DEFAULT);
        }

        $this->db->where('id', $data->id);
        $this->db->update('user', $ud);

        return array('success' => true);
    }

    public function current_tapel()
    {
        return $this->db->get_where('tapel', ['active' => true])->row()->tapel;
    }

    public function api_login($username, $password)
    {
        $q = $this->db->get_where('user', array('username' => $username));
        if ($q->num_rows() > 0) {
            $result = $q->row();
            if (password_verify($password, $result->password)) {
                $this->login_log($username, NULL, 'Login Sukses', 11);
                return [
                    'status' => true,
                    'level' => $result->level,
                    'npsn' => $result->id_npsn
                ];
            } else {
                $this->login_log($username, $password, 'Login Gagal, Password Salah', 10);
                return [
                    'status' => false
                ];
            }
        } else {
            $this->login_log($username, $password, 'Login Gagal, Tidak Ditemukan di Database', 12);
            return ['error' => 'Username tidak ditemukan', 'status' => false];
        }
    }

    public function get_unread()
    {
        return $this->db->get_where('user', ['id' => $this->session->uid])->row()->unread;
    }


    public function update_timer()
    {
        if ($this->session->phone_timer <= 0) {
            $this->session->set_userdata('phone_timer', 300);
        }
        return $this->session->set_userdata('phone_timer', ($this->session->phone_timer - 10));
    }

    public function resend_sms()
    {

        $ui = $this->db->get_where('user', ['id' => $this->session->uid])->row();

        $ds = $this->db->get_where('data_sekolah', ['npsn' => $ui->id_npsn])->row();
        $go = $this->db->get_where('data_operator', ['asal_sekolah' => $ds->id])->row();
        $this->input->set_cookie('_user_id', $ds->id, (86400 * 30));

        $q = $this->db->get_where('user', ['id' => $this->session->uid])->row();


        $this->db->where('id', $this->session->uid);
        if ($ui->last_sent == date('Y-m-d')) {
            if ($ui->sent > 0) {
                $this->db->update('user', ['sent' => ($ui->sent + 1)]);
            } else {
                $this->db->update('user', ['sent' => 1]);
            }
        } else {
            $this->db->update('user', ['sent' => 1, 'last_sent' => date('Y-m-d')]);
        }

        if ($q->sent <= 3) {

            $verify = substr(md5(uniqid()), 0, 5);

            $target = $go->no_hp;
            $message = 'Kode Verifikasi aplikasi SIMDIK adalah: ' . $verify;

            $result = $this->send_sms($target, $message, ['username' => $q->username]);

            $this->db->where('id', $this->session->uid);
            $this->db->update('user', ['verify' => $verify]);

            $this->sms_log('Mengirim Ulang', 1, $result['credit']);

            return ['success' => true];

        } else {
            $this->db->order_by('id', 'desc');
            $cr = $this->db->get_where('log_sms')->row()->last_credit;
            $this->sms_log('Batas Maksimal Pengiriman SMS Tercapai', 2, $cr);

            return ['error' => 'Batas pengiriman SMS untuk hari ini telah habis'];
        }

    }

    public function verify_phone($code)
    {

        $ui = $this->db->get_where('user', ['id' => $this->session->uid])->row();

        if ($ui) {
            if (strlen($ui->verify) > 0) {
                if ($ui->verify == strtolower($code)) {
                    $this->session->set_userdata('phone_verification', false);
                    $this->input->set_cookie('USER_UUID', $ui->verify_uuid, (86400 * 30));
                    return ['success' => true];
                } else {
                    return ['error' => 'Kode verifikasi salah.'];
                }
            } else {
                $this->session->set_userdata('timer', 0);
                return ['error' => 'Kode tidak ditemukan, mohon coba kirim ulang kode verifikasi.'];
            }

        } else {
            return ['error' => 'Kesalahan, mohon coba login kembali.'];
        }
    }


    private function set_session($array)
    {
        foreach ($array as $key => $row) {
            $this->session->set_userdata($key, $row);
        }
    }

    private function send_sms($destination, $text, $params)
    {
        $data = [
            "destination" => [$destination],
            "text" => $text
        ];

        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => "https://mesabot.com/api/v2/send",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => json_encode($data),
            CURLOPT_HTTPHEADER => array(
                "client-id: ".$_ENV['MESABOT_ID'],
                "client-secret: ".$_ENV['MESABOT_SECRET'],
                "content-type: application/json",
            )
        ));

        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);

        $response = curl_exec($curl);

        $response = json_decode($response);

        $uuid = $response->refs[0]->uuid;
        $credit = $response->credit;

        $this->db->where('username', $params['username']);
        $this->db->update('user', ['verify_uuid' => $uuid]);

        return ['uuid' => $uuid, 'credit' => $credit];
    }

    // LOG FUNCTION

    private function sms_log($log, $type, $credit)
    {
        // 0 = Sent
        // 1 = Resent
        // 2 = MAX ALLOW
        $this->db->insert('log_sms', [
            'npsn' => $this->session->npsn,
            'log' => $log,
            'type' => $type,
            'last_credit' => $credit
        ]);
    }

    private function login_log($username, $password, $log, $type)
    {
        // SUCCESS = 1
        // WRONG PASSWORD  = 0
        // UNKNOWN USERNAME = 2
        // WRONG ACCESS = 3
        // API _ SUCCESS = 5
        //
        $this->db->insert('log_login', [
            'username' => $username,
            'password' => $password,
            'log' => $log,
            'type' => $type,
            'ip' => $this->input->ip_address()
        ]);
    }

}
