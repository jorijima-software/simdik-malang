<?php

class JenisTingkatPendidikan extends \Illuminate\Database\Eloquent\Model {
    protected $table = 'jenis_tingkat_pend';
    protected $guarded = [];
    public $timestamps = false;
}