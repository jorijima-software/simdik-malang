<?php
/**
 * Created by PhpStorm.
 * User: ragasubekti
 * Date: 1/12/18
 * Time: 7:47 PM
 */

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use Port\Excel\ExcelReader;


class Import extends CI_Controller
{

    public function confirm_import($id = null)
    {
        $this->load->model('Ptk');
        $this->load->model('User');
        if (isset($_SESSION['as'])) {
            $ptk = new Ptk();

            $sekolah = $this->session->as == 1 ? User::where('id', $this->session->uid)->first()->sekolah->id : $id;
            echo json_encode($ptk->confirmImport($sekolah));
        } else {
            echo json_encode(['error' => 'Insufficent Priviledge']);
        }
    }

    public function import_json_ext($input)
    {
        $this->load->model('User');
        $this->load->model('Sekolah');
        // this scripts used to import json from the site we wanted ?
        $this->load->model('Sekolah');
        // kedungkandang
        $source = [
            1 => '056104',
            2 => '056101',
            3 => '056103',
            4 => '056105',
            5 => '056102'
        ];

        $inputs = $source[$input];

        $kb = file_get_contents('http://sekolah.hprasetyou.com/s/kecamatan/' . $inputs . '?filter=kb'); // get the data from here

        $decoded = json_decode($kb);

        foreach ($decoded as $key => $row) {
            $nama = $row->nama_sekolah;
            $npsn = $row->npsn;
            $status = ($row->status == 'SWASTA') ? 0 : 1;
            $kecamatan = $input;
            $alamat = $row->alamat;
            $tingkatPendidikan =

            $sc = Sekolah::where('npsn', $npsn)->count();
            if ($sc <= 0) {
                Sekolah::create([
                    'npsn' => $npsn,
                    'nama' => $nama,
                    'alamat' => $alamat,
                    'status' => $status,
                    'id_kecamatan' => $kecamatan,
                    'id_jenis_tingkat_sekolah' => 6
                ]);
            }

            $uc = User::where('username', $npsn)->count();
            if ($uc <= 0) {
                User::create([
                    'username' => $npsn,
                    'id_npsn' => $npsn,
                    'password' => password_hash($npsn, PASSWORD_DEFAULT),
                    'level' => 1
                ]);
            }
            //
            echo $nama . ' | ' . $row->status . '<br>';
        }
    }

    public function template_ptk_sekolah()
    {
        $this->load->helper('download');
        $spreadsheet = new Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();
        $sheet
            ->setCellValue('A1', 'nuptk')
            ->setCellValue('B1', 'nama_lengkap')
            ->setCellValue('C1', 'jenis_pegawai')
            ->setCellValue('D1', 'nip')
            ->setCellValue('E1', 'gelar_depan')
            ->setCellValue('F1', 'gelar_belakang')
            ->setCellValue('G1', 'nik')
            ->setCellValue('H1', 'tempat_lahir')
            ->setCellValue('I1', 'tanggal_lahir')
            ->setCellValue('J1', 'agama')
            ->setCellValue('K1', 'nama_ibu')
            ->setCellValue('L1', 'no_telp')
            ->setCellValue('M1', 'alamat');
        $writer = new Xlsx($spreadsheet);
        $name = 'simdik_template_ptk.xls';
        $writer->save($name);
        force_download(FCPATH . $name, null);
    }

    public function upload_parse_ptk_sekolah($id = null)
    {
        if (!isset($_SESSION['as'])) {
            echo json_encode(['error' => 'You need to login before uploading any files']);
            die();
        }

        if ($this->session->as == 0 && !$id) {
            echo json_encode(['error' => 'You need to specify which school is the target']);
            die();
        }

        if ($this->session->as == 1) {
            $this->load->model('User');
            $user = User::where('id', $this->session->uid)->first();
            $id = $user->sekolah->id;
        }

        $config['upload_path'] = './uploads/';
        $config['allowed_types'] = '*';
        $config['encrypt_name'] = true;

        $this->load->library('upload', $config);

        if (!$this->upload->do_upload('file')) {
            $error = array('error' => $this->upload->display_errors());

            echo json_encode($error);
            die();
        } else {
            $data = array('upload_data' => $this->upload->data());
            $file = $data['upload_data']['file_name'];

            $reader = new \SplFileObject(FCPATH . 'uploads/' . $file);
            $reader = new ExcelReader($reader);

            $data = [];

            foreach ($reader as $key => $row) {
                if ($key == 0) continue;
                array_push($data, [

                    'nuptk' => $row[0],
                    'nama_lengkap' => $row[1],
                    'jenis_pegawai' => $row[2],
                    'nip' => $row[3],
                    'gelar_depan' => $row[4],
                    'gelar_belakang' => $row[5],
                    'nik' => $row[6],
                    'nama_ibu' => $row[10],

                    'tempat_lahir' => $row[7],
                    'tanggal_lahir' => $row[8],
                    'agama' => $row[9],


                    'no_hp' => $row[11],
                    'alamat' => $row[12],
                    'id_sekolah' => $id

                ]);
            }

            $this->load->model('Ptk');
            $ptk = new Ptk();
            $insert = $ptk->import_xls($data);
            if (!$insert['error']) {
                echo json_encode(['success' => true]);
            } else {
                echo json_encode($insert);
            }

        }
    }
}