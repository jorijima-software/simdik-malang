angular.module('diknasmlg').controller('teacherController', function ($scope, $uibModal, NgTableParams, $q, $TeacherFactory, $EmployeeFactory) {

    var apiPath = localStorage.getItem('PATHLOC');

    $scope.search = '';
    $scope.edit = {};

    $scope.bgIndex = [
        'bg-aqua',
        'bg-yellow',
        'bg-green',
        'bg-orange',
        'bg-red'
    ];

    $scope.statsbox = {};

    $scope.pegawaiList = [];
    $scope.employeeList = [];

    $scope.filter = {
        nuptk: 0,
        pns: 0,
        pegawai: {}
    };

    $scope.onBtnFilterNuptk = function (val) {
        $scope.filter.nuptk = val;
        $scope.teacherTable.reload();
    };

    $scope.onBtnFilterPns = function (val) {
        $scope.filter.pns = val;
        $scope.teacherTable.reload();
    };

    $scope.onFilterPegawaiChange = function() {
        $scope.teacherTable.reload();
    };

    $scope.onBtnAddClicked = function () {
        $uibModal.open({
            templateUrl: apiPath + 'teacher/add',
            size: 'lg',
            backdrop: 'static',
            controller: 'addTeacherModal'
        }).result.then(function () {
            $scope.teacherTable.reload();
        }, function () {
            $scope.teacherTable.reload();
        });
    };

    $scope.onBtnDetailClicked = function (row) {
        $uibModal.open({
            templateUrl: apiPath + 'teacher/detail',
            controller: 'detailTeacherModal',
            size: 'lg',
            resolve: {
                data: function () {
                    return row;
                }
            }
        })
    };

    $scope.onBtnDeleteClicked = function (row) {
        $uibModal.open({
            templateUrl: apiPath + 'teacher/delete',
            controller: 'deleteTeacherModal',
            resolve: {
                data: function () {
                    return row;
                }
            }
        }).result.then(function () {
            $scope.teacherTable.reload();
        }, function () {
            $scope.teacherTable.reload();
        })
    };

    $scope.onBtnEditClicked = function (row) {
        $uibModal.open({
            templateUrl: apiPath + 'teacher/edit',
            controller: 'editTeacherModal',
            size: 'lg',
            backdrop: 'static',
            resolve: {
                data: function () {
                    return row;
                }
            }
        }).result.then(function () {
            $scope.teacherTable.reload();
        }, function () {
            $scope.teacherTable.reload();
        })
    };

    $scope.onInputSearchChange = function () {
        $scope.teacherTable.reload();
    };

    $scope.teacherTable = new NgTableParams({}, {
        getData: function (params) {
            var count = params.count();
            var start = count * (params.page() - 1);
            var search = $scope.search;
            var filter = $scope.filter;
            return $q(function (resolve, reject) {
                $TeacherFactory.show(start, count, search, filter, function (reply) {
                    if (reply.error) return reject(reply.error);
                    params.total(reply.total);
                    return resolve(reply.data);
                })
            })
        }
    });

    function init() {
        $TeacherFactory.statsbox(function (reply) {
            if (!reply.error) {
                $scope.statsbox = reply;
            }
        });

        $EmployeeFactory.get(function (reply) {
            $scope.employeeList = reply;
            $scope.employeeList.unshift({id: '0', nama: 'Semua'});
        });
    }

    init();

});