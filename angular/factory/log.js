angular.module('diknasmlg').factory('$LogFactory', function ($http, $q) {

    var factory = {};

    factory.latest = function() {
        return $q(function(resolve, reject) {
            $http.get(API('log/latest')).then(function (r) {
                if (r.status == 200) {
                    return resolve(r.data);
                } else {
                    return reject(r);
                }
            });
        })
    }

    factory.rombel = function (start, count, filter) {
        return $q(function (resolve, reject) {
            $http.get(API('api/log/rombel/' + start + '/' + count)).then(function (r) {
                if (r.status == 200) {
                    return resolve(r.data);
                } else {
                    return reject(r);
                }
            });
        });
    };

    factory.sms = function (start, count, filter) {
        return $q(function (resolve, reject) {
            $http.get(API('api/log/sms/' + start + '/' + count)).then(function (r) {
                if (r.status == 200) {
                    return resolve(r.data);
                } else {
                    return reject(r);
                }
            })
        });
    };

    factory.login = function (start, count, filter) {
        return $q(function (resolve, reject) {
            $http.get(API('api/log/login/' + start + '/' + count)).then(function (r) {
                if (r.status == 200) {
                    return resolve(r.data);
                } else {
                    return reject(r);
                }
            })
        });
    };

    factory.ptk = function (start, count, filter) {
        return $q(function (resolve, reject) {
            $http.get(API('api/log/ptk/' + start + '/' + count)).then(function (r) {
                if (r.status == 200) {
                    return resolve(r.data);
                } else {
                    return reject(r);
                }
            });
        });
    };

    factory.guru = function (start, count, filter) {
        return $q(function (resolve, reject) {
            $http.get(API('api/log/guru/' + start + '/' + count)).then(function (r) {
                if (r.status == 200) {
                    return resolve(r.data);
                } else {
                    return reject(r);
                }
            });
        });
    };

    return factory;

    function API(path) {
        return localStorage.getItem('PATHLOC') + path;
    }

});