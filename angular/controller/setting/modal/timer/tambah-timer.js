angular.module('diknasmlg').controller('ModalTambahSettingTimer', function(
    $scope,
    $uibModalInstance,
    $TimerFactory,
    Notify
) {
    $scope.timer = {}

    $scope.dismiss = function() {
        $uibModalInstance.dismiss()
    }

    $scope.onBtnSaveClicked = function () {
        $TimerFactory.add($scope.timer).then(function(result) {
            if(result && !result.error) {
                Notify.success('Sukses', 'Berhasil menambahkan timer')
                $uibModalInstance.close()
            } else {
                Notify.error('Kesalahan', result.error)
            }
        })
    }
})