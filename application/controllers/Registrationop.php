<?php
/**
 * Created by PhpStorm.
 * User: arsyad09
 * Date: 11/21/17
 * Time: 10:49 PM
 */

class Registrationop extends CI_Controller
{
    public function index()
    {
        $content = 'registration-op';
        $this->load->view('article', array('content' => $content));
    }

    public function show($start = 0, $count = 10, $search = null) {
        $this->load->model('Operator_model');
        header('Content-Type: application/json');

        $filter = $this->input->get();

        echo json_encode($this->Operator_model->show($start, $count, $search, $filter));
    }

    public function create() {
        header('Content-Type: application/json');

        $this->load->model('Operator_model');
        $data = json_decode(trim(file_get_contents('php://input')));

        echo json_encode($this->Operator_model->create($data));
    }

    public function delete($id) {
        header('Content-Type: application/json');

        $this->load->model('Operator_model');

        echo json_encode($this->Operator_model->delete($id));
    }

    public function update() {
        header('Content-Type: application/json');

        $data = json_decode(trim(file_get_contents('php://input')));

        echo json_encode($this->Operator_model->update($data));
    }
    public function add(){
        $this->load->view('modal/operator/add-operator');
    }
}