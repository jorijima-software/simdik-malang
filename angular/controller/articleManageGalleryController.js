angular.module('diknasmlg').controller('articleManageGalleryController', function ($scope, NgTableParams, $q, $uibModal, $GalleryFactory) {

    var apiPath = localStorage.getItem('PATHLOC');

    $scope.onBtnUploadPhotoClicked = function() {
        $uibModal.open({
            templateUrl: apiPath + 'Gallery/upload',
            controller: 'uploadPhotoModal',
            size: 'md'
        })
    };

    $scope.galleryTable = new NgTableParams({}, {
        getData: function (params) {
            var count = params.count();
            var start = count * (params.page() - 1);
            var query = 'asd';
            return $q(function (resolve, reject) {
                $GalleryFactory.show(start, count, query, function (reply) {
                    if (reply.error) return reject(reply.error);
                    params.total(reply.total);
                    return resolve(reply.data);
                })
            })
        }
    });

    $scope.onBtnDeleteClicked = function (row) {
        $uibModal.open({
            templateUrl: apiPath + 'gallery/delete_modal',
            controller: 'deleteGalleryModal',
            resolve: {
                data: function () {
                    return row;
                }
            }
        }).result.then(function () {
            $scope.galleryTable.reload();
        }, function () {
            $scope.galleryTable.reload();
        })
    };

});