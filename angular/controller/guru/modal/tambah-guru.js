/*
    Created by Raga Subekti
    at  29/11/17 00.38 by PhpStorm
*/
angular.module('diknasmlg').controller('tambahGuru', function ($scope, $uibModalInstance, data, edit, Notify, $GuruFactory, $PtkFactory) {

    // $scope.mapelList = [];
    $scope.teacherList = [];

    if(!edit) {
        $scope.new = {
            guru: {}
        };
    } else {
        $scope.new = {
            id: data.id_tugas,
            guru: data
        }
    }

    $scope.onInputSearchChange = function (query) {
        $PtkFactory.select(query).then(function (reply) {
            $scope.teacherList = reply.data;
        });
    };

    $scope.onBtnSaveClicked = function () {
        if (!edit) {
            var guru = $scope.new;
            $GuruFactory.insert(guru).then(function (reply) {
                if (reply.error) {
                    Notify.error('Kesalahan', reply.error);
                } else {
                    Notify.success('Sukses', 'Data guru berhasil ditambahkan');
                    $uibModalInstance.close();
                }
            })
        } else {
            var guru = $scope.new;
            $GuruFactory.update(guru).then(function(reply) {
                if (reply.error) {
                    Notify.error('Kesalahan', reply.error);
                } else {
                    Notify.success('Sukses', 'Data guru berhasil diubah');
                    $uibModalInstance.close();
                }
            })
        }
    };

    $scope.dismiss = function () {
        $uibModalInstance.dismiss();
    };

    function init() {
        $PtkFactory.select('', function (reply) {
            $scope.teacherList = reply.data;
        });
    }

    init();

});