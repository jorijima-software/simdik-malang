angular.module('diknasmlg').controller('editTugasTambahan', function($scope, $uibModalInstance, $TugasFactory, Notify, data, edit, $TeacherFactory, type) {

    $scope.type = (edit) ? 'Edit' : 'Tambah';

    $scope.teacherList = [];

    $scope.edit = {
        id_jenis_jabatan: (type) ? type : data.id_jenis_jabatan
    };

    if(data) {

        var skdate = (data.tanggal_sk) ? data.tanggal_sk.split('-') : null;
        skdate = (skdate) ? new Date(skdate[0], skdate[1] - 1, skdate[2]) : new Date();

        $scope.edit = {
            id: data.id,
            guru: {
                nama_lengkap: data.nama_guru,
                id: data.id_data_guru
            },
            no_sk: data.no_sk,
            tanggal_sk: skdate,
            tahun_mulai: data.tahun_mulai,
            tahun_selesai: data.tahun_selesai,
            id_jenis_jabatan: (type) ? type : data.id_jenis_jabatan
        }
    }

    $scope.dismiss = function() {
        $uibModalInstance.dismiss();
    };

    $scope.onBtnSaveClicked = function() {
        if(edit) {
            $TugasFactory.update($scope.edit, function(reply) {
                if(reply.error) {
                    Notify.error('Kesalahan', reply.error);
                } else {
                    Notify.success('Sukses', 'Berhasil menambahkan data');
                    $uibModalInstance.close();
                }
            })
        } else {
            $TugasFactory.insert($scope.edit, function(reply) {
                if(reply.error) {
                    Notify.error('Kesalahan', reply.error);
                } else {
                    Notify.success('Sukses', 'Berhasil menambahkan data');
                    $uibModalInstance.close();
                }
            })
        }
    };

    $scope.onYearStartChange = function() {
        if(edit) {

        }
    };

    $scope.onInputSearchChange = function(query) {
        $TeacherFactory.selectTeacher(query, function (reply) {
            if(reply.data) {
                $scope.teacherList = reply.data;
            } else {
                console.log(reply)
            }
        })
    };

    function init() {
        $TeacherFactory.selectTeacher('', function (reply) {
            if(reply.data) {
                $scope.teacherList = reply.data;
            } else {
            }
        })
    }

    init();
});