<?php
/**
 * Created by PhpStorm.
 * User: arsyad09
 * Date: 08/12/17
 * Time: 20:52
 */

if(!$this->session->is_logged) {
    redirect('Logindisplay');
}
$userinfo = $this->User_model->userinfo();
?>
<div class="container" style="margin-top: 75px" ng-controller="articleManageGalleryController">
    <div id="mySidenav" class="sidenav" style="margin-top: 200px">
        <a href="<?= site_url('Gallery') ?>">
            <i class="fa fa-home"></i>&nbsp
            <span>Beranda</span>
        </a>
        <a ng-click="onBtnUploadPhotoClicked()">
            <i class="fa fa-upload"></i>&nbsp
            <span>Upload</span>
        </a>
        <a href="<?= site_url('manage-gallery') ?>">
            <i class="fa fa-photo"></i>&nbsp
            <span>Manage</span>
        </a>
    </div>
    <div class="main" id="main">
        <div class="row">
            <div class="col-md-12" style="margin-bottom: 5px;text-align: center;margin-bottom: 20px;">
                <h2 class="header-on-page">GALERI SIMDIK</h2>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="table-responsive" style="height: 320px;background: white">
                    <table class="table table-striped table-condensed" ng-table="galleryTable">
                        <thead style="background:rgba(12, 153, 0, 0.8);" >
                        <tr>
                            <th class="align-center" style="width: 25px;vertical-align: middle" >No</th>
                            <th style="vertical-align: middle" class="align-center">Gambar</th>
                            <th class="align-center">Judul</th>
                            <th class="align-center">Deskripsi</th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr ng-repeat="row in $data">
                            <td class="align-center">{{($index + 1) + (galleryTable._params.count * (galleryTable._params.page -1))}}</td>
                            <td width="550px"><img class="flex-center" src="../../../uploads/gallery/{{row.files_name}}" width="150px" height="100px"></td>
                            <td class="align-center">{{row.tittle}}l</td>
                            <td class="align-center">{{row.description}}</td>
                            <td>
                                <div class="pull-right">
                                    <button class="btn btn-danger btn-xs" ng-click="onBtnDeleteClicked(row)">
                                        <i class="fa fa-times"></i> <span>Hapus</span>
                                    </button>
                                </div>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>