// Created by Raga Subekti at 11/10/17r
angular.module('diknasmlg').controller('ptkPangkatController', function (
    $scope, 
    NgTableParams, 
    $q, 
    $TeacherFactory, 
    $uibModal, 
    Notify, 
    $routeParams,
    $PtkFactory,
    $TimerFactory
) {

    $scope.timerEnd
    $scope.router = $routeParams
    $scope.detail = {}
    $scope.ptk = {}

    var apiPath = localStorage.getItem('PATHLOC');

    $scope.rankTable = new NgTableParams({}, {
        getData: function (params) {
            return $q(function (resolve, reject) {
                $TeacherFactory.rank($scope.router.id, function (reply) {
                    console.log(reply);
                    if (reply.error) return reject(reply.error);
                    return resolve(reply);
                });
            })
        }
    });

    $scope.onBtnAddClicked = function () {
        $uibModal.open({
            templateUrl: apiPath + 'teacher/addrankmodal',
            controller: 'tambahPangkatPtk',
            resolve: {
                id: function () {
                    return $routeParams.id
                }
            }
        }).result.then(reloadTable, reloadTable);
    };

    $scope.onBtnEditClicked = function (row) {
        $uibModal.open({
            templateUrl: apiPath + 'teacher/editrankmodal',
            controller: 'editPangkatPtk',
            resolve: {
                data: function () {
                    return row;
                }
            }
        }).result.then(reloadTable, reloadTable);
    };

    $scope.onBtnDeleteClicked = function (row) {
        Notify.confirm('Apakah anda yakin ingin menghapus data pangkat tersebut?', function () {
            $TeacherFactory.deleteRank(row.id, function (reply) {
                if (reply.error) return Notify.error('Kesalahan', reply.error);
                Notify.success('Sukses', 'Berhasil menghapus data pangkat');
                $scope.rankTable.reload();
            })
        }, function () {});

    };

    function reloadTable() {
        $scope.rankTable.reload();
        $PtkFactory.get($scope.router.id).then(function (result) {
            if (result && !result.error) $scope.ptk = result
        })
    }

    function init() {
        $TeacherFactory.get($scope.router.id, function (r) {
            $scope.detail = r;
        })

        $PtkFactory.get($scope.router.id).then(function (result) {
            if (result && !result.error) $scope.ptk = result
        })

        $TimerFactory.diff().then(result => {
            $scope.timerEnd = (!result.admin && result.disabled)
        })
    }
    init();

});