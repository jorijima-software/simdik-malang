<?php
/**
 * Created by PhpStorm.
 * User: arsyad09
 * Date: 12/12/17
 * Time: 11:14
 */
?>

<div class="modal-header">
    <div class="modal-title">
        <i class="fa fa-trash"></i> &nbsp; Hapus Foto?
    </div>
</div>
<div class="modal-body">
</div>
<div class="modal-footer">
    <button class="btn btn-danger btn-sm" ng-click="onBtnConfirmClicked()">
        <i class="fa fa-check"></i> Konfirmasi
    </button>
    <button class="btn btn-default btn-sm" ng-click="dismiss()">
        <i class="fa fa-times"></i> Batal
    </button>
</div>

