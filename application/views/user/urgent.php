<?php
/**
 * Created by PhpStorm.
 * User: ragas
 * Date: 12/4/2017
 * Time: 8:48 AM
 */
?>


<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Dinas Pendidikan Kota Malang | Change Password</title>
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <link rel="stylesheet" href="<?= base_url('bower_components/bootstrap/dist/css/bootstrap.min.css') ?>">
    <link rel="stylesheet" href="<?= base_url('bower_components/font-awesome/css/font-awesome.min.css') ?>">
    <link rel="stylesheet" href="<?= base_url('dist/css/AdminLTE.min.css') ?>">
    <link rel="stylesheet"
          href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<body class="hold-transition login-page">
<div class="login-box">

    <div class="login-logo">
        <div class="text-center">
            <table>
                <tr>
                    <td rowspan="2">
                        <span class="logo-mini"><img src="<?= base_url('assets/logo.png') ?>" alt="" width="50"></span>
                    </td>
                    <td style="margin: 0; font-size: 18px; text-align: left; padding-left: 10px">Dinas Pendidikan</td>
                </tr>
                <tr>
                    <td style="margin: 0; font-size: 18px; font-weight: 800; text-align: left; padding-left: 10px">Kota Malang</td>
                </tr>
            </table>
        </div>
    </div>

    <div class="login-logo">
        <div class="text-left" style="font-weight: 800">
            Ubah Password
        </div>
    </div>

    <div class="alert alert-warning">
        <i class="fa fa-exclamation-triangle"></i> <strong>PERINGATAN!</strong> <br>
        <span>Anda saat ini masih menggunakan password default, mohon ganti segera untuk keamanan anda.</span>
    </div>
    <!-- /.login-logo -->
    <div class="login-box-body">
        <form action="<?= site_url('user/urgent') ?>" method="post">
            <?php if (isset($error)) { ?>
                <div class="form-group">
                    <div class="alert alert-danger">
                        <i class="fa fa-exclamation-triangle"></i> &nbsp; <?= $error ?>
                    </div>
                </div>
            <?php } ?>
            <div class="form-group">
                <label for="">Password Baru</label>
                <input type="password" class="form-control" placeholder="Password Baru" name="username">
            </div>
            <div class="form-group">
                <label for="">Ulangi Password</label>
                <input type="password" class="form-control" placeholder="Ulangi Password" name="password">
            </div>
            <div class="form-group">
                <button type="submit" class="btn btn-danger btn-block btn-flat">Ubah</button>
            </div>

        </form>
    </div>
</div>
<!-- /.login-box-body -->
</div>
<!-- /.login-box -->

<!-- jQuery 3 -->
<script src="<?= base_url('bower_components/jquery/dist/jquery.min.js') ?>"></script>
<!-- Bootstrap 3.3.7 -->
<script src="<?= base_url('bower_components/bootstrap/dist/js/bootstrap.min.js') ?>"></script>
</body>
</html>

