angular.module('diknasmlg').controller('tambahRombel', function ($scope, $uibModalInstance, $TugasFactory, $RombelFactory, $SchoolFactory, Notify, data) {

    $scope.isSubmit = false;


    $scope.tapelList = [];
    $scope.arrayKelas = [];

    $scope.tingkatArray = [];

    if (!data) {
        $scope.new = {
            tapel: {},
            tingkat: {},
            rombel: []
        };
    } else {
        $scope.much = data.rombel.length;
        $scope.new = {
            id: data.id,
            tapel: {
                id: data.id_tapel,
                tapel: data.tahun_pelajaran
            },
            tingkat: {
                tingkat: data.tingkat
            }
        };

        $scope.arrayKelas = data.rombel;
    }

    $scope.dismiss = function () {
        $uibModalInstance.dismiss();
    };

    $scope.onBtnSaveClicked = function () {
        $scope.isSubmit = true;

        $scope.new.rombel = $scope.arrayKelas;

        if (!data) {
            var rombel = $scope.new;
            $RombelFactory.insert(rombel).then(function (reply) {
                $scope.isSubmit = false;

                if (reply.error) {
                    Notify.error('Kesalahan', reply.error);
                } else {
                    Notify.success('Sukses', 'Berhasil menambah data rombel');
                    $uibModalInstance.close();
                }
            })
        } else {
            var rombel = $scope.new;
            $RombelFactory.update(rombel).then(function (reply) {
                $scope.isSubmit = false;

                if (reply.error) {
                    Notify.error('Kesalahan', reply.error);
                } else {
                    Notify.success('Sukses', 'Berhasil mengubah data rombel');
                    $uibModalInstance.close();
                }
            })
        }
    };

    $scope.onInputChange = function (q) {
        if ($scope.arrayKelas.length < q) {
            var length = (q) - $scope.arrayKelas.length;
            for (var i = 0; i < parseInt(length); i++) {
                $scope.arrayKelas.push({
                    nama: '',
                    laki: 0,
                    perempuan: 0
                });
            }
        } else {
            var length = q - $scope.arrayKelas.length;
            $scope.arrayKelas.splice(length);
        }
    };

    function init() {
        $TugasFactory.getTapel('', function (reply) {
            $scope.tapelList = reply;
        });

        $SchoolFactory.tingkatKelas(function(r) {
            $scope.tingkatArray = r;
        })
    }

    init();

});