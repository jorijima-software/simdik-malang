angular.module('diknasmlg').factory('$TimerFactory', function ($q, $http) {

    var PATH = localStorage.getItem('PATHLOC')
    var factory = {}

    factory.get = function (start, count) {
        return $q(function (resolve, reject) {
            $http.get(PATH + 'timer/apiCountdown/' + start + '/' + count).then(function (response) {
                if (response.status === 200) {
                    return resolve(response.data)
                } else {
                    return reject(response)
                }
            })
        })
    }

    factory.countdown = function () {
        return $q(function (resolve, reject) {
            $http.get(PATH + 'api/countdown').then(function (response) {
                if (response.status === 200) {
                    return resolve(response.data)
                } else {
                    return reject(response)
                }
            })
        })
    }

    factory.diff = function () {
        return $q(function (resolve, reject) {
            factory.countdown().then(function (result) {
                if (result.time) {
                    let timer = Math.floor((new Date(result.time).getTime() - new Date().getTime()) / 1000)
                    return resolve({disabled: !(timer > 0), admin: result.admin})
                } else {
                    return resolve({error: 'Unknown Error'})
                }
            })
        })
    }

    factory.add = function(data) {
        return $q(function(resolve, reject) {
            $http.post(PATH + 'timer/addCountdown', data).then(function(response) {
                if (response.status === 200) {
                    return resolve(response.data)
                } else {
                    return reject(response)
                }
            })
        })
    }

    factory.activate = function(id) {
        return $q(function (resolve, reject) {
            $http.get(PATH + 'timer/activateCountdown/' + id).then(function (response) {
                if (response.status === 200) {
                    return resolve(response.data)
                } else {
                    return reject(response)
                }
            })
        })
    }

    factory.delete = function(id) {
        return $q(function (resolve, reject) {
            $http.get(PATH + 'timer/deleteCountdown/' + id).then(function (response) {
                if (response.status === 200) {
                    return resolve(response.data)
                } else {
                    return reject(response)
                }
            })
        })
    }

    factory.update = function(data) {
        return $q(function(resolve, reject) {
            $http.post(PATH + 'timer/editCountdown/' + data.id, data).then(function (response) {
                if (response.status === 200) {
                    return resolve(response.data)
                } else {
                    return reject(response)
                }
            })
        })
    }

    return factory

})