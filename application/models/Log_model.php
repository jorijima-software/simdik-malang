<?php
/**
 * Created by PhpStorm.
 * User: ragas
 * Date: 12/8/2017
 * Time: 2:10 AM
 */

class Log_model extends CI_Model {

    public function latestfive() {
        $data = $this->db->query("
            (SELECT * FROM log_ptk ORDER BY id DESC) UNION ALL(SELECT * FROM log_rombel ORDER BY id DESC) UNION ALL (SELECT * FROM log_guru ORDER BY id DESC)  
            ORDER BY id DESC LIMIT 5
        ")->result();
        $return = [];
        foreach ($data as $key => $row) {
            $data = $row;
            $user = $this->db->get_where('user', ['id' => $row->id_user])->row();
            $data->username = $user->username;
            $sekolah = $this->db->get_where('data_sekolah', ['npsn' => $user->id_npsn])->row();
            $data->namaSekolah = $sekolah ? $sekolah->nama : null;
            array_push($return, $data);
        }
        return $return;
    }

    public function rombel($start, $count, $filter) {
        $this->db->select('log_rombel.*, user.username, data_sekolah.nama');
        $this->db->from('log_rombel');

        $this->db->join('user', 'id_user = user.id');
        $this->db->join('data_sekolah', 'user.id_npsn = data_sekolah.npsn');

        // check the filter

        $this->db->order_by('date', 'DESC');
        
        $total = clone $this->db;
        $total = $total->count_all_results();

        $this->db->limit($count, $start);

        $data = $this->db->get()->result();

        return ['data' => $data, 'total' => $total];
    }

    public function login($start, $count, $filter)
    {
        $this->db->select('log_login.*, data_sekolah.nama nama_sekolah');
        $this->db->from('log_login');
        $this->db->join('data_sekolah', 'log_login.username = data_sekolah.npsn', 'left');

        $this->db->order_by('id', 'DESC');

        $total = clone $this->db;
        $total = $total->count_all_results();

        $this->db->limit($count, $start);

        $data = $this->db->get()->result();

        return ['data' => $data, 'total' => $total];
    }

    public function sms($start, $count)
    {
        $this->db->select('log_sms.*, data_sekolah.nama as nama_sekolah');
        $this->db->from('log_sms');
        $this->db->join('data_sekolah', 'log_sms.npsn = data_sekolah.npsn');

        $this->db->order_by('id', 'desc');

        $total = clone $this->db;
        $total = $total->count_all_results();

        $this->db->limit($count, $start);
        $data = $this->db->get()->result();

        $this->db->order_by('id', 'desc');
        $cr = $this->db->get('log_sms')->row()->last_credit;

        return ['data' => $data, 'total' => $total, 'last_credit' => $cr];
    }

    public function ptk($start, $count) {
        $this->db->select('log_ptk.*,  user.username, data_sekolah.nama');
        $this->db->from('log_ptk');
        $this->db->join('user', 'id_user = user.id');
        $this->db->join('data_sekolah', 'user.id_npsn = data_sekolah.npsn');

        $this->db->order_by('id', 'desc');

        $total = clone $this->db;
        $total = $total->count_all_results();

        $this->db->limit($count, $start);
        $data = $this->db->get()->result();

        return ['data' => $data, 'total' => $total];
    }

    public function guru($start, $count) {
        $this->db->select('log_guru.*,  user.username, data_sekolah.nama');
        $this->db->from('log_guru');
        $this->db->join('user', 'id_user = user.id');
        $this->db->join('data_sekolah', 'user.id_npsn = data_sekolah.npsn');

        $this->db->order_by('id', 'desc');

        $total = clone $this->db;
        $total = $total->count_all_results();

        $this->db->limit($count, $start);
        $data = $this->db->get()->result();

        return ['data' => $data, 'total' => $total];
    }


    public function operator_activity($npsn, $start, $count) {

        $q = $this->db->get_where('user', ['id_npsn' => $npsn])->row();

        $query = '
            (SELECT log, date, type, CONCAT(\'rombel\') as source FROM log_rombel WHERE id_user = '.$q->id.')
            UNION ALL
            (SELECT log, date, type, CONCAT(\'guru\') as source FROM log_guru WHERE id_user = '.$q->id.')
            UNION ALL 
            (SELECT log, date, type, CONCAT(\'ptk\') as source FROM log_ptk WHERE id_user = '.$q->id.')
            ORDER BY date DESC';

        $total = $this->db->query($query)->num_rows();
        $data = $this->db->query($query. ' LIMIT ' . $start . ',' . $count)->result();

        return ['data' => $data, 'total' => $total];
    }

}