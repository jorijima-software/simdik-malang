<?php
/**
 * Created by PhpStorm.
 * User: rega
 * Date: 26/10/17
 * Time: 7:49
 */

class User extends CI_Controller
{


    public function api_auth() {
//        public function api_auth() {
        if($this->input->method() == 'post') {
            $this->load->model('User_model');

            echo json_encode($this->User_model->api_login_post());
        } else {
            echo json_encode(['error' => 'Invalid request']);
        }
//        }
    }

    public function show($start = 0, $count = 10, $search = null)
    {
        $this->load->model('User_model');
        header('Content-Type: application/json');
        echo json_encode($this->User_model->show($start, $count, $search));
    }

    public function password()
    {

        $result = array();

        if ($this->input->method() == 'post') {
            $result = $this->User_model->changePassword($this->input->post());
        }

        $content = 'user/password';
        $this->load->view('layout', array('content' => $content, 'result' => $result));
    }

    public function add()
    {
        $this->load->view('modal/user/add');
    }

    public function create()
    {
        header('Content-Type: application/json');

        $this->load->model('User_model');
        $data = json_decode(trim(file_get_contents('php://input')));

        echo json_encode($this->User_model->create($data));
    }

    public function deleteModal()
    {
        $this->load->view('modal/user/delete');
    }

    public function delete($id)
    {
        header('Content-Type: application/json');

        $this->load->model('User_model');

        echo json_encode($this->User_model->delete($id));
    }

    public function updateModal()
    {
        $this->load->view('modal/user/edit');
    }

    public function update()
    {
        header('Content-Type: application/json');

        $data = json_decode(trim(file_get_contents('php://input')));

        echo json_encode($this->User_model->update($data));
    }

    public function profile()
    {
        $content = 'user/profile';
        $this->load->view('layout', array('content' => $content));
    }

    public function api_user_login($username, $password)
    {
        $this->load->model('User_model');
        echo json_encode($this->User_model->api_login($username, $password));
    }

    public function page_urgent() {
        $result = [];

        if($this->input->method() == 'post') {
            $result = [];
        }

        $this->load->view('user/urgent', ['result' => $result]);
    }


    public function phone_timer() {
        if($this->session->phone_timer) {
            $ctime = $this->session->phone_timer - time();


            echo ($ctime <= 0 ? 0 : $ctime);
        } else {
            echo 0;
        }
    }

    public function page_verify() {
        $this->load->model('User_model');

        $result = [];

        if($this->input->method() == 'post') {
            $result = [];
        }

        $this->load->view('user/two_step', ['result' => $result]);
    }

    public function resend_sms() {
        $ctime = $this->session->phone_timer - time();

//        $ctime = 0;
        if($ctime > 0) {
            echo json_encode(['error' => 'Waktu mengirim ulang belum selesai!']);
        } else {
            $this->load->model('User_model');
            echo json_encode($this->User_model->resend_sms());
        }
    }

    function verify_phone() {
        $data = json_decode(trim(file_get_contents('php://input')));
        $this->load->model('User_model');
        echo json_encode($this->User_model->verify_phone($data->code));
    }

}
