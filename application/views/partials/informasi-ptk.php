<?php
/**
 * Created by PhpStorm.
 * User: ragasubekti
 * Date: 23/12/17
 * Time: 02.30
 */
?>
<div class="box box-info">
    <div class="box-header with-border">
        <div class="box-title">
            Informasi PTK
        </div>
    </div>
    <div class="box-body">
        <table class="table table-striped table-hover">
            <tr>
                <th>NUPTK</th>
                <td>{{detail.nuptk}}</td>
            </tr>
            <tr>
                <th>Nama Lengkap</th>
                <td>{{detail.nama}}</td>
            </tr>
            <tr>
                <th>NIP</th>
                <td>{{detail.nip}}</td>
            </tr>
            <tr>
                <th>Jenis Pegawai</th>
                <td>{{detail.jenisPegawai}}</td>
            </tr>
            <tr>
                <th>Unit Kerja</th>
                <td>{{detail.unitKerja}}</td>
            </tr>
        </table>
    </div>
</div>
