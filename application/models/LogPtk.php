<?php
class LogPtk extends \Illuminate\Database\Eloquent\Model
{
    protected $table = 'log_ptk';
    protected $guarded = [];
    public $timestamps = false;
}