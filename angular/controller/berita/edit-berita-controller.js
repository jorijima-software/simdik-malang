angular.module('diknasmlg').controller('editBeritaController', function($scope, $routeParams, $BeritaFactory, Notify, $location) {
    $scope.routes = $routeParams;

    $scope.edit = {};

    $scope.onBtnEditSave = function() {
        if((!$scope.edit.kepada.id_target && $scope.edit.kepada.id_target !== 0) || $scope.edit.kepada.id_target < 0) {
            Notify.error('Kesalahan', 'Mohon masukkan target (kepada!)');
        } else if($scope.edit.judul.length <= 0) {
            Notify.error('Kesalahan', 'Mohon masukkan judul!');
        } else if($scope.edit.isi.length <= 0) {
            Notify.error('Kesalahan', 'Mohon masukkan isi!');
        } else {
            $BeritaFactory.update($scope.edit, function(reply) {
                if(reply.error) {
                    Notify.error('Kesalahan', reply.error);
                } else {
                    Notify.success('Sukses', 'Berhasil membuat berita!');
                    $location.path('/berita');
                }
            });
        }
    };

    function init() {
        $BeritaFactory.detail($scope.routes.id, function(reply) {
            $scope.edit = {
                id: reply.id,
                judul: reply.judul,
                kepada: {
                    id: reply.specific_target,
                    id_target: reply.target,
                    nama: reply.nama_target,
                },
                isi: reply.informasi
            };
        })
    }
    init();
});