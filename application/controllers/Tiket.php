<?php
/**
 * Created by PhpStorm.
 * User: ragasubekti
 * Date: 27/11/17
 * Time: 08.52
 */

class Tiket extends CI_Controller {

    public function page_index()
    {
        $content = null;

        $this->load->view('layout', ['content' => $content, 'js' => [
            'angular/factory/tiket.js',
            'angular/controller/tiket/tiket-controller.js',
            'angular/controller/tiket/tiket-tambah-controller.js'
        ]]);
    }

    public function page_tambah() {
        $this->load->model('Tiket_model');

        $result = [];
        if($this->input->method() == 'post') {
            $result = $this->Tiket_model->create();
        }

        $this->load->view('layout', ['content' => 'tiket/tambah', 'result' => $result, 'ticket_id' => $this->Tiket_model->ticket_id()]);
    }

    public function page_update() {

    }

    public function spa_tiket_index() {
        $this->load->view('tiket/index');
    }

    public function spa_tiket_tambah() {
        $this->load->view('tiket/tambah');
    }

    public function api_show($start = 0, $count = 10) {
        $this->load->model('Tiket_model');
        echo json_encode($this->Tiket_model->show($start, $count));
    }

    public function api_delete($id) {
        if($this->input->method() == 'post') {
            $this->load->model('Tiket_model');
            echo json_encode($this->Tiket_model->delete($id));
        }
    }

    public function api_ticket_id() {
//        if($this->session->as) {
            $this->load->model('Tiket_model');
            echo json_encode($this->Tiket_model->ticket_id());
//        }
    }

    public function api_insert() {
        if($this->input->method() == 'post') {
            $this->load->model('Tiket_model');
            $this->Tiket_model->insert();
        }
    }

}