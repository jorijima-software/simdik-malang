<?php
/**
 * Created by PhpStorm.
 * User: ragasubekti
 * Date: 21/11/17
 * Time: 20.06
 */

?>

<div class="modal-header">
    <div class="modal-title">Detail Tugas Tambahan</div>
</div>

<div class="modal-body">
    <table class="table table-striped">
        <tr>
            <th>Nama Tugas Tambahan</th>
            <td>{{detail.nama}}</td>
        </tr>
    </table>
    <div ng-if="detail.id <= 1"> <!-- lek kepala sekolah & wakil XD <!/-->
        <form>
            <form>
                <div class="form-group">
                    <label for="">Nama</label>
                    <ui-select ng-model="hm.guru" theme="bootstrap">
                        <ui-select-match placeholder="Pilih salah satu PTK">{{$select.selected.nama_lengkap}}
                        </ui-select-match>
                        <ui-select-choices repeat="item in teacherList" refresh="onInputSearchChange($select.search)" refresh-delay="0">
                            {{item.nama_lengkap}}
                        </ui-select-choices>
                    </ui-select>
                </div>
                <div class="row">
                    <div class="col-xs-8">
                        <div class="form-group">
                            <label for="">No. SK</label>
                            <input type="text" class="form-control" ng-model="hm.no_sk">
                        </div>
                    </div>
                    <div class="col-xs-4">
                        <div class="form-group">
                            <label for="">Tanggal SK</label>
                            <p class="input-group">
                                <input type="text" class="form-control" uib-datepicker-popup="dd-MM-yyyy" ng-model="hm.tanggal_sk"
                                       datepicker-options="dateOptions"
                                       close-text="Close" is-open="datepicker" placeholder="dd-mm-yyyy" readonly ng-click="datepicker = !datepicker"/>
                                <span class="input-group-btn">
                            <button type="button" class="btn btn-default" ng-click="datepicker = !datepicker">
                                <i class="glyphicon glyphicon-calendar"></i>
                            </button>
                        </span>
                            </p>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-6">
                        <div class="form-group">
                            <label for="">Tahun Mulai</label>
                            <input type="text" class="form-control" ng-model="hm.tahun_mulai" ng-change="onYearStartChange()">
                        </div>
                    </div>
                    <div class="col-xs-6">
                        <div class="form-group">
                            <label for="">Tahun Selesai</label>
                            <input type="text" class="form-control" ng-model="hm.tahun_selesai">
                        </div>
                    </div>
                </div>
            </form>
        </form>
    </div>
    <div ng-if="detail.id > 1" style="margin-bottom: 50px">
        <button class="btn btn-sm btn-primary" style="margin-bottom: 5px" ng-click="onBtnInsertClicked()" ng-disabled="timerEnd">
            <i class="fa fa-plus"></i> &nbsp; <span>Tambah</span>
        </button>
        <table class="table table-striped table-condensed" ng-table="tgtbDetailTable">
            <thead>
            <tr>
                <th>No</th>
                <th>Nama</th>
                <th>No SK</th>
                <th>Tgl SK</th>
                <th>Th Mulai</th>
                <th>Th Selesai</th>
                <th></th>
            </tr>
            </thead>
            <tbody>
            <tr ng-repeat="row in $data">
                <td>{{($index + 1) + (tgtbDetailTable._params.count * (tgtbDetailTable._params.page -1))}}</td>
                <td>{{row.nama_guru}}</td>
                <td>{{row.no_sk}}</td>
                <td>{{row.tanggal_sk}}</td>
                <td>{{row.tahun_mulai}}</td>
                <td>{{row.tahun_selesai}}</td>
                <td>
                    <div class="pull-right">
                        <button class="btn btn-primary btn-xs" ng-click="onBtnEditClicked(row)" ng-disabled="timerEnd">
                            <i class="fa fa-pencil"></i> <span>Edit</span>
                        </button>
                        <button class="btn btn-danger btn-xs" ng-click="onBtnDeleteClicked(row)" ng-disabled="timerEnd">
                            <i class="fa fa-times"></i> <span>Hapus</span>
                        </button>
                    </div>
                </td>
            </tr>
            </tbody>
        </table>
    </div>
</div>

<div class="modal-footer">
    <button class="btn btn-primary btn-sm" ng-if="detail.id <= 1" ng-click="onBtnSaveClicked()" ng-disabled="timerEnd">
        <i class="fa fa-check"></i> <span>Simpan</span>
    </button>
    <button class="btn btn-default btn-sm" ng-click="dismiss()">
        <i class="fa fa-times"></i> <span>Batal</span>
    </button>
</div>