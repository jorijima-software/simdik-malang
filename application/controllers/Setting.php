<?php
/**
 * Created by PhpStorm.
 * User: ragasubekti
 * Date: 08/10/17
 * Time: 16.32
 */
class Setting extends CI_Controller {



    public function agama() {
        $content = 'setting/agama';
        $this->load->view('layout', array('content' => $content));
    }

    public function tkpendidikan() {
        $content = 'setting/tkpendidikan';
        $this->load->view('layout', array('content' => $content));
    }

    public function kecamatan() {
        $content = 'setting/kecamatan';
        $this->load->view('layout', array('content' => $content));
    }

    public function pengguna() {
        $content = 'setting/pengguna';
        $this->load->view('layout', array('content' => $content));
    }

    public function tugas_tambahan() {
        $content = 'setting/tugas-tambahan';
        $this->load->view('layout', array('content' => $content));
    }

    public function page_semester() {
        $content = 'setting/semester';
        $this->load->view('layout', array('content' => $content));
    }

}