<?php
/**
 * Created by PhpStorm.
 * User: ragasub
 * Date: 2/4/18
 * Time: 1:48 PM
 */
?>

<div class="modal-body">
    <div class="row">
        <div class="col-md-12">
            <table class="table table-striped table-hover">
                <tr>
                    <th>NPSN</th>
                    <td>{{detail.npsn}}</td>
                </tr>
                <tr>
                    <th>Nama Sekolah</th>
                    <td>{{detail.namaSekolah}}</td>
                </tr>
                <tr>
                    <th>Nama Operator</th>
                    <td>{{detail.operator}}</td>
                </tr>
            </table>
        </div>
    </div>
    <div class="row">
        <div class="col-md-4">
            <div class="text-center">
                <b>
                    <small>Data PTK</small>
                </b>
            </div>
            <canvas id="doughnut" class="chart chart-doughnut"
                    chart-data="ptk" chart-labels="label">
            </canvas>
            <div class="text-center">{{detail.persentase}}%</div>
        </div>
        <div class="col-md-4">
            <div class="text-center">
                <b>
                    <small>Data PNS</small>
                </b>
            </div>
            <canvas id="doughnut" class="chart chart-doughnut"
                    chart-data="pns" chart-labels="label">
            </canvas>
            <div class="text-center">{{detail.persentasePns}}%</div>
        </div>
        <div class="col-md-4">
            <div class="text-center">
                <b>
                    <small>Data Non-PNS</small>
                </b>
            </div>
            <canvas id="doughnut" class="chart chart-doughnut"
                    chart-data="npns" chart-labels="label">
            </canvas>
            <div class="text-center">
                {{detail.persentaseNonPns}}%
            </div>
        </div>
    </div>
</div>
