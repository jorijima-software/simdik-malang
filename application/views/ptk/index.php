<?php
/**
 * Created by PhpStorm.
 * User: ragasubekti
 * Date: 06/10/17
 * Time: 16.17
 */
?>

<div ng-controller="ptkController" ng-cloak>

    <section class="content-header">
        <h1>
            Data PTK
        </h1>
    </section>

    <section class="content">
        <div class="row hidden-sm hidden-xs">
            <div class="col-lg-4 col-xs-6">
                <div class="small-box bg-yellow">
                    <div class="inner">
                        <h3>{{stats2.all}}</h3>

                        <p>Total PTK</p>
                    </div>
                    <div class="icon">
                        <i class="fa fa-graduation-cap"></i>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-xs-6">
                <div class="small-box bg-aqua">
                    <div class="inner">
                        <h3>{{stats2.notHaving}}</h3>

                        <p>Belum mempunyai NUPTK</p>
                    </div>
                    <div class="icon">
                        <i class="fa fa-times"></i>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-xs-6">
                <div class="small-box bg-green">
                    <div class="inner">
                        <h3>{{stats2.having}}</h3>
                        <p>Sudah mempunyai NUPTK</p>
                    </div>

                    <div class="icon">
                        <i class="fa fa-check"></i>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-8">
                <div class="box box-default">
                    <div class="box-header with-border">
                        <strong>Aktifitas Terbaru</strong>
                    </div>

                    <div class="box-body">
                        <table class="table table-condensed">
                            <tr ng-repeat="row in latestLog">
                                <td style="background-color: #00c0ef; border-radius: 4px; color: #fff">{{row.username}}</td>
                                <td>
                                    <span ng-if="row.namaSekolah && row.namaSekolah.length > 0">{{row.namaSekolah}}</span>
                                    <span ng-if="!row.namaSekolah && !row.namaSekolah.length > 0">{{row.username}}</span>
                                </td>
                                <td>{{row.log}}</td>
                                <td>
                                    <span class="label label-danger" ng-if="row.type == 0">DELETE</span>
                                    <span class="label label-warning" ng-if="row.type == 2">UPDATE</span>
                                    <span class="label label-success"  ng-if="row.type == 1">INSERT</span>
                                </td>
                                <td><i class="fa fa-clock-o"></i>&nbsp;{{row.date}}</td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <strong>Statistik Pegawai</strong>
                    </div>
                    <div class="box-body">
                        <canvas id="doughnut" class="chart chart-doughnut"
                                chart-data="data" chart-labels="labels">
                        </canvas>
                    </div>
                </div>
            </div>
        </div>

        <?php require_once FCPATH . 'application/views/ptk/table.php'; ?>
        
    </section>

</div>