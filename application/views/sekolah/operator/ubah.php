<?php
/**
 * Created by PhpStorm.
 * User: ragas
 * Date: 12/5/2017
 * Time: 7:40 PM
 */
?>
<div class="modal-header">
    Ubah Informasi Operator
</div>
<div class="modal-body">
    <form>
        <div class="form-group">
            <label for="">Nama Operator</label>
            <input type="text" class="form-control" ng-model="operator.nama" ng-disabled="operator.nama.length >= 5">
        </div>
        <div class="form-group">
            <label for="">No. Handphone</label>
            <div class="input-group">
                <div class="input-group-addon">
                    <i class="fa fa-phone"></i>
                </div>
                <input type="text" class="form-control" ng-model="operator.no_hp">
            </div>
        </div>
        <div class="form-group">
            <label for="">No. Whatsapp</label>
            <div class="input-group">
                <div class="input-group-addon">
                    <i class="fa fa-whatsapp"></i>
                </div>
                <input type="text" class="form-control" ng-model="operator.no_wa">
            </div>
        </div>
    </form>
</div>
<div class="modal-footer">
    <button class="btn btn-sm btn-primary" ng-click="onBtnSaveClicked()">
        <i class="fa fa-check"></i> <span>Simpan</span>
    </button>
    <button class="btn btn-sm btn-default" ng-click="dismiss()">
        <i class="fa fa-times"></i> <span>Batal</span>
    </button>
</div>
