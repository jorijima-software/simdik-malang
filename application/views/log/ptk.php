<?php
/**
 * Created by PhpStorm.
 * User: ragasubekti
 * Date: 12/12/17
 * Time: 13.20
 */
?>

<section class="content-header">
    <h1>Log PTK</h1>
</section>

<section class="content" ng-controller='logPtk' ng-cloack>
    <div class="box box-success">
        <div class="box-body">
            <table class="table table-condensed table-striped" ng-table='logTable'>
                <thead>
                <tr>
                    <th>No</th>
                    <th>User</th>
                    <th>Waktu</th>
                    <th>Type</th>
                    <th>Log</th>
                </tr>
                </thead>
                <tbody>
                <tr ng-repeat="row in $data">
                    <td>{{($index + 1) + (logTable._params.count * (logTable._params.page -1))}}</td>
                    <td>
                        <span ng-if="row.nama.length > 0">{{row.nama}}</span>
                        <span ng-hide="row.nama.length > 0">{{row.username}}</span>
                    </td>
                    <td>{{row.date}}</td>
                    <td>
                        <span class="label label-danger" ng-if="row.type == 0">DELETE</span>
                        <span class="label label-warning" ng-if="row.type == 2">UPDATE</span>
                        <span class="label label-success"  ng-if="row.type == 1">INSERT</span>
                    </td>
                    <td>{{row.log}}</td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>
</section>