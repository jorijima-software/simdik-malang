<?php
/**
 * Created by PhpStorm.
 * User: ragasubekti
 * Date: 28/11/17
 * Time: 23.20
 */

class Berita extends CI_Controller
{

    public function page_index()
    {
        // based on levels
        $this->load->view('layout', ['js' => [
            'angular/controller/berita/berita-controller.js',
            'angular/controller/berita/buat-berita-controller.js',
            'angular/factory/berita.js',
            'angular/controller/berita/detail-berita-controller.js',
            'angular/controller/berita/edit-berita-controller.js'
        ]]);
    }

    public function spa_index()
    {
        $this->load->view('berita/index');
    }

    public function spa_baru()
    {
        $this->load->view('berita/baru');
    }


    public function spa_edit()
    {
        $this->load->view('berita/edit');
    }

    public function spa_detail() {
        $this->load->view('berita/detail');
    }

    public function api_target_get($query = null)
    {
        $this->load->model('Berita_model');
        echo json_encode($this->Berita_model->get_target($query));
    }


    public function api_berita_detail()
    {
        header('Content-Type: application/json');
        $this->load->model('Berita_model');
        echo json_encode($this->Berita_model->detail_berita($this->input->get('id')));
    }

    public function api_berita_baru()
    {
        if ($this->input->method() == 'post') {
            header('Content-Type: application/json');
            $data = json_decode(trim(file_get_contents('php://input')));
            $this->load->model('Berita_model');
            echo json_encode($this->Berita_model->tambah_berita($data));
        }
    }

    public function api_berita_update()
    {
        if ($this->input->method() == 'post') {
            header('Content-Type: application/json');
            $data = json_decode(trim(file_get_contents('php://input')));
            $this->load->model('Berita_model');
            echo json_encode($this->Berita_model->update_berita($data));
        }
    }

    public function api_berita_delete() {
        header('Content-Type: application/json');
        $this->load->model('Berita_model');
        echo json_encode($this->Berita_model->delete_berita($this->input->get('id')));
    }

    public function api_berita_show()
    {
        header('Content-Type: application/json');
        $this->load->model('Berita_model');
        $start = $this->input->get('start');
        echo json_encode($this->Berita_model->show_berita($start));
    }
}