angular.module('diknasmlg').factory('$ImportFactory', function($http, $q) {
    var PATH = localStorage.getItem('PATHLOC')
    var factory = {}

    factory.update = function(data) {
        return $q(function(resolve, reject) {
            $http.post(PATH + 'api/import/ptk/edit/' + data.id, data).then(function (result) {
                if(result.status == 200) {
                    return resolve(result.data)
                } else {
                    return reject(result)
                }
            })
        })
    }

    factory.delete = function(id) {
        return $q(function(resolve, reject) {
            $http.get(PATH + 'api/import/ptk/delete/' + id).then(function(result) {
                if(result.status == 200) {
                    return resolve(result.data)
                } else {
                    return reject(result)
                }
            })
        })
    }

    factory.confirm = function() {
        var id = ''
        
        if(arguments.length > 0) {
            id = arguments[0]
        }

        return $q(function(resolve, reject) {
            $http.post(PATH + 'api/import/ptk/verify-data/confirm/'+id).then(function(result) {
                if(result.status == 200) {
                    return resolve(result.data)
                } else {
                    return reject(result)
                }
            })
        })
    }

    return factory;
})