// Created by Raga Subekti at 03/11/17
angular.module('diknasmlg').controller('ptkController', function ($scope, $uibModal, NgTableParams, $q, $TeacherFactory, $EmployeeFactory, Notify, $PtkFactory, $LogFactory) {

    var apiPath = localStorage.getItem('PATHLOC');

    $scope.search = '';
    $scope.edit = {};

    $scope.bgIndex = [
        'bg-aqua',
        'bg-yellow',
        'bg-green',
        'bg-orange',
        'bg-red'
    ];

    $scope.statsbox = {};

    $scope.pegawaiList = [];
    $scope.employeeList = [];

    $scope.filter = {
        nuptk: 0,
        pns: 0,
        pegawai: {}
    };

    $scope.onBtnImportClicked = function () {
        $uibModal.open({
            templateUrl: apiPath + 'ptk/modal_import_xls',
            controller: 'sekolah.ptk.import'
        })
    }


    $scope.onKekuranganClicked = function (row) {
        $uibModal.open({
            templateUrl: apiPath + 'ptkbaru/modalkekurangan',
            controller: function ($scope, $uibModalInstance) {
                $scope.detail = row
                $scope.dismiss = function () {
                    $uibModalInstance.dismiss()
                }
            }
        })
    }
    $scope.onBtnFilterNuptk = function (val) {
        $scope.filter.nuptk = val;
        $scope.teacherTable.reload();
    };

    $scope.onBtnFilterPns = function (val) {
        $scope.filter.pns = val;
        $scope.teacherTable.reload();
    };

    $scope.onFilterPegawaiChange = function () {
        $scope.teacherTable.reload();
    };

    $scope.onBtnAddClicked = function () {
        $uibModal.open({
            templateUrl: apiPath + 'modal/ptk/tambah',
            size: 'lg',
            backdrop: 'static',
            controller: 'tambahPtk'
        }).result.then(function () {
            $scope.teacherTable.reload();
        }, function () {
            $scope.teacherTable.reload();
        });
    };

    $scope.onBtnDetailClicked = function (row) {
        $uibModal.open({
            templateUrl: apiPath + 'modal/ptk/detail',
            controller: 'detailPtk',
            size: 'lg',
            resolve: {
                data: function () {
                    return row;
                }
            }
        });
    };

    $scope.onBtnDeleteClicked = function (row) {
        Notify.confirm('Apakah anda yakin ingin menghapus PTK <code>' + row.nama_lengkap + '</code> ?', function () {
            $TeacherFactory.delete(row.id, function (reply) {
                if (reply.error) {
                    Notify.error('Kesalahan', reply.error);
                } else {
                    Notify.success('Sukses', 'Data PTK berhasil dihapus');
                }

                $scope.teacherTable.reload();
            });
        }, function () {});
    };

    $scope.onBtnEditClicked = function (row) {
        $uibModal.open({
            templateUrl: apiPath + 'modal/ptk/edit',
            controller: 'editPtk',
            size: 'lg',
            backdrop: 'static',
            resolve: {
                data: function () {
                    return row;
                }
            }
        }).result.then(function () {
            $scope.teacherTable.reload();
        }, function () {
            $scope.teacherTable.reload();
        })
    };

    $scope.onInputSearchChange = function () {
        $scope.teacherTable.reload();
    };

    $scope.teacherTable = new NgTableParams({}, {
        getData: function (params) {
            var count = params.count();
            var start = count * (params.page() - 1);
            var search = $scope.search;
            var filter = $scope.filter;
            return $q(function (resolve, reject) {
                $TeacherFactory.showOperator(start, count, search, filter, function (reply) {
                    if (reply.error) return reject(reply.error);
                    params.total(reply.total);
                    return resolve(reply.data);
                })
            })
        }
    });

    function init() {
        $PtkFactory.stats().then(function (r) {
            $scope.statsbox = r;
        });

        $EmployeeFactory.get(function (reply) {
            $scope.employeeList = reply;
            $scope.employeeList.unshift({
                id: '0',
                nama: 'Semua'
            });
        });
    }

    init();

});