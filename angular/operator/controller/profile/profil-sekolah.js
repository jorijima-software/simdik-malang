angular.module('diknasmlg').controller('profilSekolah', function($scope, $uibModal, $SchoolFactory) {
    var API = localStorage.getItem('PATHLOC');
    $scope.operator = {};
    $scope.percent = {};

    $scope.onBtnOperatorProfile = function() {
        $uibModal.open({
            templateUrl: API + 'modal/sekolah/operator/ubah',
            controller: 'profilOperator'
        }).result.then(function(){
            init()
        }, function() {
            init();
        });
    };

    function init() {
        $SchoolFactory.getOperator().then(function (r) {
            if (r) {
                $scope.operator = {
                    nama: r.nama_operator,
                    no_hp: r.no_hp,
                    no_wa: r.no_wa
                }
            }
        }).catch(function(err) {
            console.log(err);
        });



        $SchoolFactory.percentage().then(function(r) {
            $scope.persen = r;

        })

    }

    init();
});