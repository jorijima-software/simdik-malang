<?php
/**
 * Created by PhpStorm.
 * User: ragasubekti
 * Date: 21/11/17
 * Time: 16.25
 */
class Tugastb_model extends CI_Model {

    public function show($start,$count, $filter = null) {

        $this->db->select('*');
        $this->db->from('data_tugas_tambahan');
        $this->db->join('jenis_jabatan', 'id_jenis_jabatan = jenis_jabatan.id', 'left');
        $this->db->join('data_sekolah', 'id_data_sekolah = data_sekolah.id', 'left');
        $this->db->join('data_guru', 'id_data_guru = data_guru.id', 'left');

        $total = clone $this->db;

        $this->db->limit($count, $start);

        return array('total' => $total->count_all_results(), 'data' => $this->db->get()->result());
        // do something next

    }

    public function get_tgtb_op($start, $count) {

        $fq = $this->db->get_where('user', array('id' => $this->session->uid))->row();
        $sq = $this->db->get_where('data_sekolah', array('npsn' => $fq->id_npsn))->row();

        $this->db->select('*');
        $this->db->from('jenis_jabatan');
//        $this->db->where('id_data_sekolah', $sq->id);

        $total = clone $this->db;

        $this->db->limit($count,$start);

        $cdt = $this->db->get()->result();


        foreach ($cdt as $key => $row) {
            $cdt[$key]->total = $this->db->get_where('data_tugas_tambahan', array('id_jenis_jabatan' => $row->id, 'id_data_sekolah' => $sq->id))->num_rows();
        }

        return array('data' => $cdt, 'total' => $total->count_all_results());
    }

    public function get_tgtb_detail_op($id, $start, $count) {


        $fq = $this->db->get_where('user', array('id' => $this->session->uid))->row();
        $sq = $this->db->get_where('data_sekolah', array('npsn' => $fq->id_npsn))->row();

        $this->db->select('data_tugas_tambahan.*, data_guru.nama_lengkap nama_guru');
        $this->db->from('data_tugas_tambahan');

        $this->db->join('data_guru', 'id_data_guru = data_guru.id');

        $this->db->where('id_jenis_jabatan', $id);
        $this->db->where('id_data_sekolah', $sq->id);

        $total = clone $this->db;

        $this->db->limit($count, $start);

        $data = $this->db->get()->result();

        return array('total' => $total->count_all_results(), 'data' => $data);
    }

    public function insert_tgtb($data) {

        $fq = $this->db->get_where('user', array('id' => $this->session->uid))->row();
        $sq = $this->db->get_where('data_sekolah', array('npsn' => $fq->id_npsn))->row();

        if($data->id_jenis_jabatan <= 1) {
            // find
            $fnq = $this->db->get_where('data_tugas_tambahan', array('id_data_sekolah' => $sq->id, 'id_jenis_jabatan' => 1));

            if($fnq->num_rows() > 0) {
                $update = array(
                    'id_data_guru' => $data->guru->id,
                    'tahun_mulai' => $data->tahun_mulai,
                    'tahun_selesai' => $data->tahun_selesai,
                    'no_sk' => $data->no_sk,
                    'tanggal_sk' => $data->tanggal_sk
                );

                $this->db->where('id', $fnq->row()->id);
                $this->db->update('data_tugas_tambahan', $update);

                return array('success' => true);

            } else {
                // insert
                $insert = array(
                    'id_data_sekolah' => $sq->id,
                    'id_jenis_jabatan' => $data->id_jenis_jabatan,
                    'id_data_guru' => $data->guru->id,
                    'tahun_mulai' => isset($data->tahun_mulai) ? $data->tahun_mulai : '2017',
                    'tahun_selesai' => isset($data->tahun_selesai) ? $data->tahun_mulai : '2017',
                    'no_sk' => isset($data->no_sk) ? $data->no_sk : '',
                    'tanggal_sk' => date_format(date_add(date_create($data->tanggal_sk), date_interval_create_from_date_string('1 days')), 'Y-m-d')
                );

                $this->db->insert('data_tugas_tambahan', $insert);

                return array('success' => true);
            }
        } else {
            $insert = array(
                'id_data_sekolah' => $sq->id,
                'id_jenis_jabatan' => $data->id_jenis_jabatan,
                'id_data_guru' => $data->guru->id,
                'tahun_mulai' => isset($data->tahun_mulai) ? $data->tahun_mulai : '2017',
                'tahun_selesai' => isset($data->tahun_selesai) ? $data->tahun_mulai : '2017',
                'no_sk' => isset($data->no_sk) ? $data->no_sk : '',
                'tanggal_sk' => date_format(date_add(date_create($data->tanggal_sk), date_interval_create_from_date_string('1 days')), 'Y-m-d')
            );

            $this->db->insert('data_tugas_tambahan', $insert);

            return array('success' => true);
        }
    }

    public function update_tgtb($data) {
        $update = array(
            'id_data_guru' => $data->guru->id,
            'tahun_mulai' => $data->tahun_mulai,
            'tahun_selesai' => $data->tahun_selesai,
            'no_sk' => $data->no_sk,
            'tanggal_sk' => date_format(date_add(date_create($data->tanggal_sk), date_interval_create_from_date_string('1 days')), 'Y-m-d')
        );

        $this->db->where('id', $data->id);
        $this->db->update('data_tugas_tambahan', $update);

        return array('success' => true);

    }

    public function delete_tgtb($id) {

        if($this->db->get_where('data_tugas_tambahan', array('id' => $id))->num_rows() > 0) {

        $this->db->where('id', $id);
        $this->db->delete('data_tugas_tambahan');

        return array('success' => true);
        } else {
            return array('error' => 'Data tidak ditemukan');
        }
    }

    public function headmaster() {

        $fq = $this->db->get_where('user', array('id' => $this->session->uid))->row();
        $sq = $this->db->get_where('data_sekolah', array('npsn' => $fq->id_npsn))->row();


        $this->db->select('data_tugas_tambahan.*, data_guru.nama_lengkap nama_guru');
        $this->db->join('data_guru', 'id_data_guru = data_guru.id');

        return $this->db->get_where('data_tugas_tambahan', array('id_data_sekolah' => $sq->id, 'id_jenis_jabatan' => 1))->row();
    }

}