<?php
/**
 * Created by PhpStorm.
 * User: arsyad09
 * Date: 10/13/17
 * Time: 4:19 PM
 */
?>
<div class="modal-header">
    <div class="modal-title">
        <i class="fa fa-eye"></i> &nbsp; Detail Kecamatan
    </div>
</div>
<div class="modal-body">
    <uib-tabset active="active">
        <uib-tab heading="Data Kecamatan">
            <table class="table table-striped table-condensed">
                <tr>
                    <th>Nama Kecamatan</th>
                    <td>{{detail.nama}}</td>
                </tr>
                <tr>
                    <th>Jumlah TK</th>
                    <td>{{detail.tk}}</td>
                </tr>
                <tr>
                    <th>Jumlah SD</th>
                    <td>{{detail.sd}}</td>
                </tr>
                <tr>
                    <th>Jumlah SMP</th>
                    <td>{{detail.smp}}</td>
                </tr>
                <tr>
                    <th>Jumlah Pegawai</th>
                    <td>{{detail.total_pegawai}}</td>
                </tr>

            </table>
        </uib-tab>
        <uib-tab heading="Data TK">
            <table class="table table-striped table-condensed table-bordered" ng-table="dataTKTable">
                <thead style="background:rgba(12, 153, 0, 0.8);  color: white;">
                <tr>
                    <th>No</th>
                    <th>NPSN</th>
                    <th>Tingkat</th>
                    <th>Nama Sekolah</th>
                    <th>Status Sekolah</th>
                    <th>Kecamatan</th>
                    <th></th>
                </tr>
                </thead>
                <tbody>
                <tr ng-repeat="row in $data" >
                    <td>{{($index + 1) + (dataTKTable._params.count * (dataTKTable._params.page -1))}}
                    </td>
                    <td>{{row.nisn}}</td>
                    <td>{{row.tingkat_sekolah}}</td>
                    <td>{{row.nama}}</td>
                    <td>{{status[row.status]}}</td>
                    <td>{{row.nama_kecamatan}}</td>
                    <td>
                        <div class="pull-right">
                            <button class="btn btn-success btn-xs" ng-click="onBtnDetailClicked(row)">
                                <i class="fa fa-eye"></i> Detail
                            </button>
                        </div>
                    </td>
                </tr>
                </tbody>
            </table>
        </uib-tab>
        <uib-tab heading="Data SD">
            <table class="table table-striped table-condensed table-bordered" ng-table="dataSDTable">
                <thead style="background:rgba(12, 153, 0, 0.8);  color: white;">
                <tr>
                    <th>No</th>
                    <th>NPSN</th>
                    <th>Tingkat</th>
                    <th>Nama Sekolah</th>
                    <th>Status Sekolah</th>
                    <th>Kecamatan</th><th></th>
                </tr>
                </thead>
                <tbody>
                <tr ng-repeat="row in $data" >
                    <td>{{($index + 1) + (dataSDTable._params.count * (dataSDTable._params.page -1))}}
                    </td>
                    <td>{{row.nisn}}</td>
                    <td>{{row.tingkat_sekolah}}</td>
                    <td>{{row.nama}}</td>
                    <td>{{status[row.status]}}</td>
                    <td>{{row.nama_kecamatan}}</td>
                    <td>
                        <div class="pull-right">
                            <button class="btn btn-success btn-xs" ng-click="onBtnDetailClicked(row)">
                                <i class="fa fa-eye"></i> Detail
                            </button>
                        </div>
                    </td>
                </tr>
                </tbody>
            </table>
        </uib-tab>
        <uib-tab heading="Data SMP">
            <table class="table table-striped table-condensed table-bordered" ng-table="dataSMPTable">
                <thead style="background:rgba(12, 153, 0, 0.8);  color: white;">
                <tr>
                    <th>No</th>
                    <th>NPSN</th>
                    <th>Tingkat</th>
                    <th>Nama Sekolah</th>
                    <th>Status Sekolah</th>
                    <th>Kecamatan</th>
                    <th></th>
                </tr>
                </thead>
                <tbody>
                <tr ng-repeat="row in $data" >
                    <td>{{($index + 1) + (dataSMPTable._params.count * (dataSMPTable._params.page -1))}}
                    </td>
                    <td>{{row.nisn}}</td>
                    <td>{{row.tingkat_sekolah}}</td>
                    <td>{{row.nama}}</td>
                    <td>{{status[row.status]}}</td>
                    <td>{{row.nama_kecamatan}}</td>
                    <td>
                        <div class="pull-right">
                            <button class="btn btn-success btn-xs" ng-click="onBtnDetailClicked(row)">
                                <i class="fa fa-eye"></i> Detail
                            </button>
                        </div>
                    </td>
                </tr>
                </tbody>
            </table>
        </uib-tab>
        <uib-tab heading="Data Pegawai">
            <div class="row">
                <div class="col-lg-4 col-lg-offset-8">
                    <div class="form-group">
                        <!--<div class="input-group">
                            <input type="text" class="form-control" placeholder="Pencarian" ng-model="search"
                                   ng-keyup="onInputSearchChange()">
                            <div class="input-group-addon">
                                <i class="fa fa-search"></i>
                            </div>
                        </div>-->
                    </div>
                </div>
            </div>
            <div class="row">
                <table class="table table-striped table-condensed table-bordered"
                       ng-table="teacherTable">
                    <thead style="background:rgba(12, 153, 0, 0.8);  color: white;">
                    <tr>
                        <th rowspan="2" style="vertical-align: middle;text-align: center">No</th>
                        <th rowspan="2" style="vertical-align: middle;text-align: center">NUPTK</th>
                        <th rowspan="2" style="vertical-align: middle;text-align: center">Nama Lengkap</th>
                        <th rowspan="2" style="vertical-align: middle;text-align: center">Jenis Pegawai</th>
                        <th colspan="3" style="vertical-align: middle;text-align: center">Riwayat</th>
                        <th rowspan="2"></th>
                    </tr>
                    <tr>
                        <th style="vertical-align: middle;text-align: center">Sekolah</th>
                        <th style="vertical-align: middle;text-align: center">Pangkat</th>
                        <th style="vertical-align: middle;text-align: center">Gaji Berkala</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr ng-repeat="row in $data">
                        <td>{{($index + 1) + (teacherTable._params.count * (teacherTable._params.page -1))}}</td>
                        <td>{{row.nuptk}}</td>
                        <td>{{row.nama_lengkap}}</td>
                        <td>{{row.jenis_pegawai}}</td>
                        <td>
                            {{row.data_sekolah}}
                        </td>
                        <td>
                            <span ng-if="row.status_pns == 1"> {{row.pangkat_guru}}  </span>
                            <span ng-if="row.status_pns == 0">
                                            Khusus PNS
                            </span>
                        </td>
                        <td>
                            <span ng-if="row.status_pns == 1">{{row.gaji_berkala}} </span>
                            <span ng-if="row.status_pns == 0">
                                Khusus PNS
                            </span>
                        </td>
                        <td>
                            <div class="pull-right">
                                <button class="btn btn-success btn-xs" ng-click="onBtnDetailEmployeeClicked(row)">
                                    <i class="fa fa-eye"></i> Detail
                                </button>
                            </div>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </uib-tab>
    </uib-tabset>
</div>
<div class="modal-footer">
    <button class="btn btn-default btn-sm" ng-click="dismiss()">
        <i class="fa fa-times"></i> Tutup
    </button>
</div>
