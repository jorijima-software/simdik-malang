<?php
/**
 * Created by PhpStorm.
 * User: arsyad09
 * Date: 10/11/17
 * Time: 4:22 PM
 */
?>
<div class="container" style="margin-top: 75px" ng-controller="articleEmployeeController">
    <div class="row">
        <div class="row hidden-sm hidden-xs">
            <div class="col-lg-4 col-xs-6">
                <div class="small-box bg-yellow">
                    <div class="inner">
                        <h3>{{statsbox.total}}</h3>

                        <p>Total PTK</p>
                    </div>
                    <div class="icon">
                        <i class="fa fa-graduation-cap"></i>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-xs-6">
                <div class="small-box bg-aqua">
                    <div class="inner">
                        <h3>{{statsbox.nuptk.belum}}</h3>

                        <p>Belum mempunyai NUPTK</p>
                    </div>
                    <div class="icon">
                        <i class="fa fa-times"></i>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-xs-6">
                <div class="small-box bg-green">
                    <div class="inner">
                        <h3>{{statsbox.nuptk.sudah}}</h3>
                        <p>Sudah mempunyai NUPTK</p>
                    </div>

                    <div class="icon">
                        <i class="fa fa-check"></i>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-3">
                <div class="small-box bg-red">
                    <div class="inner">
                        <h3>{{statsbox.uk.belum}}</h3>
                        <p>Belum mempunyai Unit Kerja</p>
                    </div>
                </div>
            </div>
            <div class="col-lg-3">
                <div class="small-box bg-green">
                    <div class="inner">
                        <h3>{{statsbox.uk.sudah}}</h3>
                        <p>Sudah mempunyai Unit Keja</p>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-xs-6" ng-repeat="item in statsbox.jenis">
                <div class="small-box" ng-random-class ng-classes="bgIndex">
                    <div class="inner">
                        <h3>{{item.total}}</h3>
                        <p>Pegawai {{item.jenis}}</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-8" style="margin-bottom: 5px">
            <h2 class="header-on-page">DATA PEGAWAI</h2>
            <!--
            <button class="btn btn-export"></button>
            <button class="btn btn-cetak"></button>-->
        </div>
        <div class="col-md-4" style="margin-bottom: 5px">
            <div class="form-group">
                <div class="input-group">
                    <div class="input-group-btn">
                        <button class="btn btn-default" ng-click="isThisHidden = !isThisHidden">
                            <i class="fa fa-list"></i> <span>Filter</span>
                        </button>
                    </div>
                    <input type="text" class="form-control" placeholder="Pencarian" ng-model="search"
                           ng-keyup="onInputSearchChange()">
                    <div class="input-group-addon">
                        <i class="fa fa-search"></i>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div ng-hide="!isThisHidden">
        <div class="well well-sm">
            <div class="row">
                <!--                            <div class="col-md-3">-->
                <!--                                <div class="form-group">-->
                <!--                                    <label for="">Kecamatan</label>-->
                <!--                                </div>-->
                <!--                            </div>-->
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="">Jenis Pegawai</label>
                        <ui-select ng-model="filter.pegawai" theme="bootstrap" search-enabled="false" on-select="onFilterPegawaiChange()"">
                        <ui-select-match placeholder="Pilih jenis pegawai">
                            {{$select.selected.nama}}
                        </ui-select-match>
                        <ui-select-choices repeat="item in employeeList track by $index">
                            <span>{{item.nama}}</span>
                        </ui-select-choices>
                        </ui-select>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="">NUPTK</label> <br>
                        <div class="btn-group btn-group-sm">
                            <button class="btn btn-default active" ng-click="onBtnFilterNuptk(0)" ng-class="{'active': filter.nuptk == 0}">Semua</button>
                            <button class="btn btn-default" ng-click="onBtnFilterNuptk(1)"  ng-class="{'active': filter.nuptk == 1}">Belum</button>
                            <button class="btn btn-default" ng-click="onBtnFilterNuptk(2)"  ng-class="{'active': filter.nuptk == 2}">Sudah</button>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="">PNS</label> <br>
                        <div class="btn-group btn-group-sm">
                            <button class="btn btn-default active" ng-click="onBtnFilterPns(0)" ng-class="{'active': filter.pns == 0}">Semua</button>
                            <button class="btn btn-default" ng-click="onBtnFilterPns(1)" ng-class="{'active': filter.pns == 1}">Non PNS</button>
                            <button class="btn btn-default" ng-click="onBtnFilterPns(2)" ng-class="{'active': filter.pns == 2}">PNS</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="table-responsive" style="background: white">
        <table class="table table-striped table-condensed table-bordered"
               ng-table="teacherTable">
            <thead style="background:rgba(12, 153, 0, 0.8);  color: white;">
            <tr>
                <th style="vertical-align: middle;text-align: center">No</th>
                <th style="vertical-align: middle;text-align: center">NUPTK</th>
                <th style="vertical-align: middle;text-align: center">Nama Lengkap</th>
                <th style="vertical-align: middle;text-align: center">Jenis Pegawai</th>
                <th style="vertical-align: middle;text-align: center">Sertifikasi</th>
                <th></th>
            </tr>
            <tr>
            </thead>
            <tbody>
            <tr ng-repeat="row in $data">
                <td>{{($index + 1) + (teacherTable._params.count * (teacherTable._params.page -1))}}</td>
                <td>{{row.nuptk}}</td>
                <td>{{row.nama_lengkap}}</td>
                <td>{{row.jenis_pegawai}}</td>
                <td>{{sertifikasi[row.status_sertifikasi]}}</td>
                <td>
                    <div class="pull-right">
                        <button class="btn btn-success btn-xs" ng-click="onBtnDetailClicked(row)">
                            <i class="fa fa-eye"></i> Detail
                        </button>
                    </div>
                </td>
            </tr>
            </tbody>
        </table>
    </div>
</div>