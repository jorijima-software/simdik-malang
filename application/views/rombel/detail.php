<?php
/**
 * Created by PhpStorm.
 * User: ragasubekti
 * Date: 17/12/17
 * Time: 22.53
 */
$tapel_aktif = $this->User_model->current_tapel();
?>

<div ng-cloak>

    <section class="content-header" style="margin-bottom: 60px">
        <div class="pull-right" style="">
            <div class="small-box bg-blue" style="padding: 5px">
                <div class="text-center">Tahun Pelajaran Aktif</div>
                <h3 style="margin: 0; padding: 0"><?=$tapel_aktif?></h3>
            </div>
        </div>
        <h1 style="vertical-align: middle">Detail Rombel</h1>
    </section>
    <section class="content">
        <?php include_once APPPATH.'/views/partials/informasi-sekolah.php'; ?>
        <div class="box">
            <div class="box-body">
                <div class="row">
                    <div class="col-md-2">
                        <?php if ($this->session->as > 0) { ?>
                            <button class="btn btn-primary btn-sm" style="margin-bottom: 5px"
                                    ng-click="onBtnAddClicked()">
                                <i class="fa fa-plus"></i> <span>Tambah</span>
                            </button>
                        <?php } ?>
                    </div>
                    <div class="col-md-7"></div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <ui-select ng-model="tapel.active" theme="bootstrap" search-enabled="false" on-select="onTapelSelect()">
                                <ui-select-match placeholder="Tahun Pelajaran">{{$select.selected.tapel}}
                                </ui-select-match>
                                <ui-select-choices repeat="item in tapelList track by $index">
                                    {{item.tapel}}
                                </ui-select-choices>
                            </ui-select>
                        </div>
                    </div>

                </div>

                <table class="table table-bordered table-condensed" ng-table="rombelTable">
                    <thead>
                    <tr>
                        <th>Nama Rombel</th>
                        <th>Laki-Laki</th>
                        <th>Perempuan</th>
                        <th>Total</th>
                        <th></th>
                    </tr>
                    </thead>
                    <tbody ng-repeat="group in $groups">
                    <tr class="success">
                        <td colspan="5">
                            Tahun Pelajaran <strong>{{group.data[0].tahun_pelajaran}}</strong> Tingkat
                            <strong>{{group.data[0].tingkat}}</strong>
                            <div class="pull-right">
<!--                                <button class="btn btn-danger btn-xs" ng-click="onBtnDeleteAllClicked(group.data)">Hapus-->
<!--                                    Semua-->
<!--                                </button>-->
                            </div>
                        </td>
                    </tr>
                    <tr ng-repeat="row in group.data">
                        <td>{{row.nama}}</td>
                        <td>{{row.laki}}</td>
                        <td>{{row.perempuan}}</td>
                        <td>{{row.total}}</td>
                        <td>
                            <div class="pull-right">
<!--                                <button class="btn btn-primary btn-xs">-->
<!--                                    <i class="fa fa-pencil"></i> <span>Edit</span>-->
<!--                                </button>-->
<!--                                <button class="btn btn-danger btn-xs">-->
<!--                                    <i class="fa fa-times"></i> <span>Hapus</span>-->
<!--                                </button>-->
                            </div>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </section>
</div>