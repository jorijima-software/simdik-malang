<?php
/**
 * Created by PhpStorm.
 * User: ragasubekti
 * Date: 08/10/17
 * Time: 11.05
 */
class District extends  CI_Controller {
    public function get() {
        $this->load->model('District_model');

        header('Content-Type: application/json');
        echo json_encode($this->District_model->get());
    }

    public function regency($query = '') {
        $this->load->model('District_model');

        header('Content-Type: application/json');
        echo json_encode($this->District_model->regency($query));
    }
    public function detail() {
        $this->load->view('modal/district/detail');
    }
    public function show($id, $query = '') {
        $this->load->model('District_model');

        header('Content-Type: application/json');
        echo json_encode($this->District_model->district($id, $query));
    }
    public function schoolondistrict() {
        $this->load->model('District_model');

        header('Content-Type: application/json');
        echo json_encode($this->District_model->schoolondistrict());
    }
    public function showschool($start, $count, $query = null, $query2 = null) {
        $this->load->model('District_model');

        header('Content-Type: application/json');
        echo json_encode($this->District_model->showschool($start, $count, $query, $query2));
    }
    public function showteacher($start, $count, $query = null, $search = null){
    $this->load->model('District_model');

    header('Content-Type: application/json');
    echo json_encode($this->District_model->showteacher($start, $count, $query, $search));
    }
}