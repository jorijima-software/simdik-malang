<?php
/**
 * Created by PhpStorm.
 * User: arsyad09
 * Date: 12/12/17
 * Time: 20:04
 */

class Article extends CI_Controller
{
    public function article_district()
    {
        $content = 'article/article-district';
        $this->load->view('article', array('content' => $content));
    }

    public function article_employee()
    {
        $content = 'article/article-employee';
        $this->load->view('article', array('content' => $content));
    }

    public function article_percentage()
    {
        $content = 'article/article-percentage';
        $this->load->view('article', array('content' => $content));
    }

    public function article_school()
    {
        $content = 'article/article-school';
        $this->load->view('article', array('content' => $content));
    }

    public function article_statistic()
    {
        $content = 'article/article-statistic';
        $this->load->view('article', array('content' => $content));
    }
}