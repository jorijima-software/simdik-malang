// Created by Raga Subekti at 08/10/17
angular.module('diknasmlg').factory('$LevelFactory', function($http) {
    var factory = {};
    var apiPath = localStorage.getItem('PATHLOC');

    factory.get = function (callback) {
        $http.get(apiPath + 'level/get').then(function(reply) {
            if(reply.status == 200) {
                return callback(reply.data);
            } else {
                console.log(reply)
            }
        }, function(err) {
            console.log(err)
        })
    };

    return factory;
});