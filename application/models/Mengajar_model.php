<?php
/**
 * Created by PhpStorm.
 * User: ragasubekti
 * Date: 23/11/17
 * Time: 13.54
 */

class Mengajar_model extends CI_Model {

    public function get_mapel($query) {
        $query = urldecode($query);

        $this->db->from('tugas_mengajar');
        $this->db->like('nama', $query);
        $this->db->limit(10);

        return $this->db->get()->result();
    }

}