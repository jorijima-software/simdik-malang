angular.module('diknasmlg').controller('PtkRiwayatKerja', function(
    $routeParams,
    $scope,
    $q,
    NgTableParams,
    $PtkFactory,
    $uibModal,
    $RiwayatFactory,
    Notify,
    $TimerFactory
) {

    $scope.timerEnd
    $scope.routes = $routeParams;
    $scope.detail = {}
    $scope.ptk = {}

    $scope.riwayatKerjaTable = new NgTableParams({}, {
        getData: function (params) {
            var id = $scope.routes && $scope.routes.id ? $scope.routes.id : null
            var count = params.count()
            var start = count * (params.page() - 1)
            var filter = {}
            return $q(function (resolve, reject) {
                $RiwayatFactory.showKerja(id, start, count, filter).then(function (result) {
                    if(result && !result.error) {
                        params.total(result.total);
                        return resolve(result.data)
                    } else {
                        return reject(result.error)
                    }
                })
            })
        }
    })

    $scope.onBtnAddClicked = function() {
        $uibModal.open({
            templateUrl: apiPath + 'riwayat/modal_kerja_tambah',
            controller: 'PtkRiwayatKerjaTambah'
        }).result.then(function () {
            $scope.riwayatKerjaTable.reload()
            $PtkFactory.get($scope.router.id).then(function (result) {
                if (result && !result.error) $scope.ptk = result
            })
        }, function () {
            $scope.riwayatKerjaTable.reload()
            $PtkFactory.get($scope.router.id).then(function (result) {
                if (result && !result.error) $scope.ptk = result
            })
        })
    }
    
    $scope.onBtnEditClicked = function (row) {
        $uibModal.open({
            templateUrl: apiPath + 'riwayat/modalKerjaEdit',
            controller: 'PtkRiwayatKerjaEdit',
            resolve: {
                data: function() {
                    return row
                }
            }
        }).result.then(function () {
            $scope.riwayatKerjaTable.reload()
            $PtkFactory.get($scope.router.id).then(function (result) {
                if (result && !result.error) $scope.ptk = result
            })
        }, function () {
            $scope.riwayatKerjaTable.reload()
            $PtkFactory.get($scope.router.id).then(function (result) {
                if (result && !result.error) $scope.ptk = result
            })
        })
    }
    
    $scope.onBtnDeleteClicked = function (row) {
        Notify.confirm('Apakah anda yakin ingin menghapus data tersebut?', function () {
            $RiwayatFactory.hapusKerja(row.id).then(function(result) {
                if(result && !result.error) {
                    Notify.success('Sukses', 'Berhasil menghapus data riwayat kerja')
                    $scope.riwayatKerjaTable.reload()
                } else {
                    Notify.error('Kesalahan', reply.error)
                    $scope.riwayatKerjaTable.reload()
                }
            })
        }, function () {
            
        })
    }

    function init() {
        $PtkFactory.informasi($scope.routes.id).then(reply => {
            if(reply && !reply.error) {
                $scope.detail = reply
            }
        })

        $PtkFactory.get($scope.routes.id).then(result => {
            if(result && !result.error) {
                $scope.ptk = result
            }
        })

        $TimerFactory.diff().then(result => {
            $scope.timerEnd = (!result.admin && result.disabled)
        })
    }

    init()
})