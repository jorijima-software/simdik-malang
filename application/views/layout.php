<?php
/**
 * Created by PhpStorm.
 * User: ragasubekti
 * Date: 06/10/17
 * Time: 13.02
 */

if (isset($js)) {
    array_unshift($js,
        'bower_components/moment/min/moment.min.js', 'bower_components/moment/locale/id.js', 'assets/dist/summernote.min.js', 'bower_components/angular-route/angular-route.min.js', 'bower_components/angular-animate/angular-animate.min.js',
        'bower_components/chart.js/dist/Chart.bundle.min.js',
        'bower_components/angular-chart.js/dist/angular-chart.min.js');
} else {
    $js = [
        'bower_components/moment/min/moment.min.js', 'bower_components/moment/locale/id.js', 'bower_components/summernote/dist/summernote.min.js', 'bower_components/angular-route/angular-route.min.js', 'bower_components/angular-animate/angular-animate.min.js',
        'bower_components/chart.js/dist/Chart.bundle.min.js',
        'bower_components/angular-chart.js/dist/angular-chart.min.js',
    ];
}

if ($this->session->phone_verification) {
    redirect('/phone_verification');
}

$v = $_ENV['APP_VERSION'];

?>
<!DOCTYPE html>
<html ng-app="diknasmlg">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Dinas Pendidikan Kota Malang</title>
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <link rel="stylesheet" href="<?= base_url('bower_components/bootstrap/dist/css/bootstrap.min.css') ?>">
    <link rel="stylesheet" href="<?= base_url('bower_components/font-awesome/css/font-awesome.min.css') ?>">
    <link rel="stylesheet" href="<?= base_url('dist/css/AdminLTE.min.css') ?>">
    <link rel="stylesheet" href="<?= base_url('dist/css/skins/skin-blue.min.css') ?>">
    <link rel="stylesheet" href="<?= base_url('bower_components/angular-bootstrap/ui-bootstrap-csp.css') ?>">
    <link rel="stylesheet" href="<?= base_url('bower_components/angular-ui-select/dist/select.min.css') ?>">
    <link rel="stylesheet" href="<?= base_url('bower_components/noty/lib/noty.css') ?>">
    <link rel="stylesheet" href="<?= base_url('bower_components/highcharts/css/highcharts.css') ?>">
    <link rel="stylesheet" href="<?= base_url('bower_components/highcharts-ng/dist/highcharts-ng.css') ?>">
    <link rel="stylesheet" href="<?= base_url('assets/dist/summernote.css') ?>">
    <link rel="stylesheet" href="<?= base_url('assets/animate.min.css') ?>">
    <link rel="stylesheet" href="<?= base_url('assets/animation.css') ?>">
    <link rel="stylesheet" href="<?= base_url('bower_components/angular-loading-bar/build/loading-bar.css') ?>">
    <link href="https://fonts.googleapis.com/css?family=Roboto+Mono" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Product+Sans" rel="stylesheet">


    <link rel="stylesheet"
          href="<?= base_url('assets/fonts/fonts.css') ?>">
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">
    <!--    <div id="loading-bar-container"></div>-->

    <?php
    require_once 'layout/header.php';
    require_once 'layout/sidebar.php';
    ?>
    <div class="content-wrapper">
        <?php
        if (isset($content)) {
            require_once $content . '.php';
        } else {
            echo "";
            echo "<div ng-view class='view'></div>";
        }
        ?>
    </div>
</div>
<script src="<?= base_url('node_modules/lodash/lodash.min.js')?>"></script>
<script src="<?= base_url('bower_components/jquery/dist/jquery.min.js') ?>"></script>
<script src="<?= base_url('bower_components/bootstrap/dist/js/bootstrap.min.js') ?>"></script>
<script src="<?= base_url('dist/js/adminlte.min.js') ?>"></script>
<script src="<?= base_url('bower_components/noty/lib/noty.min.js') ?>"></script>
<?php if($_ENV['DEBUG'] != 'true') { ?> 
<script src="<?= base_url('bower_components/angular/angular.js') ?>"></script>
<?php } else { ?>
<script src="<?= base_url('bower_components/angular/angular.min.js') ?>"></script>
<?php } ?>
<script src="<?= base_url('node_modules/ng-table/bundles/ng-table.js') ?>"></script>
<script src="<?= base_url('bower_components/angular-bootstrap/ui-bootstrap-tpls.min.js') ?>"></script>
<script src="<?= base_url('bower_components/angular-sanitize/angular-sanitize.min.js') ?>"></script>
<script src="<?= base_url('bower_components/angular-ui-select/dist/select.min.js') ?>"></script>
<script src="<?= base_url('bower_components/highcharts/highcharts.js') ?>"></script>
<script src="<?= base_url('bower_components/highcharts/highcharts-3d.js') ?>"></script>
<script src="<?= base_url('bower_components/highcharts/modules/exporting.js') ?>"></script>
<script src="<?= base_url('bower_components/highcharts-ng/dist/highcharts-ng.min.js') ?>"></script>
<script src="<?= base_url('bower_components/ng-currency/dist/ng-currency.js') ?>"></script>
<script src="<?= base_url('bower_components/angular-summernote/src/angular-summernote.js') ?>"></script>
<script src="<?= base_url('bower_components/angular-loading-bar/build/loading-bar.min.js') ?>"></script>
<script src="<?= base_url('bower_components/angular-file-upload/dist/angular-file-upload.min.js') ?>"></script>

<script src="<?= base_url('angular/app.js') ?>?<?= md5($v) ?>"></script>
<script src="<?= base_url('angular/routes.js') ?>?<?= md5($v) ?>"></script>

<script src="<?= base_url('angular/services/notify.js') ?>"></script>

<?php foreach ($js as $key => $row) {
    $url = base_url($row) . '?' . md5($v);
    echo "<script src=\"{$url}\"></script>";
} ?>

<script async src="https://www.googletagmanager.com/gtag/js?id=UA-111581217-1"></script>
<script>
    window.dataLayer = window.dataLayer || [];

    function gtag() {
        dataLayer.push(arguments);
    }

    gtag('js', new Date());

    gtag('config', 'UA-111581217-1');
</script>

<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-WRXGDBX');</script>
<!-- End Google Tag Manager -->
<!---->

</body>
</html>