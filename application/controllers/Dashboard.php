<?php

/**
 * Created by PhpStorm.
 * User: ragasubekti
 * Date: 06/10/17
 * Time: 15.40
 */
class Dashboard extends CI_Controller
{

    public function spa_index() {
        $this->load->view('dashboard/index');
    }

    public function spa_not_found() {
        $this->load->view('dashboard/not-found');
    }

    public function page_index()
    {

        if ($this->session->as > 0) {
            redirect('/#!/sekolah/profil');
            die();
        }

        $content = 'dashboard/index';

        $js = [
            'angular/controller/dashboard/dashboard-controller.js',
            'angular/factory/school.js'
        ];

        $this->load->view('layout', array('content' => $content, 'js' => $js));
    }

}