// Created by Raga Subekti at 09/10/17
angular.module('diknasmlg').factory('$EducationFactory', function($http) {
    var apiPath = localStorage.getItem('PATHLOC');

    var factory = {};

    factory.get = function(callback) {
        $http.get(apiPath + 'api/pendidikan').then(function(reply) {
            if(reply.status == 200) {
                return callback(reply.data);
            } else {
                console.log(reply)
            }
        }, function(err) {
            console.log(err)
        });
    };

    return factory;

});