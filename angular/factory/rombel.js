angular.module('diknasmlg').factory('$RombelFactory', function ($http, $q) {

    var factory = {};

    factory.insert = function (data) {
        return $q(function (resolve, reject) {
            $http.post(apiPath + 'api/rombel/insert', {
                tingkat: data.tingkat,
                rombel: data.rombel,
                tapel: data.tapel
            }).then(
                function (success) {
                    if (success.status == 200) {
                        return resolve(success.data);
                    } else {
                        return reject(success);
                    }
                },
                function (err) {
                    return reject(err);
                }
            );

        });
    };

    factory.update = function (data) {
        return $q(function (resolve, reject) {
            $http.post(apiPath + 'api/rombel/update', {
                id: data.id,
                tingkat: data.tingkat,
                rombel: data.rombel,
                tapel: data.tapel,
                old: data.old
            }).then(
                function (success) {
                    if (success.status == 200) {
                        return resolve(success.data);
                    } else {
                        return resolve(success);
                    }
                },
                function (err) {
                    return reject(err);
                }
            );
        });
    };

    factory.delete = function (id) {
        return $q(function (resolve, reject) {
            $http.post(apiPath + 'api/rombel/delete/', {
                id: id
            }).then(function (reply) {
                if (reply.status == 200) {
                    return resolve(reply.data)
                } else {
                    return reject(reply)
                }
            }, function (err) {
                return reject(err);
            })
        })
    };

    factory.show = function (tapel) {
        return $q(function (resolve, reject) {
            $http.get(apiPath + 'api/rombel/show/' + tapel).then(
                function (success) {
                    if (success.status == 200) {
                        return resolve(success.data);
                    } else {
                        return reject(success);
                    }
                },
                function (err) {
                    return reject(err);
                }
            );
        });
    };

    factory.stats = function () {
        return $q(function (resolve, reject) {
            $http.get(apiPath + 'api/rombel/stats').then(
                function (success) {
                    if (success.status == 200) {
                        return resolve(success.data);
                    } else {
                        return reject(success);
                    }
                },
                function (err) {
                    return reject(err);
                }
            );
        });
    };

    factory.school = function(start, count, filter) {
        return $q(function(resolve, reject) {
            $http.get(apiPath + 'api/rombel/school/' + start + '/'+ count + '?' + jQuery.param(filter)).then(function(r) {
                if(r.status === 200) {
                    return resolve(r.data);
                } else {
                    return reject(r);
                }
            })
        })
    };

    factory.deleteAll = function(group) {
        return $q(function(resolve, reject) {
            $http.post(apiPath + 'api/rombel/delete/group', {
                group: group
            }).then(function(r) {
                if(r.status === 200) {
                    return resolve(r.data);
                } else {
                    return reject(r);
                }
            })
        })
    };

    factory.updateRow = function(rombel) {
        return $q(function(resolve, reject) {
            $http.post(apiPath + 'api/rombel/update/row', {
                rombel: rombel
            }).then(function(r) {
                if(r.status === 200) {
                    return resolve(r.data);
                } else {
                    return reject(r);
                }
            })
        })
    };

    factory.get = function(tapel, id) {
        return $q(function (resolve, reject) {
            $http.get(apiPath + 'api/rombel/show/' +tapel+ '/'+ id).then(function (r) {
                if(r.status === 200) {
                    return resolve(r.data);
                } else {
                    return reject(r);
                }
            })
        })
    };

    return factory;

});