<?php
/**
 * Created by PhpStorm.
 * User: ragas
 * Date: 12/4/2017
 * Time: 10:50 AM
 */
//$this->load->model('User_model');
$userinfo = $this->User_model->userinfo();
//var_dump($_SESSION);
if (!$this->session->phone_verification) {
    redirect('/dashboard');
}
?>


<!DOCTYPE html>
<html ng-app="logindiknas">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Dinas Pendidikan Kota Malang | Change Password</title>
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <link rel="stylesheet" href="<?= base_url('bower_components/bootstrap/dist/css/bootstrap.min.css') ?>">
    <link rel="stylesheet" href="<?= base_url('bower_components/font-awesome/css/font-awesome.min.css') ?>">
    <link rel="stylesheet" href="<?= base_url('dist/css/AdminLTE.min.css') ?>">
    <link rel="stylesheet"
          href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<body class="hold-transition login-page">
<div class="login-box" ng-controller="phoneVerification">
    <div class="login-logo">
        <div class="text-center">
            <table>
                <tr>
                    <td rowspan="2">
                        <span class="logo-mini"><img src="<?= base_url('assets/logo.png') ?>" alt="" width="50"></span>
                    </td>
                    <td style="margin: 0; font-size: 18px; text-align: left; padding-left: 10px">Dinas Pendidikan</td>
                </tr>
                <tr>
                    <td style="margin: 0; font-size: 18px; font-weight: 800; text-align: left; padding-left: 10px">Kota
                        Malang
                    </td>
                </tr>
            </table>
        </div>
    </div>
    <div class="login-logo">
        <div class="text-left" style="font-weight: 800">
            Phone Verification
        </div>
    </div>

    <?php if (!$this->session->sms_max) { ?>
        <div class="alert alert-info">
            <span>Kode verifikasi telah terkirim ke nomer berikut: <strong><?= substr($userinfo->no_hp, 0, -4) ?>xxxx</strong></span>
        </div>
    <?php } else { ?>
        <div class="alert alert-warning">
            Batas maksimal sms verifikasi per hari telah tercapai, silahkan gunakan kode verifikasi yang terakhir anda dapatkan di <strong><?= substr($userinfo->no_hp, 0, -4) ?>xxxx</strong>!
        </div>
    <?php } ?>
    <!-- /.login-logo -->
    <div class="login-box-body">
        <form method="post">
            <?php if (isset($error)) { ?>
                <div class="form-group">
                    <div class="alert alert-danger">
                        <i class="fa fa-exclamation-triangle"></i> &nbsp; <?= $error ?>
                    </div>
                </div>
            <?php } ?>
            <div class="form-group" ng-if="error.length>0">
                <div class="alert alert-danger">
                    <i class="fa fa-exclamation-triangle"></i> <strong>ERROR!</strong> <span>{{error}}</span>
                </div>
            </div>
            <div class="form-group">
                <label for="">Kode Verifkasi</label>
                <input type="text" class="form-control" placeholder="Kode" ng-model="verify_code"
                       ng-change="verify_code = verify_code.toUpperCase()">
            </div>
            <div class="form-group">
                <button type="submit" class="btn btn-primary btn-block btn-flat"
                        ng-click="onBtnSubmitClicked(verify_code)">Submit
                </button>
            </div>
            <div class="form-group">
                <div class="text-danger" ng-if="timer > 0">Kirim ulang dalam <strong id="timer-countdown">05:00</strong>
                </div>
                <button type="submit" class="btn btn-default btn-block btn-flat" style="margin-top: 10px"
                        ng-disabled="timer > 0" ng-click="onBtnResendClicked()">Kirim Ulang
                </button>
            </div>
            <div class="form-group">
                <a type="submit" class="btn btn-danger btn-block btn-flat" style="margin-top: 10px"
                   href="<?= site_url('/logout') ?>">Logout</a>
            </div>
        </form>
    </div>
</div>
</div>
<script src="<?= base_url('bower_components/jquery/dist/jquery.min.js') ?>"></script>
<script src="<?= base_url('bower_components/angular/angular.min.js') ?>"></script>
<script src="<?= base_url('angular/controller/user/phone-verification.js') ?>"></script>

<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-111581217-1"></script>
<script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());

    gtag('config', 'UA-111581217-1');
</script>
</body>
</html>


