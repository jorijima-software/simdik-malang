/*
    Created by Raga Subekti
    at  29/11/17 06.25 by PhpStorm
*/
angular.module('diknasmlg').controller('tugasTambahanController', function (
    $scope,
    NgTableParams,
    $q,
    $TugasFactory,
    $uibModal,
    $TimerFactory
) {

    var apiPath = localStorage.getItem('PATHLOC');

    $scope.timerEnd

    $scope.tugasTable = new NgTableParams({}, {
        getData: function (params) {
            var count = params.count();
            var start = count * (params.page() - 1);
            return $q(function (resolve, reject) {
                $TugasFactory.opShow(start, count, function (reply) {
                    if (reply.error) return reject(reply.error);
                    params.total(reply.total);
                    return resolve(reply.data);
                })
            });
        }
    });

    $scope.onBtnDetailClicked = function (row) {
        $uibModal.open({
            templateUrl: apiPath + 'teacher/modal_tgtb_detail',
            controller: 'tambahTugasTambahan',
            resolve: {
                data: function () {
                    return row;
                }
            },
            size: 'lg'
        }).result.then(function () {
            $scope.tugasTable.reload();
        }, function () {
            $scope.tugasTable.reload();
        })
    }

    function init() {
        $TimerFactory.diff().then(result => {
            $scope.timerEnd = (!result.admin && result.disabled)
        })
    }

    init()

});