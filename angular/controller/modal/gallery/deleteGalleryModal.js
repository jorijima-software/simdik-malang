angular.module('diknasmlg').controller('deleteGalleryModal', function(data, Notify, $uibModalInstance, $scope, $GalleryFactory) {

    $scope.data = data;

    $scope.dismiss = function() {
        $uibModalInstance.dismiss();
    };

    $scope.onBtnConfirmClicked = function() {
        $GalleryFactory.delete(data.id, function(reply) {
            if(reply.error) {
                Notify.error('Kesalahan', reply.error);
            } else {
                Notify.success('Sukses', 'Data sekolah berhasil dihapus');
                $uibModalInstance.close();
            }
        })
    };

});