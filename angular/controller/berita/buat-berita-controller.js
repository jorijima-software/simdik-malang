angular.module('diknasmlg').controller('buatBeritaController', function($scope, $BeritaFactory, Notify, $location) {

    $scope.targetList = [];

    $scope.new = {kepada: {}, judul: '', isi:''};

    $scope.onBtnCancelClicked = function() {
    };

    $scope.onTargetChange = function(query) {
        $BeritaFactory.target(query, function(reply) {
            $scope.targetList = reply;
        })
    };

    $scope.onBtnSaveClicked = function() {
        if((!$scope.new.kepada.id_target && $scope.new.kepada.id_target !== 0) || $scope.new.kepada.id_target < 0) {
            Notify.error('Kesalahan', 'Mohon masukkan target (kepada!)');
        } else if($scope.new.judul.length <= 0) {
            Notify.error('Kesalahan', 'Mohon masukkan judul!');
        } else if($scope.new.isi.length <= 0) {
            Notify.error('Kesalahan', 'Mohon masukkan isi!');
        } else {
            $BeritaFactory.baru($scope.new, function(reply) {
                if(reply.error) {
                    Notify.error('Kesalahan', reply.error);
                } else {
                    Notify.success('Sukses', 'Berhasil membuat berita!');
                    $location.path('/berita');
                }
            });
        }
    };

    // auto runner
    function init() {
        $BeritaFactory.target('', function(reply) {
            $scope.targetList = reply;
        })
    }
    init();
});