// Created by Raga Subekti at 08/10/17
angular.module('diknasmlg').controller('detailPtk', function(data, $scope, $uibModalInstance, $PtkFactory) {

    $scope.detail = {};

    $scope.pola = {
        0: 'Portofolio',
        1: 'PLPG',
        2: 'PPG'
    };

    $scope.dismiss = function() {
        $uibModalInstance.dismiss();
    };

    function init() {
        $PtkFactory.get(data.id).then(function (response) {
            $scope.detail = response;
        });
    }

    init()

});