angular.module('diknasmlg').controller('beritaController', function($scope, $BeritaFactory, $location, Notify, NgTableParams, $q) {

    $scope.onBtnDetailClicked = function(berita) {
        $location.path('/berita/detail/' + berita.id);
    };

    $scope.beritaTable = new NgTableParams({}, {
        getData: function(params) {
            return $q(function (resolve ,reject) {
                $BeritaFactory.show(0, function(reply) {
                    if(reply.error) return reject(reply.error);
                    params.total(reply.total);
                    return resolve(reply.data);
                })
            })
        }
    });

    $scope.onBtnDeleteClicked = function(berita) {
        Notify.confirm('Apakah anda yakin ingin menghapus berita berjudul <strong>' + berita.judul + '</strong>?', function(){
            $BeritaFactory.delete(berita.id, function(reply) {
                if(reply.error) {
                    Notify.error('Kesalahan', reply.error);
                } else {
                    Notify.success('Sukses', 'Berhasil menghapus berita')
                }
                $scope.beritaTable.reload();
            })
        }, function () {
        })
    };

    $scope.onBtnEditClicked = function(berita) {
        $location.path('/berita/edit/' + berita.id)
    };

    function init() {
        $scope.beritaTable.reload();
    }
    init();

});