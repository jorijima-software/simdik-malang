angular
    .module('diknasmlg')
    .factory('$BankFactory', function($q, $http) {
        var factory = {}
        var PATH = localStorage.getItem('PATHLOC')

        factory.get = function() {
            return $q(function(resolve, reject) {
                $http.get(PATH + 'api/bank').then(function(response) {
                    if(response.status === 200) {
                        return resolve(response.data)
                    } else {
                        return reject(response)
                    }
                })
            })
        }

        return factory
    })