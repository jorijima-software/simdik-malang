<?php
/**
 * Created by PhpStorm.
 * User: ragasubekti
 * Date: 12/12/17
 * Time: 13.18
 */
?>

<section class="content-header" style="margin-bottom: 60px">
    <div class="pull-right" style="">
        <div class="small-box bg-orange" style="padding: 5px">
            <div class="text-center">ID Tiket</div>
            <h3 style="margin: 0; padding: 0">#{{tiket.id}}</h3>
        </div>
    </div>
    <h1>Buat Tiket Baru</h1>
</section>

<section class="content">
    <div class="box box-warning">
        <div class="box-header">
            <div class="box-title">Buat Tiket Baru</div>
        </div>
        <div class="box-body">
            <form class="form-horizontal" ng-submit="onFormSubmit()">
                <div class="form-group">
                    <div class="col-sm-12">
                        <div class="alert alert-danger" ng-if="error.length > 0">
                            <i class="fa fa-exclamation-triangle"></i> <strong>ERROR!</strong> <br>
                            <p>{{error}}</p>
                        </div>
                    </div>

                </div>
                <div class="form-group">
                    <label class="control-label col-sm-2">Nama Permasalahan: </label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" placeholder="NAMA PERMASALAHAN" name="nama"
                               ng-model="tiket.nama">
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-2">Group Data: </label>
                    <div class="col-sm-10">
                        <select name="group_data" class="form-control" ng-model="tiket.groupData">
                            <option value="0">Data Operator</option>
                            <option value="1">Data Profile Sekolah</option>
                            <option value="2">Data PTK</option>
                            <option value="3">Data Guru</option>
                            <option value="4">Data Rombel</option>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-2">Keterangan</label>
                    <div class="col-sm-10">
                    <textarea name="keterangan" rows="5" class="form-control" ng-model="tiket.keterangan"
                              placeholder="KETERANGAN"></textarea>
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-sm-2"></div>
                    <div class="col-sm-10">
                        <button class="btn btn-primary btn-sm">
                            <i class="fa fa-check"></i> <span>Kirim</span>
                        </button>
                        <a href="<?= site_url('tiket') ?>/#!/tiket" class="btn btn-default btn-sm">
                            <i class="fa fa-caret-left"></i> <span>Kembali</span>
                        </a>
                    </div>
                </div>
            </form>
        </div>
    </div>
</section>
