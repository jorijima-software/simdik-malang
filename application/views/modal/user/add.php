<?php
/**
 * Created by PhpStorm.
 * User: rega
 * Date: 26/10/17
 * Time: 16:02
 */
?>

<div class="modal-header">
    <div class="modal-title">
        <i class="fa fa-plus"></i> <span>Tambah Pengguna</span>
    </div>
</div>
<div class="modal-body">
    <form>
        <div class="form-group">
            <label>Username</label>
            <input type="text" class="form-control" ng-model="new.username">
        </div>
        <div class="form-group">
            <label>Password</label>
            <div class="input-group">
            <input type="{{passwordType}}" class="form-control" ng-model="new.password">
            <div class="input-group-btn">
            <button class="btn btn-default" ng-click="onPasswordType()">
                <i class="fa fa-eye"></i>
            </button>
            </div>
            </div>
        </div>
        <div class="form-group">
            <label>Level</label>
            <select class="form-control" ng-model="new.level">
                <option value="0">Admin</option>
                <option value="1">Operator</option>
            </select>
        </div>
        <div class="form-group" ng-if="new.level == 1">
        <label>Sekolah</label>
        <ui-select ng-model="new.sekolah" theme="bootstrap">
                <ui-select-match placeholder="Pilih sekolah">
                {{$select.selected.nama}}
                </ui-select-match>
                <ui-select-choices repeat="item in schoolList track by $index" refresh="onInputSearchChange($select.search)" refresh-delay="0">
                    {{item.nama}}
                </ui-select-choices>
            </ui-select>
        </div>
    
    </form>
</div>
<div class="modal-footer">
    <button class="btn btn-primary btn-sm" ng-click="onBtnSaveClicked()">
        <i class="fa fa-check"></i> <span>Simpan</span>
    </button>
    <button class="btn btn-default btn-sm" ng-click="dismiss()">
        <i class="fa fa-times"></i> <span>Batal</span>
    </button>
</div>