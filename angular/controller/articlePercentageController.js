angular.module('diknasmlg').controller('articlePercentageController', function ($scope, $uibModal, NgTableParams, $q, $TeacherFactory) {

    $scope.data = {};
    $scope.PegawaiNuptk = {
        chart: {
            type: 'pie',
            options3d: {
                enabled: true,
                alpha: 45,
                beta: 0
            },
            animation:false
        },
        title: {
            text: 'PROSENTASE PNS / CPNS MENURUT NUPTK'
        },
        tooltip: {
            pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
        },
        plotOptions: {
            pie: {
                allowPointSelect: true,
                cursor: 'pointer',
                depth: 35,
                dataLabels: {
                    enabled: true,
                    format: '{point.name}'
                }
            }
        },
        series: [{
            type: 'pie',
            name: 'Pegawai',
            data: [
                {
                    name:'Belum Mempunyai NUPTK',
                    y:2073
                },{
                    name: 'Sudah Mempunyai NUPTK',
                    y: 6971,
                    sliced: true,
                    selected: true
                }
            ]
        }]
    };
    $scope.PegawaiDetail = {
        chart: {
            type: 'pie',
            options3d: {
                enabled: true,
                alpha: 45,
                beta: 0
            },
            animation:false
        },
        title: {
            text: 'PROSENTASE PNS / CPNS DETAIL'
        },
        tooltip: {
            pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
        },
        plotOptions: {
            pie: {
                allowPointSelect: true,
                cursor: 'pointer',
                depth: 35,
                dataLabels: {
                    enabled: true,
                    format: '{point.name}'
                }
            }
        },
        series: [{
            type: 'pie',
            name: 'Pegawai',
            data: [
                ['Pegawai GTT', 1914],
                ['Pegawai GTTPNS', 13],
                ['GTY', 2612],
                {
                    name: 'Pegawai PNS',
                    y: 3033,
                    sliced: true,
                    selected: true
                },
                ['Pegawai PNS DEPAG', 33],
                ['Pegawai PNS DPK', 88],
                ['Pegawai PTT/HONORER', 956],
                ['Pegawai PTY', 349]
            ]
        }]
    };
    $scope.PegawaiSertifikasi = {
        chart: {
            type: 'pie',
            options3d: {
                enabled: true,
                alpha: 45,
                beta: 0
            },
            animation:false
        },
        title: {
            text: 'PROSENTASE PNS / CPNS MENURUT SERTIFIKASI'
        },
        tooltip: {
            pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
        },
        plotOptions: {
            pie: {
                allowPointSelect: true,
                cursor: 'pointer',
                depth: 35,
                dataLabels: {
                    enabled: true,
                    format: '{point.name}'
                }
            }
        },
        series: [{
            type: 'pie',
            name: 'Pegawai',
            data: [
                {
                    name: 'Pegawai Sudah Sertifikasi',
                    y: 3887,
                    sliced: true,
                    selected: true
                },
                ['Pegawai Belum Sertifikasi', 4956]
            ]
        }]
    };

    function init() {
        $TeacherFactory.statsbox(function(reply) {
            if(reply.error) {
                console.log(reply);
            }

            $scope.data = reply;
        });
        setTimeout(function () {
            console.log($scope.data);
            $scope.PegawaiNuptk.series[0].data[0].y = $scope.data.nuptk.belum;
            $scope.PegawaiNuptk.series[0].data[1].y = $scope.data.nuptk.sudah;


        }, 1000);
    }

    init();

});