<?php
/**
 * Created by PhpStorm.
 * User: ragas
 * Date: 12/8/2017
 * Time: 1:51 AM
 */

class Rombel_model extends CI_Model
{

    public function __construct()
    {
        parent::__construct();
        if (!$this->session->as) {
            return ['error' => 'Invalid Credential'];
        }
    }

    public function insert($data)
    {
        if (!isset($data->tingkat->tingkat)) return ['error' => 'Mohon pilih tingkat!'];
        if (!isset($data->tapel->id)) return ['error' => 'Mohon pilih tahun pelajaran!'];

        $rombel = $data->rombel;
        $tingkat = $data->tingkat->tingkat;
        $tapel = $data->tapel;

        $ud = $this->db->get_where('user', array('id' => $this->session->userdata('uid')))->row();
        $sekolah = $this->db->get_where('data_sekolah', array('npsn' => $ud->id_npsn))->row()->id;
        // check if the existing rombel exist

        $this->db->select('id_rombel');
        $this->db->order_by('id_rombel', 'desc');
        $this->db->from('rombel_sekolah');
        $this->db->where('id_tapel', $tapel->id);
        $this->db->where('tingkat', $tingkat);
        $cur = $this->db->get();
        // start

        if ($cur->num_rows() > 0) {
            $rid = $cur->row()->id_rombel;
        } else {
            $this->db->select('id_rombel');
            $this->db->order_by('id_rombel', 'desc');
            $this->db->from('rombel_sekolah');
            $rid = $this->db->get()->row()->id_rombel + 1;
        }
        // end

        $insert = [];
        foreach ($rombel as $key => $row) {
            array_push($insert, [
                'id_sekolah' => $sekolah,
                'id_tapel' => $tapel->id,
                'nama' => $row->nama,
                'tingkat' => $tingkat,
                'id_rombel' => $rid,
                'laki' => $row->laki,
                'perempuan' => $row->perempuan
            ]);
        }


        $this->log('Tambah Rombel tingkat ' . $tingkat . ' Tahun Pelajaran ' . $tapel->tapel, 1);
        $this->db->insert_batch('rombel_sekolah', $insert);

        return array('success' => true);
    }

    public function update($data)
    {
        if (!isset($data->tingkat->tingkat)) return ['error' => 'Mohon pilih tingkat!'];
        if (!isset($data->tapel->id)) return ['error' => 'Mohon pilih tahun pelajaran!'];

        $rombel = $data->rombel;
        $tingkat = $data->tingkat->tingkat;
        $tapel = $data->tapel;

        $ud = $this->db->get_where('user', array('id' => $this->session->userdata('uid')))->row();
        $sekolah = $this->db->get_where('data_sekolah', array('npsn' => $ud->id_npsn))->row()->id;

        $this->db->select('id_rombel');
        $this->db->order_by('id_rombel', 'desc');
        $this->db->from('rombel_sekolah');
        $this->db->where('id_tapel', $tapel->id);
        $this->db->where('tingkat', $tingkat);
        $cur = $this->db->get();

        if ($cur->num_rows() > 0) {
            $rid = $cur->row()->id_rombel;
        } else {
            $rid = $data->id;
        }

        foreach ($rombel as $key => $row) {
            if (isset($row->id)) {
                $this->db->where('id', $row->id);
                $update = [
                    'id_sekolah' => $sekolah,
                    'id_tapel' => $tapel->id,
                    'nama' => $row->nama,
                    'tingkat' => $tingkat,
                    'id_rombel' => $rid,
                    'laki' => $row->laki,
                    'perempuan' => $row->perempuan
                ];
                $this->db->update('rombel_sekolah', $update);
            } else {
                $this->db->insert('rombel_sekolah', [
                    'id_sekolah' => $sekolah,
                    'id_tapel' => $tapel->id,
                    'nama' => $row->nama,
                    'tingkat' => $tingkat,
                    'id_rombel' => $rid,
                    'laki' => $row->laki,
                    'perempuan' => $row->perempuan
                ]);
            }
        }

        $this->log('Update Rombel tingkat ' . $tingkat . ' Tahun Pelajaran ' . $tapel->tapel, 2);

        return array('success' => true);
    }

    public function delete_group($group)
    {
        $rombel = $this->db->get_where('rombel_sekolah', ['id' => $group[0]->id])->row();
        $tapel = $this->db->get_where('tapel', ['id' => $rombel->id_tapel])->row()->tapel;
        $this->log('Hapus Rombel tingkat ' . $rombel->tingkat . ' Tahun Pelajaran ' . $tapel, 0);

        foreach ($group as $key => $row) {
            $this->db->delete('rombel_sekolah', ['id' => $row->id]);
        }

        return ['success' => true];
    }

    public function delete($id)
    {
        $q = $this->db->get_where('rombel_sekolah', ['id' => $id]);
        if($q->num_rows() > 0) {
            $this->db->delete('rombel_sekolah', ['id' => $id]);
            $this->log('Hapus Rombel ' . $q->row()->nama, 0);
            return ['success' => true];
        } else {
            return ['error' => 'Data tidak ditemukan'];
        }
    }

    public function show($tapel, $sekolah = null)
    {
        if ($this->session->as > 0) {
            $ud = $this->db->get_where('user', array('id' => $this->session->userdata('uid')))->row();
            $is = $this->db->get_where('data_sekolah', array('npsn' => $ud->id_npsn))->row()->id;
        }

        $this->db->select('rombel_sekolah.*, tapel.tapel tahun_pelajaran, (laki+perempuan) total');
        $this->db->from('rombel_sekolah');
        $this->db->join('tapel', 'id_tapel = tapel.id');

        if ($this->session->as > 0) {
            $this->db->where('id_sekolah', $is);
        } else {
            $this->db->where('id_sekolah', $sekolah);
        }

        $this->db->order_by('tingkat', 'asc');

        $this->db->where('id_tapel', $tapel);

        $total = clone $this->db;
        $total = $total->count_all_results();

//        $this->db->limit($count, $start);

        $data = $this->db->get()->result();
        return ['data' => $data, 'total' => $total];
//        if ($this->session->as > 0) {

//        }
    }

    public function school_list($start, $count, $filter)
    {

        //get active rombel

        $tapel = $this->db->get_where('tapel', ['active' => true])->row()->id;

        $this->db->select('
            data_sekolah.id,
            data_sekolah.nama,
            kecamatan.nama nama_kecamatan,
            SUM(rombel_sekolah.laki) total_laki,
            SUM(rombel_sekolah.perempuan) total_perempuan,
            SUM(rombel_sekolah.laki+rombel_sekolah.perempuan) total,
            COUNT(*) rombel
        ');

        $this->db->from('rombel_sekolah');
        $this->db->group_by('id_sekolah');
        $this->db->join('data_sekolah', 'id_sekolah  = data_sekolah.id');
        $this->db->join('kecamatan', 'data_sekolah.id_kecamatan = kecamatan.id');
        $this->db->where('id_tapel', $tapel);

        if(isset($filter['search'])) {
            $this->db->like('data_sekolah.nama', $filter['search']);
            $this->db->or_like('data_sekolah.npsn', $filter['search']);
//            $this->db->like('data_sekolah.nama', $filter['search']);
        }

        $total = clone $this->db;
        $total = $total->count_all_results();

        $this->db->limit($count, $start);

        $data = $this->db->get()->result();

        return ['data' => $data, 'total' => $total];
    }


    public function stats()
    {
        if ($this->session->as > 0) {
            $ud = $this->db->get_where('user', array('id' => $this->session->userdata('uid')))->row();
            $is = $this->db->get_where('data_sekolah', array('npsn' => $ud->id_npsn))->row()->id;
        }
        $tapel = $this->db->get_where('tapel', ['active' => true])->row()->id;

        $this->db->select('SUM(laki) laki, SUM(perempuan) perempuan, SUM(laki+perempuan) total_siswa');
        $this->db->from('rombel_sekolah');
        $this->db->join('data_sekolah', 'id_sekolah = data_sekolah.id');
        $this->db->join('tapel', 'id_tapel = tapel.id', 'left');
        if ($this->session->as > 0) $this->db->where('id_sekolah', $is);
        $this->db->where('id_tapel', $tapel);

        $result = $this->db->get()->row();

        $result->laki = ($result->laki) ? intval($result->laki) : 0;
        $result->perempuan = ($result->perempuan) ? intval($result->perempuan) : 0;
        $result->total_siswa = ($result->total_siswa) ? intval($result->total_siswa) : 0;

        $this->db->select('id_rombel');
        $this->db->from('rombel_sekolah');
        $this->db->where('id_tapel', $tapel);
        if ($this->session->as > 0) $this->db->where('id_sekolah', $is);

        $result->rombel = $this->db->get()->num_rows();

        return $result;
    }

    public function update_row($rombel) {
        if($this->db->get_where('rombel_sekolah', ['id' => $rombel->id])->num_rows() > 0) {
            $this->db->where('id', $rombel->id);
            $this->db->update('rombel_sekolah', [
                'nama' => $rombel->nama,
                'laki' => $rombel->laki,
                'perempuan' => $rombel->perempuan
            ]);

            $tapel = $this->db->get_where('tapel', ['id' => $rombel->id_tapel])->row();

            $this->log('Update Rombel tingkat ' . $rombel->tingkat . ' Tahun Pelajaran ' . $tapel->tapel, 2);

            return ['success' => true];
        } else {
            return ['error' => 'Data tidak ditemukan'];
        }
    }

    private function log($log, $type)
    {
        $this->db->insert('log_rombel', [
            'log' => $log,
            'type' => $type,
            'id_user' => $this->session->uid
        ]);
    }

}