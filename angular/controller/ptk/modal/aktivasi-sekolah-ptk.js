// Created by Raga 2Subekti at 18/10/17
angular.module('diknasmlg').controller('aktivasiSekolahPtk', function(data, Notify, $uibModalInstance, $scope, $TeacherFactory) {

    $scope.isSubmit = false;


    $scope.data = data;

    $scope.dismiss = function() {
        $uibModalInstance.dismiss();
    };

    $scope.onBtnConfirmClicked = function() {
        $scope.isSubmit = true;

        $TeacherFactory.activateSchool(data.idPtk, data.id, function(reply) {
            $scope.isSubmit = false;

            if(reply.error) {
                Notify.error('Kesalahan', reply.error);
            } else {
                Notify.success('Sukses', 'Sekolah berhasil diaktifkan');
                $uibModalInstance.close();
            }
        });
    }
});