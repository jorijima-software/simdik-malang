// Created by Raga Subekti at 08/10/17
angular.module('diknasmlg').controller('editSekolah', function (data, $scope, $uibModalInstance, $DistrictFactory, $LevelFactory, $SchoolFactory, Notify) {

    $scope.statusList = [{id: 0, nama: 'Swasta'}, {id: 1, nama: 'Negeri'}];

    $scope.districtList = [];
    $scope.levelList = [];

    var selectedStatus = {0: 'Swasta', 1: 'Negri'};

    $scope.edit = {
        id: data.id,
        nisn: data.npsn,
        nama: data.namaSekolah,
        tingkat: {
            id: data.idTingkatSekolah,
            nama: data.tingkatSekolah
        },
        alamat: data.alamat,
        status: {
            id: data.status,
            nama: selectedStatus[data.status]
        },
        kecamatan: {
            id: data.idKecamatan,
            nama: data.kecamatan
        }
    };

    $scope.onBtnSaveClicked = function () {
        $SchoolFactory.update($scope.edit, function (reply) {
            if (reply.error) {
                Notify.error('Kesalahan', reply.error)
            } else {
                Notify.success('Sukses', 'Berhasil memperbarui data sekolah');
                $uibModalInstance.close();
            }
        })
    };

    $scope.dismiss = function () {
        $uibModalInstance.dismiss();
    };

    function init() {
        $DistrictFactory.get(function (reply) {
            $scope.districtList = reply;
        });

        $LevelFactory.get(function (reply) {
            $scope.levelList = reply;
        })
    }

    init();
});