// Created by Raga Subekti at 18/10/17
angular.module('diknasmlg').controller('editSekolahPtk', function(data, Notify, $scope, $TeacherFactory, $uibModalInstance, $DistrictFactory, $EducationFactory) {

    $scope.isSubmit = false;

    $scope.rankList = [];
    $scope.schoolList = [];

    $scope.regencyList = [];

    $scope.edit = {
        tk: {
            id: data.tingkat.id,
            nama: data.tingkat.nama
        },
        nama: data.nama,
        masuk: parseInt(data.tahun.masuk),
        lulus: parseInt(data.tahun.lulus),
        kota: {
            id: data.kota.id,
            name: data.kota.nama
        },
        kecamatan: {
            name: data.kecamatan.nama
        },
        id: data.id,
        jurusan: data.jurusan
    };

    $scope.dismiss = function() {
        $uibModalInstance.dismiss();
    };

    $scope.onBtnSaveClicked = function() {
        $scope.isSubmit = true;

        $TeacherFactory.updateSchool($scope.edit, function(reply) {
            $scope.isSubmit = false;

            if(reply.error) {
                Notify.error('Kesalahan', reply.error);
            } else {
                Notify.success('Sukses', 'Sukses mengubah data sekolah');
                $uibModalInstance.close();
            }
        })
    }

    $scope.onRegencySelect = function() {
        $DistrictFactory.show($scope.edit.kota.id, '', function(reply) {
            $scope.districtList = reply;
        })
    };

    $scope.onInputKotaChange = function(query) {
        $DistrictFactory.regency(query, function(reply) {
            $scope.regencyList = reply;
        });
    };

    function init() {
        $DistrictFactory.regency('', function (reply) {
            $scope.regencyList = reply;
        });

        $EducationFactory.get(function(reply) {
            $scope.educationList = reply;
        });

        $DistrictFactory.show($scope.edit.kota.id, '', function(reply) {
            $scope.districtList = reply;
        })
    }

    init()

});