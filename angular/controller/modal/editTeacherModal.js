// Created by Raga Subekti at 07/10/17
angular.module('diknasmlg').controller('editTeacherModal', function (data, $scope, $uibModalInstance, $EmployeeFactory, $SchoolFactory, $TeacherFactory, Notify, $ReligionFactory, $EducationFactory, $DistrictFactory) {

    $scope.edit = {
        id: data.id,
        nuptk: data.nuptk,
        nama_lengkap: data.nama_lengkap,
        gelar_depan: data.gelar_depan,
        gelar_belakang: data.gelar_belakang,
        nip_lama: data.nip_lama,
        nip_baru: data.nip_baru,
        pegawai: {
            id: data.id_jenis_pegawai,
            nama: data.jenis_pegawai
        },
        sekolah: {
            id: data.id_sekolah,
            nama: data.nama_sekolah
        },
        rumah: {},
        profil: {},
        sert: {}
    };

    $scope.schoolList = [];
    $scope.employeeTypeList = [];
    $scope.onRumahRegencyChange = function (query) {
        $DistrictFactory.regency(query, function (reply) {
            $scope.rumahRegencyList = reply;
        })
    };

    $scope.onRumahRegencySelect = function () {
        $DistrictFactory.show($scope.edit.rumah.kota.id, '', function (reply) {
            $scope.rumahKecamatanList = reply;
        })
    };

    $scope.onSekolahRegencySelect = function () {
        $DistrictFactory.show($scope.edit.sekolah_guru.kota.id, '', function (reply) {
            $scope.rumahKecamatanList = reply;
        })
    };

    $scope.onSekolahKecamatanChange = function (query) {
        $DistrictFactory.show($scope.edit.sekolah_guru.kota.id, query, function (reply) {
            $scope.rumahKecamatanList = reply;
        })
    };

    $scope.onRumahKecamatanChange = function (query) {
        $DistrictFactory.show($scope.edit.rumah.kota.id, query, function (reply) {
            $scope.rumahKecamatanList = reply;
        })
    };

    $scope.dateOptions = {
        formatYear: 'yy',
        maxDate: new Date(),
        startingDay: 1
    };


    $scope.onInputSearchChange = function (query) {
        return $SchoolFactory.get(query, function (reply) {
            $scope.schoolList = reply;
        })
    };

    $scope.onHomeDistrictChange = function () {
        $scope.edit.rumah.kecamatan = $scope.edit.rumah.kecamatan.toUpperCase();
    };

    $scope.onHomeCityChange = function () {
        $scope.edit.rumah.kota = $scope.edit.rumah.kota.toUpperCase();
    };

    $scope.onSchoolNameChange = function () {
        $scope.edit.sekolah_guru.nama = $scope.edit.sekolah_guru.nama.toUpperCase();
    };

    $scope.onSchoolDistrictChange = function () {
        $scope.edit.sekolah_guru.kecamatan = $scope.edit.sekolah_guru.kecamatan.toUpperCase();
    };

    $scope.onSchoolCityChange = function () {
        $scope.edit.sekolah_guru.kota = $scope.edit.sekolah_guru.kota.toUpperCase();
    };

    $scope.onBtnSaveClicked = function () {

        $TeacherFactory.update($scope.edit, function (reply) {
            if (reply.error) {
                Notify.error('Kesalahan', reply.error);
            } else if (reply.success) {
                Notify.success('Sukses', 'Berhasil mengubah data PTK');
                $uibModalInstance.close();
            }
        })
    };

    $scope.onBirthPlaceChange = function () {
        $scope.edit.profil.tempat_lahir = $scope.edit.profil.tempat_lahir.toUpperCase();
    };

    $scope.dismiss = function () {
        $uibModalInstance.dismiss();
    };

    function init() {
        $EmployeeFactory.get(function (reply) {
            $scope.employeeTypeList = reply;
        });

        $SchoolFactory.get('', function (reply) {
            $scope.schoolList = reply;
        });

        $ReligionFactory.show(function (reply) {
            $scope.religionList = reply;
        });

        $EducationFactory.get(function (reply) {
            $scope.educationList = reply;
        });

        $DistrictFactory.regency('', function (reply) {
            $scope.rumahRegencyList = reply;
        });

        $TeacherFactory.get(data.id, function (reply) {

            if (reply.profil && reply.profil.tgl_lahir) {
                var dates = reply.profil.tgl_lahir.split('-');
                var birthdate = new Date(dates[0], dates[1] - 1, dates[2]);
            }

            var birthdate = new Date();

            $scope.edit.profil = {
                tempat_lahir: reply.profil.tempat_lahir,
                tanggal_lahir: birthdate,
                agama: {
                    id: reply.profil.id_jenis_agama,
                    nama: reply.profil.nama_agama
                },
                nik: reply.nik,
                nama_ibu: reply.nama_ibu
            };

            $scope.edit.rumah = {
                kota: {
                    name: reply.rumah.kota
                },
                kecamatan: {
                    name: reply.rumah.kec
                },
                no_telp: reply.rumah.nohp,
                alamat: reply.rumah.alamat_rumah
            };

            $scope.edit.sert = {
                pola: reply.sertifikasi.pola_sertifikasi,
                no: reply.sertifikasi.no_peserta,
                tahun: reply.sertifikasi.tahun_sertifikasi,
                bidang: reply.sertifikasi.id_bidang_studi
            };

        });
    }

    init();
});