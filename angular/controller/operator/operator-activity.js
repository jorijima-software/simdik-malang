'use strict';
angular.module('diknasmlg').controller('operatorActivity', function ($scope, $OperatorFactory, $routeParams, $q, NgTableParams) {

    $scope.router = $routeParams;

    $scope.detail = {};

    $scope.activityTable = new NgTableParams({}, {
        getData: function(params) {
            var count = params.count();
            var start = count * (params.page() - 1);
            var npsn = $scope.router.id;
            return $q(function(resolve, reject) {
                $OperatorFactory.activity(npsn, start, count).then(function(r) {
                    if(!r.error) {
                        params.total(r.total);
                        return resolve(r.data);
                    } else {
                        return reject(r);
                    }
                })
            })
        }
    })

    function init() {
        $OperatorFactory.detail($scope.router.id).then(function (r) {
            $scope.detail = r;
        })
    }

    init();

});