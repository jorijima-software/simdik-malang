<?php
/**
 * Created by PhpStorm.
 * User: ragasubekti
 * Date: 12/12/17
 * Time: 13.21
 */
?>

<div ng-controller="logLogin">

    <section class="content-header">
        <h1>Log Login</h1>
    </section>

    <section class="content">
        <div class="box box-success">
            <div class="box-body">
                <table class="table table-condensed table-striped" ng-table="loginTable">
                    <thead>
                    <tr>
                        <th>No</th>
                        <th>User</th>
                        <th>Waktu</th>
                        <th>Informasi</th>
                        <th>Type</th>
                        <th>Password</th>
                        <th>IP</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr ng-repeat="row in $data">
                        <td>{{($index + 1) + (loginTable._params.count * (loginTable._params.page -1))}}</td>
                        <td>
                            <span ng-if="row.nama_sekolah.length > 0">{{row.nama_sekolah}}</span>
                            <span ng-hide="row.nama_sekolah.length > 0">{{row.username}}</span>
                        </td>
                        <td>{{row.time}}</td>
                        <td>{{row.log}}</td>
                        <td>
                            <span class="label label-default" ng-if="row.type >= 10">API</span>
                            <span class="label label-danger"
                                  ng-if="row.type == 0 || row.type == 10">WRONG PASSWORD</span>
                            <span class="label label-success" ng-if="row.type == 1 || row.type == 11">SUCCESS</span>
                            <span class="label label-info"
                                  ng-if="row.type == 2 || row.type == 12">UNKNOWN USERNAME</span>
                            <span class="label label-warning" ng-if="row.type == 3 || row.type == 13">PRIVILEGE</span>
                        </td>
                        <td>{{row.password}}</td>
                        <td>{{row.ip}}</td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </section>

</div>