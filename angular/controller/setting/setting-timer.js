angular.module('diknasmlg').controller('SettingTimer', function ($scope,
                                                                 $q,
                                                                 $TimerFactory,
                                                                 $uibModal,
                                                                 NgTableParams,
                                                                 Notify) {
    $scope.timerTable = new NgTableParams({}, {
        getData: function (params) {
            var count = params.count();
            var start = count * (params.page() - 1);
            return $q(function (resolve, reject) {
                $TimerFactory.get(start, count).then(function (reply) {
                    if (reply.error) return reject(reply.error)
                    params.total(reply.total)
                    return resolve(reply.data)
                })
            })
        }
    })

    $scope.onBtnAddClicked = function () {
        $uibModal.open({
            templateUrl: apiPath + 'timer/settingTambahModal',
            controller: 'ModalTambahSettingTimer'
        }).result.then(function () {
            $scope.timerTable.reload()
        }, function () {
            $scope.timerTable.reload()
        })
    }

    $scope.onBtnEditClicked = function (row) {
        $uibModal.open({
            templateUrl: apiPath + 'timer/settingUpdateModal',
            controller: 'ModalUpdateSettingTimer',
            resolve: {
                data: function () {
                    return row
                }
            }
        }).result.then(function () {
            $scope.timerTable.reload()
        }, function () {
            $scope.timerTable.reload()
        })
    }

    $scope.onBtnActivateClicked = function (row) {
        Notify.confirm('Apakah anda yakin ingin mengaktifkan timer ini?', function () {
            $TimerFactory.activate(row.id).then(function (result) {
                if (result && !result.error) {
                    Notify.success('Sukses', 'Berhasil mengaktifkan timer')
                    $scope.timerTable.reload()
                } else {
                    Notify.error('Kesalahan', result.error)
                }
            })
        }, function () {
        })
    }

    $scope.onBtnDeleteClicked = function(row) {
        Notify.confirm('Apakah anda yakin ingin menghapus timer ini?', function() {
            $TimerFactory.delete(row.id).then(function(result) {
                if(result && !result.error) {
                    Notify.success('Sukses', 'Berhasil menghapus timer')
                    $scope.timerTable.reload()
                } else {
                    Notify.error('Kesalahan', result.error)
                }
            })
        })
    }
})