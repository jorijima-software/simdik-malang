angular.module('diknasmlg').factory('$LessonFactory', function($http) {

    var factory = {};

    var apiPath = localStorage.getItem('PATHLOC');

    factory.get = function(query, callback) {
        $http.get(apiPath + 'api/lesson/get/' + query).then(function(reply) {
            if(reply.status == 200) {
                return callback(reply.data);
            } else {
                console.log(reply)
            }
        }, function(err) {
            console.log(err)
        })
    };

    return factory;

});