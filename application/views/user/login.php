<?php
/**
 * Created by PhpStorm.
 * User: ragasubekti
 * Date: 06/10/17
 * Time: 13.03
 */

if ($this->session->is_logged && $this->session->as > 0) {
    redirect('/#!/school/profile');
} else if ($this->session->is_logged) {
    redirect('/#!/dashboard');
}
?>

<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>SIMDIK | Dinas Pendidikan Kota Malang</title>
    <meta name="Description" content="Sistem Informasi Manajemen Pendidikan (SIMDIK) Dinas Pendidikan Kota Malang">
    <meta name="Keywords" content="simdik, dinas pendidikan, malang">

    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <link rel="stylesheet" href="<?=base_url('bower_components/bootstrap/dist/css/bootstrap.min.css')?>">
    <link rel="stylesheet" href="<?=base_url('bower_components/font-awesome/css/font-awesome.min.css')?>">
    <link rel="stylesheet" href="<?=base_url('dist/css/AdminLTE.min.css')?>">
    <link rel="stylesheet"
          href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
          <style>
              #second-par canvas {
                position: absolute;
                width: 100%;
                height: 100%;
                top: 0;
                left: 0;
                z-index: 1
              }
          </style>
</head>
<body class="hold-transition login-page" style="display: flex; align-items: center;">
<!--<div class="login-box">-->
<div class="" style="width: 100%; min-height: 500px; margin: 50px 100px">
    <div class="col-md-4" style="min-height: 500px; ">
    <div class="login-box-body" style="min-height: 500px;  padding: 25px; background: #fff; display: flex; align-items: center ">
        <div style="width: 100%">
        <div class="login-logo">
            <div class="text-center">
                <table>
                    <tr>
                        <td rowspan="2">
                            <span class="logo-mini"><img src="<?=base_url('assets/logo.png')?>" alt="Dinas Pendidikan Kota Malang" width="50"></span>
                        </td>
                        <td style="margin: 0; font-size: 14px; text-align: left; padding-left: 10px">Dinas Pendidikan Kota Malang
                        </td>
                    </tr>
                    <tr>
                        <td style="margin: 0; font-size: 28px; text-align: left; padding-left: 10px">
                            SIMDIK
                        </td>
                    </tr>
                </table>
            </div>
        </div>
        <form action="<?=site_url('login')?>" method="post">
            <?php if (isset($error)) {?>
                <div class="form-group">
                    <div class="alert alert-danger">
                        <i class="fa fa-exclamation-triangle"></i> &nbsp; <?=$error?>
                    </div>
                </div>
            <?php }?>
            <div class="form-group hidden" id="browser">
                <div class="alert alert-danger">
                    <i class="fa fa-exclamation-triangle"></i> <strong>Browser tidak didukung</strong> <br>
                    Mohon upgrade browser anda, kami sarankan menggunakan <b>Chrome 62/lebih baru</b> atau <b>Firefox 57/lebih baru</b>
                </div>
            </div>
            <div class="form-group has-feedback">
                <input type="text" class="form-control" placeholder="Username" name="username">
                <span class="fa fa-user form-control-feedback"></span>
            </div>
            <div class="form-group has-feedback">
                <input type="password" class="form-control" placeholder="Password" name="password">
                <span class="glyphicon glyphicon-lock form-control-feedback"></span>
            </div>
            <div class="form-group">
                <select name="login_as" id="" class="form-control">
                    <option value="0">Admin</option>
                    <option value="1" selected>Operator</option>
                </select>
            </div>
            <button type="submit" class="btn btn-primary btn-block" id="loginbtn">Sign In</button>
        </form>
        </div>
    </div>
    </div>
    <div class="col-md-8" style="min-height: 500px; margin-left: -25px; background: #000022">
        <div style="display:flex; justify-content: center; align-items: center; min-height: 500px; max-height: 80%;width: 100%; ">
            <div>
                <div style="color: #fff; font-size: 38px; flex-wrap: wrap;">Selamat datang di aplikasi SIMDIK</div>
                
                <div id="second-par">
                    
                </div>
            </div>
        </div>
    </div>
</div>
</div>
<script src="<?=base_url('bower_components/jquery/dist/jquery.min.js')?>"></script>
<script src="<?=base_url('angular/browser-check.js')?>"></script>
<script src="<?=base_url('bower_components/bootstrap/dist/js/bootstrap.min.js')?>"></script>
<script src="https://cdn.jsdelivr.net/particles.js/2.0.0/particles.min.js"></script>
<script>
    particlesJS("second-par", {
        "particles": {
            "number": {
                "value": 380,
                "density": {
                    "enable": true,
                    "value_area": 1200
                }
            },
            "color": {
                "value": ["#aa73ff", "#f8c210", "#83d238", "#33b1f8"]
            },
            "shape": {
                "type": "circle",
                "stroke": {
                    "width": 0,
                    "color": "#000000"
                },
                "polygon": {
                    "nb_sides": 15
                }
            },
            "opacity": {
                "value": 0.5,
                "random": false,
                "anim": {
                    "enable": false,
                    "speed": 1.5,
                    "opacity_min": 0.15,
                    "sync": true
                }
            },
            "size": {
                "value": 2.5,
                "random": false,
                "anim": {
                    "enable": true,
                    "speed": 2,
                    "size_min": 0.15,
                    "sync": false
                }
            },
            "line_linked": {
                "enable": true,
                "distance": 110,
                "color": "#33b1f8",
                "opacity": 0.5,
                "width": 1
            },
            "move": {
                "enable": true,
                "speed": 1.6,
                "direction": "none",
                "random": false,
                "straight": false,
                "out_mode": "out",
                "bounce": false,
                "attract": {
                    "enable": false,
                    "rotateX": 600,
                    "rotateY": 1200
                }
            }
        },
        "interactivity": {
            "detect_on": "canvas",
            "events": {
                "onhover": {
                    "enable": false,
                    "mode": "repulse"
                },
                "onclick": {
                    "enable": false,
                    "mode": "push"
                },
                "resize": true
            },
            "modes": {
                "grab": {
                    "distance": 400,
                    "line_linked": {
                        "opacity": 1
                    }
                },
                "bubble": {
                    "distance": 400,
                    "size": 40,
                    "duration": 2,
                    "opacity": 8,
                    "speed": 3
                },
                "repulse": {
                    "distance": 50,
                    "duration": 0.4
                },
                "push": {
                    "particles_nb": 4
                },
                "remove": {
                    "particles_nb": 2
                }
            }
        },
        "retina_detect": true
    });
    var count_particles, stats, update;
    stats = new Stats;
    stats.setMode(0);
    stats.domElement.style.position = 'absolute';
    stats.domElement.style.left = '0px';
    stats.domElement.style.top = '0px';
    document.body.appendChild(stats.domElement);
    count_particles = document.querySelector('.js-count-particles');
    update = function() {
        stats.begin();
        stats.end();
        if (window.pJSDom[0].pJS.particles && window.pJSDom[0].pJS.particles.array) {
            count_particles.innerText = window.pJSDom[0].pJS.particles.array.length;
        }
        requestAnimationFrame(update);
    };
    requestAnimationFrame(update);
</script>
</body>
</html>
