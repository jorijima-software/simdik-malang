<section class="content-header">
    <h1>Riwayat Kerja PTK</h1>
</section>
<section class="content">
    <?php require_once APPPATH.'views/partials/informasi-ptk.php'; ?>

    <div class="box box-primary">
        <div class="box-header with-border">
            <div class="box-title">Riwayat Kerja</div>
        </div>
        <div class="box-body">
            <div class="row">
                <div class="col-md-9">
                    <button class="btn btn-primary btn-sm" ng-click="onBtnAddClicked()" ng-disabled="timerEnd">
                        <i class="fa fa-plus"></i> Tambah
                    </button>
                </div>
                <div class="col-md-3">
                    <div class="pull-right">
                        <span class="btn btn-info" uib-tooltip="Masa Kerja dihitung mulai dari TMT paling awal/lama">
                            Masa Kerja <strong>{{ptk.kepegawaian.masaKerja}}</strong>
                        </span>
                    </div>
                </div>
            </div>
            <table class="table table-condensed table-striped table-hover" ng-table="riwayatKerjaTable">
                <thead>
                    <tr>
                        <th>No</th>
                        <th>No. SK</th>
                        <th>Unit Kerja</th>
                        <th>Lokasi</th>
                        <th>Gaji</th>
                        <th>TMT</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    <tr ng-repeat="row in $data">
                        <td>{{$index+1}}</td>
                        <td>{{row.sk}}</td>
                        <td>{{row.unitKerja}}</td>
                        <td>
                            <span ng-show="row.namaKota.length > 0">{{row.namaKota}}</span>
                            <span ng-hide="row.namaKota.length > 0">KOTA MALANG</span>
                        </td>
                        <td>{{row.gaji | currency:"Rp":0}}</td>
                        <td>{{row.tmt}}</td>
                        <td>
                            <div class="pull-right">
                                <button class="btn btn-primary btn-xs" ng-click="onBtnEditClicked(row)" ng-disabled="timerEnd">
                                    <i class="fa fa-pencil"></i>
                                    <span>Edit</span>
                                </button>
                                <button class="btn btn-danger btn-xs" ng-click="onBtnDeleteClicked(row)" ng-disabled="timerEnd">
                                    <i class="fa fa-trash"></i>&nbsp;Hapus
                                </button>
                            </div>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</section>