<?php
/**
 * Created by PhpStorm.
 * User: ragasubekti
 * Date: 04/11/17
 * Time: 13.33
 */

$this->load->model('User_model');
$schinfo = $this->User_model->profile();

$status = array(
    0 => 'Swasta',
    1 => 'Negeri'
);

$geo = array(
    0 => 'Pedesaan',
    1 => 'Perkotaan',
    2 => 'Daerah Perbatasan'
);

?>
<div ng-controller="profilSekolah">
    <?php if ($this->session->need_change_password) { ?>
    <div style="padding: 20px 30px; background-color: #dd4b39; z-index: 999999; font-size: 16px; font-weight: 600;">
        <span
                style="color: rgba(255, 255, 255, 0.9); display: inline-block; margin-right: 10px; text-decoration: none;">
            <strong><i class="fa fa-exclamation-triangle"></i> &nbsp; Peringatan! </strong> Password yang anda gunakan masih <i>default</i>, mohon ganti segera!</span>
        <a href="<?= site_url('user/profile') ?>" class="btn btn-default btn-sm"
           style="margin-top: -5px; border: 0px; box-shadow: none; color: #dd4b39; font-weight: 600; background: rgb(255, 255, 255);">Ganti
            Password!</a></div>
    <div>
        <?php } ?>
        <div style="padding: 20px 30px; background: rgb(243, 156, 18); z-index: 999999; font-size: 16px; font-weight: 600;"
             ng-if="!operator || !operator.nama || (operator.nama && operator.nama.length <= 0)" ng-cloak>
        <span
                style="color: rgba(255, 255, 255, 0.9); display: inline-block; margin-right: 10px; text-decoration: none;">
            <strong><i class="fa fa-exclamation-triangle"></i> &nbsp; Peringatan! </strong> Anda belum memberi informasi
            tentang operator sekolah, mohon isi segera untuk keamanan akun anda.</span>
            <a ng-click="onBtnOperatorProfile()" class="btn btn-default btn-sm"
               style="margin-top: -5px; border: 0px; box-shadow: none; color: rgb(243, 156, 18); font-weight: 600; background: rgb(255, 255, 255);">Ubah
                Profil Operator!</a></div>
        <div>
            <section class="content-header">
                <h1>
                    Profil Sekolah
                </h1>
            </section>
            <section class="content">
                <div class="row">
                    <div class="col-md-12">
                        <div class="box">
                            <div class="box-body">
                                <div class="row">
                                    <div class="col-md-8">
                                        <table class="table table-striped">
                                            <tr>
                                                <th>NPSN</th>
                                                <td>
                                                    <?= $schinfo->npsn ?>
                                                </td>
                                            </tr>
                                            <tr>
                                                <th>
                                                    Nama Sekolah
                                                </th>
                                                <td>
                                                    <?= $schinfo->nama ?>
                                                </td>
                                            </tr>
                                            <tr>
                                                <th>Tingkat</th>
                                                <td>
                                                    <?= $schinfo->nama_tingkat ?>
                                                </td>
                                            </tr>
                                            <tr>
                                                <th>Kecamatan</th>
                                                <td><?= $schinfo->nama_kecamatan ?></td>
                                            </tr>
                                            <tr>
                                                <th>Desa</th>
                                                <td><?= $schinfo->desa ?></td>
                                            </tr>
                                            <tr>
                                                <th>Alamat</th>
                                                <td><?= $schinfo->alamat ?></td>
                                            </tr>
                                            <tr>
                                                <th>No. Telp</th>
                                                <td><?= $schinfo->notelp ?></td>
                                            </tr>
                                            <tr>
                                                <th>Faximile</th>
                                                <td><?= $schinfo->fax ?></td>
                                            </tr>
                                            <tr>
                                                <th>Email</th>
                                                <td><?= $schinfo->email ?></td>
                                            </tr>

                                            <tr>
                                                <th>Website</th>
                                                <td><?= $schinfo->website ?></td>
                                            </tr>

                                            <tr>
                                                <th>Status</th>
                                                <td><?= $status[$schinfo->status] ?></td>
                                            </tr>

                                            <tr>
                                                <th>Geografis</th>
                                                <td>
                                                    <?= $geo[$schinfo->geografis] ?>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="text-center" style="max-height: 400px; margin-bottom: 10px">
                                            <?php if (!($schinfo->picture)) { ?>
                                                <img src="<?= base_url('assets/no-image.svg') ?>" alt="" width="100%">
                                            <?php } else { ?>
                                                <img src="<?= base_url('uploads/' . $schinfo->picture) ?>" alt=""
                                                     width="100%">
                                            <?php } ?>
                                        </div>
                                            <p class="text-center">
                                                <strong>Kelengkapan Data</strong>
                                            </p>

                                            <div class="progress-group" style="margin-bottom: 5px;">
                                                <span class="progress-text">Data PTK</span>
                                                <span class="progress-number"><b>{{persen.ptk}}%</b></span>

                                                <div class="progress sm" style="margin-bottom: 3px">
                                                    <div class="progress-bar progress-bar-aqua" style="width: {{persen.ptk}}%"></div>
                                                </div>
                                                <small style="color: #aaa;">Data didapat dari kelengkapan <b>Biodata</b> PTK</small>
                                            </div>
                                            <!-- /.progress-group -->
                                            <div class="progress-group" style="margin-bottom: 5px;">
                                                <span class="progress-text">Data PNS</span>
                                                <span class="progress-number"><b>{{persen.pns}}%</b></span>

                                                <div class="progress sm" style="margin-bottom: 3px">
                                                    <div class="progress-bar progress-bar-red" style="width: {{persen.pns}}%"></div>
                                                </div>
                                                <small style="color: #aaa;">Data dihitung dari <b>Riwayat Sekolah, Riwayat Pangkat, Riwayat Gaji</b></small>
                                            </div>
                                            <!-- /.progress-group -->
                                            <div class="progress-group" style="margin-bottom: 5px;">
                                                <span class="progress-text">Data Non-PNS</span>
                                                <span class="progress-number"><b>{{persen.npns}}%</b></span>

                                                <div class="progress sm" style="margin-bottom: 3px">
                                                    <div class="progress-bar progress-bar-green" style="width: {{persen.npns}}%"></div>
                                                </div>
                                                <small style="color: #aaa;">Data dihitung dari <b>Riwayat Sekolah, Riwayat Kerja</b></small>

                                            </div>
                                        <div class="box box-widget widget-user-2"
                                             ng-if="operator && operator.nama && operator.nama.length > 0">
                                            <div class="widget-user-header bg-blue">
                                                <h3 class="widget-user-username text-capitalize" style="margin-left: 0">
                                                    {{operator.nama}}</h3>
                                                <h5 class="widget-user-desc" style="margin-left: 0">Operator
                                                    Sekolah</h5>
                                            </div>
                                            <div class="box-footer no-padding">
                                                <ul class="nav nav-stacked">
                                                    <li>
                                                        <a href><i class="fa fa-phone"></i> &nbsp; {{operator.no_hp}}
                                                        </a>
                                                    </li>
                                                    <li>
                                                        <a href><i class="fa fa-whatsapp"></i>
                                                            &nbsp; {{operator.no_wa}}
                                                        </a>
                                                    </li>
                                                    <li>
                                                        <a>
                                                            <button class="btn btn-xs btn-primary"
                                                                    ng-click="onBtnOperatorProfile()">
                                                                <i class="fa fa-pencil"></i> <span>Ubah</span>
                                                            </button>
                                                        </a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="box-footer">
                                <a class="btn btn-primary btn-sm" href="<?= site_url('school/manage') ?>">
                                    <i class="fa fa-pencil"></i> <span>Ubah</span>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>

    <script type="text/ng-template" id="locked.html">
        <div class="modal-header">
            <h3 class="modal-title">Terimakasih!</h3>
        </div>
        <div class="modal-body">

        </div>
    </script>