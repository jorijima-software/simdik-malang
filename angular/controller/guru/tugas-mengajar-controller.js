/*
    Created by Raga Subekti
    at  29/11/17 00.41 by PhpStorm
*/
angular.module('diknasmlg').controller('tugasMengajarController', function (
    $scope,
    $uibModal,
    NgTableParams,
    $q,
    $TeacherFactory,
    Notify,
    $routeParams,
    $PtkFactory,
    $TimerFactory
) {

    $scope.routes = $routeParams;

    $scope.timerEnd
    $scope.detail = {};
    var apiPath = localStorage.getItem('PATHLOC');

    $scope.onBtnAddClicked = function () {
        $uibModal.open({
            templateUrl: apiPath + 'teacher/modal_dmengajar_tambah',
            controller: 'tambahTugasMengajar',
            resolve: {
                data: function () {
                    return null;
                },
                id: function () {
                    return $scope.routes.id;
                }
            }
        }).result.then(refreshTable, refreshTable)
    };

    $scope.semester = {
        1: 'Ganjil',
        2: 'Genap'
    };

    $scope.tmengajarTable = new NgTableParams({}, {
        getData: function (params) {
            var count = params.count();
            var start = count * (params.page() - 1);
            var id = $scope.routes.id;
            return $q(function (resolve, reject) {
                $TeacherFactory.showTugasMengajar(id, start, count, {}, function (reply) {
                    if (reply.error) return reject(reply.error);
                    params.total(reply.total);
                    return resolve(reply.data);
                })
            })
        }
    });

    $scope.onBtnDeleteClicked = function (row) {
        Notify.confirm('Apakah anda yakin ingin menghapus data tersebut?', function () {
            $TeacherFactory.deleteTugasMengajar(row.id, function (reply) {
                if (reply.error) {
                    Notify.error('Kesalahan', reply.error);
                } else {
                    Notify.success('Sukses', 'Berhasil menghapus data tugas mengajar')
                }

                refreshTable();
            })
        }, function () { })
    };

    $scope.onBtnEditClicked = function (row) {
        $uibModal.open({
            templateUrl: apiPath + 'teacher/modal_dmengajar_tambah',
            controller: 'tambahTugasMengajar',
            resolve: {
                data: function () {
                    return row;
                },
                id: function () {
                    return false;
                }
            }
        }).result.then(refreshTable, refreshTable)
    };

    function refreshTable() {
        $scope.tmengajarTable.reload();
    }

    function init() {
        $PtkFactory.detail($scope.routes.id).then(function (r) {
            $scope.detail = r;
        })

        $TimerFactory.diff().then(result => {
            $scope.timerEnd = (!result.admin && result.disabled)
        })
    }

    init();

});