 // Created by Raga Subekti at 12/10/17
angular.module('diknasmlg').controller('dashboardController', function ($scope, $SchoolFactory, $timeout) {

    $scope.data = []
    $scope.labels = []
    $scope.series = []

    $scope.schoolStats = []
    $scope.dataMasuk = []

    $scope.bgIndex = [
        'bg-aqua',
        'bg-yellow',
        'bg-green',
        'bg-orange',
        'bg-red'
    ]

    function init() {
        $SchoolFactory.spread(function (reply) {
            reply.forEach(function (item) {
                $scope.labels.push(item.nama)
                $scope.data.push(item.total)
            })
        });

        $SchoolFactory.statsbox(function(reply) {
            $scope.schoolStats = reply;
        })

        $SchoolFactory.dashboardPercentage().then(function(reply) {
            $scope.dataMasuk = reply;
        })

    }

    init();


});