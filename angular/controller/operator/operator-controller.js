/*
    Created by Raga Subekti
    at  06/12/17 18.05 by PhpStorm
*/
angular.module('diknasmlg').controller('operatorController', function($scope, $uibModal, $OperatorFactory, NgTableParams, $q, Notify) {

    var API = localStorage.getItem('PATHLOC');

    $scope.search = '';

    $scope.onSearchChange = function() {
        $scope.operatorTable.reload();
    };

    $scope.operatorTable = new NgTableParams({}, {
        getData: function(params) {
            var count = params.count();
            var start = count * (params.page() - 1);
            var search = $scope.search;
            return $q(function(resolve, reject) {
                $OperatorFactory.shows(start, count, search, {}).then(function(r) {
                    if(r.data) {
                        params.total(r.total);
                        return resolve(r.data);
                    } else {
                        return reject(r.error);
                    }
                })
            })
        }
    });

    $scope.onBtnAddClicked = function() {
        $uibModal.open({
            templateUrl: API + 'modal/operator/tambah',
            controller: 'tambahOperator',
            resolve: {
                edit: function() {
                    return null;
                }
            }
        }).result.then(function() {
            $scope.operatorTable.reload();
        }, function() {
            $scope.operatorTable.reload();
        })
    };

    $scope.onBtnEditClicked = function(row) {
        $uibModal.open({
            templateUrl: API + 'modal/operator/tambah',
            controller: 'tambahOperator',
            resolve: {
                edit: function() {
                    return row;
                }
            }
        }).result.then(function() {
            $scope.operatorTable.reload();
        }, function() {
            $scope.operatorTable.reload();
        })
    };

    $scope.onBtnDeleteClicked = function(row) {
        Notify.confirm('Apakah anda yakin ingin menghapus operator <strong>' + row.nama_operator + '</strong>?', function() {
            $OperatorFactory.deletes(row.id).then(function(r) {
                if(r.error) {
                    Notify.error('Kesalahan', r.error);
                } else {
                    Notify.success('Sukses', 'Berhasil menghapus data operator!');
                }

                $scope.operatorTable.reload();
            })
        }, function() {

        })
    }
});