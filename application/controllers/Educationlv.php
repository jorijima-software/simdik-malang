<?php
/**
 * Created by PhpStorm.
 * User: ragasubekti
 * Date: 09/10/17
 * Time: 07.20
 */

class Educationlv extends CI_Controller {

    public function get() {
        $this->load->model('Educationlv_model');
        header('Content-Type: application/json');
        echo json_encode($this->Educationlv_model->get());
    }

}