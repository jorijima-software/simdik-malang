angular
    .module('diknasmlg')
    .factory('$PtkRekeningFactory', function ($q, $http) {
        var factory = {}
        var PATH = localStorage.getItem('PATHLOC')

        factory.store = function (data) {
            return $q(function (resolve, reject) {
                $http.post(PATH + 'api/ptk/rekening/store', data).then(function(response) {
                    if(response.status === 200) {
                        return resolve(response.data)
                    } else {
                        return reject(response)
                    }
                })
            })
        }

        factory.get = function (id) {
            return $q(function (resolve, reject) {
                $http.get(PATH + 'api/ptk/rekening/' + id).then(function(response) {
                    if(response.status === 200) {
                        return resolve(response.data)
                    } else {
                        return reject(response)
                    }
                })
            })
        }

        return factory
    })