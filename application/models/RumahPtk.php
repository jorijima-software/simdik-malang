<?php
/**
 * Created by PhpStorm.
 * User: ragasubekti
 * Date: 1/9/18
 * Time: 7:32 PM
 */
class RumahPtk extends \Illuminate\Database\Eloquent\Model {
    protected $table = 'rumah_guru';
    protected $guarded = [];
    public $timestamps = false;
}