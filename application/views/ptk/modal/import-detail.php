<div class="modal-header">
    Detail PTK
</div>
<div class="modal-body">
    <uib-tabset>
        <uib-tab heading="Kepegawaian">
            <table class="table table-striped table-hover">
                <tr>
                    <th>NUPTK</th>
                    <td colspan="3">{{detail.nuptk}}</td>
                </tr>
                <tr>
                    <th>Nama</th>
                    <td colspan="3">{{detail.nama_lengkap}}</td>
                </tr>
                <tr>
                    <th>NIP</th>
                    <td colspan="3">{{detail.nip}}</td>
                </tr>
                <tr>
                    <th>Gelar Depan</th>
                    <td>{{detail.gelar_depan}}</td>
                    <th>Gelar Belakang</th>
                    <td>{{detail.gelar_belakang}}</td>
                </tr>
                <tr>
                    <th>Jenis Pegawai</th>
                    <td colspan="3">{{detail.jenis_pegawai.nama}}</td>
                </tr>
            </table>
        </uib-tab>
        <uib-tab heading="Profil">
            <table class="table table-striped table-hover">
                <tr>
                    <th>Tempat Lahir</th>
                    <td>{{detail.tempat_lahir}}</td>
                </tr>
                <tr>
                    <th>Tanggal Lahir</th>
                    <td>{{detail.tanggal_lahir}}</td>
                </tr>
                <tr>
                    <th>Agama</th>
                    <td>{{detail.nama_agama.nama}}</td>
                </tr>
                <tr>
                    <th>NIK</th>
                    <td>{{detail.nik}}</td>
                </tr>
                <tr>
                    <th>Nama Ibu</th>
                    <td>{{detail.nama_ibu}}</td>
                </tr>
            </table>
        </uib-tab>
        <uib-tab heading="Rumah">
            <table class="table table-striped table-hover">
                <tr>
                    <th>Alamat</th>
                    <td>{{detail.alamat}}</td>
                </tr>
                <tr>
                    <th>No. Telp</th>
                    <td>{{detail.no_telp}}</td>
                </tr>
            </table>
        </uib-tab>
    </uib-tabset>
</div>
<div class="modal-footer">
<button class="btn btn-default btn-sm" ng-click="dismiss()">
    Tutup
</button>
</div>