<?php
/**
 * Created by PhpStorm.
 * User: ragasubekti
 * Date: 12/12/17
 * Time: 13.20
 */
?>

<div ng-controller="logSms">
    <section class="content-header" style="margin-bottom: 60px">
        <div class="pull-right" style="">
            <div class="small-box bg-green" style="padding: 5px">
                <div class="text-center">Kredit SMS</div>
                <h3 style="margin: 0; padding: 0" class="text-center">{{credit}}</h3>
            </div>
        </div>
        <h1>Log SMS</h1>
    </section>

    <section class="content">
        <div class="box box-success">
            <div class="box-body">
                <table class="table table-condensed table-striped" ng-table="smsTable">
                    <thead>
                    <tr>
                        <th>No</th>
                        <th>User</th>
                        <th>Waktu</th>
                        <th>Log</th>
                        <th>Status</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr ng-repeat="row in $data">
                        <td>{{($index + 1) + (smsTable._params.count * (smsTable._params.page -1))}}</td>
                        <td>{{row.nama_sekolah}}</td>
                        <td>{{row.date}}</td>
                        <td>{{row.log}}</td>
                        <td>
                        <span class="label label-success" ng-if="row.type == 0">
                            SUCCESS
                        </span>
                            <span class="label label-danger" ng-if="row.type == 2">
                            MAX LIMIT
                        </span>
                            <span class="label label-warning" ng-if="row.type==1">
                            RESEND
                        </span>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </section>
</div>
