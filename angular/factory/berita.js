angular.module('diknasmlg').factory('$BeritaFactory', function($http) {

    var apiPath = localStorage.getItem('PATHLOC');

    var factory = {};

    factory.target = function(query, callback) {
        $http.get(apiPath + 'api/berita/target/' + query).then(function(reply) {
            if(reply.status == 200) {
                return callback(reply.data);
            } else {
                console.log(reply)
            }
        }, function(err) {
            console.log(err)
        });
    };

    factory.baru = function(data, callback) {
        $http.post(apiPath + 'api/berita/baru', {
            target: data.kepada,
            judul: data.judul,
            isi: data.isi
        }).then(function(reply) {
            if(reply.status == 200) {
                return callback(reply.data);
            } else {
                console.log(reply)
            }
        }, function(err) {
            console.log(err)
        });
    };

    factory.show = function(start, callback) {
        var param = {
            start: start
        };
        $http.get(apiPath + 'api/berita/show?' + jQuery.param(param)).then(function(reply) {
            if(reply.status == 200) {
                return callback(reply.data);
            } else {
                console.log(reply)
            }
        }, function(err) {
            console.log(err)
        });
    };

    factory.detail = function(id, callback) {

        var param = {
            id: id
        };

        $http.get(apiPath + 'api/berita/detail?' + jQuery.param(param)).then(function(reply) {
            if(reply.status == 200) {
                return callback(reply.data);
            } else {
                console.log(reply)
            }
        }, function(err) {
            console.log(err)
        });
    };

    factory.delete = function(id, callback) {
        var param = {
            id: id
        };

        $http.get(apiPath + 'api/berita/delete?' + jQuery.param(param)).then(function(reply) {
            if(reply.status == 200) {
                return callback(reply.data);
            } else {
                console.log(reply)
            }
        }, function(err) {
            console.log(err)
        });
    };

    factory.update = function(data, callback) {
        $http.post(apiPath + 'api/berita/update', {
            id: data.id,
            target: data.kepada,
            judul: data.judul,
            isi: data.isi
        }).then(function(reply) {
            if(reply.status == 200) {
                return callback(reply.data);
            } else {
                console.log(reply)
            }
        }, function(err) {
            console.log(err)
        });
    };

    return factory;

});