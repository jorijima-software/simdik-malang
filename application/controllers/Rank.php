<?php
/**
 * Created by PhpStorm.
 * User: ragasubekti
 * Date: 19/10/17
 * Time: 16.12
 */
class Rank extends CI_Controller {

    public function get()
    {
        header('Content-Type: application/json');
        $this->load->model('Rank_model');


        echo json_encode($this->Rank_model->get());
    }

}