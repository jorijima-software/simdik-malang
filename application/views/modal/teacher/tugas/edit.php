<?php
/**
 * Created by PhpStorm.
 * User: ragasubekti
 * Date: 22/11/17
 * Time: 03.57
 */
?>

<div class="modal-header">
    <div class="modal-title">{{type}} Tugas Tambahan</div>
</div>

<div class="modal-body">
    <form>
        <div class="form-group">
            <label for="">Nama</label>
            <ui-select ng-model="edit.guru" theme="bootstrap">
                <ui-select-match placeholder="Pilih salah satu PTK">{{$select.selected.nama_lengkap}}
                </ui-select-match>
                <ui-select-choices repeat="item in teacherList" refresh="onInputSearchChange($select.search)" refresh-delay="0">
                    {{item.nama_lengkap}}
                </ui-select-choices>
            </ui-select>
        </div>
        <div class="row">
            <div class="col-xs-8">
                <div class="form-group">
                    <label for="">No. SK</label>
                    <input type="text" class="form-control" ng-model="edit.no_sk">
                </div>
            </div>
            <div class="col-xs-4">
                <div class="form-group">
                    <label for="">Tanggal SK</label>
                    <p class="input-group">
                        <input type="text" class="form-control" uib-datepicker-popup="dd-MM-yyyy" ng-model="edit.tanggal_sk"
                               datepicker-options="dateOptions"
                               close-text="Close" is-open="datepicker" placeholder="dd-mm-yyyy" readonly ng-click="datepicker = !datepicker"/>
                        <span class="input-group-btn">
                            <button type="button" class="btn btn-default" ng-click="datepicker = !datepicker">
                                <i class="glyphicon glyphicon-calendar"></i>
                            </button>
                        </span>
                    </p>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-6">
                <div class="form-group">
                    <label for="">Tahun Mulai</label>
                    <input type="text" class="form-control" ng-model="edit.tahun_mulai" ng-change="onYearStartChange()">
                </div>
            </div>
            <div class="col-xs-6">
                <div class="form-group">
                    <label for="">Tahun Selesai</label>
                    <input type="text" class="form-control" ng-model="edit.tahun_selesai">
                </div>
            </div>
        </div>
    </form>
</div>

<div class="modal-footer">
    <button class="btn btn-primary btn-sm" ng-click="onBtnSaveClicked()">
        <i class="fa fa-check"></i> <span>Simpan</span>
    </button>
    <button class="btn btn-default btn-sm" ng-click="dismiss()">
        <i class="fa fa-times"></i> <span>Batal</span>
    </button>
</div>
