<?php
/**
 * Created by PhpStorm.
 * User: ragasubekti
 * Date: 06/10/17
 * Time: 16.23
 */

// TODO: Code splitting

class Teacher_model extends CI_Model
{

    public function get($id)
    {
        $this->db->select('data_guru.*, jenis_pegawai.nama as jenis_pegawai, data_sekolah.nama as nama_sekolah');
        $this->db->from('data_guru');
        $this->db->join('jenis_pegawai', ' jenis_pegawai.id = data_guru.id_jenis_pegawai', 'left');
        $this->db->join('data_sekolah', 'data_sekolah.id = data_guru.id_sekolah', 'left');

        $this->db->where('data_guru.id', $id);

        $count = clone $this->db;

        if ($count->count_all_results() > 0) {

            $data = $this->db->get()->row();


            $this->db->select('COUNT(*) as data_sekolah');
            $this->db->from('sekolah_guru');
            $this->db->where('id_data_guru', $data->id);

            $data_sekolah = $this->db->get()->row()->data_sekolah;
            $data->data_sekolah = $data_sekolah;

            $this->db->select('COUNT(*) as total_gaji');
            $this->db->from('gaji_berkala_guru');
            $this->db->where('nip', $data->nip_baru);

            $result = $this->db->get()->row();
            $data->gaji_berkala = $result->total_gaji;

            $this->db->select('COUNT(*) as total_jg');
            $this->db->from('pangkat_guru');
            $this->db->where('nip', $data->nip_baru);

            $result = $this->db->get()->row();
            $data->pangkat_guru = $result->total_jg;

            $this->db->select('profil_guru.*, jenis_agama.nama as nama_agama');
            $this->db->from('profil_guru');
            $this->db->join('jenis_agama', 'id_jenis_agama = jenis_agama.id', 'left');

            $this->db->where('id_data_guru', $id);

            $data->profil = $this->db->get()->row();
            $data->rumah = $this->db->get_where('rumah_guru', array('id_data_guru' => $id))->row();
            $data->sertifikasi = $this->db->get_where('sertifikasi_guru', array('id_data_guru' => $id))->row();
            if ($data->sertifikasi) $data->sertifikasi->bidang = $this->db->get_where('jenis_bidang_studi', ['id' => isset($data->sertifikasi->id_bidang_studi) ? $data->sertifikasi->id_bidang_studi : null])->row();
            $status_sertifikasi = $this->db->get_where('sertifikasi_guru', array('id_data_guru' => $id));
            $data->status_sertifikasi = ($status_sertifikasi->num_rows() > 0 && strlen($status_sertifikasi->row()->no_peserta > 0)) ? true : false;

            return $data;

        } else {
            return array();
        }
    }

    public function statsbox()
    {
    }

    public function operatorStatsbox()
    {
    }


    public function getRank($id)
    {
        $data = $this->get($id);
        $this->db->select('pangkat_guru.*, jenis_golongan.gol, jenis_golongan.ruang, jenis_golongan.nama, data_sekolah.nama nama_sekolah, unit_kerja, regencies.name nama_kota');
        $this->db->from('pangkat_guru');
        $this->db->where('nip', $data->nip_baru);
        $this->db->join('jenis_golongan', 'pangkat_guru.id_jenis_golongan = jenis_golongan.id');
        $this->db->join('regencies', 'id_kota = regencies.id', 'left');
        $this->db->join('data_sekolah', 'id_sekolah = data_sekolah.id', 'left');
        $this->db->order_by('tmt', 'DESC');        
        return $this->db->get()->result();
    }

    public function sekolah_tambah($id, $tk, $name, $kota, $kecamatan, $masuk, $lulus, $jurusan)
    {
        $data = array(
            'id_data_guru' => $id,
            'id_jenis_tingkat_pend' => $tk,
            'nama_sekolah' => $name,
            'kec_sekolah' => $kecamatan,
            'kota_sekolah' => $kota,
            'thn_masuk' => $masuk,
            'thn_lulus' => $lulus,
            'jurusan' => $jurusan,
            'status' => 0
        );

        $this->db->insert('sekolah_guru', $data);


        if ($this->db->affected_rows() <= 0) {
            return array('error' => $this->db->error());
        } else {
            return array('success' => true);
        }
    }

    public function editSchool($id, $tk, $name, $kota, $kecamatan, $masuk, $lulus, $jurusan)
    {
        $data = array(
            'id_jenis_tingkat_pend' => $tk,
            'nama_sekolah' => $name,
            'kec_sekolah' => $kecamatan,
            'kota_sekolah' => $kota,
            'thn_masuk' => $masuk,
            'thn_lulus' => $lulus,
            'jurusan' => $jurusan,
            'status' => 0
        );

        $this->db->where('id', $id);

        $this->db->update('sekolah_guru', $data);


        if ($this->db->affected_rows() <= 0 && $this->db->error()['code'] !== 0) {
            return array('error' => $this->db->error());
        } else {
            return array('success' => true);
        }
    }

    public function sekolah_delete($id)
    {
//        if ($this->db->get_where('sekolah'))
        $this->db->where('id', $id);
        $this->db->delete('sekolah_guru');

        return array('success' => true);
    }

    public function activateSchool($id_guru, $id_sekolah)
    {
        $this->db->where('id_data_guru', $id_guru);
        $this->db->update('sekolah_guru', array('status' => 0));

        $this->db->where('id', $id_sekolah);
        $this->db->update('sekolah_guru', array('status' => 1));

        return array('success' => true);
    }

    public function getSalary($id)
    {
        $q = $this->db->get_where('data_guru', array('id' => $id))->row();
        $this->db->select('gaji_berkala_guru.*, jenis_golongan.gol, jenis_golongan.ruang, jenis_golongan.nama as jenis_golongan');
        $this->db->where('nip', $q->nip_baru);
        $this->db->from('gaji_berkala_guru');
        $this->db->join('jenis_golongan', 'id_pangkat_golongan = jenis_golongan.id');
        $this->db->order_by('tmt', 'DESC');
        return $this->db->get()->result();
    }

    public function addSalary($id_guru, $sk, $tmt, $pangkat, $jumlah)
    {
        $dg = $this->db->get_where('data_guru', array('id' => $id_guru))->row();

        if(!(strlen($dg->nip_baru) > 0)) {
            return ['error' => 'NIP Kosong, mohon isi NIP untuk melanjutkan'];
        }

        $insert = array(
            'nip' => $dg->nip_baru,
            'tmt' => Carbon\Carbon::parse($tmt, 'Asia/Jakarta')->addDays(1)->format('Y-m-d'),
            'no_sk' => $sk,
            'jumlah_gaji' => $jumlah,
            'id_pangkat_golongan' => $pangkat->id
        );

        $this->db->insert('gaji_berkala_guru', $insert);


        if ($this->db->affected_rows() <= 0) {
            return array('error' => $this->db->error());
        } else {
            return array('success' => true);
        }
    }

    public function updateSalary($data)
    {
        $ud = array(
            'no_sk' => $data->sk,
            'tmt' => Carbon\Carbon::parse($data->tmt, 'Asia/Jakarta')->addDays(1)->format('Y-m-d'),
            'jumlah_gaji' => $data->gaji,
            'id_pangkat_golongan' => $data->pangkat->id
        );

        $this->db->where('id', $data->id);
        $this->db->update('gaji_berkala_guru', $ud);

        return array('success' => true);
    }

    public function deleteSalary($id)
    {
        $this->db->delete('gaji_berkala_guru', array('id' => $id));

        return array('success' => true);
    }

    public function addRank($id, $sk, $tmt, $sekolah, $pangkat, $area, $unit_kerja, $kota, $type)
    {
        $dg = $this->db->get_where('data_guru', array('id' => $id))->row();

        if(!(strlen($dg->nip_baru) > 0)) {
            return ['error' => 'NIP Kosong, mohon isi NIP untuk melanjutkan'];
        }

        $insert = array(
            'nip' => $dg->nip_baru,
            'no_sk' => $sk,
            'tmt' => Carbon\Carbon::parse($tmt, 'Asia/Jakarta')->addDays(1)->format('Y-m-d'),
            'id_sekolah' => ($sekolah && isset($sekolah->id)) ? $sekolah->id : null,
            'id_jenis_golongan' => $pangkat->id,
            'area' => ($area) ? $area->id : 0,
            'unit_kerja' => ($unit_kerja) ? $unit_kerja : '',
            'type' => isset($type) ? $type : 1,
            'id_kota' => ($kota && isset($kota->id)) ? $kota->id : null
        );

        $this->db->insert('pangkat_guru', $insert);


        if ($this->db->affected_rows() <= 0) {
            return array('error' => $this->db->error());
        } else {
            return array('success' => true);
        }
    }

    public function updateRank($data)
    {
        $update = array(
            'no_sk' => $data->sk,
            'tmt' => Carbon\Carbon::parse($data->tmt, 'Asia/Jakarta')->addDays(1)->format('Y-m-d'),
            'id_sekolah' => ($data->sekolah && isset($data->sekolah->id)) ? $data->sekolah->id : null,
            'id_jenis_golongan' => $data->pangkat->id,
            'area' => $data->area,
            'unit_kerja' => ($data->unit_kerja) ? $data->unit_kerja : '',
            'type' => isset($data->type) ? $data->type : 1,
            'id_kota' => ($data->kota && isset($data->kota->id)) ? $data->kota->id : null
        );

        $this->db->where('id', $data->id);
        $this->db->update('pangkat_guru', $update);

        return array('success' => true);
    }

    public function deleteRank($id)
    {
        $this->db->delete('pangkat_guru', array('id' => $id));

        return array('success' => true);
    }

    public function getSchoolTeacher($id, $start, $count, $query = null, $filter = null)
    {
        $this->db->select('data_guru.*, jenis_pegawai.nama as jenis_pegawai, data_sekolah.nama as nama_sekolah');
        $this->db->from('data_guru');
        $this->db->where('data_guru.id_sekolah', $id);

        if (isset($filter['pns'])) {
            switch ($filter['pns']) {
                case  1:
                    $this->db->where('status_pns', 0);
                    break;
                case 2:
                    $this->db->where('status_pns', 1);

            }
        }

        if (isset($filter['nuptk'])) {
            switch ($filter['nuptk']) {
                case 1:
                    $this->db->where('nuptk IS NULL');
                    break;
                case 2:
                    $this->db->where('nuptk IS NOT NULL');
                    break;
            }
        }

        if (isset($filter['pegawai']['id']) && $filter['pegawai']['id'] > 0) {
            $this->db->where('id_jenis_pegawai', $filter['pegawai']['id']);
        }

        $query = urldecode($query);

        if ($query && $query != 'null' && $query !== null) {
            $this->db->where("(nuptk LIKE '%{$query}%' OR nama_lengkap LIKE '%{$query}%' OR nip_lama LIKE '%{$query}%' OR nip_baru LIKE '%{$query}%')");
        };

        $this->db->join('jenis_pegawai', 'jenis_pegawai.id = data_guru.id_jenis_pegawai', 'left');
        $this->db->join('data_sekolah', 'data_sekolah.id = data_guru.id_sekolah', 'left');

        $total = clone $this->db;

        $this->db->limit($count, $start);
        $this->db->order_by('id', 'asc');

        $data = $this->db->get()->result();

        $q = $this->db->last_query();
        foreach ($data as $key => $row) {

            $this->db->select('COUNT(*) as data_sekolah');
            $this->db->from('sekolah_guru');
            $this->db->where('id_data_guru', $row->id);

            $data_sekolah = $this->db->get()->row()->data_sekolah;
            $data[$key]->data_sekolah = $data_sekolah;

            $this->db->select('COUNT(*) as total_gaji');
            $this->db->from('gaji_berkala_guru');
            $this->db->where('nip', $row->nip_baru);

            $data[$key]->gaji_berkala = $this->db->get()->row()->total_gaji;

            $this->db->select('COUNT(*) as total_jg');
            $this->db->from('pangkat_guru');
            $this->db->where('nip', $row->nip_baru);

            $result = $this->db->get()->row();
            $data[$key]->pangkat_guru = $result->total_jg;
        }

        return array('data' => $data, 'total' => $total->count_all_results(), 'params' => $q);
    }

    public function selectTeacher($query)
    {
        if ($this->session->as > 0) {
            $fq = $this->db->get_where('user', array('id' => $this->session->uid))->row();
            $sq = $this->db->get_where('data_sekolah', array('npsn' => $fq->id_npsn))->row();
        }

        $query = urldecode($query);
        // get _based on uid of user and npsn
        $this->db->select('data_guru.*, jenis_pegawai.nama as jenis_pegawai');
        $this->db->from('data_guru');
        if ($this->session->as > 0) $this->db->where('id_sekolah', $sq->id);
        $this->db->like('nama_lengkap', $query);
        $this->db->join('jenis_pegawai', 'id_jenis_pegawai = jenis_pegawai.id');
        $this->db->limit(10);

        return array('data' => $this->db->get()->result());
    }


    public function api_bstudi_show($query)
    {

    }


    public function api_ptk($npsn)
    {
        $q = $this->db->get_where('data_sekolah', ['npsn' => $npsn]);
        if ($q->num_rows() > 0) {
            $schid = $q->row()->id;
            $ptk = $this->db->get_where('data_guru', ['id_sekolah' => $schid])->num_rows();

            $this->db->select('*');
            $this->db->from('tugas_guru');
            $this->db->join('data_guru', 'id_data_guru = data_guru.id');
            $this->db->where('id_sekolah', $schid);

            $guru = $this->db->get()->num_rows();

            return [
                'ptk' => $ptk,
                'guru' => $guru
            ];

        } else {
            return ['error' => 'Data tidak ditemukan'];
        }
    }

    public function bstudy_get($query)
    {
        $this->db->from('jenis_bidang_studi');
        $this->db->like('nama', urldecode($query));
        $this->db->or_like('kode', urldecode($query));
        $this->db->limit(10);

        return $this->db->get()->result();
    }


    public function jurusan_get($query)
    {
        $this->db->from('data_jurusan');
        $this->db->like('nama', urldecode($query));
        $this->db->limit(10);

        return $this->db->get()->result();
    }

    public function migrate()
    {
        $result = $this->db->get('temp_jurusan')->result();
        $this->db->get_where('data_jurusan', []);
    }

}