<?php
/**
 * Created by PhpStorm.
 * User: ragasub
 * Date: 12/22/17
 * Time: 4:30 PM
 */
?>
<div class="box">
    <div class="box-header with-border">
        <div class="box-title">
            Informasi Sekolah
        </div>
    </div>
    <div class="box-body">
        <div class="row">
            <div class="col-md-8">
                <table class="table table-striped">
                    <tr>
                        <th>NPSN</th>
                        <td>
                            {{detail.npsn}}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            Nama Sekolah
                        </th>
                        <td>
                            {{detail.nama}}
                        </td>
                    </tr>
                    <tr>
                        <th>Tingkat</th>
                        <td>
                            {{ detail.tingkat_sekolah }}
                        </td>
                    </tr>
                    <tr>
                        <th>Kecamatan</th>
                        <td>{{ detail.nama_kecamatan }}</td>
                    </tr>
                    <tr>
                        <th>Desa</th>
                        <td>{{ detail.desa }}</td>
                    </tr>
                    <tr>
                        <th>Alamat</th>
                        <td>{{ detail.alamat }}</td>
                    </tr>
                    <tr>
                        <th>No. Telp</th>
                        <td>{{ detail.notelp }}</td>
                    </tr>
                    <tr>
                        <th>Faximile</th>
                        <td>{{ detail.fax }}</td>
                    </tr>
                    <tr>
                        <th>Email</th>
                        <td>{{ detail.email }}</td>
                    </tr>

                    <tr>
                        <th>Website</th>
                        <td>{{ detail.website }}</td>
                    </tr>

                    <tr>
                        <th>Status</th>
                        <td>{{ status[detail.status] }}</td>
                    </tr>

                    <tr>
                        <th>Geografis</th>
                        <td>
                            {{ geo[detail.geografis] }}
                        </td>
                    </tr>
                    <tr>
                        <th>Kepala Sekolah</th>
                        <td>{{ detail.headmaster }}</td>
                    </tr>
                </table>
            </div>
            <div class="col-md-4">
                <div class="text-center">
                    <img src="<?= base_url('assets/no-image.svg')?>" alt="" width="100%" ng-if="!detail.picture || !detail.picture.length > 0">
                    <img src="<?= base_url('uploads/')?>{{detail.picture}}" alt="" width="100%" ng-if="detail.picture.length > 0">
                </div>
            </div>
        </div>
    </div>
</div>
