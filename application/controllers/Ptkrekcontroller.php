<?php

class PtkRekController extends CI_Controller
{

    public function modalDetail() {
        $this->load->view('ptk/modal/rekening/detail');
    }

    /**
     * Render JSON of PTK Rekening Details
     *
     * @param [type] $id
     * @return void
     */
    public function get($id)
    {
        $this->load->model('PtkRekening');
        header('Content-Type: application/json');

        $rekening = PtkRekening::where('data_guru_id', $id);
        // var_dump($rekening->get());

        if ($rekening->count() > 0) {
            $parse = $rekening->first();
            
            echo json_encode([
                'success' => true,
                'data' => [
                    'nama' => $parse->nama,
                    'jenisBank' => [
                        'nama' => $parse->bank->nama,
                        'id' => $parse->bank->id,
                    ],
                    'cabang' => $parse->cabang,
                    'idPtk' => $parse->data_guru_id,
                    'noRekening' => $parse->no_rekening,
                ],
            ]);
        } else {
            echo json_encode(['error' => 'Data Not Found', 'code' => 404]);
        }
    }

    public function store() {
        $this->load->model('PtkRekening');
        $rekening = new PtkRekening();

        $_post = json_decode(trim(file_get_contents('php://input')));

        echo json_encode($rekening->store($_post));
    }
}
