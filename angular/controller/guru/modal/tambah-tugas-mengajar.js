/*
    Created by Raga Subekti
    at  29/11/17 00.38 by PhpStorm
*/
angular.module('diknasmlg').controller('tambahTugasMengajar', function ($scope, $uibModalInstance, $TugasFactory, $SchoolFactory, $TeacherFactory, Notify, data, id) {

    $scope.dismiss = function () {
        $uibModalInstance.dismiss();
    };

    if (!data) {
        $scope.new = {
            id: id, mapel: {}, tapel: {}, sekolah: {}, jumlah_jam: 0, semester: 1
        };
    } else {
        $scope.new = {
            id: data.id,
            mapel: {
                id: data.id_tugas_mengajar,
                nama: data.mata_pelajaran
            },
            tapel: {
                id: data.id_tapel,
                tapel: data.tapel
            },
            sekolah: {
                id: data.id_sekolah,
                nama: data.nama_sekolah
            },
            jumlah_jam: parseInt(data.jumlah_jam),
            semester: data.semester
        }
    }

    $scope.tapelList = [];
    $scope.mapelList = [];
    $scope.unitKerjaList = [];


    $scope.onTapelChange = function (query) {
        $TugasFactory.getTapel(query, function (reply) {
            $scope.tapelList = reply;
        });
    };

    $scope.onMapelChange = function (query) {
        $TugasFactory.mapelSelect(query, function (reply) {
            $scope.mapelList = reply;
        });
    };

    $scope.onUkChange = function (query) {
        $SchoolFactory.get(query, function (reply) {
            $scope.unitKerjaList = reply;
        });
    };

    $scope.onBtnSaveClicked = function () {
        if(!data) {
        $TeacherFactory.tambahTugasMengajar($scope.new, function (reply) {
            if (reply.error) {
                Notify.error('Kesalahan', reply.error);
            } else {
                Notify.success('Sukses', 'Berhasil menambah data tugas mengajar');
                $uibModalInstance.close();
            }
        })
        } else {
            $TeacherFactory.updateTugasMengajar($scope.new, function(reply) {
                if (reply.error) {
                    Notify.error('Kesalahan', reply.error);
                } else {
                    Notify.success('Sukses', 'Berhasil menambah data tugas mengajar');
                    $uibModalInstance.close();
                }
            })
        }
    };

    $scope.dismiss = function () {
        $uibModalInstance.dismiss();
    };

    function init() {
        $TugasFactory.mapelSelect('', function (reply) {
            $scope.mapelList = reply;
        });

        $TugasFactory.getTapel('', function (reply) {
            $scope.tapelList = reply;
        });

        $SchoolFactory.get('', function (reply) {
            $scope.unitKerjaList = reply;
        });
    }

    init();

});