angular.module('diknasmlg').controller('articleSchoolController', function ($scope, $SchoolFactory, NgTableParams, $uibModal, $q, $DistrictFactory) {

    var apiPath = localStorage.getItem('PATHLOC');

    $scope.bgIndex = [
        'bg-aqua',
        'bg-yellow',
        'bg-green',
        'bg-orange',
        'bg-red'
    ];

    $scope.statsbox = {};

    $scope.educationList = [];
    $scope.kecamatanList = [];

    $scope.search = '';

    $scope.filter = {
        kecamatan: {},
        tingkat: {}
    };

    $scope.onInputSearchChange = function () {
        $scope.schoolTable.reload();
    };
    $scope.onBtnDetailClicked = function(row) {
        $uibModal.open({
            templateUrl: apiPath + 'school/detail',
            controller: 'detailSchoolModal',
            size: 'lg',
            resolve: {
                data: function () {
                    return row;
                }
            }
        })
    };

     $scope.schoolTable = new NgTableParams({}, {
         getData: function (params) {
             var count = params.count();
             var start = count * (params.page() - 1);
             var search = $scope.search;
             var filter = $scope.filter;

             return $q(function (resolve, reject) {
                 $SchoolFactory.show(start, count, search, filter, function (reply) {
                     if (reply.error) return reject(reply.error);
                     params.total(reply.total);
                     return resolve(reply.data);
                 })
             })
         }
     });

    $scope.onFilterKecamatanChange = function() {
        refreshTable();
    };

    $scope.onFilterTingkatChange = function() {
        refreshTable();
    };

    function refreshTable() {
        $scope.schoolTable.reload();
    }

    function init() {
        $SchoolFactory.statsbox(function(reply) {
            $scope.statsbox = reply;
        });

        $DistrictFactory.get(function(reply) {
            $scope.kecamatanList = reply;

            $scope.kecamatanList.unshift({id: 0, nama: 'Semua'});
        });

        $SchoolFactory.type(function(reply) {
            $scope.educationList = reply;

            $scope.educationList.unshift({id: 0, nama: 'Semua'});
        });

    }

    init();
});