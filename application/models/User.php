<?php

/**
 * Created by PhpStorm.
 * User: ragasubekti
 * Date: 1/10/18
 * Time: 6:05 AM
 */
class User extends \Illuminate\Database\Eloquent\Model
{
    protected $table = 'user';
    protected $guarded = [];
    public $timestamps = false;

    public function sekolah()
    {
        $CI =& get_instance();
        $CI->load->model('Sekolah');
        return $this->hasOne('Sekolah', 'npsn', 'id_npsn');
    }
}