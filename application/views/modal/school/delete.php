<?php
/**
 * Created by PhpStorm.
 * User: ragasubekti
 * Date: 08/10/17
 * Time: 12.34
 */
?>
<div class="modal-header">
    <div class="modal-title">
        <i class="fa fa-trash"></i> &nbsp; Hapus Guru
    </div>
</div>
<div class="modal-body">
    Hapus data sekolah berikut?
    <table class="table table-striped table-condensed">
        <tr>
            <th>NPSN</th>
            <td><code>{{detail.npsn}}</code></td>
        </tr>
        <tr>
            <th>Nama Sekolah</th>
            <td>{{detail.nama}}</td>
        </tr>
        <tr>
            <th>Kecamatan</th>
            <td>{{detail.kecamatan}}</td>
        </tr>
    </table>
</div>
<div class="modal-footer">
    <button class="btn btn-danger btn-sm" ng-click="onBtnConfirmClicked()">
        <i class="fa fa-check"></i> Konfirmasi
    </button>
    <button class="btn btn-default btn-sm" ng-click="dismiss()">
        <i class="fa fa-times"></i> Batal
    </button>
</div>
