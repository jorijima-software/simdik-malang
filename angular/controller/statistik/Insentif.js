angular.module('diknasmlg').controller('StatistikInsentif', function($scope, $StatistikFactory, $DistrictFactory, $LevelFactory, $SchoolFactory, $q, NgTableParams, $window) {

    const PATH = localStorage.getItem('PATHLOC')

    $scope.filter = {}

    $scope.InsentifTable = new NgTableParams({}, {
        counts: [10, 100, 1000],
        getData: (params) => {
            let count = params.count()
            let start = count * (params.page() - 1)
            let filter = $scope.filter
                filter.start = start
                filter.count = count
            return $q((resolve, reject) => {
                $StatistikFactory.insentif(filter).then(result => {
                    if(result && !result.error) {
                        params.total(result.total)
                        return resolve(result.data)
                    }
                    return reject(result)
                })
            })
        }
    })

    $scope.onFilter = () => {
        $scope.InsentifTable.reload()
    }


    $scope.onInputSearchChange = search => {
        $SchoolFactory.mini(search).then(reply => {
            $scope.sekolahList = reply
        })
    }

    $scope.onBtnExportClicked = () => {
        $window.open('api/2/statistik/insentif/export?' + jQuery.param($scope.filter))
    }

    function init() {
        $StatistikFactory.pensiunAll().then(result => {
            $scope.all = result.data
        })

        $DistrictFactory.get(reply => {
            $scope.kecamatanMalang = reply
        })

        $LevelFactory.get(reply => {
            $scope.tingkatSekolah = reply
        })

        $SchoolFactory.mini('').then(reply => {
            $scope.sekolahList = reply
        })
    }

    init()
})