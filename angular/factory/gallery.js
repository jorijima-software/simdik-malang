angular.module('diknasmlg').factory('$GalleryFactory', function ($http) {

    var factory = {};
    var apiPath = localStorage.getItem('PATHLOC');

    factory.show = function (start, count, search, callback) {
        $http.get(apiPath + 'api/gallery/show/' + start + '/' + count + '/' + search ).then(
            function (success) {
                if (success.status == 200) {
                    return callback(success.data);
                }
            },
            function (err) {

            }
        );
    };

    factory.delete = function (id, callback) {
        $http.post(apiPath + 'api/gallery/delete/' + id, {}).then(function (reply) {
            if (reply.status == 200) {
                return callback(reply.data);
            } else {
                console.log(err);
            }
        }, function (err) {

        });
    };

    return factory;
});