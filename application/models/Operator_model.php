<?php
/**
 * Created by PhpStorm.
 * User: arsyad09
 * Date: 11/21/17
 * Time: 10:50 PM
 */

class Operator_model extends CI_Model
{

    public function show($start, $count, $search, $filter)
    {
        $this->db->select('
            data_operator.*, 
            data_sekolah.nama as nama_sekolah, 
            data_sekolah.id_jenis_tingkat_sekolah, 
            data_sekolah.npsn, 
            user.verify,
            (SELECT time FROM log_login WHERE username = data_sekolah.npsn AND type = 1 ORDER BY id DESC LIMIT 1) last_login
            ');
        $this->db->from('data_operator');

        if ($search && $search != 'null' && $search !== null) {
            $this->db->like('nama_operator', $search);
            $this->db->or_like('data_sekolah.nama', $search);
            $this->db->or_like('no_hp', $search);
            $this->db->or_like('no_wa', $search);
            $this->db->or_like('npsn', $search);
        }

        if (isset($filter['tingkat']['id']) && $filter['tingkat']['id'] > 0) {
            $this->db->where('id_jenis_tingkat_sekolah', $filter['tingkat']['id']);
        }

        $this->db->join('data_sekolah', 'data_operator.asal_sekolah = data_sekolah.id');
        $this->db->join('user', 'data_sekolah.npsn = user.id_npsn');
        $total = clone $this->db;

        $this->db->limit($count, $start);
        return array('data' => $this->db->get()->result(), 'total' => $total->count_all_results());
    }

    public function delete($id)
    {
        $this->db->where('id', $id);

        $this->db->delete('data_operator');

        return array('success' => true);
    }

    public function operator_insert($data)
    {
        if ($this->session->as == 0) {
            if(!isset($data->asal_sekolah) || !isset($data->asal_sekolah->id)) return ['error' => 'Mohon pilih unit kerja!'];
            if(strlen($data->nama_operator) <= 0) return ['error' => 'Mohon masukkan nama operator'];
            if(strlen($data->no_hp) <= 0) return ['error' => 'Mohon masukkan nomor hp'];
            $q = $this->db->get_where('data_operator', ['asal_sekolah' => $data->asal_sekolah->id]);
            if ($q->num_rows() > 0) {
                return ['error' => 'Operator telah terdaftar di sekolah tersebut!'];
            } else {
                $this->db->insert('data_operator', [
                    'nama_operator' => $data->nama_operator,
                    'asal_sekolah' => $data->asal_sekolah->id,
                    'no_hp' => $data->no_hp,
                    'no_wa' => $data->no_wa
                ]);

                return ['success' => true];
            }
        } else {
            return ['error' => 'Insufficient Privilege'];
        }
    }

    public function operator_update($data)
    {
        if ($this->session->as == 0) {
            if ($this->db->get_where('data_operator', ['id' => $data->id])->num_rows() > 0) {
                $this->db->where('id', $data->id);
                $this->db->update('data_operator', [
                    'nama_operator' => $data->nama_operator,
                    'asal_sekolah' => $data->asal_sekolah->id,
                    'no_hp' => $data->no_hp,
                    'no_wa' => $data->no_wa
                ]);

                return ['success' => true];
            } else {
                return ['error' => 'Data tidak ditemukan'];
            }
        } else {
            return ['error' => 'Insufficient Privilege'];
        }
    }

    public function create($data)
    {
        $fu = $this->db->get_where('data_operator', array('asal_sekolah' => $data->asal_sekolah->id));
        if ($fu->num_rows() > 0) {
            $nu = array(
                'nama_operator' => $data->nama_operator,
                'asal_sekolah' => $data->asal_sekolah->id,
                'no_hp' => $data->no_hp,
                'no_wa' => $data->no_wa
            );

            $this->db->where('asal_sekolah', $data->asal_sekolah->id);
            $this->db->update('data_operator', $nu);

            return array('success' => true);
        } else {
            $nu = array(
                'nama_operator' => $data->nama_operator,
                'asal_sekolah' => $data->asal_sekolah->id,
                'no_hp' => $data->no_hp,
                'no_wa' => $data->no_wa
            );

            $this->db->insert('data_operator', $nu);

            return array('success' => true);
        }
    }

    public function update($data)
    {
        $ud = array(
            'nama_operator' => $data->nama_operator,
            'asal_sekolah' => $data->asal_sekolah,
            'no_hp' => isset($data->no_hp) ? $data->no_hp : null,
            'no_wa' => isset($data->no_wa) ? $data->no_wa : null
        );

        $this->db->where('id', $data->id);
        $this->db->update('data_operator', $ud);

        return array('success' => true);
    }

    public function detail($npsn) {
        return $this->db->query('
            SELECT 
              a.id_npsn npsn, c.no_hp, c.no_wa, b.nama, c.nama_operator, 
              (SELECT time FROM log_login WHERE username = a.id_npsn AND type = 1 ORDER BY id DESC LIMIT 1) last_login
            FROM `user` a
            INNER JOIN `data_sekolah` b ON a.id_npsn = b.npsn 
            INNER JOIN `data_operator` c ON b.id = c.asal_sekolah
            WHERE id_npsn = ' . $npsn
        )->row();
    }

}