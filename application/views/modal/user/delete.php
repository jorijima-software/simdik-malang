<div class="modal-header">
    <div class="modal-title">
        Konfirmasi Hapus
    </div>
</div>

<div class="modal-body">
    Apakah anda yakin ingin menghapus pengguna berikut? 
    <table class="table table-condensed table-striped">
    <tr>
        <th>Username</th>
        <td>{{detail.username}}</td>
    </tr>
    <tr>
        <th>Level</th>
        <td>{{levels[detail.level]}}</td>
    </tr>
    </table>
</div>

<div class="modal-footer">
    <button class="btn btn-danger btn-sm" ng-click="onBtnConfirmClicked()">
        <i class="fa fa-check"></i> <span>Konfirmasi</span>
    </button>

    <button class="btn btn-default btn-sm" ng-click="dismiss()">
        <i class="fa fa-times"></i> <span>Batal</span>
    </button>
</div>