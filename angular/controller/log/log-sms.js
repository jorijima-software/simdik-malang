/*
    Created by Raga Subekti
    at  12/12/17 14.03 by PhpStorm
*/
angular.module('diknasmlg').controller('logSms', function ($scope, NgTableParams, $q, $LogFactory) {

    $scope.credit = 0;

    $scope.smsTable = new NgTableParams({}, {
        getData: function (params) {
            var count = params.count();
            var start = count * (params.page() - 1);
            var filter = {};
            return $q(function (resolve, reject) {
                $LogFactory.sms(start, count, filter).then(function (r) {
                    if (!r.error) {
                        params.total(r.total);
                        $scope.credit = r.last_credit;
                        return resolve(r.data);
                    } else {
                        console.log(r);
                        return reject(r);
                    }
                })
            })
        }
    })

});