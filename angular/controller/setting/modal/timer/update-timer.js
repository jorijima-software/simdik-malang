angular.module('diknasmlg').controller('ModalUpdateSettingTimer', function ($scope,
                                                 $uibModalInstance,
                                                 $TimerFactory,
                                                 Notify,
                                                 data) {
    $scope.timer = {}

    $scope.dismiss = function () {
        $uibModalInstance.dismiss()
    }

    $scope.onBtnSaveClicked = function () {
        $TimerFactory
            .update($scope.timer)
            .then(function(result) {
                if(result && !result.error) {
                    Notify.success('Sukses', 'Berhasil memperbarui timer')
                    $uibModalInstance.close()
                } else {
                    Notify.error('Kesalahan', result.error)
                }
            })
    }

    function init() {
        var parsed = data.endtime.split('-');
        time = new Date(parsed[0], parsed[1] - 1, parsed[2].split(' ')[0]);

        $scope.timer.id = data.id
        $scope.timer.endtime = time
        $scope.timer.message = data.message
    }

    init()
})