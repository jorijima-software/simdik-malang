<?php
/**
 * Created by PhpStorm.
 * User: arsyad09
 * Date: 10/10/17
 * Time: 3:53 PM
 */
die();
$userinfo = $this->User_model->userinfo();
?>
<!doctype html>
<html lang="en" ng-app="diknasmlg">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Dinas Pendidikan Kota Malang</title>
    <link rel="stylesheet" href="<?= base_url('bower_components/bootstrap/dist/css/bootstrap.min.css') ?>">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="<?= base_url('bower_components/font-awesome/css/font-awesome.min.css') ?>">
    <link rel="stylesheet" href="<?= base_url('bower_components/angular-bootstrap/ui-bootstrap-csp.css') ?>">
    <link rel="stylesheet" href="<?= base_url('bower_components/angular-ui-select/dist/select.min.css') ?>">
    <link rel="stylesheet" href="<?= base_url('bower_components/noty/lib/noty.css') ?>">
    <link rel="stylesheet" href="<?= base_url('dist/css/sly-horizontal.css') ?>">
    <link rel="stylesheet" href="<?= base_url('dist/css/AdminLTE.css') ?>">

    <link rel="stylesheet"
          href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
    <link rel="stylesheet"
          href="https://fonts.googleapis.com/css?family=Oswald:300,400,600,700,300italic,400italic,600italic">
    <link href="https://fonts.googleapis.com/css?family=Ubuntu:400,400i,500,500i,700,700i" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Mukta:400,500,600,700,800" rel="stylesheet">
    <link rel="stylesheet" href="<?= base_url('bower_components/highcharts/css/highcharts.css') ?>">
    <link rel="stylesheet" href="<?= base_url('bower_components/highcharts-ng/dist/highcharts-ng.css') ?>">
    <link rel="stylesheet" href="<?= base_url('bower_components/unitegallery/dist/css/unite-gallery.css') ?>">
    <link rel="stylesheet" href="<?= base_url('bower_components/unitegallery/dist/themes/default/ug-theme-default.css') ?>">
    <link rel="stylesheet" href="<?= base_url('bower_components/unitegallery/dist/skins/alexis/alexis.css') ?>">
    <link rel="stylesheet" href="<?= base_url('dist/css/margin.css') ?>">
    <link rel="stylesheet" href="<?= base_url('dist/css/index-article.css') ?>">
</head>
<body>
<?php if($this->session->is_logged) { ?>
    <section class="header-navbar header-display">
        <div class="container">
            <div class="row">
                <div class="col">
                    <a href="<?= site_url('display') ?>" class="logo-container btn-logo-header">
                        <img src="<?= base_url('assets/logo-header.png') ?>">
                    </a>
                    <div class="btn-header-article pull-right">
                        <a href="<?= site_url('display/statistic') ?>" class="btn btn-primary btn-header">
                            <span>Statistik</span>
                        </a>
                        <a href="<?= site_url('display/employee') ?>" class="btn btn-primary btn-header">
                            <span>Pegawai</span>
                        </a>
                        <a href="<?= site_url('display/school') ?>" class="btn btn-primary btn-header">
                            <span>Sekolah</span>
                        </a>
                        <a href="<?= site_url('display/district') ?>" class="btn btn-primary btn-header">
                            <span>Kecamatan</span>
                        </a>
                        <a href="<?= site_url('display/percentage') ?>" class="btn btn-primary btn-header">
                            <span>Prosentase PNS/CPNS</span>
                        </a>
                        <a href="<?= site_url('display/registration') ?>" class="btn btn-primary btn-header">
                            <span>Operator</span>
                        </a>
                        <a href="<?= site_url('display/underconstruction') ?>" class="btn btn-primary btn-header">
                            <span>Galeri</span>
                        </a>
                        <a class="dropdown user user-menu">
                            <a class="btn btn-user" class="dropdown-toggle" data-toggle="dropdown">
                            <span class="hidden-xs"><?php
                                if (is_null($userinfo->nama)) {
                                    echo $userinfo->username;
                                } else {
                                    echo $userinfo->nama;
                                }
                                ?></span>
                                <i class="fa fa-caret-down"></i>
                            </a>
                            <ul class="dropdown-menu dropdown-list " style="margin-right: 90px">
                                <li class="user-footer">
                                    <div class="pull-left">
                                        <a href="<?= site_url('/dashboard') ?>" class="btn btn-primary btn-header">Mode Admin</a>
                                    </div>
                                    <div class="pull-right">
                                        <a href="<?= site_url('/logoutdisplay') ?>" class="btn btn-primary btn-header">Sign out</a>
                                    </div>
                                </li>
                            </ul>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </section>
<?php } else { ?>
    <section class="header-navbar header-display">
        <div class="container">
            <div class="row">
                <div class="col">
                    <a href="<?= site_url('display') ?>" class="logo-container btn-logo-header">
                        <img src="<?= base_url('assets/logo-header.png') ?>">
                    </a>
                    <div class="btn-header-article pull-right">
                        <a href="<?= site_url('display/statistic') ?>" class="btn btn-primary btn-header">
                            <span>Statistik</span>
                        </a>
                        <a href="<?= site_url('display/employee') ?>" class="btn btn-primary btn-header">
                            <span>Pegawai</span>
                        </a>
                        <a href="<?= site_url('display/school') ?>" class="btn btn-primary btn-header">
                            <span>Sekolah</span>
                        </a>
                        <a href="<?= site_url('display/district') ?>" class="btn btn-primary btn-header">
                            <span>Kecamatan</span>
                        </a>
                        <a href="<?= site_url('display/percentage') ?>" class="btn btn-primary btn-header">
                            <span>Prosentase PNS/CPNS</span>
                        </a>
                        <a href="<?= site_url('display/registration') ?>" class="btn btn-primary btn-header">
                            <span>Operator</span>
                        </a>
                        <a href="<?= site_url('display/underconstruction') ?>" class="btn btn-primary btn-header">
                            <span>Galeri</span>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </section>
<?php }?>
<div class="body-background" data-parallax="scroll" data-image-src="<?= base_url('assets/background.png') ?>">
    <section class="content">
        <?php
        require_once $content . '.php';
        ?>
    </section>
</div>
<section class="footer-display">
    <div class="top-footer">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <table class="logo" style="margin-top: 10px;text-align: center">
                        <tr>
                            <td><img src="<?= base_url('assets/kemendikbud.png') ?>" width="70px"
                                     height="70px" style="margin-right: 10px">&nbsp&nbsp
                            </td>
                            <td class="footer-text" style="width: 210px">KEMENTERIAN PENDIDIKAN<br> DAN KEBUDAYAAN</td>

                            <td><img src="<?= base_url('assets/logo.png') ?>" width="70px" height="70px"
                                     style="margin-left: 30px;margin-right: 10px;">&nbsp&nbsp
                            </td>
                            <td class="footer-text" style="width: 110px;">PEMERINTAH<br> KOTA MALANG</td>

                            <td><img src="<?= base_url('assets/logo-jatim.png') ?>" width="60px"
                                     height="70px" style="margin-left: 40px;margin-right: 10px">&nbsp&nbsp
                            </td>
                            <td class="footer-text" style="width: 200px;">PEMERINTAH<br>PROVINSI JAWA
                                TIMUR
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
            <hr style="margin-bottom: 5px">
            <div class="row">
                <div class="col-12">
                    <table style="width: 100%;text-align: center">
                        <tr>
                            <td width="100%">
                                <span class="footer-text">DINAS PENDIDIKAN KOTA MALANG</span>
                            </td>
                        </tr>
                        <tr>
                            <td width="100%">
                                <span class="footer-text" >Jl. Veteran No.19, Ketawanggede, Kec.Lowokwaru, Jawa Timur 65145, Indonesia</span>
                            </td>
                        </tr>
                        <tr>
                            <td width="100%">
                                <span class="footer-text">Telp. (0341) 551333; E-mail: disdik@malangkota.go.id</span>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <div class="bottom-footer"></div>
</section>

<script src="<?= base_url('bower_components/jquery/dist/jquery.min.js') ?>"></script>
<script src="<?= base_url('bower_components/bootstrap/dist/js/bootstrap.min.js') ?>"></script>
<script src="<?= base_url('dist/js/adminlte.min.js') ?>"></script>
<script src="<?= base_url('bower_components/noty/lib/noty.min.js') ?>"></script>
<script src="<?= base_url('bower_components/angular/angular.js') ?>"></script>
<script src="<?= base_url('bower_components/ng-table/bundles/ng-table.min.js') ?>"></script>
<script src="<?= base_url('bower_components/angular-bootstrap/ui-bootstrap-tpls.min.js') ?>"></script>
<script src="<?= base_url('bower_components/angular-sanitize/angular-sanitize.min.js') ?>"></script>
<script src="<?= base_url('bower_components/angular-ui-select/dist/select.min.js') ?>"></script>
<script src="<?= base_url('bower_components/highcharts/highcharts.js') ?>"></script>
<script src="<?= base_url('bower_components/highcharts/highcharts-3d.js') ?>"></script>
<script src="<?= base_url('bower_components/highcharts/modules/exporting.js') ?>"></script>
<script src="<?= base_url('bower_components/highcharts-ng/dist/highcharts-ng.min.js') ?>"></script>
<script src="<?= base_url('bower_components/angular-google-chart/ng-google-chart.min.js') ?>"></script>
<script src="<?= base_url('bower_components/ng-currency/dist/ng-currency.js') ?>"></script>
<script src="<?= base_url('bower_components/angular-summernote/src/angular-summernote.js') ?>"></script>
<script src="<?= base_url('bower_components/angular-route/angular-route.js') ?>"></script>
<script src="<?= base_url('bower_components/angular-animate/angular-animate.js') ?>"></script>

<script src="<?= base_url('bower_components/unitegallery/dist/js/unitegallery.js') ?>"></script>
<script src="<?= base_url('bower_components/unitegallery/dist/themes/tiles/ug-theme-tiles.js') ?>"></script>

<script src="<?= base_url('bower_components/angular-ui-select/dist/select.min.js') ?>"></script>
<script src="<?= base_url('bower_components/sly/dist/sly.min.js') ?>"></script>


<script src="<?= base_url('angular/app.js') ?>"></script>
<script src="<?= base_url('angular/routes.js') ?>"></script>

<script src="<?= base_url('angular/factory/teacher.js') ?>"></script>
<script src="<?= base_url('angular/factory/employee.js') ?>"></script>
<script src="<?= base_url('angular/factory/school.js') ?>"></script>
<script src="<?= base_url('angular/factory/district.js') ?>"></script>
<script src="<?= base_url('angular/factory/operator.js') ?>"></script>
<script src="<?= base_url('angular/factory/user.js') ?>"></script>
<script src="<?= base_url('angular/factory/gallery.js') ?>"></script>
<script src="<?= base_url('angular/factory/ptk.js') ?>"></script>

<script src="<?= base_url('angular/services/notify.js') ?>"></script>
<script src="<?= base_url('angular/controller/articleEmployeeController.js') ?>"></script>
<script src="<?= base_url('angular/controller/articleSchoolController.js') ?>"></script>
<script src="<?= base_url('angular/controller/articleDistrictController.js') ?>"></script>
<script src="<?= base_url('angular/controller/articleStatisticController.js') ?>"></script>
<script src="<?= base_url('angular/controller/articlePercentageController.js') ?>"></script>
<script src="<?= base_url('angular/controller/articleGalleryController.js') ?>"></script>
<script src="<?= base_url('angular/controller/articleManageGalleryController.js') ?>"></script>
<script src="<?= base_url('angular/controller/registerOpController.js') ?>"></script>
<script src="<?= base_url('angular/controller/modal/addOperatorModal.js') ?>"></script>
<script src="<?= base_url('angular/controller/sekolah/modal/detail-sekolah.js') ?>"></script>
<script src="<?= base_url('angular/controller/modal/detailTeacherModal.js') ?>"></script>
<script src="<?= base_url('angular/controller/modal/detailDistrictModal.js') ?>"></script>
<script src="<?= base_url('angular/controller/modal/gallery/uploadPhotoModal.js') ?>"></script>
<script src="<?= base_url('angular/controller/modal/gallery/deleteGalleryModal.js') ?>"></script>
<script src="<?= base_url('angular/controller/ptk/modal/detail-ptk.js') ?>"></script>
<script src="<?= base_url('dist/js/horizontal.js') ?>"></script>
<script src="<?= base_url('dist/js/parallax.js') ?>"></script>
<script>
    $(window).scroll(function () {
        var header = $('.header-navbar'),
            scroll = $(window).scrollTop();

        if (scroll >= 1) {
            header.addClass('fix-navbar').removeClass('header-display');
        }
        else {
            header.addClass('header-display').removeClass('fix-navbar');
        }

    });
    jQuery(document).ready(function(){
        jQuery("#gallery").unitegallery();
    });

    jQuery("#gallery").unitegallery({
        tiles_type:"nested"
    });


</script>

<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-111581217-1"></script>
<script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());

    gtag('config', 'UA-111581217-1');
</script>
</body>
</html>