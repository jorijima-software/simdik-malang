<?php
/**
 * Created by PhpStorm.
 * User: ragasubekti
 * Date: 17/11/17
 * Time: 22.44
 */

class Tugas extends CI_Controller {

    public function modalSettingTambah() {
        $this->load->view('modal/setting/tugas/tambah');
    }

    // api related

    public function show($start, $count) {
        header('Content-Type: application/json');

        $this->load->model('Tugas_model');

        echo json_encode($this->Tugas_model->show($start, $count));
    }


    public function add() {
        header('Content-Type: application/json');

        $data = json_decode(trim(file_get_contents('php://input')));

        $name = $data->name;

        $this->load->model('Tugas_model');

        echo json_encode($this->Tugas_model->add($name));
    }

    // public function tambahKepalaSekolah() {
    //     $this->load->model('TugasTambahan');
    //     $_post =
    // }
}