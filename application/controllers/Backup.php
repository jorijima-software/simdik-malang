<?php
/**
 * Created by PhpStorm.
 * User: ragasubekti
 * Date: 02/12/17
 * Time: 14.25
 */

class Backup extends CI_Controller {

    public function cron_all_tables() {
        $this->load->model('Backup_model');
        echo $this->Backup_model->all_tables();
    }

    public function page_index() {
        $this->load->view('layout', ['content' => 'backup/index', 'js' => [
            ''
        ]]);
    }
}