<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>PRINT GAJI</title>
    <link rel="stylesheet" href="<?=base_url('/bower_components/bootstrap/dist/css/bootstrap.min.css')?>">
    <!-- <link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet"> -->
    <style>
        table, tr, td, span, b, p , strong, u, i, * {
            font-size: 12pt;
            /* font-family: 'Open Sans'; */
        }
    </style>
</head>

<body>
    <table width="100%;" style="margin-top: -40px">
        <tr>
            <td width="20%" style="vertical-align: middle">
                <img src="https://gugus1blimbing.files.wordpress.com/2012/09/kota-malang-outline.jpg" width="105px">
            </td>
            <td style="text-align: center; vertical-align: middle">
                <span style="font-size: 16pt; font-weight: 800">PEMERINTAH KOTA MALANG</span>
                <br/>
                <span style="font-size: 16pt; font-weight: 800">DINAS PENDIDIKAN KOTA MALANG</span>
                <br/>
                <span>Jl. Veteran No. 19 Telp. 551333 - 560946 - 584499 Malang Kode Pos 65119</span>
                <span style="font-size: 8.5pt">Website: http://diknas.malangkota.go.id/ Email: disdik_mlg@yahoo.co.id MALANG</span>
            </td>
        </tr>
    </table>
    <div style="margin-top: -12.5px">
        <div style="border-bottom: 1px solid #000; margin-bottom: 1px">&nbsp;</div>
        <div style="border-top: 3px solid #000; padding-top: 1px">&nbsp;</div>
    </div>
    <table width="100%" style="font-size: 10pt; margin-top: -10px">
        <tr>
            <td></td>
            <td></td>
            <td>Malang, <?=$print_date?></td>
        </tr>
        <!-- <tr>
            <td colspan="3">&nbsp;</td>
        </tr> -->
        <tr>
            <td width="10%">Nomor</td>
            <td width="50%">: <?=$nomor?></td>
            <td>Kepada</td>
        </tr>
        <tr>
            <td>Sifat</td>
            <td>: Penting</td>
            <td>Yth. Kepala Badan Pengelola Keuangan</td>
        </tr>
        <tr>
            <td>Lampiran</td>
            <td>: -</td>
            <td>Dan Aset Daerah Kota Malang</td>
        </tr>
        <tr>
            <td>Perihal</td>
            <td>: Kenaikan Gaji Berkala</td>
            <td>di Malang</td>
        </tr>
        <tr>
            <td></td>
            <td>
                &nbsp; Sdr. <?=$name?>
            </td>
            <td></td>
        </tr>
        <tr>
            <td colspan="3">&nbsp;</td>
        </tr>
        <tr>
            <td>
                <td colspan="2" style="padding-left: 7px">
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Dengan ini diberitahukan bahwa berhubung dengan dipenuhinya
                    masa kerja dan syarat-syarat lainnya kepada:
                </td>
        </tr>
        <tr>
            <td rowspan="24"></td>
            <td colspan="2" style="padding-left: 7px">
                <table width="100%">
                    <tr>
                        <td width="40%" style="padding-top: 10px">1. N a m a</td>
                        <td>: <b><?=$name?></b></td>
                    </tr>
                    <tr>
                        <td>2. NIP Lama / NIP Baru</td>
                        <td>: <?=$nip?></td>
                    </tr>
                    <tr>
                        <td>3. Tempat / Tgl. Lahir</td>
                        <td>: <?=$birthplace?>, <?=$birthdate?></td>
                    </tr>
                    <tr>
                        <td>4. Pangkat / Jabatan</td>
                        <td>: <?=$pangkat?> / <?=$jabatan?></td>
                    </tr>
                    <tr>
                        <td>5. Unit Kerja</td>
                        <td>: <?=$unit_kerja?></td>
                    </tr>
                    <tr>
                        <td>6. Instansi</td>
                        <td>: Dinas Pendidikan Kota Malang</td>
                    </tr>
                    <tr>
                        <td>7. Gaji Pokok Lama</td>
                        <td>: Rp. &nbsp;
                            <b><?=$gaji_lama?></b>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" style="padding-left: 15px">
                            <b>atas dasar Surat Keputusan terakhir tentang gaji / pangkat yang ditetapkan :</b>
                        </td>
                    </tr>
                    <tr>
                        <td style="padding-left: 15px">
                            a. Oleh Pejabat
                        </td>
                        <td>: <?=$oleh?></td>
                    </tr>
                    <tr>
                        <td style="padding-left: 15px">b. Tanggal</td>
                        <td>:  <?=$tanggal_tmt_last?></td>
                    </tr>
                    <tr>
                        <td style="padding-left: 15px">&nbsp;&nbsp;&nbsp;&nbsp;Nomor</td>
                        <td>: <?=$nosk_last?></td>
                    </tr>
                    <tr>
                        <td style="padding-left: 15px" width="40%">c. Tanggal mulai berlakunya gaji
                            <br/>&nbsp;&nbsp;&nbsp;&nbsp;tersebut</td>
                        <td style="vertical-align: middle">: <?=$tmt_last?></td>
                    </tr>
                    <tr>
                        <td style="padding-left: 15px" width="40%">d. Masa Kerja Golongan pada
                            <br>&nbsp;&nbsp;&nbsp;&nbsp;tanggal tersebut</td>
                        <td style="vertical-align: middle">: <?=$masa_kerja_last?></td>
                    </tr>
                    <tr>
                        <td colspan="2" style="padding-left: 15px">
                            <b>
                                <u>Diberikan kenaikan gaji berkala hingga memperoleh :</u>
                            </b>
                        </td>
                    </tr>
                    <tr>
                        <td style="padding-top: 10px">8. Gaji pokok baru</td>
                        <td>: Rp.
                            <b><?=$gaji_pokok_baru?></b>
                            <br>
                            <span style="font-size: 10pt; padding-left: 10px"><?=$gaji_pokok_teks?> Rupiah</span>
                        </td>
                    </tr>
                    <tr>
                        <td>9. Berdasarkan Masa Kerja</td>
                        <td>: <?=$masa_kerja?>
                    </tr>
                    <tr>
                        <td>10. Dalam Golongan ruang</td>
                        <td>:
                            <b><?=$golongan?></b>
                        </td>
                    </tr>
                    <tr>
                        <td>11. Terhitung mulai tanggal</td>
                        <td>:
                            <b><?=$tmt?></b>
                        </td>
                    </tr>
                    <tr>
                        <td>12. Keterangan</td>
                        <td>: Pegawai Negeri Sipil Daerah</td>
                    </tr>
                    <tr>
                        <td colspan="2" style="padding-top: 20px" style="text-align: justify">
                            <div style="text-align: justify">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Diharapkan agar sesuai dengan
                            Peraturan Pemerintah Nomor 7 Tahun 1977 sebagaimana telah diubah ketujuhbelas kalinya dengan
                            Peraturan Pemerintah No. 30 Tahun 2015, kepada Pegawai Negeri Sipil tersebut dapat dibayarkan
                            berdasarkan gaji pokok yang baru.</div>
                        </td>
                    </tr>
                </table>
                </td>
        </tr>
    </table>
    <table width="100%" style="margin-top: -74px">
        <tr>
            <td width="65%"></td>
            <td>KEPALA DINAS PENDIDIKAN</td>
        </tr>
        <tr>
            <td height="60px" colspan="2">&nbsp;</td>
        </tr>
        <tr>
            <td></td>
            <td><b><u>Dra. ZUBAIDAH, MM</u></b></td>
        </tr>
        <tr>
            <td></td>
            <td>Pembina Utama Muda</td>
        </tr>
        <tr>
            <td></td>
            <td>NIP: 196012131984032002</td>
        </tr>
        <tr>
            <td style="font-size: 11pt">Tembusan Kepada Yth:</t>
            <td></td>
        </tr>
        <tr>
            <td style="font-size: 11pt">
                1. Kepala Badan Kepegawaian Daerah Kota Malang <br/>
                2. Kepala Sub. Bag. Keuangan Dinas Pendidikan Kota Malang <br/>
                3. Yang Bersangkutan
            </td>
            <td style="vertical-align: top"></td>
        </tr>
    </table>
    <!-- </div> -->
</body>
</html>
