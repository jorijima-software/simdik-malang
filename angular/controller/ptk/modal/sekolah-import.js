angular.module('diknasmlg').controller('sekolah.ptk.import', function (
    $scope, 
    FileUploader, 
    $uibModalInstance, 
    Notify,
    $location,
    $routeParams
) {
    var apiPath = localStorage.getItem('PATHLOC');
    
    $scope.routes = $routeParams;

    $scope.id = ($scope.routes.id) ? $scope.routes.id : '';

    var uploader = $scope.uploader = new FileUploader({
        url: apiPath + 'api/xls/ptk/sekolah/import/' + $scope.id,
        queueLimit: 1
    });

    uploader.onCompleteAll = function() {
        Notify.success('Sukses', 'Berhasil mengupload file, mohon lakukan verifikasi data');
        $uibModalInstance.close();
        $location.path('ptk/import/verify-data/' + $scope.id)
    };

})