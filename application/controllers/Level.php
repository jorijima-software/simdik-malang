<?php
/**
 * Created by PhpStorm.
 * User: ragasubekti
 * Date: 08/10/17
 * Time: 11.20
 */
class Level extends CI_Controller {

    public function get() {
        $this->load->model('Level_model');
        header('Content-Type: application/json');

        echo json_encode($this->Level_model->get());
    }

}