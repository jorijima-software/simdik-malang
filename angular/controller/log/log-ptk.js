angular.module('diknasmlg').controller('logPtk', function($LogFactory, $scope, NgTableParams, $q) {

    $scope.search = '';

    $scope.logTable = new NgTableParams({}, {
        getData: function(params) {
            var count = params.count();
            var start = count * (params.page() - 1);
            var search = $scope.search;
            return $q(function(resolve, reject) {
                $LogFactory.ptk(start, count, {search: search}).then(function(r) {
                    params.total(r.total);
                    return resolve(r.data);
                });
            });
        }
    });

});