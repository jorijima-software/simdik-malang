/*
    Created by Raga Subekti
    at  17/12/17 23.42 by PhpStorm
*/

angular.module('diknasmlg').controller('rombelDetailController', function ($scope, $q, NgTableParams, $RombelFactory, $routeParams, $SchoolFactory, $TugasFactory) {
    $scope.routes = $routeParams;

    $scope.tapelList = [];

    $scope.tapel = {};
    $scope.detail = {};

    var apiPath = localStorage.getItem('PATHLOC');

    $scope.onBtnDeleteClicked = function (row) {
        Notify.confirm('Apakah anda yakin ingin menghapus rombel ini?', function () {
            $RombelFactory.delete(row.rombel).then(function (reply) {
                if (reply.error) {
                    Notify.error('Kesalahan', reply.error);
                } else {
                    Notify.success('Sukses', 'Data rombel berhasil dihapus');
                }
                $scope.rombelTable.reload();
                reloadStats();
            });
        }, function () {});
    };

    $scope.rombelTable = new NgTableParams({
        group: 'tingkat',
        sorting: { tingkat: "ASC" }
    }, {
        counts: [],
        getData: function (params) {
            var tapel = ($scope.tapel && $scope.tapel.active && $scope.tapel.active.id) ? $scope.tapel.active.id : 0;
            var id = $scope.routes.id;
            return $q(function (resolve, reject) {
                $RombelFactory.get(tapel, id).then(function (reply) {
                    if (!reply.error) {
                        // params.total(reply.total);
                        return resolve(reply.data);
                    }
                    return reject(reply.error);
                });
            });
        }
    });

    $scope.onTapelSelect = function() {
        $scope.rombelTable.reload();
        console.log($scope.tapel)
    };

    function reloadStats() {
        $RombelFactory.stats().then(function (reply) {
            $scope.statsbox = reply;
        })
    }

    function init() {
        $RombelFactory.stats().then(function (reply) {
            $scope.statsbox = reply;

        });

        $SchoolFactory.detail($scope.routes.id).then(function(r) {
            $scope.detail = r;
        });

        $TugasFactory.getTapel('', function (reply) {
            $scope.tapelList = reply;

            reply.map(function (data) {
                if(data.active == 1) {
                    $scope.tapel.active = data;
                }
            });

            $scope.rombelTable.reload();
        });

        $scope.rombelTable.reload();

    }

    init();
});