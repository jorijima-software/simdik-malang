angular.module('diknasmlg').factory('$RiwayatFactory', function(
    $http,
    $q
) {
    var PATH = localStorage.getItem('PATHLOC')

    var factory = {}

    factory.hapusKerja = function(id) {
        return $q(function(resolve, reject) {
            $http.post(PATH + 'api/ptk/riwayat/kerja/hapus/' + id).then(function(response) {
                if(response.status === 200) {
                    return resolve(response.data)
                } else {
                    return reject(response)
                }
            })
        })
    }

    factory.editKerja = function(id, riwayat) {
        return $q(function (resolve, reject) {
            $http.post(PATH + 'api/ptk/riwayat/kerja/edit/' + id, riwayat).then(function(response) {
                if(response.status === 200) {
                    return resolve(response.data)
                } else {
                    return reject(response)
                }
            })
        })
    }

    factory.tambahKerja = function (id, data) {
        return $q(function (resolve, reject) {
            $http.post(PATH + 'api/ptk/riwayat/kerja/tambah/' + id, data).then(function (response) {
                if(response.status === 200) {
                    return resolve(response.data)
                } else {
                    return reject(response)
                }
            })
        })
    }

    factory.showKerja = function (id, start, count, filter) {
        return $q(function (resolve, reject) {
            $http.get(PATH + 'api/ptk/riwayat/kerja/' + id + '/' + start + '/' + count + '?' + jQuery.param(filter)).then(function (response) {
                if(response.status === 200) {
                    return resolve(response.data)
                } else {
                    return reject(response)
                }
            })
        })
    }

    return factory
})