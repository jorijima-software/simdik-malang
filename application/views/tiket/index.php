<?php
/**
 * Created by PhpStorm.
 * User: ragasubekti
 * Date: 12/12/17
 * Time: 12.11
 */
?>
<div ng-controller="tiketController">
<section class="content-header">
    <h1>Tiket</h1>
</section>

<section class="content">
    <div class="box box-warning">
        <div class="box-header">
            <div class="box-title">Daftar Tiket</div>
        </div>
        <div class="box-body">
            <div class="row">
                <div class="col-md-8" style="margin-bottom: 10px">
                    <a href="<?=site_url('tiket')?>/#!/tiket/tambah" class="btn btn-primary btn-sm">
                        <i class="fa fa-plus"></i> <span>Buat Tiket</span>
                    </a>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="table-responsive">
                        <table class="table table-striped table-condensed table-hover" ng-table="tiketTable">
                            <thead>
                            <tr>
                                <th>ID Tiket</th>
                                <th>Waktu</th>
                                <th>Judul</th>
                                <th>Group Data</th>
                                <th>Status</th>
                                <th></th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr ng-repeat="row in $data">
                                <td>#{{row.id_tiket}}</td>
                                <td>{{row.date}}</td>
                                <td>{{row.nama}}</td>
                                <td>{{groupData[row.group_data]}}</td>
                                <td>
                                    <span class="label label-danger" ng-if="row.status == 0">
                                        <i class="fa fa-times-circle"></i> <span>CLOSED</span>
                                    </span>
                                    <span class="label label-info" ng-if="row.status == 2">
                                        <i class="fa fa-check-circle-o"></i> <span>ANSWERED</span>
                                    </span>
                                    <span class="label label-success" ng-if="row.status == 1">
                                        <i class="fa fa-check-circle"></i> <span>OPEN</span>
                                    </span>
                                </td>
                                <td>
                                    <div class="pull-right">
                                    <button class="btn btn-info btn-xs">
                                        Response
                                    </button>
                                    <button class="btn btn-primary btn-xs">Edit</button>
                                    <button class="btn btn-danger btn-xs" ng-click="onBtnDeleteClicked(row)">Delete</button>
                                    </div>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
</div>