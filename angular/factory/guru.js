angular.module('diknasmlg').factory('$GuruFactory', function ($http, $q) {
    var factory = {};

    var apiPath = localStorage.getItem('PATHLOC');

    factory.insert = function (data) {
        return $q(function (resolve, reject) {
            $http.post(apiPath + 'api/guru/insert', {
                id_guru: data.guru.id
            }).then(function (reply) {
                if (reply.status == 200) {
                    return resolve(reply.data);
                } else {
                    return reject(reply);
                }
            }, function (err) {
                return reject(err);
            });
        });
    };

    factory.update = function (data) {
        return $q(function (resolve, reject) {
            $http.post(apiPath + 'api/guru/update', {
                id: data.id,
                id_guru: data.guru.id
            }).then(function (reply) {
                if (reply.status == 200) {
                    return resolve(reply.data);
                } else {
                    return reject(reply);
                }
            }, function (err) {
                return resolve(err);
            });
        });
    };

    factory.delete = function (id) {
        return $q(function (resolve, reject) {
            $http.post(apiPath + 'api/guru/delete/' + id, {}).then(function (reply) {
                if (reply.status === 200) {
                    return resolve(reply.data)
                } else {
                    return reject(reply);
                }
            })
        })
    };

    factory.show = function (start, count, filter) {
        return $q(function (resolve, reject) {
            $http.get(apiPath + 'api/guru/' + start + '/' + count + '?' + jQuery.param(filter)).then(function (reply) {
                if (reply.status == 200) {
                    return resolve(reply.data)
                } else {
                    return reject(reply);
                }
            }, function (err) {
                return reject(err);
            })
        })
    };

    factory.stats = function () {
        return $q(function (resolve, reject) {
            $http.get(apiPath + 'api/guru/stats').then(function (reply) {
                if (reply.status == 200) {
                    return resolve(reply.data)
                } else {
                    return reject(reply)
                }
            }, function (err) {
                return reject(err);
            })
        })
    };


    return factory;
});