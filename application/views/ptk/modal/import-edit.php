<div class="modal-header">
    Edit PTK
</div>
<div class="modal-body">
    <uib-tabset>
        <uib-tab heading="Kepegawaian">
            <div class="form-group">
                <label>NUPTK</label>
                <input type="text" class="form-control" ng-model="edit.nuptk">
            </div>
            <div class="form-group">
                <label>Nama Lengkap</label>
                <input type="text" class="form-control" ng-model="edit.nama_lengkap">
            </div>
            <div class="form-group">
                <label>NIP</label>
                <input type="text" class="form-control" ng-model="edit.nip">
            </div>
            <div class="row">
                <div class="col-sm-6">
                    <div class="form-group">
                        <label>Gelar Depan</label>
                        <input type="text" class="form-control" ng-model="edit.gelar_depan">
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-group">
                        <label>Gelar Belakang</label>
                        <input type="text" class="form-control" ng-model="edit.gelar_belakang">
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label>Jenis Pegawai</label>
                <ui-select ng-model="edit.jenis_pegawai" theme="bootstrap" search-enabled="false">
                    <ui-select-match placeholder="Pilih Jenis Pegawai">{{$select.selected.nama}}
                    </ui-select-match>
                    <ui-select-choices repeat="item in jenisPegawaiList track by $index">
                        {{item.nama}}
                    </ui-select-choices>
                </ui-select>
            </div>
        </uib-tab>
        <uib-tab heading="Profil">
            <div class="form-group">
                <label>Tempat Lahir</label>
                <input type="text" class="form-control" ng-model="edit.tempat_lahir">
            </div>
            <div class="form-group">
                <label>Tanggal Lahir</label>
                <p class="input-group">
                    <input  type="text" class="form-control" uib-datepicker-popup="dd-MM-yyyy"
                            ng-model="edit.tanggal_lahir"
                            datepicker-options="dateOptions"
                            close-text="Close" is-open="datepicker" placeholder="dd-mm-yyyy"
                            ng-click="datepicker = !datepicker"/>
                    <span class="input-group-btn">
                        <button type="button" class="btn btn-default" ng-click="datepicker = !datepicker">
                            <i class="glyphicon glyphicon-calendar"></i>
                        </button>
                    </span>
                </p>
            </div>
            <div class="form-group">
                <label>Agama</label>
                <ui-select ng-model="edit.nama_agama" theme="bootstrap" search-enabled="false">
                    <ui-select-match placeholder="Pilih Agama">{{$select.selected.nama}}
                    </ui-select-match>
                    <ui-select-choices repeat="item in religionList track by $index">
                        {{item.nama}}
                    </ui-select-choices>
                </ui-select>
            </div>
            <div class="form-group">
                <label>NIK</label>
                <input type="text" class="form-control" ng-model="edit.nik">
            </div>
            <div class="form-group">
                <label>Nama Ibu</label>
                <input type="text" class="form-control" ng-model="edit.nama_ibu">
            </div>
        </uib-tab>
        <uib-tab heading="Rumah">
            <div class="form-group">
                <label>Alamat</label>
                <textarea ng-model="edit.alamat" class="form-control"></textarea>
            </div>
            <div class="form-group">
                <label>No. Telp</label>
                <input type="text" class="form-control" ng-model="edit.no_telp">
            </div>
        </uib-tab>
    </uib-tabset>
</div>
<div class="modal-footer">
    <button class="btn btn-primary btn-sm" ng-click="onBtnSaveClicked()" ng-disabled='loading'>
        <span class="fa fa-circle-o-notch fa-spin" ng-show='loading'>&nbsp;</span>Simpan
    </button>
    <button class="btn btn-default btn-sm" ng-click="dismiss()">Tutup</button>
</div>