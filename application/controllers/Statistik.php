<?php
/**
 * Copyright 2018, Raga Subekti (ragasubekti@outlook.com)
 * All code below is not publicly available
 */

set_time_limit(120);

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

class Statistik extends CI_Controller {
    public function pageInsentif() {
        $this->load->view('statistik/insentif.html');
    }

    public function pageGuru() {
        $this->load->view('statistik/guru.html');
    }

    public function pagePensiun() {
        $this->load->view('statistik/pensiun.html');
    }

    public function pageDataMasuk() {
        $this->load->view('statistik/data-masuk.html');
    }

    public function exportInsentif() {
        $this->load->model('Ptk');
        $this->load->model('RiwayatKerjaPtk');
        $this->load->model('RiwayatPangkatPtk');

        $sekolah = $this->input->get('sekolah');
        $kecamatan = $this->input->get('kecamatan');
        $tingkat = $this->input->get('tingkat');

        $_ptk = new Ptk;

        $spreadsheet = new Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();

        $sheet
            ->setCellValue('A1', 'No')
            ->setCellValue('B1', 'NUPTK')
            ->setCellValue('C1', 'Nama')
            ->setCellValue('D1', 'Jenis Pegawai')
            ->setCellValue('E1', 'Unit Kerja')
            ->setCellValue('F1', 'Usia')
            ->setCellValue('G1', 'TMT')
            ->setCellValue('H1', 'Bank')
            ->setCellValue('I1', 'No Rekening')
            ->setCellValue('J1', 'Cabang')
            ->setCellValue('K1', 'Masa Kerja')
        ;

        
        $ptk = Ptk::where('status_pns', 0)
            ->when($sekolah['id'], function($q) use ($sekolah) {
                $q->where('id_sekolah', $sekolah['id']);
            })
            ->when($kecamatan['id'], function($q) use ($kecamatan) {
                $q->whereHas('unitKerja', function($qw) use ($kecamatan) {
                   $qw->where('id_kecamatan', $kecamatan['id']); 
                });
            })
            ->when($tingkat['id'], function($q) use ($tingkat) {
                $q->whereHas('unitKerja', function($qw) use ($tingkat) {
                    $qw->where('id_jenis_tingkat_sekolah', $tingkat['id']);
                });
            })
            ->whereHas('unitKerja', function($q) {
                $q->where(function($qw) {
                    $qw->whereIn('id_jenis_tingkat_sekolah', [2,3])->where('status', 1);
                })->orWhere(function($qw) {
                    $qw->whereNotIn('id_jenis_tingkat_sekolah', [2,3]);
                });
            });

        foreach($ptk->orderBy('id_sekolah', 'ASC')->get() as $key => $row) {
            $no = $key + 1;
            $ix = $key + 2;

            $lahir_pns = substr($row->nip_baru, 0, 8);
            
            $tmt = ($row->status_pns != 1)
                ? RiwayatKerjaPtk::where('id_data_guru', $row->id)->orderBy('tmt', 'asc')->first()
                : RiwayatPangkatPtk::where('nip', $row->nip_baru)->orderBy('tmt', 'asc')->first();

            $sheet
                ->setCellValue('A'.$ix, $no)
                ->setCellValue('B'.$ix, $row->nuptk)
                ->setCellValue('C'.$ix, $row->nama_lengkap)
                ->setCellValue('D'.$ix, $row->jenisPegawai->nama)
                ->setCellValue('E'.$ix, $row->unitKerja->nama)
                ->setCellValue('F'.$ix, ($row->profil)
                ? (($row->status_pns == 1 && strlen($row->nip_baru) > 8)
                    ? Carbon\Carbon::parse(substr($lahir_pns, 0, 4) . '-' . substr($lahir_pns, 4, 2) . '-' . substr($lahir_pns, 6, 2))->diff(Carbon\Carbon::now())->format('%y')
                    : Carbon\Carbon::parse($row->profil->tgl_lahir)->diff(Carbon\Carbon::now())->format('%y'))
                : (($row->status_pns == 1 && strlen($row->nip_baru)) ? Carbon\Carbon::parse(substr($lahir_pns, 0, 4) . '-' . substr($lahir_pns, 4, 2) . '-' . substr($lahir_pns, 6, 2))->diff(Carbon\Carbon::now())->format('%y') : '0'))
                ->setCellValue('G'.$ix, $tmt ? Carbon\Carbon::parse($tmt->tmt)->format('d-m-Y') : '')
                ->setCellValue('H'.$ix, ($row->rekening) ? $row->rekening->bank->nama : '')
                ->setCellValue('I'.$ix, ($row->rekening) ? $row->rekening->no_rekening : '')
                ->setCellValue('J'.$ix, ($row->rekening) ? $row->rekening->cabang : '')
                ->setCellValue('K'.$ix, $_ptk->masaKerja($row->id))
            ;
        }

        $writer = new Xlsx($spreadsheet);

        $sekolah = Sekolah::find($id);

        $name = FCPATH.'uploads/SIMDIK_EXPORT_INSENTIF_'.strtoupper(uniqid()).'.xls';
        $writer->save($name);
        $this->load->helper('download');
        force_download($name, null);
    }

    public function apiInsentif() {
        $this->load->model('Ptk');
        $this->load->model('RiwayatKerjaPtk');
        $this->load->model('RiwayatPangkatPtk');

        $start = $this->input->get('start') ?? 0;
        $count = $this->input->get('count') ?? 10;

        $sekolah = $this->input->get('sekolah');
        $kecamatan = $this->input->get('kecamatan');
        $tingkat = $this->input->get('tingkat');

        $_ptk = new Ptk;

        $ptk = Ptk::take($count)
            ->where('status_pns', 0)
            ->when($sekolah['id'], function($q) use ($sekolah) {
                $q->where('id_sekolah', $sekolah['id']);
            })
            ->when($kecamatan['id'], function($q) use ($kecamatan) {
                $q->whereHas('unitKerja', function($qw) use ($kecamatan) {
                   $qw->where('id_kecamatan', $kecamatan['id']); 
                });
            })
            ->when($tingkat['id'], function($q) use ($tingkat) {
                $q->whereHas('unitKerja', function($qw) use ($tingkat) {
                    $qw->where('id_jenis_tingkat_sekolah', $tingkat['id']);
                });
            })
            ->whereHas('unitKerja', function($q) {
                $q->where(function($qw) {
                    $qw->whereIn('id_jenis_tingkat_sekolah', [2,3])->where('status', 1);
                })->orWhere(function($qw) {
                    $qw->whereNotIn('id_jenis_tingkat_sekolah', [2,3]);
                });
            });
            
        $total = $ptk->count();

        $parsed = [];
        foreach($ptk->skip($start)->get() as $key => $row) {
            $lahir_pns = substr($row->nip_baru, 0, 8);
            
            $tmt = ($row->status_pns != 1)
                ? RiwayatKerjaPtk::where('id_data_guru', $row->id)->orderBy('tmt', 'asc')->first()
                : RiwayatPangkatPtk::where('nip', $row->nip_baru)->orderBy('tmt', 'asc')->first();


            array_push($parsed, [
                'nuptk' => $row->nuptk,
                'nama' => $row->nama_lengkap,
                'jenisPegawai' => $row->jenisPegawai->nama,
                'unitKerja' => $row->unitKerja->nama,
                'usia' => ($row->profil)
                ? (($row->status_pns == 1 && strlen($row->nip_baru) > 8)
                    ? Carbon\Carbon::parse(substr($lahir_pns, 0, 4) . '-' . substr($lahir_pns, 4, 2) . '-' . substr($lahir_pns, 6, 2))->diff(Carbon\Carbon::now())->format('%y')
                    : Carbon\Carbon::parse($row->profil->tgl_lahir)->diff(Carbon\Carbon::now())->format('%y'))
                : (($row->status_pns == 1 && strlen($row->nip_baru)) ? Carbon\Carbon::parse(substr($lahir_pns, 0, 4) . '-' . substr($lahir_pns, 4, 2) . '-' . substr($lahir_pns, 6, 2))->diff(Carbon\Carbon::now())->format('%y') : '0'),
                'tmt' => $tmt ? Carbon\Carbon::parse($tmt->tmt)->format('d-m-Y') : '',
                'masaKerja' => $_ptk->masaKerja($row->id),
                'rekening' => ($row->rekening) ? [
                    'bank' => $row->rekening->bank->nama,
                    'noRekening' => $row->rekening->no_rekening,
                    'cabang' => $row->rekening->cabang
                ] : []
            ]);
        }

        $this->toJson(['data' => $parsed, 'total' => $total]);
    }

    public function apiPensiunStats() {
        $years = Carbon\Carbon::now()->format('Y');
        $this->load->model('Ptk');

        $ptk = [];
        $ptk[0] = Ptk::where('nip_baru', 'like', ($years-59).'%')->count();
        $ptk[1] = Ptk::where('nip_baru', 'like', ($years-58).'%')->count();
        $ptk[2] = Ptk::where('nip_baru', 'like', ($years-57).'%')->count();

        $this->toJson(['data' => $ptk]);
    }

    public function apiPensiun() {
        $start = $this->input->get('start') ?? 0;
        $count = $this->input->get('count') ?? 10;

        $sekolah    = $this->input->get('sekolah');
        $tingkat    = $this->input->get('tingkat');
        $kecamatan  = $this->input->get('kecamatan');

        $this->load->model('Sekolah');
        $this->load->model('Ptk');

        $sekolah = Sekolah::take($count)
                    ->when($sekolah['id'], function($q) use($sekolah) {
                        $q->where('id', $sekolah['id']);
                    })
                    ->when($tingkat['id'], function($q) use ($tingkat) {
                        $q->where('id_jenis_tingkat_sekolah', $tingkat['id']);
                    })
                    ->when($kecamatan['id'], function($q) use ($kecamatan) {
                        $q->where('id_kecamatan', $kecamatan['id']);
                    })
                    ;
        $total = $sekolah->count();

        $parsed = [];
        foreach($sekolah->skip($start)->get() as $key => $row) {
            $years = Carbon\Carbon::now()->format('Y');
            $ptk = [];
            $ptk[0] = Ptk::where('id_sekolah', $row->id)->where('nip_baru', 'like', ($years-59).'%');
            $ptk[1] = Ptk::where('id_sekolah', $row->id)->where('nip_baru', 'like', ($years-58).'%');
            $ptk[2] = Ptk::where('id_sekolah', $row->id)->where('nip_baru', 'like', ($years-57).'%');
            array_push($parsed, [
                'id' => $row->id,
                'npsn' => $row->npsn,
                'namaSekolah' => $row->nama,
                'tingkat' => $row->tingkatSekolah->nama,
                'kecamatan' => $row->kecamatan->nama,
                'pensiun' => [
                    [
                        'total' => $ptk[0]->count(),
                        'ptk' => $ptk[0]->get()
                    ],[
                        'total' => $ptk[1]->count(),
                        'ptk' => $ptk[1]->get()
                    ],[
                        'total' => $ptk[2]->count(),
                        'ptk' => $ptk[2]->get()
                    ],
                ]
            ]);
        }

        $this->toJson(['data' => $parsed, 'total' => $total]);
    }

    public function apiDataMasuk() {
        header('Content-Type: application/json');
        $this->load->model('Sekolah');
        
        $start = $this->input->get('start') ?? 0;
        $count = $this->input->get('count') ?? 10;

        $sk = new Sekolah();
        
        $sekolah = Sekolah::take($count)->when($tingkat = $this->input->get('tingkat'), function ($query) use ($tingkat) {
            if($tingkat['id'] > 0) $query->where('id_jenis_tingkat_sekolah', $tingkat['id']);
        })
        ->when($kecamatan = $this->input->get('kecamatan'), function ($query) use ($kecamatan) {
            if($kecamatan['id'] > 0) $query->where('id_kecamatan', $kecamatan['id']);
        })
        ->when($status = $this->input->get('status'), function($query) use ($status) {
            $query->where('status', $status['id']);
        });
        
        $total = $sekolah->count();
        
        $new_sekolah = [];
        
        $new_sekolah['data'] = [];
        
        foreach ($sekolah->skip($start)->get() as $key => $row) {
            $persen = $row->ptk->count() > 0 ? round($row->persentase->count() / $row->ptk->count() * 100, 2) : 0;
            array_push($new_sekolah['data'], [
                'id' => $row->id,
                'npsn' => $row->npsn,
                'namaSekolah' => $row->nama,
                'persentase' => $persen,
                'tingkatSekolah' => $row->tingkatSekolah->nama,
                'kecamatan' => $row->kecamatan->nama,
                'alamat' => $row->alamat,
                'idKecamatan' => $row->kecamatan->id,
                'idTingkatSekolah' => $row->tingkatSekolah->id,
                'status' => $row->status,
                'persentasePns' => $sk->persenPns($row->id)['persen'],
                'persentaseNonPns' => $sk->persenNonPns($row->id)['persen'],
                'totalPegawai' => $row->ptk->count(),
                'operator' => ($row->operator) ? $row->operator->nama_operator : ''
            ]);
        }
        
        $new_sekolah['total'] = $total;
        
        echo json_encode($new_sekolah);
    }

    public function apiGuru() {
        // header('Content-Type: application/json');
        $this->load->model('TugasMapel');
        $this->load->model('Semester');

        $semester = Semester::where('active', 1)->first();
        $sekolah = $this->input->get('sekolah');
        $status = $this->input->get('status');
        $tingkat = $this->input->get('tingkat');

        $instance = TugasMapel::
            selectRaw('id_tugas_mengajar, COUNT(*) as total')
            ->groupBy('id_tugas_mengajar')
            ->when($sekolah['id'], function($q) use ($sekolah) {
                $q->whereHas('ptk', function($qw) use($sekolah) {
                    $qw->where('id_sekolah', $sekolah['id']);
                });
            })
            ->when($tingkat['id'], function($q) use($tingkat) {
                $q->whereHas('ptk', function($qw) use ($tingkat) {
                    $qw->whereHas('unitKerja', function($qwe) use ($tingkat){
                        $qwe->where('id_jenis_tingkat_sekolah', $tingkat['id']);
                    });
                });
            })
            ->when($status['id'] && $status['id'] >= 0, function($q) use($status) {
                $q->whereHas('ptk', function($qw) use ($status) {
                    $qw->whereHas('unitKerja', function($qwe) use ($status){
                        $qwe->where('status', $status['id']);
                    });
                });
            })
            ->where('semester', $semester->id)
            ->get();

        $parsed = [];
        $total = 0;
        foreach($instance as $key => $row) {
            array_push($parsed, [
                'nama' => $row->tugasMengajar->nama,
                'jumlah' => $row->total
            ]);
            $total += $row->total;
        }
        echo json_encode(['data' => $parsed, 'total' => $total]);
    }

    public function toJson($output) {
        header('Content-Type: application/json');
        echo json_encode($output);
    }
}