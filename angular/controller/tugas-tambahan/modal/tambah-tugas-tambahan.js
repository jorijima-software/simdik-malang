angular.module('diknasmlg').controller('tambahTugasTambahan', function (
    $scope,
    $uibModalInstance,
    data,
    NgTableParams,
    $q,
    $TugasFactory,
    $uibModal,
    Notify,
    $TeacherFactory,
    $TimerFactory
) {

    var apiPath = localStorage.getItem('PATHLOC');

    $scope.timerEnd

    $scope.detail = data;
    $scope.teacherList = [];


    $scope.dismiss = function () {
        $uibModalInstance.dismiss();
    };

    $scope.hm = {
        id_jenis_jabatan: data.id
    };

    if ($scope.detail.id <= 1) {
        $TugasFactory.getHm(function (reply) {
            if (reply.nama_guru) {
                var skdate = reply.tanggal_sk.split('-');
                skdate = new Date(skdate[0], skdate[1] - 1, skdate[2]);
                $scope.hm = {
                    guru: {
                        id: reply.id_data_guru,
                        nama_lengkap: reply.nama_guru
                    },
                    no_sk: reply.no_sk,
                    tanggal_sk: skdate,
                    tahun_mulai: reply.tahun_mulai,
                    tahun_selesai: reply.tahun_selesai,
                    id_jenis_jabatan: data.id
                }
            }
        })
    }

    $scope.tgtbDetailTable = new NgTableParams({}, {
        getData: function (params) {
            var count = params.count();
            var start = count * (params.page() - 1);
            return $q(function (resolve, reject) {
                $TugasFactory.detail($scope.detail.id, start, count, function (reply) {
                    if (reply.error) return reject(reply.error);
                    params.total(reply.total);
                    return resolve(reply.data);
                })
            })
        }
    });

    $scope.onBtnEditClicked = function (row) {
        $uibModal.open({
            templateUrl: apiPath + 'teacher/modal_tgtb_edit',
            controller: 'editTugasTambahan',
            resolve: {
                data: function () {
                    return row;
                },
                edit: function () {
                    return true;
                },
                type: function () {
                    return null;
                }
            }
        }).result.then(function () {
            $scope.tgtbDetailTable.reload();
        }, function () {
            $scope.tgtbDetailTable.reload();
        })
    };

    $scope.onBtnInsertClicked = function () {
        $uibModal.open({
            templateUrl: apiPath + 'teacher/modal_tgtb_edit',
            controller: 'editTugasTambahan',
            resolve: {
                data: function () {
                    return {};
                },
                edit: function () {
                    return false;
                }, type: function () {
                    return data.id;
                }
            }
        }).result.then(function () {
            $scope.tgtbDetailTable.reload();
        }, function () {
            $scope.tgtbDetailTable.reload();
        })
    };

    $scope.onInputSearchChange = function (query) {
        $TeacherFactory.selectTeacher(query, function (reply) {
            if (reply.data) {
                $scope.teacherList = reply.data;
            } else {
                console.log(reply)
            }
        })
    };

    $scope.onBtnDeleteClicked = function (row) {
        Notify.confirm('Apakah anda yakin ingin menghapus <code>' + row.nama_guru + '</code>', function () {
            $TugasFactory.delete(row.id, function (reply) {
                if (reply.error) {
                    Notify.error('Kesalahan', reply.error);
                } else {
                    Notify.success('Sukses', 'Berhasil menghapus data');
                    $scope.tgtbDetailTable.reload();
                }
            })
        }, function () {
        });
    };

    $scope.onBtnSaveClicked = function () {
        $TugasFactory.insert($scope.hm, function (reply) {
            if (reply.error) {
                Notify.error('Kesalahan', reply.error);
            } else {
                Notify.success('Sukses', 'Berhasil menyimpan data');
                $uibModalInstance.close();
            }
        })
    };

    function init() {
        $TeacherFactory.selectTeacher('', function (reply) {
            if (reply.data) {
                $scope.teacherList = reply.data;
            } else {
                console.log(reply)
            }
        })


        $TimerFactory.diff().then(result => {
            $scope.timerEnd = (!result.admin && result.disabled)
        })
    }

    init();

});