// Created by Raga Subekti at 08/10/17
angular.module('diknasmlg').controller('tambahSekolah', function($scope, $uibModalInstance, $DistrictFactory, $LevelFactory, $SchoolFactory, Notify) {

    $scope.districtList = [];
    $scope.levelList = [];
    $scope.statusList = [{id: 0, nama: 'Swasta'}, {id: 1, nama: 'Negeri'}];

    $scope.new = {
        nisn: '',
        tingkat: {},
        alamat: '',
        status: {},
        kecamatan: {}
    };

    $scope.onBtnSaveClicked = function () {
        $SchoolFactory.insert($scope.new, function(reply) {
            if(reply.error) {
                Notify.error('Kesalahan', reply.error);
            } else {
                Notify.success('Sukses', 'Sukses menambahkan data sekolah');
                $uibModalInstance.close();
            }
        })
    };

    $scope.dismiss = function() {
        $uibModalInstance.dismiss();
    };

    function init() {
        $DistrictFactory.get(function(reply) {
            $scope.districtList = reply;
        });

        $LevelFactory.get(function(reply) {
            $scope.levelList = reply;
        })
    }

    init();
});