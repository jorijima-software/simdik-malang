<?php
/**
 * Created by PhpStorm.
 * User: ragasubekti
 * Date: 08/10/17
 * Time: 16.47
 */

class Religion extends CI_Controller {

    public function __construct()
    {
        parent::__construct();

        // if(!$this->session->userdata('is_logged')) {
        //     redirect('login');
        // }
    }

    public function show() {
        $this->load->model('Religion_model');
        header('Content-Type: application/json');
        echo json_encode($this->Religion_model->show());
    }

    public function statsbox() {
        $this->load->model('Religion_model');
        header('Content-Type: application/json');
        echo json_encode($this->Religion_model->statsbox());

    }

}