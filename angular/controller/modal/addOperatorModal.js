angular.module('diknasmlg').controller('addOperatorModal', function ($scope, $q, $uibModalInstance, Notify, $OperatorFactory, $SchoolFactory) {

    $scope.schoolList = [];
    $scope.select = {};
    $scope.select.selected = {};

    $scope.onSchoolSelectChange = function (query) {
        return $SchoolFactory.get(query, function (reply) {
            $scope.schoolList = reply;
        })
    };

    $scope.new = {
        'nama_operator': '',
        'no_hp': '',
        'no_wa': '',
        'asal_sekolah': {
            id: '',
            nama: ''
        }
    };

    $scope.onBtnSaveClicked = function () {
        $OperatorFactory.create($scope.new, function(reply) {
            console.log(reply);
            if(reply.error) {
                Notify.error('Kesalahan', reply.error);
            } else {
                Notify.success('Sukses', 'Sukses menambahkan data operator');
                $uibModalInstance.dismiss();
            }
        })
    };

    $scope.dismiss = function() {
        $uibModalInstance.dismiss();
    };
    function init() {
        $SchoolFactory.get('', function (reply) {
            $scope.schoolList = reply;
        });
    }

    init();

});