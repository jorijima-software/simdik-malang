// Created by Raga Subekti at 08/10/17
angular.module('diknasmlg').factory('$ReligionFactory', function ($http) {


    var apiPath = localStorage.getItem('PATHLOC');

    var factory = {};

    factory.show = function (callback) {
        $http.get(apiPath + 'api/religion').then(function (reply) {
            if (reply.status == 200) {
                return callback(reply.data);
            } else {
                console.log(reply)
            }
        }, function (err) {
            console.log(err)
        })
    };

    factory.statsbox = function (callback) {
        $http.get(apiPath + 'api/religion/statsbox').then(function (reply) {
            if (reply.status == 200) {
                return callback(reply.data);
            } else {
                console.log(reply)
            }
        }, function (err) {
            console.log(err)
        })
    };

    return factory;

});