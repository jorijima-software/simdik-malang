angular.module('diknasmlg').controller('editUserModal', function($scope, $SchoolFactory, $UserFactory, data, $uibModalInstance, Notify) {
  
    $scope.schoolList = [];
    
        $scope.dismiss = function () {
            $uibModalInstance.dismiss();
        };
    
        $scope.edit = {
            id: data.id,
            username: data.username,
            password: '',
            level: data.level,
            sekolah: {
                id: data.id_sekolah,
                nama: data.nama_sekolah,
                npsn: data.id_npsn
            }
        };
    
        $scope.passwordType = 'password';
    
        $scope.onPasswordType = function() {
            if($scope.passwordType == 'password') {
                $scope.passwordType = 'text';
            } else {
                $scope.passwordType = 'password';
            }
        };
    
        $scope.onInputSearchChange = function (query) {
            return $SchoolFactory.get(query, function (reply) {
                $scope.schoolList = reply;
            })
        };
    
        $scope.onBtnSaveClicked = function() {
            $UserFactory.update($scope.edit, function(reply) {
                if(reply.error) {
                    Notify.error('Kesalahan', reply.error);
                } else {
                    Notify.success('Sukses', 'Berhasil mengubah user');
                    $uibModalInstance.close();
                }
            })
        };
    
        function init() {
            $SchoolFactory.get('', function (reply) {
                $scope.schoolList = reply;
            });
        }
    
        init();
    
})