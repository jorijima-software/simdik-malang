<?php

class Guru_model extends CI_Model
{

    public function update($data)
    {
        $q = $this->db->get_where('tugas_guru', array('id' => $data->id));
        if ($q->num_rows() > 0) {

            $w = $this->db->get_where('data_guru', ['id' => $q->row()->id_data_guru])->row();
            $e = $this->db->get_where('data_guru', ['id' => $data->id_guru])->row();

            $update = array(
                'id_data_guru' => $data->id_guru
            );

            $this->db->where('id', $data->id);
            $this->db->update('tugas_guru', $update);

            $this->log('Update Guru ' . $w->nama_lengkap . '->' . $e->nama_lengkap, 2);

            return array('success' => true, $data);
        } else {
            return array('error' => 'Data tidak ditemukan');
        }
    }

    public function delete($id)
    {
        $q = $this->db->get_where('tugas_guru', array('id' => $id));
        if ($q->num_rows() > 0) {

            $w = $this->db->get_where('data_guru', ['id' => $q->row()->id_data_guru])->row();
            $this->db->delete('tugas_guru', ['id' => $id]);

            $this->log('Hapus Guru ' . $w->nama_lengkap, 0);

            return array('success' => true);
        } else {
            return array('error' => 'Data tidak ditemukan');
        }
    }

    public function stats()
    {
        if ($this->session->as > 0) {
            $fq = $this->db->get_where('user', array('id' => $this->session->uid))->row();
            $sq = $this->db->get_where('data_sekolah', array('npsn' => $fq->id_npsn))->row();
        }

        $tb_semester = $this->db->get_where('semester', array('active' => true))->row();
        $tb_tapel = $this->db->get_where('tapel', ['active' => true])->row();

        if ($this->session->as > 0) {
            $this->db->select('
            (SELECT id FROM semester WHERE active = 1) id_semester,
            (SELECT id FROM tapel WHERE active = 1) s_id_tapel,
            (SELECT COALESCE(SUM(jumlah_jam), 0) FROM data_tugas_mapel WHERE semester = id_semester AND id_tapel = s_id_tapel AND id_tugas_guru = data_guru.id) total_jam,
        ');

            $this->db->from('tugas_guru');
            $this->db->where('id_sekolah', $sq->id);
            $this->db->join('data_guru', 'id_data_guru = data_guru.id');

            $total = clone $this->db;
            $total = $total->count_all_results();
            $lebih = 0;
            $data = $this->db->get()->result();

            foreach ($data as $key => $row) {
                if($row->total_jam >= 24) {
                    $lebih++;
                }
            }

            return ['lebih' => intval($lebih), 'total' => $total, 'kurang' => ($total - $lebih)];
        } else {
            $q = 'SELECT 
            COUNT(*) lebih
          FROM (
            SELECT 
              SUM(jumlah_jam) jumlah_jam
            FROM data_tugas_mapel
            WHERE semester = ' . $tb_semester->id . ' AND id_tapel = ' . $tb_tapel->id . '
            GROUP BY id_tugas_guru
            HAVING jumlah_jam >= 24
          ) result';

            $lebih = $this->db->query($q)->row()->lebih;

            $total = $this->db->get('tugas_guru')->num_rows();

            return ['total' => $total, 'lebih' => intval($lebih), 'kurang' => ($total - $lebih)];
        }


    }

    public function show($start, $count, $filter)
    {

        $search = $filter['search'];

        if ($this->session->as > 0) {
            $fq = $this->db->get_where('user', array('id' => $this->session->uid))->row();
            $sq = $this->db->get_where('data_sekolah', array('npsn' => $fq->id_npsn))->row();
        }


        $tb_semester = $this->db->get_where('semester', array('active' => true));
        $tb_tapel = $this->db->get_where('tapel', ['active' => true]);

        $this->db->select('
            data_guru.*,
            tugas_guru.id id_tugas,
            (SELECT id FROM semester WHERE active = 1) id_semester,
            (SELECT id FROM tapel WHERE active = 1) s_id_tapel,
            (SELECT nama FROM jenis_pegawai a WHERE id_jenis_pegawai = a.id) jenis_pegawai,
            (SELECT COALESCE(SUM(jumlah_jam), 0) FROM data_tugas_mapel WHERE semester = id_semester AND id_tapel = s_id_tapel AND id_tugas_guru = data_guru.id) total_jam,
            (SELECT b.nama FROM data_tugas_mapel a, tugas_mengajar b WHERE a.id_tugas_mengajar=b.id AND semester = id_semester AND id_tapel = s_id_tapel AND id_tugas_guru = data_guru.id) mapel
        ');


        $this->db->from('tugas_guru');

        if (strlen($search) > 0) {
            $this->db->like('data_guru.nama_lengkap', urldecode($search));
        }

        if ($this->session->as > 0) $this->db->where('id_sekolah', $sq->id);

        $this->db->join('data_guru', 'id_data_guru = data_guru.id');
//        $this->db->join('jenis_pegawai', 'data_guru.id_jenis_pegawai = jenis_pegawai.id', 'left');

        $total = clone $this->db;

        $total = $total->count_all_results();

        $this->db->limit($count, $start);

        $data = $this->db->get()->result();


        return array('data' => $data, 'total' => $total, 'compiled' => null);
    }

    public function insert($data)
    {
        if (isset($data->id_guru)) {
            // return $id_guru;
            $id_guru = $data->id_guru;

            if ($this->db->get_where('tugas_guru', array('id_data_guru' => $id_guru))->num_rows() > 0) {
                return array('error' => 'Data duplikat');
            } else {
                $insert = array(
                    'id_data_guru' => $id_guru
                );

                $this->db->insert('tugas_guru', $insert);
                $this->log('Tambah Guru', 1);
                return array('success' => true);
            }
        } else {
            return ['error' => 'Mohon pilih dari daftar PTK!'];
        }
    }

    private function log($log, $type)
    {
        $this->db->insert('log_guru', [
            'log' => $log,
            'type' => $type,
            'id_user' => $this->session->uid
        ]);
    }

}