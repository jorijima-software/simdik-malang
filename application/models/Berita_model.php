<?php
/**
 * Created by PhpStorm.
 * User: ragasub
 * Date: 12/1/17
 * Time: 2:16 PM
 */

class Berita_model extends CI_Model
{

    public function get_target($query = null)
    {
        $query = urldecode($query);

        $all_target = [];

        array_push($all_target, [
            'id' => 0,
            'nama' => 'Semua',
            'id_target' => 0,
            'target' => 'Semua'
        ]);

        $this->db->from('jenis_tingkat_sekolah');
        $this->db->like('nama', $query);
        $tingkat = $this->db->get()->result();

        foreach ($tingkat as $key => $row) {
            array_push($all_target, [
                'id' => $row->id,
                'nama' => $row->nama,
                'id_target' => 1,
                'target' => 'Sekolah'
            ]);
        }

        $this->db->from('data_sekolah');
        $this->db->like('nama', $query);
        $this->db->limit('10');
        $sekolah = $this->db->get()->result();

        foreach ($sekolah as $key => $row) {
            array_push($all_target, [
                'id' => $row->id,
                'nama' => $row->nama,
                'id_target' => 2,
                'target' => 'Instansi'
            ]);
        }

        return $all_target;
    }

    public function tambah_berita($data)
    {
        $insert = [
            'judul' => $data->judul,
            'id_user' => $this->session->uid,
            'target' => $data->target->id_target,
            'specific_target' => ($data->target->id_target > 0) ? $data->target->id : null,
            'informasi' => $data->isi
        ];

        $this->db->insert('berita', $insert);

        $this->db->update('user', ['unread' => 1]);

        return ['success' => true];
    }

    public function show_berita($start = 0, $count = 10)
    {
        $this->db->where('id', $this->session->uid);
        $this->db->update('user', ['unread' => 0]);
        if ($this->session->as > 0) {
            // first get the school data
            $user = $this->db->get_where('user', ['id' => $this->session->uid])->row();
            $school = $this->db->get_where('data_sekolah', ['npsn' => $user->id_npsn])->row();

            $this->db->select('berita.id, created_at, updated_at, judul, username, nama');
            $this->db->from('berita');
            $this->db->join('user', 'id_user = user.id');
            $this->db->join('data_sekolah', 'user.id_npsn = data_sekolah.npsn', 'left');

            $this->db->where("
                `target` = '0' 
                OR (`target` = '1' AND `specific_target` = '{$school->id_jenis_tingkat_sekolah}')
                OR (`target` = '2' AND `specific_target` = '{$school->id}')
            ", NULL, FALSE);

            $total = clone $this->db;
            $this->db->limit($count, $start);
            $this->db->order_by('created_at', 'DESC');

            $data = $this->db->get()->result();
            return ['data' => $data, 'total' => $total->count_all_results()];
        } else {
            $this->db->select('berita.id, created_at, updated_at, judul, username, nama');
            $this->db->from('berita');
            $this->db->join('user', 'id_user = user.id');
            $this->db->join('data_sekolah', 'user.id_npsn = data_sekolah.npsn', 'left');
            $total = clone $this->db;
            $this->db->limit($count, $start);
            $this->db->order_by('created_at', 'DESC');

            $data = $this->db->get()->result();
            return ['data' => $data, 'total' => $total->count_all_results()];
        }
    }

    public function delete_berita($id)
    {
        if ($this->db->get_where('berita', ['id' => $id])->num_rows() > 0) {
            $this->db->where('id', $id);
            $this->db->delete('berita');

            return ['success' => true];
        } else {
            return ['error' => 'Data tidak ditemukan'];
        }
    }

    public function detail_berita($id)
    {
        $this->db->select('berita.*, created_at, updated_at, judul, username, nama, informasi');

        $this->db->from('berita');

        $this->db->join('user', 'id_user = user.id');
        $this->db->join('data_sekolah', 'user.id_npsn = data_sekolah.npsn', 'left');

        $this->db->where('berita.id', $id);

        $result = $this->db->get()->row();

        if ($result->target == 0) {
            $result->nama_target = 'Semua';
        } else if ($result->target == 1) {
            $result->nama_target = $this->db->get_where('jenis_tingkat_sekolah', ['id' => $result->specific_target])->row()->nama;
        } else if ($result->target == 2) {
            $result->nama_target = $this->db->get_where('data_sekolah', ['id' => $result->specific_target])->row()->nama;
        }

        return $result;
    }

    public function update_berita($data)
    {
        if ($this->db->get_where('berita', ['id' => $data->id])->num_rows() > 0) {
            $update = [
                'judul' => $data->judul,
                'id_user' => $this->session->uid,
                'target' => $data->target->id_target,
                'specific_target' => ($data->target->id_target > 0) ? $data->target->id : null,
                'informasi' => $data->isi
            ];

            $this->db->where('id', $data->id);
            $this->db->update('berita', $update);


            return ['success' => true];
        } else {
            return ['error' => 'Data tidak ditemukan'];
        }
    }

}