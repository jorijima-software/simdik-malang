<?php
/**
 * Created by PhpStorm.
 * User: ragasub
 * Date: 12/22/17
 * Time: 8:18 PM
 */

// THIS CLASS IS ROOT FOR THE SINGLE PAGE APPLICATION PLEASE DONT ADD NEW METHOD

class Boilerplate extends CI_Controller
{
    public function root()
    {
        $js = [
            'angular/factory/teacher.js',
            'angular/factory/employee.js',
            'angular/factory/school.js',
            'angular/factory/religion.js',
            'angular/factory/district.js',
            'angular/factory/education.js',
            'angular/factory/rank.js',
            'angular/factory/salary.js',
            'angular/factory/rombel.js',
            'angular/factory/ptk.js',
            'angular/factory/tugas.js',
            'angular/factory/berita.js',
            'angular/factory/operator.js',
            'angular/factory/guru.js',
            'angular/factory/level.js',
            'angular/factory/log.js',
            'angular/factory/Import.js',
            'angular/factory/riwayat.js',
            'angular/factory/timer.js',
            'angular/factory/Bank.js',
            'angular/factory/PtkRekening.js',
            'angular/factory/Statistik.js',

            'angular/controller/ptk/ptk-controller.js',
            'angular/controller/ptk/modal/detail-ptk.js',
            'angular/controller/ptk/modal/edit-ptk.js',
            'angular/controller/ptk/modal/tambah-ptk.js',
            'angular/controller/ptk/ptk-sekolah-controller.js',
            'angular/controller/ptk/modal/tambah-sekolah-ptk.js',
            'angular/controller/ptk/modal/edit-sekolah-ptk.js',
            'angular/controller/ptk/modal/aktivasi-sekolah-ptk.js',
            'angular/controller/ptk/ptk-pangkat-controller.js',
            'angular/controller/ptk/modal/edit-pangkat-ptk.js',
            'angular/controller/ptk/modal/tambah-pangkat-ptk.js',
            'angular/controller/ptk/ptk-gaji-controller.js',
            'angular/controller/ptk/modal/edit-gaji-ptk.js',
            'angular/controller/ptk/modal/tambah-gaji-ptk.js',

            'angular/controller/rombel/rombel-detail.js',

            'angular/controller/berita/berita-controller.js',
            'angular/controller/berita/buat-berita-controller.js',
            'angular/controller/berita/detail-berita-controller.js',
            'angular/controller/berita/edit-berita-controller.js',

            'angular/controller/operator/operator-controller.js',
            'angular/controller/operator/modal/tambah-operator.js',
            'angular/controller/operator/operator-activity.js',

            'angular/controller/guru/guru-controller.js',
            'angular/controller/guru/modal/tambah-guru.js',
            'angular/controller/guru/tugas-mengajar-controller.js',
            'angular/controller/guru/modal/tambah-tugas-mengajar.js',

            'angular/controller/sekolah/sekolah-controller.js',
            'angular/controller/sekolah/modal/tambah-sekolah.js',
            'angular/controller/sekolah/modal/detail-sekolah.js',
            'angular/controller/sekolah/modal/edit-sekolah.js',
            'angular/controller/sekolah/detail-sekolah-controller.js',

            'angular/controller/log/log-sms.js',
            'angular/controller/log/log-ptk.js',
            'angular/controller/log/log-rombel.js',
            'angular/controller/log/log-guru.js',
            'angular/controller/log/log-login.js',

            'angular/operator/controller/profile/profil-sekolah.js',
            'angular/operator/controller/profile/profil-operator.js',


            'angular/controller/tugas-tambahan/tugas-tambahan-controller.js',
            'angular/controller/tugas-tambahan/modal/tambah-tugas-tambahan.js',
            'angular/controller/tugas-tambahan/modal/edit-tugas-tambahan.js',

            'angular/controller/dashboard/dashboard-controller.js',
            'angular/controller/ptk/modal/sekolah-import.js',
            'angular/controller/ptk/verfikasi-import.js',
            'angular/controller/ptk/modal/import-detail.js',
            'angular/controller/ptk/modal/import-edit.js',

            'angular/controller/ptk/riwayat-kerja.js',
            'angular/controller/ptk/modal/ptk-delete.js',
            'angular/controller/riwayat/tambah-riwayat-kerja.js',
            'angular/controller/sekolah/sekolah.export.js',


        ];

        if ($this->session->as > 0) {
            $op_js = [
                'angular/controller/rombel/rombel-controller.js',
                'angular/controller/rombel/modal/tambah-rombel.js',
                'angular/controller/rombel/modal/edit-row-rombel.js'
            ];

            foreach ($op_js as $key => $row) {
                array_push($js, $row);
            }
        } else {
            $adm_js = [
                'angular/controller/rombel/rombel-admin.js',
                'angular/controller/setting/setting-timer.js',
                'angular/controller/setting/setting-bidang-studi.js',
                'angular/controller/setting/setting-jenis-pegawai.js',
                'angular/controller/setting/modal/timer/tambah-timer.js',
                'angular/controller/setting/modal/timer/update-timer.js',
                'angular/controller/sekolah/modal/persentase.js',

                'angular/controller/ptk/modal/rekening/detail.js',
                'angular/controller/ptk/modal/kerja/Edit.js',
                'angular/controller/statistik/Guru.js',
                'angular/controller/statistik/DataMasuk.js',
                'angular/controller/statistik/Pensiun.js',
                'angular/controller/statistik/Insentif.js'
            ];

            foreach ($adm_js as $key => $row) {
                array_push($js, $row);
            }
        }

        $this->load->view('layout', [
            'js' => $js
        ]);

    }
}