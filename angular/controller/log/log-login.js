/*
    Created by Raga Subekti
    at  12/12/17 18.40 by PhpStorm
*/
angular.module('diknasmlg').controller('logLogin', function ($scope, $q, NgTableParams, $LogFactory) {

    $scope.loginTable = new NgTableParams({}, {
        getData: function (params) {
            var count = params.count();
            var start = count * (params.page() - 1);
            var filter = {};
            return $q(function (resolve, reject) {
                $LogFactory.login(start, count, filter).then(function (r) {
                    if (!r.error) {
                        params.total(r.total);
                        return resolve(r.data);
                    } else {
                        return reject(r);
                    }
                })
            })
        }
    })

});