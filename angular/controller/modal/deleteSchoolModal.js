// Created by Raga Subekti at 08/10/17
angular.module('diknasmlg').controller('deleteSchoolModal', function(data, $uibModalInstance, $scope, $SchoolFactory, Notify) {
    $scope.detail = data;

    $scope.onBtnConfirmClicked = function() {
        $SchoolFactory.delete(data.id, function(reply) {
            if(reply.error) {
                Notify.error('Kesalahan', reply.error);
            } else {
                Notify.success('Sukses', 'Data sekolah berhasil dihapus');
                $uibModalInstance.close();
            }
        })
    };

    $scope.dismiss = function() {
        $uibModalInstance.dismiss();
    }
});