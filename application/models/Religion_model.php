<?php

/**
 * Created by PhpStorm.
 * User: ragasubekti
 * Date: 08/10/17
 * Time: 16.46
 */
class Religion_model extends CI_Model
{

    public function show()
    {
        return $this->db->get('jenis_agama')->result();
    }

    public function statsbox()
    {
        return $this->db->query("
            SELECT
            jenis_agama.nama AS agama,
            COUNT(*) as total
        FROM
            jenis_agama,
            profil_guru
        WHERE jenis_agama.id = profil_guru.id_jenis_agama
        GROUP BY agama
      ")->result();
    }

}