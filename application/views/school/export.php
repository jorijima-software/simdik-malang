<?php
/**
 * Created by PhpStorm.
 * User: ragasubekti
 * Date: 1/29/18
 * Time: 6:35 PM
 */
?>

<div class="modal-header">
    <div class="modal-title">
        Export Sekolah
    </div>
</div>
<div class="modal-body">
    <div class="form-group">
        <label>Kecamatan</label>
        <ui-select ng-model="export.kecamatan" theme="bootstrap" search-enabled="false" placeholder="Pilih Kecamatan">
            <ui-select-match>{{$select.selected.nama}}</ui-select-match>
            <ui-select-choices repeat="item in kecamatan track by $index">
                {{item.nama}}
            </ui-select-choices>
        </ui-select>
    </div>
    <div class="form-group">
        <label>Tingkat</label>
        <ui-select ng-model="export.tingkat" theme="bootstrap" search-enabled="false" placeholder="Pilih Tingkat Sekolah">
            <ui-select-match>{{$select.selected.nama}}</ui-select-match>
            <ui-select-choices repeat="item in tingkatSekolah track by $index">
                {{item.nama}}
            </ui-select-choices>
        </ui-select>
    </div>
    <div class="form-group">
        <label>Kelengkapan Data</label>
        <ui-select ng-model="export.kelengkapan" theme="bootstrap" search-enabled="false" placeholder="Pilih Kelengkapan Data">
            <ui-select-match>{{$select.selected.nama}}</ui-select-match>
            <ui-select-choices repeat="item in kelengkapan track by $index">
                {{item.nama}}
            </ui-select-choices>
        </ui-select>
    </div>
</div>
<div class="modal-footer">
    <button class="btn btn-primary btn-sm" ng-click="onBtnExport()">
        Export
    </button>
    <button class="btn btn-default btn-sm" ng-click="dismiss()">
        Batal
    </button>
</div>