<?php 

class DeletePtk extends \Illuminate\Database\Eloquent\Model {
    protected $table = 'delete_ptk';
    protected $guarded = [];
    public $timestamps = true;
}