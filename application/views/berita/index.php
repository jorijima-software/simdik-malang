<?php
/**
 * Created by PhpStorm.
 * User: ragasubekti
 * Date: 28/11/17
 * Time: 23.20
 */
?>

<section class="content-header">
    <h1>Berita</h1>
</section>

<section class="content">

    <?php if ($this->session->as == 0) { ?>
        <a class="btn btn-primary btn-sm" style="margin-bottom: 10px"
           href="<?= site_url('/') ?>#!/berita/baru">
            <i class="fa fa-plus"></i> <span>Buat Berita</span>
        </a>
    <?php } ?>

    <div class="box box-primary">

        <div class="box-body no-padding">

            <div class="table-responsive mailbox-messages">
                <table class="table table-hover table-striped" ng-table="beritaTable">
                    <tbody>
                    <tr ng-repeat="row in $data">
                        <td>
                            <span ng-if="row.nama">{{row.nama}}</span>
                            <span ng-if="!row.nama">{{row.username}}</span>
                        </td>
                        <td>
                            <a href="#!/berita/detail/{{row.id}}" style="font-weight: 800">{{row.judul}}</a>
                        </td>
                        <td>{{row.created_at | fromNow}}</td>
                        <td>

                            <?php if ($this->session->as == 0) { ?>
                                <div class="pull-right">
                                    <button class="btn btn-primary btn-xs" ng-click="onBtnEditClicked(row)">
                                        <i class="fa fa-pencil"></i>
                                    </button>
                                    <button class="btn btn-danger btn-xs" ng-click="onBtnDeleteClicked(row)">
                                        <i class="fa fa-trash"></i>
                                    </button>
                                </div>

                            <?php } ?>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>
            <!--            <div class="callout callout-default" ng-repeat="berita in beritaList">-->
            <!--                <div class="pull-right">-->
            <!--                    <div style="margin-bottom: 3px" class="float-right">-->
            <!--                        <button class="btn btn-primary" ng-click="onBtnDetailClicked(berita)">-->
            <!--                            <i class="fa fa-eye"></i> <span>Detail</span>-->
            <!--                        </button>-->
            <!--                    </div>-->
            <!---->
            <!--                    <div class="pull-right">-->
            <!--                        <button class="btn btn-primary btn-xs" ng-click="onBtnEditClicked(berita)">-->
            <!--                            <i class="fa fa-pencil"></i> <span>Edit</span>-->
            <!--                        </button>-->
            <!--                        <button class="btn btn-danger btn-xs" ng-click="onBtnDeleteClicked(berita)">-->
            <!--                            <i class="fa fa-times"></i> <span>Hapus</span>-->
            <!--                        </button>-->
            <!--                    </div>-->
            <!--                </div>-->
            <!--                <h4>{{berita.judul}}</h4>-->
            <!--                <small>Dibuat oleh <strong>-->
            <!--                        <span ng-if="berita.nama">{{berita.nama}}</span>-->
            <!--                        <span ng-if="!berita.nama">{{berita.username}}</span>-->
            <!--                    </strong> pada <strong>{{berita.created_at}}</strong>-->
            <!--                </small>-->
            <!--            </div>-->
        </div>
    </div>
</section>