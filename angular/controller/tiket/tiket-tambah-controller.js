/*
    Created by Raga Subekti
    at  15/12/17 10.00 by PhpStorm
*/
angular.module('diknasmlg').controller('tiketTambahController', function($scope, $q, $TiketFactory) {
    $scope.tiket = {};

    (function init() {
        $TiketFactory.getTicketId().then(function(r) {
            $scope.tiket.id = r.id;
        })
    })();
});