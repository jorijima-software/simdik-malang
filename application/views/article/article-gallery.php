<?php
/**
 * Created by PhpStorm.
 * User: arsyad09
 * Date: 01/12/17
 * Time: 19:21
 */

/*$this->load->model('Gallery_model');
$galleryinfo = $this->Gallery_model->show();*/

?>
<div class="container" style="margin-top: 75px" ng-controller="articleGalleryController">
    <div id="mySidenav" class="sidenav" style="margin-top: 200px">
        <a href="<?= site_url('Gallery') ?>">
            <i class="fa fa-home"></i>&nbsp
            <span>Beranda</span>
        </a>
        <a ng-click="onBtnUploadPhotoClicked()">
            <i class="fa fa-upload"></i>&nbsp
            <span>Upload</span>
        </a>
        <a href="<?= site_url('manage-gallery') ?>">
            <i class="fa fa-photo"></i>&nbsp
            <span>Manage</span>
        </a>
    </div>
    <div class="main" id="main">
        <div class="row">
            <div class="col-md-12" style="margin-bottom: 5px;text-align: center;margin-bottom: 20px;">
                <h2 class="header-on-page">GALERI SIMDIK</h2>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div id="gallery" style="display:none;">
                    <img alt="Image 1 Title" src="<?= base_url('assets/background.png') ?>"
                         data-image="<?= base_url('assets/background.png') ?>"
                         data-description="Image 1 Description">
                    <?php /*foreach ($galleryinfo){ */ ?><!--
                    <img alt="<? /*= $galleryinfo->tittle */ ?>" src="<? /*=base_url('uploads/galery/' .  $galleryinfo->files_name)*/ ?>"
                         data-image="<? /*=base_url('uploads/galery/' .  $galleryinfo->picture)*/ ?>"
                         data-description="<? /*= $galleryinfo->description */ ?> (Upload by:<? /*= $galleryinfo->uploader */ ?>)">
                --><?php /*} */ ?>
                </div>
            </div>
        </div>
    </div>
</div>
