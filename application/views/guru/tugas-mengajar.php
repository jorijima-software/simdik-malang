<?php
/**
 * Created by PhpStorm.
 * User: ragasubekti
 * Date: 29/11/17
 * Time: 00.43
 */

?>

<div ng-controller="tugasMengajarController">

    <section class="content-header">
        <h1>
            Detail Tugas Mengajar
        </h1>
    </section>

    <section class="content">
        <div class="box">
            <div class="box-header with-border">
                <div class="box-title">Informasi Guru</div>
            </div>
            <div class="box-body">
                <table class="table table-striped">
                    <tr>
                        <th width="200px">
                            NUPTK
                        </th>
                        <td>
                            {{ detail.nuptk }}
                        </td>
                    </tr>
                    <tr>
                        <th>Nama</th>
                        <td>{{ detail.nama_lengkap }}</td>
                    </tr>
                    <tr>
                        <th>Jenis Pegawai</th>
                        <td>{{ detail.jenis_pegawai }}</td>
                    </tr>
                    <tr>
                        <th>Sertifikasi</th>
                        <td>
                            
                        </td>
                    </tr>
                </table>
            </div>
        </div>

        <div class="box">
            <div class="box-header with-border">
                <div class="box-title">Data Tugas Mengajar</div>
            </div>
            <div class="box-body">
                <button class="btn btn-sm btn-primary" style="margin-bottom: 10px" ng-click="onBtnAddClicked()" ng-disabled="timerEnd">
                    <i class="fa fa-plus"></i> <span>Tambah</span>
                </button>

                <table class="table table-striped table-condensed" ng-table="tmengajarTable">
                    <thead>
                    <tr>
                        <th>No</th>
                        <th>Tahun Pelajaran</th>
                        <th>Jumlah Jam</th>
                        <th>Mata Pelajaran</th>
                        <th>Unit Kerja</th>
                        <th>Semester</th>
                        <th></th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr ng-repeat="row in $data">
                        <td>{{($index + 1) + (tmengajarTable._params.count * (tmengajarTable._params.page -1))}}</td>
                        <td>{{row.tapel}}</td>
                        <td>{{row.jumlah_jam}} Jam</td>
                        <td>{{row.mata_pelajaran}}</td>
                        <td>{{row.nama_sekolah}}</td>
                        <td>{{semester[row.semester]}}</td>
                        <td>
                            <div class="pull-right">
                                <button class="btn btn-primary btn-xs" ng-click="onBtnEditClicked(row)" ng-disabled="timerEnd">
                                    <i class="fa fa-pencil"></i> <span>Edit</span>
                                </button>
                                <button class="btn btn-danger btn-xs" ng-click="onBtnDeleteClicked(row)" ng-disabled="timerEnd">
                                    <i class="fa fa-times"></i> <span>Hapus</span>
                                </button>
                            </div>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </section>

</div>
