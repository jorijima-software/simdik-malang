<?php
/**
 * Created by PhpStorm.
 * User: ragasubekti
 * Date: 21/10/17
 * Time: 13.21
 */

?>

<div class="modal-header">
    <div class="modal-title">Ubah Pangkat Guru</div>
</div>
<div class="modal-body">
    <form action="">
        <div class="form-group">
            <label for="">No. SK</label>
            <input type="text" class="form-control" ng-model="edit.sk">
        </div>
        <div class="form-group">
            <label for="">Tanggal Memulai Tugas (TMT)</label>
            <p class="input-group">
                <input type="text" class="form-control" uib-datepicker-popup="dd-MM-yyyy" ng-model="edit.tmt"
                       datepicker-options="dateOptions"
                       close-text="Close" is-open="datepicker" placeholder="dd-MM-yyyy" readonly ng-click="datepicker = !datepicker"/>
                <span class="input-group-btn">
            <button type="button" class="btn btn-default" ng-click="datepicker = !datepicker"><i
                    class="glyphicon glyphicon-calendar"></i></button>
          </span>
            </p>
        </div>

        <div class="form-group">
            <label for="">Jenis</label>
            <select ng-model="edit.type" class="form-control">
                <option value="0">CPNS</option>
                <option value="1" selected>PNS</option>
            </select>
        </div>
        <div class="form-group">
            <label for="">Golongan</label>
            <ui-select ng-model="edit.golongan" theme="bootstrap">
                <ui-select-match placeholder="Pilih golongan"><span ng-if="$select.selected.gol">{{$select.selected.gol}}/{{$select.selected.ruang}}</span>
                </ui-select-match>
                <ui-select-choices repeat="item in rankList track by $index">
                    {{item.gol}}/{{item.ruang}}
                </ui-select-choices>
            </ui-select>
        </div>
        <div class="form-group">
            <label for="">Unit Kerja</label>
            <ui-select ng-model="edit.area" theme="bootstrap" style="margin-bottom: 10px">
                <ui-select-match placeholder="Pilih area kerja">{{$select.selected.nama}}
                </ui-select-match>
                <ui-select-choices repeat="item in areaList track by $index">
                    {{item.nama}}
                </ui-select-choices>
            </ui-select>
        </div>
        <div class="form-group" ng-if="edit.area.id == 1">
            <label for="">Kota/Kabupaten</label>
            <ui-select ng-model="edit.kota" theme="bootstrap" on-select="onRumahRegencySelect()" ng-if="edit.area.id == 1">
                <ui-select-match placeholder="Pilih kota/kabupaten">{{$select.selected.name}}
                </ui-select-match>
                <ui-select-choices repeat="item in regencyList track by $index"
                                   refresh="onRegencyChange($select.search)" refresh-delay="0">
                    {{item.name}}
                </ui-select-choices>
            </ui-select>
        </div>
        <div class="form-group" ng-if="edit.area && (edit.area.id != 1 || edit.area.id == 1)">
            <label for="">Nama Unit Kerja</label>
            <input type="text" class="form-control" ng-model="edit.unit_kerja" placeholder="Nama Unit Kerja" ng-if="edit.area.id == 1" ng-change="edit.unit_kerja = edit.unit_kerja.toUpperCase()">

            <ui-select ng-model="edit.sekolah" theme="bootstrap" ng-if="edit.area.id != 1">
                <ui-select-match placeholder="Pilih unit kerja">{{$select.selected.nama}}
                </ui-select-match>
                <ui-select-choices repeat="item in schoolList track by $index"
                                   refresh="onSchoolChange($select.search)" refresh-delay="0">
                    {{item.nama}}
                </ui-select-choices>
            </ui-select>
        </div>



    </form>
</div>
<div class="modal-footer">
    <button class="btn btn-primary btn-sm" ng-click="onBtnSaveClicked()" ng-disabled="isSubmit">
        <i class="fa fa-check" ng-if="!isSubmit"></i><i class="fa fa-circle-o-notch fa-spin" ng-if="isSubmit"></i> <span>Simpan</span>
    </button>


    <button class="btn btn-default btn-sm" ng-click="dismiss()">
        <i class="fa fa-times"></i> <span>Batal</span>
    </button>
</div>

