/**
 * Copyright 2018, Raga Subekti (ragasubekti@outlook.com)
 * All Code written below is not for reuse or copied
 * All rights go to Raga Subekti
 */

'use strict';
angular.module('diknasmlg').controller('StatistikGuru', function (
    $scope,
    $StatistikFactory,
    $SchoolFactory,
    NgTableParams,
    $q,
) {

    $scope.filter = {}

    $scope.data = []
    $scope.labels = []
    $scope.series = []

    $scope.educationList = []
    $scope.sekolahList = []

    $scope.totals = 0

    $scope.statusSekolah = [
        { id: -1, nama: 'Semua' },
        { id: 0, nama: 'Swasta' },
        { id: 1, nama: 'Negeri' },
    ]

    $scope.MapelTable = new NgTableParams({}, {
        getData: function (params) {
            return $q((resolve, reject) => {
                $StatistikFactory.guru($scope.filter).then(result => {
                    if (result && !result.error) {
                        $scope.totals = result.total
                        return resolve(result.data)
                    } else {
                        return reject(result)
                    }
                })
            })
        }
    })

    $scope.onInputSearchChange = (search) => {
        
        $SchoolFactory.mini(search).then(reply => {
            $scope.sekolahList = reply
        })
    }

    $scope.onFilter = () => {
        $scope.MapelTable.reload()
    }

    function init() {
        $StatistikFactory.guru($scope.filter).then(result => {
            if (result && !result.error) {
                result.forEach(function (item) {
                    $scope.data.push(item.jumlah)
                    $scope.labels.push(item.nama)
                })
            }
        })

        $SchoolFactory.type(reply => {
            $scope.educationList = reply
            $scope.educationList.unshift({ id: 0, nama: 'Semua' })
        })

        $SchoolFactory.mini('').then(reply => {
            $scope.sekolahList = reply
        })
    }

    init()
})