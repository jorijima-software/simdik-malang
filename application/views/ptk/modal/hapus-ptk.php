<?php
/**
 * Created by PhpStorm.
 * User: ragasubekti
 * Date: 1/18/18
 * Time: 8:12 AM
 */
?>

<div class="modal-header">
    <div class="modal-title">Konfirmasi Hapus</div>
</div>

<div class="modal-body">
    <div class="alert alert-danger">
        <strong>Peringatan!</strong>
        <p>Mohon pastikan data di bawah berikut benar! Aksi ini tidak bisa dikembalikan</p>
    </div>
    <table class="table table-striped table-hover">
        <tr>
            <th>NUPTK</th>
            <td>{{detail.nuptk}}</td>
        </tr>
        <tr>
            <th>Nama</th>
            <td>{{detail.nama}}</td>
        </tr>
        <tr>
            <th>Jenis Pegawai</th>
            <td>{{detail.jenisPegawai}}</td>
        </tr>
    </table>

    <div class="form-group">
        <label>Masukan alasan menghapus</label>
        <input type="text" class="form-control" placeholder="Alasan..." required ng-model="alasan">
    </div>
</div>

<div class="modal-footer">
    <button class="btn btn-danger btn-sm" ng-click="onBtnConfirmClicked()">
        <i class="fa fa-check"></i>&nbsp;Konfirmasi
    </button>
    <button class="btn btn-default btn-sm" ng-click="dismiss()">
        <i class="fa fa-times"></i>&nbsp;Batal
    </button>
</div>