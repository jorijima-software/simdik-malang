<?php
/**
 * Created by PhpStorm.
 * User: ragasubekti
 * Date: 06/10/17
 * Time: 15.44
 */

$userinfo = $this->User_model->userinfo();

$unread = $this->User_model->get_unread();

$admin = array(
    array('title' => 'Dashboard', 'path' => '/', 'icon' => 'fa-home', 'hash' => '#!/dashboard'),
    array('title' => 'Berita', 'path' => '/', 'icon' => 'fa-newspaper-o', 'hash' => '#!/berita', 'right_label' => ($unread > 0) ? 'BARU' : null),
//    array('title' => 'Go To Display', 'path' => 'display', 'icon' => 'fa-external-link'),
    array('title' => 'Data PTK', 'path' => '/', 'icon' => 'fa-user', 'hash' => '#!/ptk'),
    array('title' => 'Data Guru', 'path' => '/', 'icon' => 'fa-graduation-cap', 'hash' => '#!/guru'),
    array('title' => 'Data Sekolah', 'path' => '/', 'icon' => 'fa-university', 'hash' => '#!/sekolah'),
    array('title' => 'Data Operator', 'path' => '/', 'icon' => 'fa-list', 'hash' => '#!/operator'),
    array('title' => 'Data Rombel', 'path' => '/', 'icon' => 'fa-list-ul', 'hash' => '#!/rombel'),
//    array('title' => 'Backup Database', 'path' => '/', 'icon' => 'fa-hdd-o', 'hash' => '#!/backup'),
    //    array('title' => 'Tiket', 'path' => 'tiket', 'icon' => 'fa-paper-plane', 'hash' => '/#!/tiket')
);
$operator = array(
    array('title' => 'Berita', 'path' => '/', 'icon' => 'fa-newspaper-o', 'hash' => '#!/berita', 'right_label' => ($unread > 0) ? 'BARU' : null),
//    array('title' => 'Go To Display', 'path' => 'display', 'icon' => 'fa-external-link'),
    array('title' => 'Profil Sekolah', 'path' => '/', 'icon' => 'fa-university', 'hash' => '#!/sekolah/profil'),
    array('title' => 'Data PTK', 'path' => '/', 'icon' => 'fa-user', 'hash' => '#!/ptk'),
    array('title' => 'Data Guru', 'path' => '/', 'icon' => 'fa-graduation-cap', 'hash' => '#!/guru'),
    array('title' => 'Data Tugas Tambahan', 'path' => '/', 'icon' => 'fa-list', 'hash' => '#!/tugas-tambahan'),
    array('title' => 'Data Rombel', 'path' => '/', 'icon' => 'fa-th-list', 'hash' => '#!/rombel'),
//    array('title' => 'Tiket', 'path' => 'tiket', 'icon' => 'fa-paper-plane', 'hash' => '/#!/tiket')
);

?>
    <aside class="main-sidebar">

        <section class="sidebar">
            <ul class="sidebar-menu" data-widget="tree">
                <?php if ($_SESSION['as'] == 0) {?>
                <li class="header">MENU</li>
                <?php foreach ($admin as $key => $row) {
    $url = site_url($row['path']);
    if (isset($row['hash'])) {
        $url = $url . $row['hash'];
    }
    ?>
                <li>
                    <a href="<?php echo $url; ?>">
                        <i class="fa <?php echo $row['icon'] ?>"></i>
                        <span>
                            <?php echo $row['title'] ?>
                        </span>
                        <?php if (isset($row['right_label'])) {?>
                        <span class="pull-right-container">
                            <small class="label pull-right bg-red">
                                <?php echo $row['right_label'] ?>
                            </small>
                        </span>
                        <?php }?>
                    </a>
                </li>
                <?php }?>
                <li class="treeview">
                    <a href>
                        <i class="fa fa-bar-chart"></i>
                        <span>Statistik</span>
                        <span class="pull-right-container">
                            <i class="fa fa-angle-left pull-right"></i>
                        </span>
                    </a>
                    <ul class="treeview-menu">
                        <li>
                            <a href="#!/statistik/guru">Guru</a>
                            <a href="#!/statistik/data-masuk">Data Masuk</a>
                            <a href="#!/statistik/pensiun">Pensiun</a>
                            <a href="#!/statistik/insentif">Insentif</a>
                        </li>
                    </ul>
                </li>
                <li class="treeview">
                    <a href>
                        <i class="fa fa-history"></i>
                        <span>Log</span>
                        <span class="pull-right-container">
                            <i class="fa fa-angle-left pull-right"></i>
                        </span>
                    </a>
                    <ul class="treeview-menu">
                        <li>
                            <a href="<?php echo site_url('/') ?>#!/log/sms">SMS</a>
                        </li>
                        <li>
                            <a href="<?php echo site_url('/') ?>#!/log/login">Login</a>
                        </li>
                        <li>
                            <a href="<?php echo site_url('/') ?>#!/log/ptk">Data PTK</a>
                        </li>
                        <li>
                            <a href="<?php echo site_url('/') ?>#!/log/guru">Data Guru</a>
                        </li>
                        <li>
                            <a href="<?php echo site_url('/') ?>#!/log/rombel">Data Rombel</a>
                        </li>
                    </ul>
                </li>
                <li class="treeview">
                    <a href>
                        <i class="fa fa-cog"></i>
                        <span>Setting</span>
                        <span class="pull-right-container">
                            <i class="fa fa-angle-left pull-right"></i>
                        </span>
                    </a>
                    <ul class="treeview-menu">
                        <li>
                            <a href="<?php echo site_url('/') ?>#!/setting/timer">Timer Update</a>
                        </li>
                        <li>
                            <a href="<?php echo site_url('/') ?>#!/setting/jenis-pegawai">Jenis Pegawai</a>
                        </li>
                        <li>
                            <a href="<?php echo site_url('/') ?>#!/setting/bidang-studi">Bidang Studi</a>
                        </li>
                    </ul>
                </li>
                <?php } else if ($_SESSION['as'] == 1) {?>
                <li class="header">MENU</li>
                <?php foreach ($operator as $key => $row) {
    $url = site_url($row['path']);
    if (isset($row['hash'])) {
        $url = $url . $row['hash'];
    }
    ?>
                <li>
                    <a href="<?php echo $url ?>">
                        <i class="fa <?php echo $row['icon'] ?>"></i>
                        <span>
                            <?php echo $row['title'] ?>
                        </span>
                        <?php if (isset($row['right_label'])) {?>
                        <span class="pull-right-container">
                            <small class="label pull-right bg-red">
                                <?php echo $row['right_label'] ?>
                            </small>
                        </span>
                        <?php }?>
                    </a>
                </li>
                <?php }?>
                <li class="treeview">
                    <a href>
                        <i class="fa fa-cog"></i>
                        <span>Download</span>
                        <span class="pull-right-container">
                            <span class="label label-info">BARU</span>
                            <i class="fa fa-angle-left pull-right"></i>
                        </span>
                    </a>
                    <ul class="treeview-menu">
                        <li>
                            <a href="https://drive.google.com/open?id=1LiJh8Tn3FGO0IaeeDkfQZD2b-9KHkuzY" target="_blank">
                                <i class="fa fa-docs"></i>
                                <span>Pakta Integritas</span>
                            </a>
                        </li>
                    </ul>
                </li>
                <?php }?>
            </ul>
            <!-- /.sidebar-menu -->
        </section>
        <!-- /.sidebar -->
    </aside>