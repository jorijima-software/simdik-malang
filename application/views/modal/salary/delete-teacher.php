<?php
/**
 * Created by PhpStorm.
 * User: ragasubekti
 * Date: 23/10/17
 * Time: 18.46
 */
?>
<div class="modal-body">
    Apakah anda yakin ingin menghapus data gaji berikut?
    <table class="table table-striped table-condensed">
        <tr>
            <th>No. SK</th>
            <td>{{detail.no_sk}}</td>
        </tr>
        <tr>
            <th>Tanggal Memulai Tugas (TMT)</th>
            <td>{{detail.tmt | date:"dd MMMM yyyy"}}</td>
        </tr>
        <tr>
            <th>Jumlah Gaji</th>
            <td>{{detail.jumlah_gaji | currency: "Rp ":0}}</td>
        </tr>
    </table>
</div>
<div class="modal-footer">
    <button class="btn btn-danger btn-sm" ng-click="onBtnConfirmClicked()">
        <i class="fa fa-check"></i> <span>Konfirmasi</span>
    </button>
    <button class="btn btn-default btn-sm" ng-click="dismiss()">
        <i class="fa fa-times"></i> <span>Batal</span>
    </button>
</div>
