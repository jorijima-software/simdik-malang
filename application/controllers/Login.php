<?php
/**
 * Created by PhpStorm.
 * User: ragasubekti
 * Date: 06/10/17
 * Time: 13.01
 */
class Login extends CI_Controller {

    public function index() {
        if($this->input->method() == 'get') {
            $this->load->view('user/login');
        } else if($this->input->method() == 'post') {
            $this->load->model('User_model');

            $data = $this->User_model->login($this->input->post('username'), $this->input->post('password'), $this->input->post('login_as'));

            $this->load->view('user/login', $data);
        }
    }

    public function logout() {
        $this->session->sess_destroy();

        redirect('login');
    }

    public function api_auth() {
//        if($this->input->method() == 'post') {
//            $this->load->model('User_model');
//
//            echo json_encode($this->User_model->api_login_post());
//        } else {
//            echo json_encode(['error' => 'Invalid request']);
//        }
    }

}