Aplikasi SIMDIK Kota Malang
===
**Requirement**

* PHP 7 atau lebih

* Node.js

* Bower

* NPM

* Composer


**How to Install**

* Clone this repository

* Run `npm install`

* Run `bower install`

* Run `composer install`

* Copy `.env-example` to `.env` and change into your needs

* Run the server