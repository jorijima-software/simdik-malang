/**
 * Copyright 2018, Raga Subekti (ragasubekti@outlook.com)
 * All Code written below is not for reuse or copied
 * All rights go to Raga Subekti
 */

angular.module('diknasmlg').factory('$StatistikFactory', function($http, $q) {
    let factory = {}
    const PATH = localStorage.getItem('PATHLOC')

    factory.insentif = (filter) => {
        return $q((resolve, reject) => {
            $http.get(PATH + 'api/2/statistik/insentif?' + jQuery.param(filter)).then((response) => {
                if (response.status === 200) return resolve(response.data)
                return reject(response)
            })
        })
    }

    factory.guru = (filter) => {
        return $q((resolve, reject) => {
            $http.get(PATH + 'api/2/statistik/guru?' + jQuery.param(filter)).then((response) => {
                if (response.status === 200) return resolve(response.data)
                return reject(response)
            })
        })
    }

    factory.dataMasuk = (filter) => {
        return $q((resolve, reject) => {
            $http.get(PATH + 'api/2/statistik/data-masuk?' + jQuery.param(filter)).then((response) => {
                if (response.status === 200) return resolve(response.data)
                return reject(response)
            })
        })
    }

    factory.pensiun = (filter) => {
        return $q((resolve, reject) => {
            $http.get(PATH + 'api/2/statistik/pensiun?' + jQuery.param(filter)).then(response => {
                if (response.status === 200) return resolve(response.data)
                return reject(response)
            })
        })
    }

    factory.pensiunAll = function () {
        return $q((resolve, reject) => {
            $http.get(PATH + 'api/2/statistik/pensiun/all').then(response => {
                if (response.status === 200) return resolve(response.data)
                return reject(response)
            })
        })
    }

    return factory
})