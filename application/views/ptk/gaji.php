<?php
/**
 * Created by PhpStorm.
 * User: ragasubekti
 * Date: 16/10/17
 * Time: 16.33
 */
?>



<div ng-controller="ptkGajiController" ng-cloak>

    <section class="content-header">
        <h1>
            Riwayat Gaji PTK
        </h1>
    </section>

    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="box">
                    <div class="box-header with-border">
                        <div class="box-title">
                            Informasi Guru
                        </div>
                    </div>
                    <div class="box-body">
                        <table class="table table-consended table-striped">
                            <tr>
                                <th>Nama</th>
                                <td>{{detail.nama_lengkap}}</td>
                            </tr>
                            <tr>
                                <th>NUPTK</th>
                                <td>{{detail.nuptk}}</td>
                            </tr>
                            <tr>
                                <th>NIP</th>
                                <td>{{detail.nip_baru}}</td>
                            </tr>
                            <tr>
                                <th>Jenis Pegawai</th>
                                <td>{{detail.jenis_pegawai}}</td>
                            </tr>
                            <tr>
                                <th>Unit Kerja</th>
                                <td>{{detail.nama_sekolah}}</td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="box">
                    <div class="box-header with-border">
                        <div class="box-title">
                            Informasi Gaji
                        </div>
                    </div>
                    <div class="box-body">
                        <button class="btn btn-primary btn-sm" ng-click="onBtnAddClicked()" ng-disabled="timerEnd">
                            <i class="fa fa-plus"></i> Tambah
                        </button>
                        <table class="table table-striped" ng-table="salaryTable">
                            <thead>
                            <tr>
                                <th>No</th>
                                <th>No. SK</th>
                                <th>TMT Golongan</th>
                                <th>Gol/Ruang</th>
                                <th>Jumlah Gaji</th>
                                <th></th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr ng-repeat="row in $data">
                                <td>{{($index + 1) + (salaryTable._params.count * (salaryTable._params.page -1))}}</td>
                                <td>{{row.no_sk}}</td>
                                <td>{{row.tmt | date:"dd MMMM yyyy"}}</td>
                                <td>{{row.gol}}/{{row.ruang}}</td>
                                <td>{{row.jumlah_gaji | currency:"Rp ":0}}</td>
                                <td>
                                    <div class="pull-right">
                                        <button class="btn btn-primary btn-xs" ng-click="onBtnEditClicked(row)" ng-disabled="timerEnd">
                                            <i class="fa fa-pencil"></i> <span>Edit</span>
                                        </button>
                                        <button class="btn btn-danger btn-xs" ng-click="onBtnDeleteClicked(row)" ng-disabled="timerEnd">
                                            <i class="fa fa-trash"></i> <span>Hapus</span>
                                        </button>
                                    </div>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
