<?php
/**
 * Created by PhpStorm.
 * User: arsyad09
 * Date: 10/13/17
 * Time: 5:14 PM
 */
?>
<div class="container" style="margin-top: 75px;margin-bottom: 50px" ng-controller="articlePercentageController">
    <div class="row">
        <div class="col-md-8" style="margin-bottom: 5px">
            <h2 class="header-on-page">PROSENTASE PNS/CPNS</h2>
            <!--<button class="btn btn-export"></button>
            <button class="btn btn-cetak"></button>-->
        </div>
        <div class="col-md-4" style="margin-bottom: 5px">

        </div>
    </div>
    <div class="row">
        <highchart config="PegawaiNuptk"></highchart>
    </div>
    <div class="row">
        <highchart config="PegawaiDetail"></highchart>
    </div>
    <div class="row">
        <highchart config="PegawaiSertifikasi"></highchart>
    </div>
</div>
