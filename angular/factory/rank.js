// Created by Raga Subekti at 19/10/17
angular.module('diknasmlg').factory('$RankFactory', function($http) {

    var apiPath = localStorage.getItem('PATHLOC');

    var factory = {};

    factory.get = function(callback) {
        $http.get(apiPath + 'api/ranks').then(function (reply) {
            if (reply.status == 200) {
                return callback(reply.data);
            } else {
                console.log(reply)
            }
        }, function (err) {
            console.log(err)
        })
    };

    factory.add = function(data, callback) {

    };



    return factory;
});