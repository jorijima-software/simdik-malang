<?php

/**
 * Created by PhpStorm.
 * User: ragasubekti
 * Date: 07/10/17
 * Time: 14.01
 */
class School_model extends CI_Model
{

    public function get($query)
    {
        $this->db->select('*');
        $this->db->from('data_sekolah');

        $query = urldecode($query);

        if (strlen($query) > 0 && $query != 'null' && !is_null($query)) {
            $this->db->like('nama', $query);
        }

        $this->db->limit(10, 0);

        return $this->db->get()->result();
    }

    public function show($start, $count, $query, $filter)
    {
        $this->db->select('data_sekolah.*, kecamatan.nama as nama_kecamatan, jenis_tingkat_sekolah.nama as tingkat_sekolah');
        $this->db->from('data_sekolah');

        $query = urldecode($query);

        if ($query && $query != 'null' && $query !== null) {
            $this->db->where("(data_sekolah.nama LIKE '%{$query}%' OR data_sekolah.npsn LIKE '%{$query}%')");
        }

        if (isset($filter['kecamatan']['id']) && $filter['kecamatan']['id'] > 0) {
            $this->db->where('id_kecamatan', $filter['kecamatan']['id']);
        }

        if (isset($filter['tingkat']['id']) && $filter['tingkat']['id'] > 0) {
            $this->db->where('id_jenis_tingkat_sekolah', $filter['tingkat']['id']);
        }

        $this->db->join('kecamatan', 'kecamatan.id = data_sekolah.id_kecamatan');
        $this->db->join('jenis_tingkat_sekolah', 'jenis_tingkat_sekolah.id = data_sekolah.id_jenis_tingkat_sekolah');

        $total = clone $this->db;
        $this->db->limit($count, $start);
        $this->db->order_by('id', 'asc');

        $data = $this->db->get()->result();

        foreach ($data as $key => $row) {
            $this->db->select('COUNT(*) as total');
            $this->db->from('data_guru');
            $this->db->where('id_sekolah', $row->id);

            $data[$key]->total_pegawai = $this->db->get()->row()->total;
        }

        return array('data' => $data, 'total' => $total->count_all_results(), 'params' => $filter);
    }

    public function insert($nisn, $tingkat, $nama, $kecamatan, $alamat, $status)
    {

        if (strlen($nisn) <= 0) return ['error' => 'NPSN wajib diisi!'];

        $q = $this->db->get_where('data_sekolah', ['npsn' => $nisn]);

        if ($q->num_rows() > 0) {
            return ['error' => 'Sekolah dengan NPSN yang sama telah terdaftar!'];
        } else {
            $data = array(
                'npsn' => $nisn,
                'nama' => $nama,
                'status' => $status->id,
                'id_jenis_tingkat_sekolah' => $tingkat->id,
                'alamat' => $alamat,
                'id_kecamatan' => $kecamatan->id
            );

            $this->db->insert('data_sekolah', $data);

            $nu = array(
                'username' => $nisn,
                'password' => password_hash($nisn, PASSWORD_DEFAULT),
                'level' => 1,
                'id_npsn' => $nisn
            );

            $this->db->insert('user', $nu);

            if ($this->db->affected_rows() > 0) {
                return array('success' => true);
            } else {
                return array('error' => $this->db->_error_message());
            }
        }
    }

    public function update($id, $nisn, $tingkat, $nama, $kecamatan, $alamat, $status)
    {
        if(!strlen($nisn) > 0) return ['error' => 'NPSN wajib diis i!'];
        $data = array(
            'status' => $status->id,
            'id_jenis_tingkat_sekolah' => $tingkat->id,
            'alamat' => $alamat,
            'id_kecamatan' => $kecamatan->id
        );

        if ($this->session->as == 0) {
            $data['npsn'] = $nisn;
            $sekolah = $this->db->get_where('data_sekolah', ['id' => $id]);
            if ($sekolah->num_rows() > 0 && $this->db->get_where('user', ['username' => $sekolah->row()->npsn])->num_rows() > 0) {
                $this->db->where('username', $sekolah->row()->npsn);
                $this->db->update('user', [
                    'id_npsn' => $nisn,
                    'username' => $nisn,
                    'level' => 1
                ]);
            } else {
                $this->db->insert('user', [
                    'username' => $nisn,
                    'password' => password_hash($nisn, PASSWORD_DEFAULT),
                    'id_npsn' => $nisn,
                    'level' => 1
                ]);
            }
        }

        $this->db->where('id', $id);
        $this->db->update('data_sekolah', $data);

        if ($this->db->affected_rows() > 0) {
            return array('success' => true);
        } else {
            if ($this->db->error()['code'] > 0) {
                return array('error' => $this->db->_error_message());
            } else {
                return array('success' => true);
            }
        }
    }

    public function delete($id)
    {
        $q = $this->db->get_where('data_sekolah', ['id' => $id]);
        if ($q->num_rows() > 0) {
            $this->db->delete('data_sekolah', array('id' => $id));
            $this->db->delete('user', ['id_npsn' => $q->row()->npsn]);

            if ($this->db->affected_rows() > 0) {
                return array('success' => true);
            } else {
                if ($this->db->error()['code'] > 0) {
                    return array('error' => $this->db->_error_message());
                } else {
                    return array('success' => true);
                }
            }
        } else {
            return ['error' => 'Data tidak ditemukan'];
        }
    }

    public function statsbox()
    {
        return $this->db->query("
            SELECT
                COUNT(*) AS total,
                SUM(CASE WHEN status = 0 THEN 1 ELSE 0 END) AS swasta,
                SUM(CASE WHEN status = 1 THEN 1 ELSE 0 END) AS negri,
                jenis_tingkat_sekolah.nama AS tingkat
            FROM
                data_sekolah
                INNER JOIN jenis_tingkat_sekolah ON jenis_tingkat_sekolah.id = data_sekolah.id_jenis_tingkat_sekolah
            GROUP BY
                id_jenis_tingkat_sekolah
        ")->result();
    }

    public function graph()
    {
        return $q = $this->db->query("SELECT COUNT(*) AS total, kecamatan.nama FROM data_sekolah, kecamatan WHERE id_kecamatan = kecamatan.id GROUP BY kecamatan.nama")->result();
    }

    public function type()
    {
        return $this->db->get('jenis_tingkat_sekolah')->result();
    }

    public function showteacherschool($start, $count, $query = null, $filter = null)
    {
        $this->db->select('data_guru.*, jenis_pegawai.nama as jenis_pegawai, data_sekolah.nama as nama_sekolah');
        $this->db->from('data_guru');

        $query = urldecode($query);

        if ($query && $query != 'null' && $query !== null) {
            $this->db->or_like('data_guru.id_sekolah', $query);
        }

        $this->db->join('jenis_pegawai', 'jenis_pegawai.id = data_guru.id_jenis_pegawai');
        $this->db->join('data_sekolah', 'data_sekolah.id = data_guru.id_sekolah', 'left');

        $total = clone $this->db;

        $this->db->limit($count, $start);
        $this->db->order_by('id', 'asc');

        $data = $this->db->get()->result();
        foreach ($data as $key => $row) {

            $this->db->select('COUNT(*) as data_sekolah');
            $this->db->from('sekolah_guru');
            $this->db->where('id_data_guru', $row->id);

            $data_sekolah = $this->db->get()->row()->data_sekolah;
            $data[$key]->data_sekolah = $data_sekolah;

            $this->db->select('COUNT(*) as total_gaji');
            $this->db->from('gaji_berkala_guru');
            $this->db->where('nip', $row->nip_baru);

            $data[$key]->gaji_berkala = $this->db->get()->row()->total_gaji;


            $this->db->select('COUNT(*) as total_jg');
            $this->db->from('pangkat_guru');
            $this->db->where('nip', $row->nip_baru);

            $result = $this->db->get()->row();
            $data[$key]->pangkat_guru = $result->total_jg;

        }

        return array('data' => $data, 'total' => $total->count_all_results(), 'params' => $query);
    }

    public function userMigrate()
    {
        $fq = $this->db->get('data_sekolah')->result();

        foreach ($fq as $key => $row) {
            $sq = $this->db->get_where('user', array('username' => $row->npsn));
            if ($sq && $sq->num_rows() > 0) {

            } else {
                $insert = array(
                    'username' => $row->npsn,
                    'password' => password_hash($row->npsn, PASSWORD_DEFAULT),
                    'level' => 1,
                    'id_npsn' => $row->npsn
                );

                echo $row->npsn . ' | ' . $row->nama . '<br>';

                $this->db->insert('user', $insert);
            }
        }
    }

    public function getDetail($id)
    {
        $this->db->from('data_sekolah');
        $this->db->where('data_sekolah.id', $id);
        $this->db->join('jenis_tingkat_sekolah', 'id_jenis_tingkat_sekolah = jenis_tingkat_sekolah.id', 'left');
        $this->db->join('kecamatan', 'id_kecamatan = kecamatan.id', 'left');

        $this->db->select('data_sekolah.*, jenis_tingkat_sekolah.nama tingkat_sekolah, kecamatan.nama nama_kecamatan');

        $data = $this->db->get()->row();

        $this->db->select('data_guru.nama_lengkap as nama');
        $this->db->from('data_tugas_tambahan');
        $this->db->join('data_guru', 'id_data_guru = data_guru.id', 'left');
        $this->db->where('id_sekolah', $id);
        $this->db->where('id_jenis_jabatan', 1);

        $hm = $this->db->get();

        $data->headmaster = ($hm && $hm->row()) ? $hm->row()->nama : '';
        return $data;
    }

    public function profile()
    {
        $ud = $this->db->get_where('user', array('id' => $this->session->userdata('uid')))->row();

        if ($ud->id_npsn) {

            return $this->db->get_where('data_sekolah', array('npsn' => $ud->npsn));

        } else {
            return null;
        }
    }

    public function getEl()
    {
        return $this->db->get('jenis_tingkat_sekolah')->result();
    }

    public function getRl()
    {
        return $this->db->get('kecamatan')->result();
    }

    public function updateProfile($data)
    {

        $who = $this->db->get_where('user', array('id' => $this->session->uid))->row();
        $od = $this->db->get_where('data_sekolah', array('npsn' => $who->id_npsn))->row();

        $config['upload_path'] = FCPATH . '/uploads/';
        $config['allowed_types'] = 'gif|jpg|png';
        $config['encrypt_name'] = true;
        $config['width'] = '1280';

        $this->load->library('upload', $config);

        if (!empty($_FILES['gambar']['name'])) {
            if (!$this->upload->do_upload('gambar')) {
                return array('error' => $this->upload->display_errors());
            } else {
                $pic = array('upload_data' => $this->upload->data());
            }
        }


        $data = array(
            'status' => $data['status'],
            'alamat' => $data['alamat'],
            'id_kecamatan' => $data['kecamatan'],
            'desa' => $data['desa'],
            'notelp' => $data['notelp'],
            'last_updated' => date_format(date_create(), 'Y-m-d H:i:s'),
            'kode_pos' => $data['kodepos'],
            'fax' => $data['fax'],
            'email' => $data['email'],
            'website' => $data['website'],
            'geografis' => $data['geografis'],
            'picture' => !empty($pic['upload_data']['file_name']) ? $pic['upload_data']['file_name'] : (($od->picture) ? $od->picture : null)
        );

        $this->db->where('npsn', $who->id_npsn);
        $this->db->update('data_sekolah', $data);

        if ($this->db->affected_rows() > 0) {
            return array('success' => true);
        } else {
            if ($this->db->error()['code'] > 0) {
                return array('error' => $this->db->error()['message']);
            } else {
                return array('success' => true, 'anu' => $_FILES['gambar']);
            }
        }
    }

    public function rombel_statsbox()
    {
        $ud = $this->db->get_where('user', array('id' => $this->session->userdata('uid')))->row();
        $is = $this->db->get_where('data_sekolah', array('npsn' => $ud->id_npsn))->row()->id;
        $tapel = $this->db->get_where('tapel', ['active' => true])->row()->id;

        $this->db->select('SUM(laki) laki, SUM(perempuan) perempuan, SUM(laki+perempuan) total_siswa');
        $this->db->from('rombel_sekolah');
        $this->db->join('data_sekolah', 'id_sekolah = data_sekolah.id');
        $this->db->join('tapel', 'id_tapel = tapel.id');
        $this->db->where('id_sekolah', $is);
        $this->db->where('id_tapel', $tapel);

        $result = $this->db->get()->row();

        $result->laki = ($result->laki) ? intval($result->laki) : 0;
        $result->perempuan = ($result->perempuan) ? intval($result->perempuan) : 0;
        $result->total_siswa = ($result->total_siswa) ? intval($result->total_siswa) : 0;

        $this->db->select('id_rombel');
        $this->db->from('rombel_sekolah');
        $this->db->where('id_tapel', $tapel);
        $this->db->where('id_sekolah', $is);

        $result->rombel = $this->db->get()->num_rows();

        return $result;
    }


    public function tk_kelas()
    {
        $ud = $this->db->get_where('user', array('id' => $this->session->userdata('uid')))->row();
        $tk = $this->db->get_where('data_sekolah', array('npsn' => $ud->id_npsn))->row()->id_jenis_tingkat_sekolah;

        $q = $this->db->get_where('tingkat_kelas', ['id_tingkat_sekolah' => $tk]);

        return $q->result();
    }

    public function rombel_total($npsn)
    {
        $q = $this->db->get_where('data_sekolah', ['npsn' => $npsn]);
        if ($q->num_rows() > 0) {
            $si = $q->row();

            $this->db->select('SUM(laki) laki, SUM(perempuan) perempuan, SUM(laki + perempuan) total,tapel.tapel tahun_pelajaran');
            $this->db->from('rombel_sekolah');
            $this->db->join('tapel', 'id_tapel = tapel.id');
            $this->db->group_by('id_tapel');
            $this->db->where('id_sekolah', $si->id);

            $total = clone $this->db;

            return ['data' => $this->db->get()->result(), 'total' => $total->count_all_results()];
        } else {
            return ['error' => 'Data tidak ditemukan'];
        }
    }

    public function api_profile($npsn)
    {
        $q = $this->db->get_where('data_sekolah', ['npsn' => $npsn]);
        if ($q->num_rows() > 0) {
            $this->db->select('data_sekolah.*, kecamatan.nama nama_kecamatan, jenis_tingkat_sekolah.nama nama_tingkat');
            $this->db->from('data_sekolah');
            $this->db->join('kecamatan', 'id_kecamatan = kecamatan.id');
            $this->db->join('jenis_tingkat_sekolah', 'id_jenis_tingkat_sekolah = jenis_tingkat_sekolah.id');
            $this->db->where('data_sekolah.id', $q->row()->id);

            return $this->db->get()->row();
        } else {
            return ['error' => 'Data tidak ditemukan'];
        }
    }


    public function get_operator()
    {
        $q = $this->db->get_where('data_sekolah', ['npsn' => $this->session->npsn])->row();
        $e = $this->db->get_where('data_operator', ['asal_sekolah' => $q->id])->row();
        if ($e) {
            return $e;
        } else {
            return false;
        }
    }


    public function update_operator($data)
    {
        $s = $this->db->get_where('data_sekolah', ['npsn' => $this->session->npsn])->row();
        $q = $this->db->get_where('data_operator', ['asal_sekolah' => $s->id]);

//        return $q;

        if ($q->num_rows() > 0) {
            if($q->row()->nama_operator != $data->nama) {
                return ['error' => 'Nama Operator tidak bisa diubah'];
                die();
            }
            $this->db->where('asal_sekolah', $s->id);
            $this->db->update('data_operator',
                [
                    'nama_operator' => $data->nama,
                    'no_hp' => $data->no_hp,
                    'no_wa' => $data->no_wa
                ]
            );

            return ['success' => true];
        } else {
            $this->db->insert('data_operator', [
                'asal_sekolah' => $s->id,
                'nama_operator' => $data->nama,
                'no_hp' => $data->no_hp,
                'no_wa' => $data->no_wa
            ]);

            return ['success' => true];
        }
    }

    private function rombel_log($log)
    {
        $this->db->insert('log_rombel', [
            'log' => $log,
            'id_user' => $this->session->uid
        ]);
    }

    public function reset_password($npsn)
    {
        $q = $this->db->get_where('user', ['id_npsn' => $npsn]);
        if ($q->num_rows() > 0) {
            $this->db->where('id_npsn', $npsn);
            $this->db->update('user', [
                'password' => password_hash($npsn, PASSWORD_DEFAULT)
            ]);

            return ['success' => true];
        } else {
            return ['error' => 'Data tidak ditemukan'];
        }
    }

}