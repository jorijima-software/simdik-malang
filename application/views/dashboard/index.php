<?php
/**
 * Created by PhpStorm.
 * User: ragasubekti
 * Date: 06/10/17
 * Time: 15.43
 */
?>

<div id="wrapper" ng-controller="dashboardController">
    <section class="content">

        <div class="row">
            <div class="col-md-12">
                <div class="row hidden-xs hidden-sm">
                    <div class="col-lg-4 col-xs-6" ng-repeat="item in schoolStats">
                        <div class="small-box text-center" ng-random-class ng-classes="bgIndex">
                            <div class="inner">
                                <div class="row">
                                    <div class="col-xs-3" style="font-size: smaller">
                                        <h4 style="font-weight: 800">{{item.negri}}</h4>
                                        <p>Negeri</p>
                                    </div>
                                    <div class="col-xs-3">
                                        <h4 style="font-weight: 800">{{item.swasta}}</h4>
                                        <p>Swasta</p>
                                    </div>
                                    <div class="col-xs-6">
                                        <h3>{{item.total}}</h3>
                                        <p>Total {{item.tingkat}}</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="box box-info">
                <div class="box-footer">
                    <div class="row">
                        <div class="col-sm-2" ng-repeat="row in dataMasuk">
                            <div class="description-block border-right">
                                <span class="description-percentage" ng-class="{
                                    'text-green': row.percent >= 80,
                                    'text-yellow': row.percent >= 50 && row.percent < 80,
                                    'text-red': row.percent < 50
                                }">
                                    <i class="fa fa-caret-up" ng-if="row.percent >= 80"></i>
                                    <i class="fa fa-caret-left" ng-if="row.percent >= 50 && row.percent < 80"></i>
                                    <i class="fa fa-caret-down" ng-if="row.percent < 50"></i>
                                </span>
                                <h5 class="description-header">{{row.percent}}%</h5>
                                <span class="description-text">Data Masuk {{row.tingkat}}</span>
                            </div>
                        </div>
                    </div>
                </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="box">
                    <div class="box-header with-border">
                        <div class="box-title">Ketersebaran Sekolah</div>
                    </div>
                    <div class="box-body" >
                        <canvas id="bar"
                                class="chart chart-bar"
                                chart-data="data"
                                chart-labels="labels"
                                height="100"
                        >
                        </canvas>
                    </div>
                </div>
            </div>
        </div>

    </section>
</div>