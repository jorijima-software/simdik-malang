<?php
/**
 * Created by PhpStorm.
 * User: ragasubekti
 * Date: 07/10/17
 * Time: 09.52
 */
?>

<div class="modal-header">
    <div class="modal-title">
        <i class="fa fa-trash"></i> &nbsp; Hapus Guru
    </div>
</div>
<div class="modal-body">
    Hapus data guru berikut?
    <table class="table table-striped table-condensed" style="margin-top: 5px">
        <tr>
            <th>NUPTK</th>
            <td><code>{{detail.nuptk}}</code></td>
        </tr>
        <tr>
            <th>Nama Lengkap</th>
            <td>{{detail.nama_lengkap}}</td>
        </tr>
        <tr>
            <th>Jenis Pegawai</th>
            <td>{{detail.jenis_pegawai}}</td>
        </tr>
    </table>
</div>
<div class="modal-footer">
    <button class="btn btn-danger btn-sm" ng-click="onBtnConfirmClicked()">
        <i class="fa fa-check"></i> Konfirmasi
    </button>
    <button class="btn btn-default btn-sm" ng-click="dismiss()">
        <i class="fa fa-times"></i> Batal
    </button>
</div>
