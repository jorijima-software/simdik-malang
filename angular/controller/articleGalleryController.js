angular.module('diknasmlg').controller('articleGalleryController', function ($scope, NgTableParams, $q, $uibModal, $DistrictFactory) {

    var apiPath = localStorage.getItem('PATHLOC');

    $scope.onBtnUploadPhotoClicked = function() {
        $uibModal.open({
            templateUrl: apiPath + 'Gallery/upload',
            controller: 'uploadPhotoModal',
            size: 'md'
        })
    };

});