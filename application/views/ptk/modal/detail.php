<?php
/**
 * Created by PhpStorm.
 * User: ragasubekti
 * Date: 08/10/17
 * Time: 11.26
 */
?>
<div class="modal-header">
    <div class="modal-title">
        <i class="fa fa-eye"></i> Detail PTK
    </div>
</div>
<div class="modal-body">
    <uib-tabset active="active">
        <uib-tab heading="Data Kepegawaian">
            <table class="table table-condensed table-striped">
                <tr>
                    <th>NUPTK</th>
                    <td><code>{{detail.kepegawaian.nuptk}}</code></td>
                </tr>
                <tr>
                    <th>Nama Lengkap</th>
                    <td colspan="2">{{detail.kepegawaian.nama}}</td>
                </tr>
                <tr>
                    <th>Jenis Pegawai</th>
                    <td>{{detail.kepegawaian.jenisPegawai}}</td>
                </tr>
                <tr>
                    <th>Unit Kerja</th>
                    <td>{{detail.kepegawaian.unitKerja}}</td>
                </tr>
                <tr>
                    <th>Masa Kerja</th>
                    <td>
                        <a href="#!/ptk/riwayat/pangkat/{{detail.id}}" ng-show="detail.kepegawaian.pns == 1" ng-click="dismiss()">
                            {{detail.kepegawaian.masaKerja}}
                        </a>
                        <a href="#!/ptk/riwayat/kerja/{{detail.id}}" ng-hide="detail.kepegawaian.pns == 1" ng-click="dismiss()">
                            {{detail.kepegawaian.masaKerja}}
                        </a>
                    </td>
                </tr>
                <tr>
                    <th>NIP Lama</th>
                    <td>{{detail.kepegawaian.nip.lama}}</td>
                </tr>
                <tr>
                    <th>NIP Baru</th>
                    <td>{{detail.kepegawaian.nip.baru}}</td>
                </tr>
                <tr>
                    <th>Gelar Depan</th>
                    <td>{{detail.kepegawaian.gelar.depan}}</td>
                </tr>
                <tr>
                    <th>Gelar Belakang</th>
                    <td>{{detail.kepegawaian.gelar.belakang}}</td>
                </tr>
            </table>
        </uib-tab>
        <uib-tab heading="Profil">
            <table class="table table-striped">

                <tr>
                    <th>NIK</th>
                    <th>{{detail.profil.nik}}</th>
                </tr>

                <tr>
                    <th>Tempat Lahir</th>
                    <td>{{detail.profil.lahir.tempat}}</td>
                </tr>
                <tr>
                    <th>Tanggal Lahir</th>
                    <td>{{detail.profil.lahir.tanggal}}</td>
                </tr>
                <tr>
                    <th>Umur</th>
                    <td>{{detail.profil.lahir.umur}} Tahun</td>
                </tr>
                <tr>
                    <th>Agama</th>
                    <td>{{detail.profil.agama}}</td>
                </tr>
                <tr>
                    <th>Nama Ibu</th>
                    <td>{{detail.profil.namaIbu}}</td>
                </tr>
            </table>
        </uib-tab>
        <uib-tab heading="Rumah">
            <table class="table table-striped">
                <tr>
                    <th>Kota</th>
                    <td>{{detail.rumah.kota}}</td>
                </tr>
                <tr>
                    <th>Kecamatan</th>
                    <td>{{detail.rumah.kecamatan}}</td>
                </tr>
                <tr>
                    <th>No. Telp</th>
                    <td>{{detail.rumah.noTelp}}</td>
                </tr>
                <tr>
                    <th>Alamat</th>
                    <td>{{detail.rumah.alamat}}</td>
                </tr>
            </table>
        </uib-tab>
        <uib-tab heading="Sertifikasi">
            <table class="table table-striped">
                <tr>
                    <th>Pola</th>
                    <td>{{detail.sertifikasi.pola}}</td>
                </tr>
                <tr>
                    <th>No. Peserta</th>
                    <td>{{detail.sertifikasi.noPeserta}}</td>
                </tr>
                <tr>
                    <th>Tahun</th>
                    <td>{{detail.sertifikasi.tahun}}</td>
                </tr>
                <tr>
                    <th>Bidang Studi</th>
                    <td>{{detail.sertifikasi.bidangStudi}}</td>
                </tr>
            </table>
        </uib-tab>
    </uib-tabset>
</div>
<div class="modal-footer">
    <button class="btn btn-default btn-sm" ng-click="dismiss()"><i class="fa fa-times"></i> &nbsp; Tutup</button>
</div>