angular.module('diknasmlg').controller('PtkImportEdit', function(
    $scope, 
    $uibModalInstance, 
    Notify, 
    data,
    $ReligionFactory,
    $EmployeeFactory,
    $ImportFactory
) {

    $scope.edit = data;
    $scope.edit.tanggal_lahir = moment($scope.edit.tanggal_lahir)
    
    $scope.religionList = []
    $scope.jenisPegawaiList = []

    $scope.loading = false

    $scope.onBtnSaveClicked = function() {
        $scope.loading = true
        
        $ImportFactory.update($scope.edit).then(function(result) {
            $scope.loading = false
            
            if(result && !result.error) {
                Notify.success('Sukses', 'Berhasil mengubah data')
                $uibModalInstance.close()
            } else {
                Notify.error('Kesalahan', result.error)

            }
        })
    }

    $scope.dismiss = function() {
        $uibModalInstance.dismiss();
    }

    function init() {
        $ReligionFactory.show(function (reply) {
            $scope.religionList = reply
        });

        $EmployeeFactory.get(function (reply) {
            $scope.jenisPegawaiList = reply
        });
    }

    init()

})