<?php
/**
 * Created by PhpStorm.
 * User: ragasubekti
 * Date: 16/10/17
 * Time: 16.33
 */
?>


<div ng-controller="ptkSekolahController" ng-cloak>

    <section class="content-header">
        <h1>
            Riwayat Sekolah PTK
        </h1>
    </section>

    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="box">
                    <div class="box-header with-border">
                        <div class="box-title">
                            Informasi Guru
                        </div>
                    </div>
                    <div class="box-body">
                        <table class="table table-consended table-striped">
                            <tr>
                                <th>Nama</th>
                                <td>{{detail.nama_lengkap}}</td>
                            </tr>
                            <tr>
                                <th>NUPTK</th>
                                <td>{{detail.nuptk}}</td>
                            </tr>
                            <tr>
                                <th>NIP</th>
                                <td>{{detail.nip_baru}}</td>
                            </tr>
                            <tr>
                                <th>Jenis Pegawai</th>
                                <td>{{detail.jenis_pegawai}}</td>
                            </tr>
                            <tr>
                                <th>Unit Kerja</th>
                                <td>{{detail.nama_sekolah}}</td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="box">
                    <div class="box-header with-border">
                        <div class="box-title">
                            Riwayat Sekolah
                        </div>
                    </div>
                    <div class="box-body">
                        <button class="btn btn-primary btn-sm" ng-click="onBtnAddClicked()" ng-disabled="timerEnd">
                            <i class="fa fa-plus"></i> Tambah
                        </button>
<!--                        <button class="btn btn-success btn-sm" ng-click="onBtnImportClicked()">-->
<!--                            <i class="fa fa-import-cloud"></i> Import XLS-->
<!--                        </button>-->
<!--                        <a class="btn btn-warning btn-sm" href="--><?//=site_url()?><!--#!/ptk/riwayat/sekolah/{{router.id}}/import">-->
<!--                            <i class="fa fa-check-circle"></i> Verifikasi Import-->
<!--                        </a>-->
                        <table class="table table-striped" ng-table="schoolTable">
                            <thead>
                            <tr>
                                <th>No</th>
                                <th>Tk.</th>
                                <th>Nama Sekolah</th>
                                <th>Jurusan</th>
                                <th>Kota/Kecamatan</th>
                                <th>Th. Masuk</th>
                                <th>Th. Lulus</th>
                                <th>Status</th>
                                <th></th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr ng-repeat="row in $data">
                                <td>{{($index + 1) + (schoolTable._params.count * (schoolTable._params.page -1))}}</td>
                                <td>{{row.tingkat.nama}}</td>
                                <td>{{row.nama}}</td>
                                <td>{{row.jurusan}}</td>
                                <td>{{row.kota.nama}}/{{row.kecamatan.nama}}</td>
                                <td>{{row.tahun.masuk}}</td>
                                <td>{{row.tahun.lulus}}</td>
                                <td>
                                    <span ng-if="row.status > 0" class="label label-success">Aktif</span>
                                    <span ng-if="row.status <= 0" class="label label-default">Nonaktif</span>
                                </td>
                                <td>
                                    <div class="pull-right">
                                        <button class="btn btn-info btn-xs" ng-disabled="row.status > 0" ng-click="onBtnActivateClicked(row)" ng-disabled="timerEnd">
                                            <i class="fa fa-check"></i> <span>Aktifkan</span>
                                        </button>
                                        <button class="btn btn-primary btn-xs" ng-click="onBtnEditClicked(row)" ng-disabled="timerEnd">
                                            <i class="fa fa-pencil"></i> <span>Edit</span>
                                        </button>
                                        <button class="btn btn-danger btn-xs" ng-click="onBtnDeleteClicked(row)" ng-disabled="timerEnd">
                                            <i class="fa fa-trash"></i> <span>Delete</span>
                                        </button>
                                    </div>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
