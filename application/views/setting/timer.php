<?php
/**
 * Created by PhpStorm.
 * User: ragasubekti
 * Date: 2/2/18
 * Time: 1:58 AM
 */
?>


<div ng-cloak>
    <section class="content-header">
        <h1>
            Setting Timer
        </h1>
    </section>

    <section class="content">
        <div class="box box-primary">
            <div class="box-body">
                <div style="margin-bottom: 10px">
                    <button class="btn btn-primary btn-sm" ng-click="onBtnAddClicked()">
                        Tambah
                    </button>
                </div>
                <div>
                    <table class="table table-striped table-hover" ng-table="timerTable">
                        <thead>
                        <tr>
                            <th>No</th>
                            <th>Waktu Habis</th>
                            <th>Pesan</th>
                            <th>Status</th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr ng-repeat="row in $data">
                            <td>{{($index + 1) + (timerTable._params.count * (timerTable._params.page -1))}}</td>
                            <td>{{row.endtime}}</td>
                            <td>{{row.message | limitTo:20}}{{row.message.length>20 ? '...' : ''}}</td>
                            <td>
                                <span class="label label-primary" ng-show="row.active == 1">AKTIF</span>
                                <span class="label label-default" ng-hide="row.active == 1">NONAKTIF</span>
                            </td>
                            <td>
                                <div class="pull-right">
                                    <button class="btn btn-xs btn-info" ng-click="onBtnActivateClicked(row)" ng-disabled="row.active == 1">
                                        <i class="fa fa-check"></i> <span>Aktifkan</span>
                                    </button>
                                    <button class="btn btn-xs btn-primary" ng-click="onBtnEditClicked(row)">
                                        <i class="fa fa-pencil"></i> <span>Edit</span>
                                    </button>
                                    <button class="btn btn-xs btn-danger" ng-click="onBtnDeleteClicked(row)">
                                        <i class="fa fa-trash"></i> <span>Hapus</span>
                                    </button>
                                </div>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </section>
</div>