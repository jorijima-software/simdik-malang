// Created by Raga Subekti at 23/10/17
angular.module('diknasmlg').controller('ptkGajiController', function(
    $scope, 
    $uibModal, 
    $SalaryFactory, 
    NgTableParams, 
    $q, 
    $TeacherFactory, 
    Notify, 
    $routeParams, 
    $LogFactory,
    $TimerFactory
) {

    $scope.timerEnd
    $scope.router = $routeParams;
    $scope.detail  = {};

    var apiPath = localStorage.getItem('PATHLOC');

    $scope.onBtnAddClicked = function() {
        $uibModal.open({
            templateUrl: apiPath + 'teacher/addsalarymodal',
            controller: 'tambahGajiPtk',
            resolve:  {
                id: function () {
                    return $scope.router.id
                }
            }
        }).result.then(reloadTable, reloadTable)
    };

    $scope.salaryTable = new NgTableParams({}, {
        count: [],
        getData: function(params) {
            return $q(function(resolve, reject) {
                $SalaryFactory.get($scope.router.id, function(reply) {
                    if (reply.error) return reject(reply.error);
                    return resolve(reply);
                })
            })
        }
    });

    $scope.onBtnEditClicked = function(row) {
        $uibModal.open({
            templateUrl: apiPath + 'teacher/editsalarymodal',
            controller: 'editGajiPtk',
            resolve: {
                data: function() {
                    return row;
                }
            }
        }).result.then(reloadTable, reloadTable)
    };

    $scope.onBtnDeleteClicked = function(row) {
        Notify.confirm('Apakah anda yakin ingin menghpus data tersebut?', function() {
            $TeacherFactory.deleteSalary(row.id, function(reply) {
                if(reply.error) return Notify.error('Kesalahan', reply.error);
                Notify.success('Sukses', 'Berhasil menghapus data gaji');
                $scope.salaryTable.reload();
            })
        }, function() {})
    };

    function reloadTable() {
        $scope.salaryTable.reload();
    }


    function init() {
        $TeacherFactory.get($scope.router.id, function(r) {
            $scope.detail = r;
            console.log(r);
        })

        $TimerFactory.diff().then(result => {
            $scope.timerEnd = (!result.admin && result.disabled)
        })
    }
    init();

});