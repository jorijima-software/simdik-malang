<?php

class TimerModel extends \Illuminate\Database\Eloquent\Model {
    protected $table = 'timer';
    protected $guarded = [];
    public $timestamps = false;
}